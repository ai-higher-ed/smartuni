---
This file lists all the direct contributors to SmartUni.

The names are grouped by role in the project.

The ordering of the roles and of the names does not reflect a hierarchy of importance or contribution level. 

Broader scale roles are listed towards the top, with more specifics-level roles towards the bottom.

Coding and other work was spread evenly between the team members of each team, except in the case of the team 
leaders (who contributed more time due to their position) or where special alternative contribitutions are noted. 

---

# Supervisor
- Dr. Tobias Thelen

# Project Management
- Erik Nickel
- Piper Powell

# Team Leaders
- Niklas Dettmer (StudyBuddyMatch)
- Pia Ondreka (Planner)
- Lukas Schießer (Core)

# Core Team
- Thawab Alkhiami
- Carina Koenig
- Piper Powell
- Hind Shalfeh
- Lukas Schießer
- Jan-Luca Schroeder
- Viktoria Wiebe
- Frederik Wollatz
- Areej Yassin
- Qirui Zhu

# Planner Team
- Hanna Algedri
- Nele Daske
- Melissa Donati
- Anna Jansen
- Fabienne Kock
- Jueun Lee
- Johanna Linkemeyer
- Pia Ondreka 
- Muhammad Faraz Rajput
- Anita Wagner (team support, conceptualization, design input)

# StudyBuddyMatch Team
- Raia Abua Ahmad
- Yesid Cano Castro
- Charlotte Demandt
- Niklas Dettmer
- Laura Fortmann
- Tallulah Jansen
- Erik Nickel
- Hermann Singer
- Liling Wu
- Eunhye Yun

# Documentation Site Proofreading Team
- Piper Powell
- Nele Daske
- Anita Wagner
- Fabienne Kock
- Raia Abu Ahmad
- Viktoria Wiebe
- Tallulah Jansen

# Documentation Site Technical Team
- Erik Nickel
- Yesid Castro
- Hind Shalfeh
