

# SmartUni

> SmartUni is a website-based service for students that employs artificial intelligence to help in the management of time and tasks and in the location of suitable study partners. 


## Dev Setup
1. [Install Anaconda on your machine](https://docs.anaconda.com/anaconda/install/index.html)
2. `conda create -n smartuni` Create a new Anaconda environment.
3. `conda install django` Install django with Anaconda.
4. `conda install -c conda-forge django-debug-toolbar` Install django debug toolbar with Anaconda.
5. `pip install -r requirements.txt` Install the missing requirements with pip.
6. `python -m spacy download en_core_web_lg` Download spacy's large english language model for NLP analysis algorithms.

## Running the server
1. `python3 manage.py migrate` Run the latest migrations.
2. `python3 manage.py runserver` Run the server.
3. In another terminal, run `python3 manage.py qcluster` for scheduling tasks via Django Q (e.g. the training and fine-tuning of ML models).

## Contributors
You can find all contributors in the CONTRIBUTOR.md file.

## Contributing
This repository is in an archive state unless stated otherwise. You are invited to use the code within the license terms.
You may find issues in the issues section of this repository.
The team behind the project will not be active after the completion of the project and collaboration requests might not be answered.

# Copyright
SmartUni - An Assistant for Remote Studying

Copyright (C) 2021-2022  "Study project: AI Support for remote learning scenarios in higher education"

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

