import re
import string
import nltk
import spacy
import pandas as pd
from nltk.sentiment import SentimentIntensityAnalyzer
from HanTa import HanoverTagger as ht


class AISearch:
    """Takes a pandas DataFrame, a query, a language, specification for additional stopwords and whether
    the data to compare should be split up into multiple compare docs and calculates the similarities
    for the DataFrame documents

    DataFrame: requires a column 'documents' with the strings to be compared
    query: a string of words
    language: the common short string denominator for the desired language, currently supports 'de' and 'en'
    stopwords: currently only supports 'programs' as a value (or empty) in order to add more custom stopwords
               that fit the data better
               English stopwords are taken from nltk but without negations because those are needed to calculate
               sentiment, German stopwords are taken from https://www.ranks.nl/stopwords/german
    mult: bool - whether the documents contain comma-separated word strings that should be compared separately

    AISearch uses the HanoverTagger to deal with German words spacy has difficulty with
    """
    supported_languages = ['de', 'en']
    nlp = None
    tagger = ht.HanoverTagger('morphmodel_ger.pgz')
    cutoff = 0.4

    def __init__(self, compare_data: pd.DataFrame, query: str, language: str, stopwords: str, mult: bool):
        if language in self.supported_languages:
            self.language = language
        else:
            self.language = 'en'
        exclude_list = ['parser', 'textcat_multilabel', 'senter', 'sentencizer']
        if self.language == 'en':
            self.nlp = spacy.load("en_core_web_lg", exclude=exclude_list)
        if self.language == 'de':
            self.nlp = spacy.load("de_core_news_lg", exclude=exclude_list)
        self.stopwords = self.get_stopwords(self.language, stopwords)
        self.compare_data = compare_data
        self.query = query
        self.mult = mult
        query = self.normalize_punctuation(query)
        self.nlp_query = self.nlp(query)

    def build_documents(self):
        """Creates a new pandas column called 'filtered' that receives the documents to be processed without
           negative sentiment phrases
        """
        if self.mult:
            self.compare_data['filtered'] = self.compare_data.documents.apply(
                lambda phrase: self.split_mult_by_negation(phrase)
            )
        else:
            self.compare_data['filtered'] = self.compare_data.documents.apply(
                lambda phrase: self.split_by_negation(phrase)
            )

    def calc_similarity(self, documents, col_name):
        """Calculates the document's similarities to the query and creates a new DataFrame column with the given name
           to store them
           documents: pandas Series / column
           col_name: name string for the column
        """
        nlp = self.nlp
        sim_pipe = nlp.pipe(documents)
        sim_list = self.get_similarity_list(sim_pipe)
        self.compare_data[col_name] = pd.Series(sim_list).values

    def calc_mult_similarity(self, documents, col_name):
        """Calculates the document's similarities to the query if the documents should be split by comma and
           creates a pandas column by col_name that stores them.
           documents: pandas Series / column
           col_name: name string for the column
        """
        nlp = self.nlp
        sim_list = []
        mult_pipe_list = []
        for doc in documents.values:
            doc_list = doc.split(",") if doc else []
            # create nlp objects by pipe, which is more efficient
            mult_pipe_list.append(nlp.pipe(doc_list))
        for pipe in mult_pipe_list:
            sims = self.get_similarity_list(pipe)
            # calculate mean similarity over all separate document similarity values
            sum_sims = sum(sims) / len(sims) if len(sims) != 0 else 0.0
            sim_list.append(sum_sims)
        self.compare_data[col_name] = pd.Series(sim_list).values

    def calc_query_similarity(self):
        """AISearch's pipe. First, builds the necessary pandas Dataframe columns, then normalizes the documents
           according to custom stopwords and removes punctuation, calculates the similarity either for single
           documents or documents that are comma-separated lists and should be split and compared separately,
           and finally removes irrelevant entries by a cutoff and sorts the values by similarity
        """
        self.build_documents()
        self.compare_data['normalized'] = self.compare_data.filtered.apply(
            lambda phrase: self.normalize_punctuation(phrase)
        )
        if self.mult:
            self.calc_mult_similarity(self.compare_data['normalized'], 'similarity')
        else:
            self.calc_similarity(self.compare_data['normalized'], 'similarity')

        self.compare_data.drop(self.compare_data[self.compare_data.similarity < self.cutoff].iloc[0:].index,
                               axis=0, inplace=True)
        self.compare_data.sort_values(by=['similarity'], ascending=False, inplace=True)

    def get_similarity_list(self, sim_pipe):
        """Creates and returns a list of similarities for a given nlp pipe object"""
        sim_list = []
        nlp = self.nlp
        nlp_query = self.reduce_doc(self.nlp_query)
        try:
            for doc in sim_pipe:
                compare_doc = self.reduce_doc(doc)
                sim = nlp_query.similarity(compare_doc)
                if sim == 0.0 and self.language == 'de' and compare_doc.text != '':
                    sim_list.append(nlp_query.similarity(nlp(self.get_lemmatized_phrase(doc.text))))
                else:
                    sim_list.append(sim)
        except ValueError:
            sim_list = [0.0 for i in range(0, len(self.compare_data.values))]
        return sim_list

    def get_sentiment(self, document):
        """Calculates the sentiment of the given document and returns 1 for positive or -1 for negative sentiment"""
        sia = SentimentIntensityAnalyzer()
        return sia.polarity_scores(document)['compound']

    def split_by_negation(self, phrase):
        """Splits the given document or phrase by negations if there are any and returns the positive sentiment
           phrase, or the original phrase if the document wasn't split
        """
        negations = self.get_negations_by_language(self.language)
        doc_list = re.split(negations, phrase, 1)
        if len(doc_list) > 1:
            return self.get_subphrase_from_polarity(doc_list)
        return phrase

    def split_mult_by_negation(self, phrase_series):
        """Splits the given document or phrase series by negations if there are any when mult is true, and returns
           the positive sentiment phrase, or the original phrase if the document wasn't split
        """
        mult = phrase_series.split(",")
        doc_list = []
        for phrase in mult:
            split_phrase = self.split_by_negation(phrase)
            if split_phrase:
                doc_list.append(split_phrase)
        if len(doc_list) > 0:
            return ",".join(phrase for phrase in doc_list)
        return phrase_series

    def get_subphrase_from_polarity(self, phrase_list):
        """Calculates the polarity of a document already split by negation into a list of two values.
           The calculation assumes that if the first half polarity is positive, the negation means the second
           half polarity will be negative and vice versa.
        """
        first_phrase_polarity = self.get_sentiment(phrase_list[0])
        # if the first phrase has negative polarity, return the second phrase
        # otherwise return the first phrase to get the most relevant results
        if first_phrase_polarity < 0:
            return phrase_list[1]
        return phrase_list[0]

    def get_lemmatized_phrase(self, phrase):
        """Lemmatizes the given phrase and returns only the nouns. This was implemented because spacy's model
           sometimes has difficulties with German words and returns a similarity of 0."""
        lemmata = []
        for lemma in self.tagger.analyze(phrase, taglevel=3)[1]:
            if lemma[1] == 'NN':
                lemmata.append(lemma[0])
        lemmatized = " ".join(lemma for lemma in lemmata)
        return lemmatized

    def reduce_doc(self, doc):
        """If a document has more than 3 words, it returns the document only consisting of nouns to shorten it
           because spacy is inaccurate on long sentences with the given academic dataset."""
        if len(doc.text.split()) > 3:
            nouns = self.get_nouns(doc)
            return self.nlp(nouns)
        return doc

    def get_nouns(self, doc):
        """Returns the nouns of a document and removes duplicates."""
        nouns = [word.text for word in doc if word.pos_ == 'NOUN']
        nouns = list(set(nouns))
        return " ".join(word for word in nouns)

    def get_negations_by_language(self, language):
        """Returns the correct negations for the given language that a document should be split by."""
        language = language if language in self.supported_languages else 'en'
        if language == 'en':
            return "but|not"
        if language == 'de':
            return "aber|nicht|kein|keine|außer"

    def normalize_punctuation(self, document):
        """Returns a document cleaned of punctuation and removes custom stopwords"""
        if document:
            cleaned_document = " ".join(word.lower() for word in document.split() if word.lower() not in self.stopwords)
            cleaned_document = cleaned_document.translate(str.maketrans(string.punctuation,
                                                                        ' ' * len(string.punctuation)))
            return cleaned_document
        return document

    def get_stopwords(self, language: str, stopwords: str):
        """Returns a list of stopwords depending on language. Currently supported: en, de
           English stopwords are taken from nltk and modified to exclude negations because negations are needed for
           the sentiment analysis.
           German stopwprds are taken from https://www.ranks.nl/stopwords/german and modified to exclude negations
           as well.
        """
        result = []
        if language == 'en':
            result = ['ourselves', 'hers', 'between', 'yourself', 'again', 'there', 'about', 'once', 'during', 'out',
                      'very', 'having', 'with', 'they', 'own', 'an', 'be', 'some', 'for', 'do', 'its', 'yours', 'such',
                      'into', 'of', 'most', 'itself', 'other', 'off', 'is', 's', 'am', 'or', 'who', 'as', 'from', 'him',
                      'each', 'the', 'themselves', 'until', 'below', 'are', 'we', 'these', 'your', 'his', 'through',
                      'me', 'were', 'her', 'more', 'himself', 'this', 'down', 'should', 'our', 'their', 'while',
                      'above', 'both', 'up', 'to', 'ours', 'had', 'she', 'all', 'when', 'at', 'any', 'before', 'them',
                      'same', 'and', 'been', 'have', 'in', 'will', 'on', 'does', 'yourselves', 'then', 'that',
                      'because',
                      'what', 'over', 'why', 'so', 'can', 'did', 'now', 'under', 'he', 'you', 'herself', 'has', 'just',
                      'where', 'too', 'only', 'myself', 'which', 'those', 'i', 'after', 'few', 'whom', 't', 'being',
                      'if',
                      'theirs', 'my', 'against', 'a', 'by', 'doing', 'it', 'how', 'further', 'was', 'here', 'than']
        if language == 'de':
            result = ['aber', 'als', 'am', 'an', 'auch', 'auf', 'aus', 'bei', 'bin', 'bis', 'bist', 'da', 'dadurch',
                      'daher', 'darum', 'das', 'daß', 'dass', 'dein', 'deine', 'dem', 'den', 'der', 'des', 'dessen',
                      'deshalb', 'die', 'dies', 'dieser', 'dieses', 'doch', 'dort', 'du', 'durch', 'ein', 'eine',
                      'einem',
                      'einen', 'einer', 'eines', 'er', 'es', 'euer', 'eure', 'für', 'hatte', 'hatten', 'hattest',
                      'hattet', 'hätten', 'hättest', 'hättet', 'hier', 'hinter', 'ich', 'ihr', 'ihre', 'ihm', 'im',
                      'in',
                      'ist', 'ja', 'jede', 'jedem', 'jeden', 'jeder', 'jedes', 'jener', 'jenes', 'jetzt', 'kann',
                      'kannst', 'können', 'könnt', 'machen', 'mein', 'meine', 'mit', 'muß', 'mußt', 'musst', 'müssen',
                      'müßt', 'müsst', 'nach', 'nachdem', 'nein', 'nun', 'oder', 'seid', 'sein', 'seine', 'sich', 'sie',
                      'sind', 'soll', 'sollen', 'sollst', 'sollt', 'sonst', 'soweit', 'sowie', 'und', 'unser', 'unsere',
                      'unter', 'vom', 'von', 'vor', 'wann', 'warum', 'was', 'weiter', 'weitere', 'wenn', 'wer', 'werde',
                      'werden', 'werdet', 'weshalb', 'wie', 'wieder', 'wieso', 'wir', 'wird', 'wirst', 'wo', 'woher',
                      'wohin', 'zu', 'zum', 'zur', 'über']
        if stopwords == 'programs':
            result += [
                'bachelor', 'master', 'arts', 'science', 'fachbachelor', '2-fächer-bachelor', 'm.sc.', 'b.sc', 'm.a',
                'b.a.', 'm.ed.', '2-fach-b.a.', '2-fach-m.a.', 'kooperationsfach', 'staatsexamen',
                'promotionsstudiengang', 'studienort', 'schwerpunkt', 'schwerpunkte', 'studienschwerpunkt', 'ise',
                'berufsbegleitend', 'universitäts', 'of', 'fach', 'fächer', 'wissenschaft', 'wissenschaften',
                'studiengang', 'dualer'
            ]
        return result
