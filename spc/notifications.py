from django.contrib.auth.models import Group
from notifications.signals import notify
from spc.models import Settings, UserSettings, Module
from django.core.mail import send_mail
from render_block import render_block_to_string
from django.conf import settings

class CoreNotificationHelper:

    @staticmethod
    def send_notifications(recipient, message, setting_name, url="/", verb="Message", data={}, sender=None):
        """
        sender : sender of the notification
        recipient : receiver of the notification (single user)
        message : notification message
        setting_name : notification settings name
        verb : activity, defaults to "Message"
        data: additional data that can be used e.g. for email notification
        url: url that user gets redirected to after clicking on notification
        """
        recipient_id = recipient.id
        settings_id = Settings.objects.get(name=setting_name).id
        notif_settings_value = int(UserSettings.objects.get(setting_id=settings_id, user_id=recipient_id).value)
        email_notif = False
        in_app_notif = False
        if sender is None:
            sender = setting_name.split("_")[0]
        sender = Module.objects.get(abbreviation=sender)

        if notif_settings_value == 0:
            pass
        elif notif_settings_value == 1:
            in_app_notif = True
        elif notif_settings_value == 2:
            email_notif = True
        else:
            email_notif = True
            in_app_notif = True

        if in_app_notif:
            notify.send(sender=sender,
                        recipient=recipient,
                        verb=verb,
                        description=message,
                        url=url)
                        #module=sender)

        if email_notif:
            data['link'] = url
            data['domain'] = settings.SITE
            template = Settings.objects.get(name=setting_name).template
            ctx = {
                "user": recipient,
                "data": data
            }
            subject = render_block_to_string(template, 'subject', ctx)
            plain = render_block_to_string(template, 'plain', ctx)
            html = render_block_to_string(template, 'html', ctx)
            send_mail(subject,
                      plain,
                      recipient_list=[recipient.email],
                      from_email=settings.EMAIL_HOST_USER,
                      html_message=html)

    @staticmethod
    def is_in_group(user, group_name):
        return user.groups.filter(name=group_name).exists()

    @staticmethod
    def add_user_to_group(user, group_name):
        """
        adding a user to a django group and creates the group if it is not existing

        returns user group optionally
        """
        user_group, created = Group.objects.get_or_create(name=group_name)
        print(str.format("User Group {0}, New Group Status: {1}", group_name, created))
        user_group.user_set.add(user)
        return user_group

    @staticmethod
    def remove_user_from_group(user, group_name):
        if CoreNotificationHelper.is_in_group(user, group_name):
            user_group, _ = Group.objects.get_or_create(name=group_name)
            user_group.user_set.remove(user)

