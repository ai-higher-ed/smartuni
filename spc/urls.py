from django.urls import include, path, re_path
from django.views.generic import TemplateView

from . import views
import notifications.urls

urlpatterns = [
    path('', views.landing, name='landing'),
    path('about/', views.about, name='about'),
    path('data-privacy/', views.data_privacy, name="data-privacy"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('register/', views.register, name='register'),
    path('user/', views.jumpstart, name='jumpstart'),
    path('settings/', views.settings, name='settings'),
    path('user/save_notif_settings', views.save_notif_settings, name='save-notif-settings'),
    path('bugs/report', views.reportBugs, name='report'),
    path('bugs/', views.BugListView.as_view(), name="bugs"),
    path('bugs/<int:pk>', views.BugDetailView.as_view(), name="bug"),
    path('bugs/<int:pk>/change-state', views.change_bug_state, name="change-bug-state"),
    path('buddy/<int:pk>', views.UserDetailView.as_view(), name="user-detail"),
    path('spm/', include('spm.urls')),
    path('sbm/', include('sbm.urls')),
    path('feedback/', views.feedback, name='feedback'),
    path('feedback/list/', views.feedback_list, name='feedback_list'),
    path(r'feedback/detailed/?<int:feedback_id>/', views.feedback_detailed, name='feedback_detailed'),
    path('ajax/load-programs/', views.load_programs, name="load-programs"),
    re_path(r'^download/log/(?P<logfile>(info|debug)\.log(\.[0-9]{4}-[0-9]{2}-[0-9]{2})?)$', views.download_log,
            name='download_log'),
    path('search/', views.search_results, name='search_results'),
    path('search/users', views.search_users, name='search_users'),
    path('search/programs', views.search_programs, name='search_programs'),
    path('search/institutions', views.search_institutions, name='search_institutions'),
    path(r'feedback/detailed/?<int:feedback_id>/', views.feedback_detailed, name='feedback_detailed'),
    path('notifications/', views.notification_history, name='notification_history'),
    path('inbox/notifications/', include(notifications.urls, namespace='notifications')),
    path('readnotification/<int:pk>/', views.read_notification, name="read_notification"),
]
