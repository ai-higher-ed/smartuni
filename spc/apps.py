from django.apps import AppConfig


class SpcConfig(AppConfig):
    name = 'spc'

    def ready(self):
        import spc.signals

