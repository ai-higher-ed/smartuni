from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import SmartUser, Bug, Institution, Program, UserInstitutionProgram, Feedback, Page, Messenger, Language,\
    Settings, UserSettings

# Register your models here.
admin.site.register(SmartUser, UserAdmin)
admin.site.register(Bug)
admin.site.register(Institution)
admin.site.register(Program)
admin.site.register(UserInstitutionProgram)
admin.site.register(Feedback)
admin.site.register(Page)
admin.site.register(Settings)
admin.site.register(UserSettings)
admin.site.register(Messenger)
admin.site.register(Language)

