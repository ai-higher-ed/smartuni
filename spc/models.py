from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from datetime import datetime
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.conf import settings
from django.utils import timezone
from django.core.validators import RegexValidator
from django.contrib.auth.hashers import make_password
from notifications.base.models import AbstractNotification
import pytz

def rename_photo(instance, filename):
    filehash = make_password(str(instance.id)).replace('/', '')
    return 'profilepicture/{}.png'.format(filehash)


class SmartUser(AbstractUser):
    email = models.EmailField(unique=True, blank=False, help_text="Please enter your UOS email")
    pic = ProcessedImageField(upload_to=rename_photo, default="avatar.png", processors=[ResizeToFill(200, 200)],
                              format="JPEG", options={'quality': 60})
    dob = models.DateField(blank=True, null=True)
    priorcourse_id = models.IntegerField(null=True)
    skills_id = models.IntegerField(null=True)
    institutions = models.ManyToManyField('Institution', through='UserInstitutionProgram', blank=True)
    programs = models.ManyToManyField('Program', through='UserInstitutionProgram', blank=True)
    settings = models.ManyToManyField('Settings', through='UserSettings', blank=True)
    on_site = models.BooleanField(blank=True, null=True)
    bio = models.TextField(blank=True, null=True)
    phoneNumberRegex = RegexValidator(regex=r"^[(+?\d{2})|0]\s?[\d\s]{8,16}$")
    phone = models.CharField(validators=[phoneNumberRegex], max_length=16, blank=True, null=True)
    messenger = models.ManyToManyField('Messenger', blank=True)
    language = models.ManyToManyField('Language', blank=True)
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))
    timezone = models.CharField(max_length=80, choices=TIMEZONES,default='Europe/Berlin')
    REQUIRED_FIELDS = ['email', 'first_name']

    def __str__(self):
        return self.username

    def get_full_name(self):
        return "{} {}".format(self.first_name, self.last_name)


class Settings(models.Model):
    name = models.TextField(max_length=255)

    NOTIF_VALUE = (
        ('0', 'NONE'),
        ('1', 'INAPP'),
        ('2', 'EMAIL'),
        ('3', 'BOTH')
    )

    value = models.CharField(max_length=1, blank=True, default='3', choices=NOTIF_VALUE,
                             help_text="The default value for the setting")
    description = models.TextField(max_length=500)

    CATEGORY = (
        ('0', 'SPC'),
        ('1', 'SPM'),
        ('2', 'SBM')
    )

    category = models.CharField(max_length=1, choices=CATEGORY, blank=True, default='0')
    template = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class UserSettings(models.Model):
    user = models.ForeignKey(SmartUser, on_delete=models.CASCADE)
    setting = models.ForeignKey(Settings, on_delete=models.CASCADE)

    NOTIF_VALUE = (
        ('0', 'NONE'),
        ('1', 'INAPP'),
        ('2', 'EMAIL'),
        ('3', 'BOTH')
    )
    value = models.CharField(max_length=1, choices=NOTIF_VALUE, blank=True, default='0')

    def __str__(self):
        return str(self.user.id) + ':' + self.setting.name


class Bug(models.Model):
    user_id = models.ForeignKey(SmartUser, on_delete=models.SET_NULL, null=True)
    description = models.TextField(max_length=500)
    module_id = models.ForeignKey('Page', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    closed_at = models.DateTimeField(null=True, blank=True)

    STATUS = (
        ('o', 'open'),
        ('i', 'in progress'),
        ('c', 'closed'),
    )

    state = models.CharField(
        max_length=1,
        choices=STATUS,
        blank=True,
        default='o',
        help_text='what is the current state of the bug?',
    )

    def __str__(self):
        """return str(self.id)"""
        return "user id {}, description {}".format(self.user_id, ' '.join(self.description.split()[:10]))

    def save(self, *args, **kwargs):
        if self.state == 'c':
            self.closed_at = timezone.now()
        super(Bug, self).save(*args, **kwargs)

    def get_absolute_url(self):
        """Returns the url to access a detailed record for this bug."""
        return reverse('bug', args=[str(self.id)])


class Institution(models.Model):
    """User Institute"""
    institution_name = models.CharField(max_length=255, help_text='The full name of the institute')
    user_program = models.ManyToManyField('Program', through='UserInstitutionProgram', blank=True,
                                          related_name='user_programs')
    programs = models.ManyToManyField('Program', related_name='programs')

    def __str__(self):
        return self.institution_name


class Program(models.Model):
    """Program"""
    prog_name = models.CharField(max_length=255)
    user_institution = models.ManyToManyField('Institution', through='UserInstitutionProgram',
                                              related_name='user_institutions')

    def __str__(self):
        return self.prog_name


class UserInstitutionProgram(models.Model):
    """Associative model between user, institution and program"""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    user_institution = models.ForeignKey(Institution, on_delete=models.CASCADE, null=True)
    user_program = models.ForeignKey(Program, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.user.username + "," + self.user_institution.institution_name + "," + self.user_program.prog_name


class Page(models.Model):
    page_name = models.CharField(max_length=20, help_text='Enter the name of the model')
    url_pattern = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.page_name

    def get_absolute_url(self):
        """Returns the url to access a particular instance of the model."""
        return reverse('module-detail-view', args=[str(self.id)])


STATUS = [
    ('open', 'open'),
    ('closed', 'closed'),
    ('in work', 'in work'),
    ('verification',  'verification')]


class Feedback(models.Model):
    user = models.ForeignKey('SmartUser', on_delete=models.SET_NULL, null=True)
    module = models.ForeignKey('Page', on_delete=models.SET_NULL, null=True)
    feedback = models.CharField(max_length=500, help_text='Enter your feedback.')
    rating = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], null=True)
    subtime = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, choices=STATUS, default='open', null=True)

    def __str__(self):
        return ' '.join(self.feedback.strip()[:10])

    def get_rating(self):
        return self.rating


class Messenger(models.Model):
    name = models.CharField(max_length=255)
    icon = models.ImageField(upload_to='messenger_icons')

    def __str__(self):
        return self.name


class Language(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Timezone(models.Model):
    timezone = models.CharField(max_length=255)

    def __str__(self):
        return self.timezone

class Module(models.Model):
    module_name = models.CharField(max_length=20, help_text='Enter the name of the model')
    abbreviation = models.CharField(max_length=3, null=True)

    def __str__(self):
        return self.module_name


class Notification(AbstractNotification):
    module = models.ForeignKey('Module', on_delete=models.SET_NULL, null=True, default=1)
    url = models.CharField(max_length=50, default='/', null=True)

    class Meta(AbstractNotification.Meta):
        abstract = False


