from django.test import TestCase
from spc.forms import RegisterForm, PasswordChangeForm

class RegisterFormTest(TestCase):

    def setUp(self):
        pass

    def test_validate_email(self):
        correct_email = 'test@uos.de'
        correct_email_two = 'test@uni-osnabrueck.de'
        wrong_email = 'test@mail.de'

        self.assertTrue(RegisterForm.validate_email(correct_email))
        self.assertTrue(RegisterForm.validate_email(correct_email_two))
        self.assertFalse(RegisterForm.validate_email(wrong_email))

    def test_validate_password(self):
        password_empty = ''
        password_none = None
        password_short = 'Test'
        password_correct = 'TestPassword'
        password_emptystring = '          '

        wrong_passwords = [password_empty, password_none, password_short, password_emptystring]
        msg = []
        for pwd in wrong_passwords:
            self.assertFalse(RegisterForm.validate_password(pwd, pwd, msg) == [])
            msg.clear()
        self.assertTrue(RegisterForm.validate_password(password_correct, password_correct, msg) == [])
        msg.clear()
