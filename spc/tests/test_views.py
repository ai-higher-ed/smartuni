from django.test import TestCase
from django.urls import reverse

from spc.models import Bug, SmartUser


class BugListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        number_of_bugs = 5
        for bug_id in range(number_of_bugs):
            Bug.objects.create(
                description="Test lorem ipsum"
            )
        # create user
        staff_user = SmartUser.objects.create_superuser(email="user@uos.de", first_name="Test", last_name="User", username="user", password="user")
        normal_user = SmartUser.objects.create(email="user2@uos.de", first_name="Test", last_name="Two", username="user2")
        normal_user.set_password('user2')
        normal_user.save()

    def test_view_url_exists_at_desired_location(self):
        self.client.login(username="user", password="user")
        response = self.client.get('/bugs/')
        self.assertEqual(response.status_code, 200)

    def test_view_url_accessible_by_name(self):
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('bugs'))
        self.assertEqual(response.status_code, 200)

    def test_no_access_when_logged_out(self):
        response = self.client.get(reverse('bugs'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/bugs/')

    def test_no_access_normal_user(self):
        self.client.login(username="user2", password="user2")
        response = self.client.get(reverse('bugs'))
        self.assertEqual(response.status_code, 403)
