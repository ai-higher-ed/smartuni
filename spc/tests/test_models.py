from django.test import TestCase
from datetime import datetime
from spc.models import SmartUser, Bug, Institution, Program, UserInstitutionProgram


class SUTest(TestCase):
    def setUp(self):
        SmartUser.objects.create(email='su@uos.de', username="su", first_name="Smart", last_name="Uni")

    def test_default_profile_picture(self):
        user = SmartUser.objects.get(id=1)
        picture_url = user.pic.url
        self.assertEqual(picture_url, '/media/avatar.png')

    def test_object_name(self):
        user = SmartUser.objects.get(id=1)
        expected_username = 'su'
        self.assertEqual(str(user), expected_username)


class BugTest(TestCase):
    def setUp(self):
        Bug.objects.create(description='this is a test')

    def test_default_status(self):
        bug = Bug.objects.get(id=1)
        expected_state = 'o'
        self.assertEqual(bug.state, expected_state)

    # route has to be defined before this can be tested
    #def test_get_absolute_url(self):
    #    bug = Bug.objects.get(id=1)
    #    self.assertEqual(bug.get_absolute_url(), '/bug/1')

    def test_automatic_closed_at_datetime(self):
        bug = Bug.objects.get(id=1)
        bug.state = 'c'
        bug.save()
        now = datetime.now()
        self.assertEqual(bug.closed_at.strftime("%m/%d/%Y, %H:%M:%S"), now.strftime("%m/%d/%Y, %H:%M:%S"))


class InstitutionTest(TestCase):
    def setUp(self):
        Institution.objects.create(institution_name='TestInst')

    def test_inst_name(self):
        inst = Institution.objects.get(id=1)
        self.assertEqual(inst.institution_name, 'TestInst')


class UserInstProgTest(TestCase):
    def setUp(self):
        user = SmartUser.objects.create(email='su@uos.de', dob='2000-01-01')
        prog = Program.objects.create(prog_name='TestProg')
        inst = Institution.objects.create(institution_name='TestInst')
        uip = UserInstitutionProgram.objects.create(user=user, user_institution=inst, user_program=prog)

    def test_user_account(self):
        uip = UserInstitutionProgram.objects.get(id=1)
        self.assertEqual(uip.user.email, SmartUser.objects.get(id=1).email)

    def test_institution_instance(self):
        uip = UserInstitutionProgram.objects.get(id=1)
        self.assertEqual(uip.user_institution, Institution.objects.get(id=1))

    def test_program_instance(self):
        uip = UserInstitutionProgram.objects.get(id=1)
        self.assertEqual(uip.user_program, Program.objects.get(id=1))


class InstProgTest(TestCase):
    def setUp(self):
        Program.objects.create(prog_name='TestProg')
        Program.objects.create(prog_name='TestProgTwo')
        Institution.objects.create(institution_name='TestInst')

    def test_add_programs(self):
        inst = Institution.objects.get(id=1)
        inst.programs.add(Program.objects.get(id=1))
        inst.programs.add(Program.objects.get(id=2))

        for prog in inst.programs.all():
            self.assertIsInstance(prog, Program)

    def test_add_insts(self):
        inst_two = Institution.objects.create(institution_name='InstTwo')
        prog = Program.objects.get(id=1)
        prog.user_institution.add(Institution.objects.get(id=1))
        prog.user_institution.add(inst_two)

        for inst in prog.user_institution.all():
            self.assertIsInstance(inst, Institution)
