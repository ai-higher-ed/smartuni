from urllib import request
from django import forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from spc.models import UserInstitutionProgram, Institution, Program, Feedback, STATUS, Page, SmartUser, Settings, \
    UserSettings, Bug, Messenger, Language
from django.contrib.auth.forms import UserChangeForm
import pytz

class RegisterForm(forms.ModelForm):
    """Creates the registration form with custom username, password and email validation."""
    first_name = forms.CharField(max_length=30, required=True, help_text='Alphanumeric characters only.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Alphanumeric characters only.')
    username = forms.CharField(max_length=30, required=True, help_text='Alphanumeric characters only.')
    password = forms.CharField(min_length=8, max_length=30, widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField(max_length=254, help_text='Required. Insert a valid email address.')

    class Meta:
        model = get_user_model()
        fields = ('username', 'password', 'confirm_password', 'email', 'first_name','last_name')

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        first_name = cleaned_data.get("first_name")
        last_name = cleaned_data.get("last_name")
        username = cleaned_data.get("username")
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        email = cleaned_data.get("email")

        # collects error messages pertaining to the username, email and password in a list
        # in order to raise them all at once
        error_messages = []
        if first_name == '' or first_name is None:
            error_messages.append(forms.ValidationError("Please enter a first name.", code='first_name_err'))

        if username == '' or username is None:
            error_messages.append(forms.ValidationError("Please enter a username.", code='username_err'))

        error_messages = self.validate_password(password, confirm_password, error_messages)

        users = get_user_model()
        if not self.validate_email(email) or users.objects.filter(email=email).exists():
            error_messages.append(
                forms.ValidationError("The email is not correct. Please use your uos or uni-osnabrueck address.",
                                      code='email_err'))

        # raise all errors at once if there are any
        if error_messages:
            raise forms.ValidationError(error_messages)

    @staticmethod
    def validate_email(email):
        """Checks if the email address is from the university of Osnabrück (@uos.de or @uni-osnabrueck.de)
        and returns True if the email is valid, False otherwise"""

        try:
            provider = str(email).split('@')[1]
        except (NameError, IndexError, RuntimeError):
            return False
        if provider not in ["uos.de", "uni-osnabrueck.de","uni-osnabrück.de"]:
            return False
        return True

    @staticmethod
    def validate_password(password, confirm_password, error_messages):
        """Checks if the password is at least 8 and at most 30 characters long and matches the confirmation password.
        It returns a list of ValidationError messages"""

        if password == '' or password is None:
            error_messages.append(
                forms.ValidationError("Please enter a password that is at least 8 and at most 30 characters long.",
                                      code='password_err'))
        else:
            password = password.strip()
            confirm_password = confirm_password.strip()
            if len(password) < 8:
                error_messages.append(forms.ValidationError("Password too short. Please enter at least 8 characters."))
            elif password != confirm_password:
                error_messages.append(forms.ValidationError("The passwords did not match!", code='password_err'))
        return error_messages

    def save(self, commit=True):
        user = super(RegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get("password"))
        if commit:
            user.save()
        return user


class PasswordChangeForm(forms.ModelForm):
    """Creates the form for resetting the password when logged in."""
    user_id = forms.IntegerField()
    password = forms.CharField(min_length=8, max_length=30, widget=forms.PasswordInput())
    confirm_password = forms.CharField(min_length=8, max_length=30, widget=forms.PasswordInput())
    old_password = forms.CharField(min_length=8, max_length=30, widget=forms.PasswordInput())

    class Meta:
        model = get_user_model()
        fields = ('user_id', 'password', 'confirm_password', 'old_password')

    def clean(self):
        cleaned_data = super(PasswordChangeForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        old_password = cleaned_data.get("old_password")
        user = get_user_model().objects.get(id=self.cleaned_data.get("user_id"))

        # validate the new password only if the user has validated their identity
        if user.check_password(old_password):
            if user.check_password(password):
                # the new password should be different from the old one
                raise forms.ValidationError("Your new password cannot be the same as your old password.")
            error_message = []
            error_message = RegisterForm.validate_password(password, confirm_password, error_message)
            if error_message:
                # the new password doesn't meet the requirements
                raise forms.ValidationError(error_message)
        else:
            # if a wrong old password is provided, the user cannot change their password
            raise forms.ValidationError("Wrong password. Please enter your correct old password.")

    def save(self, commit=True):
        user = get_user_model().objects.get(id=self.cleaned_data.get("user_id"))
        user.set_password(self.cleaned_data.get("password"))
        if commit:
            user.save()
        return user


class ProfileSettingsForm(UserChangeForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Last Name'}))
    email = forms.EmailField()
    dob = forms.DateField(label='Date of birth', widget=forms.DateInput(attrs={"type": "date"}))
    on_site = forms.ChoiceField(widget=forms.RadioSelect(),choices=((True,"On Site"),(False, "Remote")),label="Are you a remote or on site student")
    institution_1 = forms.ModelChoiceField(queryset=Institution.objects.all(), widget=forms.Select(attrs={'placeholder': 'Institution'}), help_text="Select your institution (e.g. University)")
    institution_2 = forms.ModelChoiceField(queryset=Institution.objects.all(),widget=forms.Select(attrs={'placeholder': 'Institution 2 - optional'}), help_text="Select your institution (e.g. University) if you're studying at two insitutions at the same time or you can also select past institutions")
    program_1 = forms.ModelChoiceField(queryset=Program.objects.all(), widget=forms.Select(attrs={'placeholder': 'Program of study'}), help_text="Select your study program")
    program_2 = forms.ModelChoiceField(queryset=Program.objects.all(), widget=forms.Select(attrs={'placeholder': 'Program of study 2 - optional'}), help_text="Select a second study program you are currently enrolled in or have benn enrolled in")
    pic = forms.ImageField(widget=forms.FileInput(attrs={'class': 'input-group-text'}), label="Select profile picture")
    bio = forms.CharField(widget=forms.Textarea, required=False)
    phone = forms.CharField(widget=forms.TextInput(attrs={'placeholder': '+XX XXX XXXXXXXXXX', 'type':'tel'}), label="Phone number (please include country code)", required=False)
    messenger = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Messenger.objects.all(), label='Which messengers do you use?', required=False)
    language = forms.ModelMultipleChoiceField(widget=forms.SelectMultiple(attrs={'class': 'select2'}), queryset=Language.objects.all(),
                                               label='Which languages do you speak?', required=False)
    timezone = forms.ChoiceField(choices=tuple(zip(pytz.all_timezones, pytz.all_timezones)), widget=forms.Select(attrs={'placeholder': 'Timezone'}), help_text="Choose your Timezone:")

    last_name.required =False
    dob.required = False
    institution_1.required = False
    program_1.required = False
    institution_2.required = False
    program_2.required = False
    on_site.required=False
    pic.required = False

    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name', 'email', 'dob', 'institution_1', 'institution_2', 'program_1', 'program_2',
                  'pic', 'bio', 'phone', 'on_site', 'messenger', 'language', 'timezone')

    def clean(self):
        inst_1 = self.cleaned_data.get("institution_1")
        inst_2 = self.cleaned_data.get("institution_2")
        prog_1 = self.cleaned_data.get("program_1")
        prog_2 = self.cleaned_data.get("program_2")

        # we only need to validate the institution and program fields if the form fields have changed
        changed_fields = self.changed_data
        if 'institution_1' in changed_fields or 'program_1' in changed_fields:
            self.validate_inst_program(inst_1, prog_1)

        if 'institution_2' in changed_fields or 'program_2' in changed_fields:
            self.validate_inst_program(inst_2, prog_2)

    def save(self, commit=True):
        inst_1 = self.cleaned_data.get("institution_1")
        inst_2 = self.cleaned_data.get("institution_2")
        prog_1 = self.cleaned_data.get("program_1")
        prog_2 = self.cleaned_data.get("program_2")

        user = super(ProfileSettingsForm, self).save(commit=False)

        # get all institutions and programs for this user if they exist, so they can be updated
        user_inst_progs = UserInstitutionProgram.objects.filter(user_id=user.id)
        changed_fields = self.changed_data
        field_list = ['institution_1', 'institution_2', 'program_1', 'program_2']
        user_inst_prog_one = None
        user_inst_prog_two = None

        if any(field in changed_fields for field in field_list):
            # if there already are two entries, it means one or both need ot be updated
            if user_inst_progs.count() == 2:
                user_inst_prog_one = user_inst_progs[0]
                user_inst_prog_one.user_institution = self.get_institution(inst_1)
                user_inst_prog_one.user_program = self.get_program(prog_1)

                user_inst_prog_two = user_inst_progs[1]
                user_inst_prog_two.user_institution = self.get_institution(inst_2)
                user_inst_prog_two.user_program = self.get_program(prog_2)
            # if there are fewer than two entries, either update the old entry or create a new one
            else:
                if 'institution_1' in changed_fields:
                    user_inst_prog_one = self.assign_userinstprog(user_inst_progs, inst_1, prog_1)
                # at this point we know that a second entry does not exist, so we can safely create a new one
                if 'institution_2' in changed_fields:
                    user_inst_prog_two = self.create_new_userinstprog(inst_2, prog_2)

        if commit:
            user.save()
            if user_inst_prog_one:
                user_inst_prog_one.save()
            if user_inst_prog_two:
                user_inst_prog_two.save()
            self.save_m2m()

    def validate_inst_program(self, inst, prog):
        """Checks if the institution and program exist in the database and if such an entry already exists.
        Throws a Validation error if institution or program don't exist or an entry already exists."""

        if not self.get_institution(inst):
            raise forms.ValidationError("The institution does not exist. Please enter a valid institution.")
        if prog:
            program = self.get_program(prog)
            if not program:
                raise forms.ValidationError("The program does not exist. Please enter a valid program.")
            duplicate = self.get_userinstprog(self.get_institution(inst), program)
            if duplicate:
                raise forms.ValidationError("The entry for this institution and program already exists.")
        if not prog:
            raise forms.ValidationError("No program for the institution provided. Please enter a valid program.")

    def get_institution(self, inst):
        """Returns an institution instance or None if it does not exist."""
        try:
            inst = Institution.objects.get(institution_name=str(inst))
        except ObjectDoesNotExist:
            inst = None
        return inst

    def get_program(self, prog):
        """Returns a program instance or None if it does not exist."""
        try:
            prog = Program.objects.get(prog_name=str(prog))
        except ObjectDoesNotExist:
            prog = None
        return prog

    def get_userinstprog(self, inst=None, prog=None):
        """Returns a UserInstitutionProgram instance or None if it does not exist."""
        try:
            user_inst_prog = UserInstitutionProgram.objects.get(Q(user=self.instance) & Q(user_institution=inst) &
                                                                Q(user_program=prog))
        except ObjectDoesNotExist:
            user_inst_prog = None

        return user_inst_prog

    def create_new_userinstprog(self, inst, prog):
        """Creates a new UserInstitutionProgram from the given inst and prog parameters."""
        return UserInstitutionProgram.objects.create(
            user=self.instance,
            user_institution=self.get_institution(inst),
            user_program=self.get_program(prog)
        )

    def assign_userinstprog(self, user_inst_progs, institution, program):
        """Creates a new UserInstitutionProgram instance or updates if one already exists."""
        # if there are no entries yet, create a new entry
        if user_inst_progs.count() == 0:
            return self.create_new_userinstprog(institution, program)
        # if an entry exists, update the entry
        if user_inst_progs.count() == 1:
            user_inst_prog = user_inst_progs.get()
            user_inst_prog.user_institution = self.get_institution(institution)
            user_inst_prog.user_program = self.get_program(program)
            return user_inst_prog
        return None


class SettingsForm(forms.ModelForm):
    class Meta:
        model = UserSettings
        fields = ('value', 'user', 'setting')


class FeedbackForm(forms.ModelForm):

    feedback = forms.CharField(widget=forms.Textarea, required=True)
    rating = forms.IntegerField(required=False, error_messages={'required': 'This field is required.'})
    module = forms.ModelChoiceField(queryset=Page.objects.all())
    user = forms.ModelChoiceField(queryset=SmartUser.objects.all())

    class Meta:
        model = Feedback
        fields = ('feedback', 'rating', 'module', 'user')

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        self.module = kwargs.pop('module', None)
        super(FeedbackForm, self).__init__(*args, **kwargs)
        if self.user:
            self.fields['user'].queryset = SmartUser.objects.filter(id=self.user.id)
        if self.module:
            self.fields['module'].queryset = Page.objects.filter(id=self.module.id)

    def clean(self):
        cleaned_data = super(FeedbackForm, self).clean()
        feedback = cleaned_data.get("feedback")
        module = cleaned_data.get("module")
        user = cleaned_data.get("user")

        error_messages = []
        if feedback is None:
            error_messages.append(forms.ValidationError("Please fill out the feedback box!"))
        if module is None:
            error_messages.append(forms.ValidationError("Module information is missing!"))
        if user is None:
            error_messages.append(forms.ValidationError("User information is missing! Are you logged in?"))

        if error_messages:
            raise forms.ValidationError(error_messages)

    def save(self, commit=True):
        feedback = super(FeedbackForm, self).save(commit=False)
        if commit:
            feedback.save()


class ReportBugsForm(forms.ModelForm):
    module_id = forms.ModelChoiceField(queryset=Page.objects.all(), label='Which service is affected?', to_field_name='url_pattern')
    description = forms.CharField(label='Please describe the error or bug you experienced', widget=forms.Textarea(attrs={'placeholder': 'Enter text here..'}), required=False)

    class Meta:
        model = Bug
        fields = ('module_id', 'description')


class SearchForm(forms.Form):
    search_string = forms.CharField(min_length=3)

    class Meta:
        fields = ('search_string',)
