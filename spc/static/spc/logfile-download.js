$(document).ready(()=> {
    $('#download-log').click(function(){
        const url = $('#download-log').data('downloadUrl');
        $.get(url, function() {
            window.location.href = url;
        });
    })
})