/* globals feather:false */

(function () {
  'use strict'



  // put all javascript init stuff here
  // ...

})()

//Tabs
$(document).ready(()=> {

    load_feather_icons();

     // You should update active tab with JS
     $('.tabs > a.tab').on('click', (event) => {
          $(event.target).parent('.tabs').find('> a.tab.active').removeClass('active');
          $(event.target).addClass('active');
          // Improving UX with smoothing scrolling
          if(!$(event.target.hash).length) return;
          $('body').stop().animate({
              scrollTop: $(event.target.hash).offset().top - $('#tabs').outerHeight()
          }, 300, 'swing');
     });

     // check if the password and password confirmation match and display a status message
     $('#password, #confirm_password').on('keyup', function () {

          if ($('#password').val() === $('#confirm_password').val()) {
              show_passwords_match_message();
          } else {
              show_passwords_mismatch_message();
          }
     });

     // hide the visible-password icon while password is invisible
    $(document).find('.feather-eye').parent().hide();

    // toggles visibility of the password input and switches to the appropriate eye icon
    $('.input-group-text').click(function(event) {
        let passwordField = $(this).parent().find('#password');
        // execute the function only if it was a password field
        if (passwordField) {
            show_correct_password_icon(event, passwordField);
        }
    });

    // makes error status messages visible under form fields if the input is invalid
    $(':input').each(display_form_status_msgs);

    // displays the search feather icon in the nav bar inside a button
    $('button.input-group-text.nav-search').each(display_feather_button);

    $('[data-bs-toggle="collapse"]').click(function() {
        $(this).toggleClass( "active" );
        if ($(this).hasClass("active")) {
            $(this).text("Hide");
            $('#initialprograms').hide();
        } else {
            $(this).text("Show more...");
            $('#initialprograms').show();
        }
    });

})

// passwords match success message
function show_passwords_match_message() {
    $('#pw_message_success').css('display', 'inline-block');
    $('#pw_message_error').hide();
}

// passwords don't match error message
function show_passwords_mismatch_message() {
    $('#pw_message_error').css('display', 'inline-block');
    $('#pw_message_success').hide();
}

function show_correct_password_icon(event, passwordField) {
    if ($(passwordField).prop('type') === 'password') {
        // change field type to text in order to show the password
        $(passwordField).prop('type', 'text');
        // switch the password-is-hidden icon to the password-is-shown icon
        $(event.currentTarget).parent().find('.input-group-text:hidden').show();
        $(event.currentTarget).find('.feather-eye-off').parent().hide();
    } else {
        // change field type to password to hide the password
        $(passwordField).prop('type', 'password');
        // switch the password-is-shown icon to the password-is-hidden icon
        $(event.currentTarget).parent().find('.input-group-text:hidden').show();
        $(event.currentTarget).find('.feather-eye').parent().hide();
    }
}

function display_form_status_msgs() {
    if (this.matches(':invalid')) {
        let ariaIds = $(this).attr('aria-describedby');
        if (ariaIds) {
            ariaIds = ariaIds.split(' ');
        }
        // there may be multiple aria-describedby ids, show only the span elements with the id
        for (let id in ariaIds) {
            $('span#' + ariaIds[id]).css('display', 'block')
        }
    }
}

function load_feather_icons() {
    // check if each icon to be displayed exists and skip displaying the ones that don't
    // so all following icons can still be displayed
    let elementsToReplace = document.querySelectorAll('[data-feather]');
    Array.from(elementsToReplace).forEach(function (element) {
        let icon = element.getAttribute('data-feather');
        if (typeof feather.icons[icon] === "undefined") {
            element.removeAttribute('data-feather');
        }
    });

    // load feather icons
    feather.replace({ 'aria-hidden': 'true' })
}


function dropdownFunction() {
    document.getElementById("notificationDropdown").classList.toggle("notishow");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdownsu-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('notishow')) {
                openDropdown.classList.remove('notishow');
            }
        }
    }
}

function display_feather_button() {
    this.innerHTML = feather.icons.search.toSvg();
}
