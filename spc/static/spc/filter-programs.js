$('#id_institution_1').change(function () {
    const url = $('#general-settings-form').attr('data-program-url');
    const institution = $(this).val();
    $.ajax({
        url: url,
        headers: {
            'X-CSRFToken': $("input[name=csrfmiddlewaretoken]").val()
            // plus other data
        },
        mode: 'same-origin',
        data: {
            "institution": institution
        },
        success: function (data) {
            $('#id_program_1').html(data);
        }
    })
})
$('#id_institution_2').change(function () {
    const url = $('#general-settings-form').attr('data-program-url');
    const institution = $(this).val();
    $.ajax({
        url: url,
        headers: {
            'X-CSRFToken': $("input[name=csrfmiddlewaretoken]").val()
            // plus other data
        },
        mode: 'same-origin',
        data: {
            "institution": institution
        },
        success: function (data) {
            $('#id_program_2').html(data);
        }
    })
})