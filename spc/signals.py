import os
from spc.models import UserSettings, SmartUser, Settings
from django.db.models.signals import post_save, pre_save


def delete_old_file(sender, instance, **kwargs):
    # on creation, signal callback won't be triggered
    if instance._state.adding and not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).pic
    except sender.DoesNotExist:
        return False

    # comparing the new file with the old one
    file = instance.pic
    if old_file != file and old_file != 'avatar.png':
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


# this handles removing the old profile picture when uploading a new one
pre_save.connect(delete_old_file, sender=SmartUser)


def after_create_new_setting(sender, instance, created, **kwargs):
    if created:
        objects = []
        users = SmartUser.objects.all()
        for user in users:
            objects.append(UserSettings(
                user_id=user.id,
                setting_id=instance.id,
                value=instance.value
            ))
        UserSettings.objects.bulk_create(objects)


post_save.connect(after_create_new_setting, sender=Settings)


def after_create_new_user(sender, instance, created, **kwargs):
    if created:
        settings = Settings.objects.all()
        objects = []
        for setting in settings:
            objects.append(UserSettings(
                user_id=instance.id,
                setting_id=setting.id,
                value=setting.value
            ))
        UserSettings.objects.bulk_create(objects)


post_save.connect(after_create_new_user, sender=SmartUser)
