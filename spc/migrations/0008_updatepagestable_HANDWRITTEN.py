import pandas as pd
from django.db import migrations
import json

def forwards_func_pages(apps, schema_editor):
    Page = apps.get_model('spc', 'page')
    Page.objects.all().delete()
    db_alias = schema_editor.connection.alias
    Page.objects.using(db_alias).bulk_create([
        Page(page_name='Welcome User Page', url_pattern='user/'),
        Page(page_name='Settings Page', url_pattern='settings/'),
        Page(page_name='SmartPlanner', url_pattern='spm/'),
        Page(page_name='StuddyBuddyMatch', url_pattern='sbm/'),
        Page(page_name='Other', url_pattern='')
    ])

def reverse_func_pages(apps, schema_editor):
    Page = apps.get_model('spc', 'page')
    db_alias = schema_editor.connection.alias
    Page.objects.using(db_alias).filter(module_name='spc').delete()
    Page.objects.using(db_alias).filter(module_name='spm').delete()
    Page.objects.using(db_alias).filter(module_name='sbm').delete()

class Migration(migrations.Migration):

    dependencies = [
        ('spc', '0007_notification_HANDWRITTEN'),
    ]

    operations = [
        migrations.RunPython(forwards_func_pages, reverse_func_pages)
    ]
