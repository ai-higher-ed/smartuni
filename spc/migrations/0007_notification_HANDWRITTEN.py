from django.db import migrations
from django.contrib.contenttypes.models import ContentType


#MUST add user and django_content_type before this will work!
def add_modules(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    Module = apps.get_model('spc', 'module')
    Module.objects.using(db_alias).bulk_create([
        Module(module_name='Core', abbreviation="SPC"),
        Module(module_name='SmartPlanner', abbreviation="SPM"),
        Module(module_name='StuddyBuddyMatch', abbreviation="SBM")
    ])

def content_type_add(apps,schema_editor):
    db_alias = schema_editor.connection.alias
    ContentType.objects.using(db_alias).bulk_create([
        ContentType(app_label=1, model=1)
    ])

def add_intial_settings(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    Settings = apps.get_model('spc', 'settings')
    Settings.objects.using(db_alias).bulk_create([
        Settings(name="SPC_PASSWORDCHANGE", value='2', description="Password change", category="0",
                 template="email/password_change.html"),
        Settings(name="SPM_EVENT", value="1", description="Event reminder", category="1",
                 template="email/event_reminder.html"),
        Settings(name="SPM_TASK", value="1", description="Task reminder", category="1",
                 template="email/task_reminder.html"),
        Settings(name="SBM_REMINDER", value="3", description="Reminder notification", category="2",
                 template="email/dont_forget_me.html"),
        Settings(name="SBM_NEWMATCH", value="3", description="New match", category="2",
                 template="email/new_match.html"),
        Settings(name="SBM_READYTOMATCH", value="3", description="Ready to match", category="2",
                 template="email/ready_to_match.html"),
        Settings(name="SBM_MATCHFEEDBACK", value="3", description="Reminder to rate matches", category="2",
                 template="email/sbm_feedback.html"),
    ])

def create_usersettings(apps, schema_editor):
    db_alias = schema_editor.connection.alias
    UserSettings = apps.get_model('spc', 'usersettings')
    Settings = apps.get_model('spc', 'settings')
    SmartUser = apps.get_model('spc', 'smartuser')
    users = SmartUser.objects.all()
    settings = Settings.objects.all()
    created_usersettings = []
    for setting in settings:
        for user in users:
            created_usersettings.append(UserSettings(
                user_id=user.id,
                setting_id=setting.id,
                value=setting.value
            ))
    UserSettings.objects.using(db_alias).bulk_create(created_usersettings)

class Migration(migrations.Migration):

    dependencies = [
        ('spc', '0006_module_settings_rename_module_name_page_page_name_and_more'),
    ]

    operations = [
        migrations.RunPython(add_modules),
        migrations.RunPython(content_type_add),
        migrations.RunPython(add_intial_settings),
        migrations.RunPython(create_usersettings),
    ]