import logging
import mimetypes
import os.path
from datetime import datetime
from django.db.models import Q
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound, JsonResponse
import django.middleware.csrf
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_protect
from smartuni.settings import DEBUG, BASE_DIR
from spc.forms import RegisterForm, ProfileSettingsForm, ReportBugsForm, FeedbackForm, SettingsForm, SearchForm
from .ai_search import AISearch
from .models import UserInstitutionProgram, Feedback, Page, Bug, Institution, Program, Messenger, Language, \
    Notification, UserSettings, Settings
from sbm.models import CourseEnrollment, Course
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views import generic
import datetime
import pytz
import pandas as pd


def landing(request):
    page = 'landing'  # for highlighting current page
    return render(request, 'spc/landing.html', locals())


def about(request):
    page = 'about'  # for highlighting current page
    return render(request, 'spc/about.html', locals())


def data_privacy(request):
    page = 'data-privacy'
    return render(request, 'spc/data_privacy.html', locals())

@login_required
def jumpstart(request):
    page = "jumpstart"
    # fetch current user
    name = request.user.first_name
    context = {
        "name": name,
        "page": page,
    }

    return render(request, 'spc/jumpstart.html', context)


def register(request):
    page = 'register'
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request, 'You successfully registered. Please login using your credentials.')
            get_logger().info("New user with id {} successfully registered.".format(user.id))
            return redirect('login')
        else:
            for err in form.non_field_errors():
                messages.error(request, err)

    if not request.user.is_authenticated:
        # show registration page
        return render(request, 'registration/register.html', locals())
    else:
        # forward to landing page, if logged-in user tries to call register page
        return render(request, 'spc/landing.html', locals())


@login_required
def settings(request):
    timezones = pytz.common_timezones
    page = 'settings'  # for highlighting current page
    # create a UserInstitutionProgram list to pass as initial values to the form, so it is not empty if data exists
    user_inst_progs = UserInstitutionProgram.objects.filter(user_id=request.user.id)
    uip_list = {}
    db_settings = Settings.objects.all()
    settings = {}
    settings['SPC'] = {}
    settings['SPM'] = {}
    settings['SBM'] = {}
    for setting in db_settings:
        cat = ''
        if setting.category == '0':
            cat = 'SPC'
        elif setting.category == '1':
            cat = 'SPM'
        elif setting.category == '2':
            cat = 'SBM'

        settings[cat][setting.name] = setting.description

    if user_inst_progs.exists():
        uip_1 = user_inst_progs[0]
        uip_list['institution_1'] = uip_1.user_institution.id
        uip_list['program_1'] = uip_1.user_program.id

    if user_inst_progs.count() == 2:
        uip_2 = user_inst_progs[1]
        uip_list['institution_2'] = uip_2.user_institution.id
        uip_list['program_2'] = uip_2.user_program.id

    notif_initial_instances = UserSettings.objects.filter(user_id=request.user.id)
    notif_initial = {}
    for notif in notif_initial_instances:
        notif_initial[notif.setting.name] = notif.value
    if request.method == 'POST':
        form = ProfileSettingsForm(request.POST, request.FILES, instance=request.user, initial=uip_list)
        form_notifs = SettingsForm(instance=request.user, initial=notif_initial)
        if form.is_valid():
            form.save()
            messages.success(request, 'Settings saved!')
            get_logger().info("User with id {} changed the following profile settings: {}".format(
                request.user.id,
                form.changed_data
            ))
        else:
            for err in form.non_field_errors():
                messages.error(request, err)
    else:
        notif_initial_instances = UserSettings.objects.filter(user_id=request.user.id)
        notif_initial = {}
        for notif in notif_initial_instances:
            notif_initial[notif.setting.name] = notif.value
        form = ProfileSettingsForm(instance=request.user, initial=uip_list)
        form_notifs = SettingsForm(instance=request.user, initial=notif_initial)

    return render(request, 'spc/settings.html', locals())


@login_required
@csrf_protect
def save_notif_settings(request):
    if request.headers.get('X-Requested-With') == 'XMLHttpRequest':
        if request.method == 'POST':
            # prepare data
            settings = []
            settings.append({
                'csrfmiddlewaretoken': request.POST['csrfmiddlewaretoken']
            })
            for key, item in request.POST.items():
                if key != 'csrfmiddlewaretoken':

                    us = get_object_or_404(UserSettings, user=request.user, setting=Settings.objects.get(name=key))
                    if us:
                        us.value = item[0]
                    else:
                        us = UserSettings.objects.create(setting=Settings.objects.get(name=key), user=request.user,
                                                         value=item[0])
                    us.save()
    return JsonResponse({'status': 'Notif settings saved!'})


@login_required
def feedback(request):
    page = 'feedback'  # for highlighting current page
    module_list = Page.objects.all()
    urls = Page.objects.values_list('url_pattern')
    urls = list(urls)
    urllist = [item[0] for item in urls]

    try:
        origin = request.META['HTTP_REFERER'].split('/')[3:4]
        origin = '/'.join(origin)
        origin = origin + '/'
        if origin in urllist:
            origin = origin
        else:
            origin = ''

    except:
        origin = ''

    if request.method == 'POST':
        user = request.user
        module = Page.objects.get(id=request.POST.get('module'))
        form = FeedbackForm(request.POST, user=user, module=module)
        if form.is_valid():
            messages.success(request, 'Your feedback was successfully submitted.')
            form.save()
            return render(request, 'spc/feedback.html', {'form': form,
                                                         'modules': module_list,
                                                         'origin': origin})
        else:
            for err in form.non_field_errors():
                messages.error(request, err)
    return render(request, 'spc/feedback.html', {'form': FeedbackForm(request.POST),
                                                 'modules': module_list,
                                                 'origin': origin})


@staff_member_required
def feedback_list(request):
    page = "feedback_list"
    feedback_list = Feedback.objects.all()

    return render(request, 'spc/feedback_list.html', {'feedbacks': feedback_list})


@staff_member_required
def feedback_detailed(request, feedback_id):
    page = "feedback_detailed"

    feedback = Feedback.objects.get(id=feedback_id)
    module = feedback.module
    user_name = feedback.user
    status = feedback.status
    date = feedback.subtime
    description = feedback.feedback
    rating = feedback.rating
    context = {
        'feedback_id': feedback_id,
        'module': module,
        'user_name': user_name,
        'status': status,
        'date': date,
        'description': description,
        'rating': rating
    }

    return render(request, 'spc/feedback_detailed.html', context)


@login_required
def reportBugs(request):
    form = ReportBugsForm()
    urls = Page.objects.values_list('url_pattern')
    urls = list(urls)
    urllist = [item[0] for item in urls]
    try:
        origin = request.META['HTTP_REFERER'].split('/')[3:4]
        origin = '/'.join(origin)
        origin = origin + '/'
        if origin in urllist:
            origin = origin
        else:
            origin = ''

    except:
        origin = ''

    if request.method == 'POST':
        form = ReportBugsForm(request.POST)
        if form.is_valid():
            messages.success(request, 'You have successfully reported a bug')
            bug = form.save()
            bug.user_id = request.user
            bug.save()
            get_logger().info("User with id {} submitted bug report {}".format(
                request.user.id,
                bug.id
            ))
    else:
        form = ReportBugsForm(initial={'module_id': origin})
    context = {'form': form}
    return render(request, 'spc/report.html', context)


class BugListView(PermissionRequiredMixin, generic.ListView):
    permission_required = 'is_staff'
    model = Bug
    template_name = "spc/bugs_list.html"
    ordering = ['created_at', 'state', 'module_id']


class BugDetailView(PermissionRequiredMixin, generic.DetailView):
    permission_required = 'is_staff'
    model = Bug
    template_name = 'spc/bug_detail_view.html'
    slug_field = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        timestamp = context['bug'].created_at
        context['logname'] = get_logfile(timestamp)
        return context


@staff_member_required
def change_bug_state(request, pk):
    """
    Marks bug's state as "in progress" or "closed" depending on current state
    """
    # gets bug instance or shows a 404 page if instance doesn't exist
    bug = get_object_or_404(Bug, pk=pk)
    if bug.state == 'i':
        bug.state = 'c'
        bug.save()
    elif bug.state == 'o':
        bug.state = 'i'
        bug.save()
    return HttpResponseRedirect(bug.get_absolute_url())


@csrf_protect
def load_programs(request):
    pk = request.GET.get('institution')
    programs = Institution.objects.get(pk=pk).programs.all()
    return render(request, 'spc/program_dropdown_options.html', {'programs': programs})


@staff_member_required()
def download_log(request, logfile):
    """
    Serves a log file for download.
    """
    response = HttpResponseNotFound('<h3>Logfile not found</h3>')
    try:
        logfile_path = os.path.join(BASE_DIR, 'logs/' + logfile)
        file = open(logfile_path)
        mime_type, _ = mimetypes.guess_type(logfile_path)
        response = HttpResponse(file, content_type=mime_type)
        response['Content-Disposition'] = f"attachment; filename={logfile}"
    except FileNotFoundError:
        get_logger().error(f"Logfile {logfile} does not exist.")
    except OSError:
        get_logger().error(f"Could not open logfile {logfile}.")
    except:
        get_logger().error(f"Unexpected error opening the logfile {logfile}.")

    return response


class UserDetailView(LoginRequiredMixin, generic.DetailView):
    model = get_user_model()
    template_name = "spc/user_detail.html"
    context_object_name = 'user_object'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        pk = self.kwargs['pk']
        user_object = get_object_or_404(get_user_model(), pk=pk)
        context["user_object"] = user_object
        context["messengers"] = Messenger.objects.all()
        context["languages"] = Language.objects.all()
        context["courses"] = CourseEnrollment.objects.filter(user=user_object).order_by("grade")
        return context


def get_logger():
    return logging.getLogger(__name__)


@login_required
def notification_history(request):
    page = "notification_history"

    notifications = Notification.objects.filter(recipient_id=request.user.id)
    context = {
        'notifications': notifications
    }

    return render(request, 'spc/notification_history.html', context)


def read_notification(request, pk):
    """
        update notification's state from unread to read
    """
    notification = get_object_or_404(Notification, pk=pk)
    if notification:
        notification.unread = 0
        notification.save()

    if not notification.data or 'url' not in notification.data.keys():
        return HttpResponseRedirect(notification.url)

    return HttpResponseRedirect(notification.data["url"])


def get_logfile(date=None):
    logname = 'debug.log' if DEBUG is True else 'info.log'
    if date is not None and date.date() != datetime.datetime.today().date():
        logname += '.' + date.strftime("%Y-%m-%d")

    # check if the file exists
    logfile_path = os.path.join(BASE_DIR, 'logs/' + logname)
    logname = logname if os.path.exists(logfile_path) else ''

    return logname


per_page = 15


def search_results(request):
    title = 'Search Results'
    query = ''
    context = None

    if request.method == 'POST':
        # handle search string
        form = SearchForm(request.POST)

        if form.is_valid():
            query = form.cleaned_data.get('search_string')
            if query is not None:
                results = search_first_page(request, query)
                context = {
                    'title': title,
                    'query': query,
                    'users': results['users'],
                    'institutions': results['institutions'],
                    'programs': results['programs'],
                    'usercount': results['usercount'],
                    'programcount': results['programcount'],
                    'instcount': results['instcount'],
                    'user_string': results['user_string'],
                    'prog_string': results['prog_string'],
                    'inst_string': results['inst_string']
                }
        else:
            context = {
                'title': title,
                'query': query,
                'users': None,
                'institutions': None,
                'programs': None,
                'usercount': 0,
                'programcount': 0,
                'instcount': 0,
                'user_string': '',
                'prog_string': '',
                'inst_string': ''
            }
    return render(request, 'spc/search_results.html', context)


@login_required
def search_users(request):
    query = request.GET.get('query')
    context = get_search_page_context(request, 'users', query)
    context['title'] = 'User search results'
    context['obj_type'] = 'users'
    return render(request, 'spc/specific_search.html', context)


def search_programs(request):
    query = request.GET.get('query')
    context = get_search_page_context(request, 'programs', query)
    context['title'] = 'Program search results'
    context['obj_type'] = 'programs'
    return render(request, 'spc/specific_search.html', context)


def search_institutions(request):
    query = request.GET.get('query')
    context = get_search_page_context(request, 'institutions', query)
    context['title'] = 'Institution search results'
    context['obj_type'] = 'institutions'
    return render(request, 'spc/specific_search.html', context)


def search_first_page(request, query):
    # search in multiple database tables
    if query is not None:
        users = None
        usercount = 0
        if request.user.is_authenticated:
            users = get_searchresult_object(request, query, 'users')
            usercount = len(users.values) if not users.empty or users is not None else 0
            users = users.head(5)

        institutions = pd.DataFrame.from_records(Institution.objects.filter(institution_name__contains=query).values())
        instcount = len(institutions) if not institutions.empty else 0
        institutions = institutions.head(5)
        institutions['programs'] = institutions.apply(lambda row: get_inst_progs(row['id']), axis=1)
        programs = get_searchresult_object(request, query, 'programs')
        programcount = len(programs.values)
        programs = programs.head(5) if not programs.empty else 0
        # prepare results for display
    else:
        users = None
        institutions = None
        programs = None
        usercount = 0
        programcount = 0
        instcount = 0
    results = {
        'users': users,
        'institutions': institutions,
        'programs': programs,
        'usercount': usercount,
        'programcount': programcount,
        'instcount': instcount,
        'user_string': get_grammar_number('users', usercount),
        'prog_string': get_grammar_number('programs', programcount),
        'inst_string': get_grammar_number('institutions', instcount)
    }
    return results


def get_search_page_context(request, search_obj, query):
    obj = None
    if search_obj == 'users':
        obj = get_searchresult_object(request, query, 'users')
    if search_obj == 'programs':
        obj = get_searchresult_object(request, query, 'programs')
    if search_obj == 'institutions':
        obj = pd.DataFrame.from_records(Institution.objects.filter(institution_name__contains=query).values())
        obj['programs'] = obj.apply(lambda row: get_inst_progs(row['id']), axis=1)
    obj_count = len(obj.values) if obj is not None else 0
    gram_number_string = get_grammar_number(search_obj, obj_count)
    page_number = int(request.GET.get('page')) \
        if int(request.GET.get('page')) <= obj_count // per_page + (obj_count % per_page > 0) \
        else obj_count // per_page + (obj_count % per_page > 0)
    pages = get_page_context(page_number, obj_count)
    first_obj = pages['page_prev'] * per_page
    page_objs = obj.iloc[first_obj:first_obj + per_page]

    context = {
        'query': query,
        'obj_count': obj_count,
        'page_number': page_number,
        'obj_string': gram_number_string,
        'obj': page_objs
    }
    for key, value in pages.items():
        context[key] = value
    return context


def get_searchresult_object(request, query, data):
    if data == 'users' and request.user.is_authenticated:
        users = get_user_model().objects.filter(Q(username__icontains=query) |
                                                Q(first_name__icontains=query) |
                                                Q(last_name__icontains=query))
        users_df = pd.DataFrame.from_records(users.values())
        users_df.index = users_df.index.astype(int)
        all_users = get_user_model().objects.all()
        # add text answer to the correct user, so similar users can be appended to exact users
        users_df['documents'] = users_df.apply(lambda row: append_textanswer_to_user(row['id'], all_users), axis=1)
        result = pd.DataFrame.from_records(all_users.values())
        # add text answer to the correct user, so it has the data that will be compared for similarity
        result['documents'] = result.apply(lambda row: append_textanswer_to_user(row['id'], all_users), axis=1)
        # create AISearch for users and calculate sorted similarity
        ai_search = AISearch(compare_data=result, query=query, language='en', stopwords='', mult=True)
        ai_search.calc_query_similarity()
        prepared_ai_docs = ai_search.compare_data.drop(['normalized', 'filtered', 'similarity'], axis=1)
        if not result.empty or not users_df.empty:
            return pd.concat([users_df, prepared_ai_docs]).drop_duplicates()
        else:
            return ai_search.compare_data

    if data == 'programs':
        # turn program queryset into pandas dataframe so the exact results and the similar results can be concatenated
        result = pd.DataFrame.from_records(Program.objects.filter(prog_name__icontains=query).values())
        all_programs = pd.DataFrame.from_records(Program.objects.all().values())
        # prepare data so the AISearch can work with it; has to contain 'documents' column
        all_programs.rename(columns={'prog_name': 'documents'}, inplace=True)
        # create AISearch for data and calculate similarity, which is automatically sorted by AISearch
        ai_search = AISearch(compare_data=all_programs, query=query, language='de', stopwords='programs', mult=False)
        ai_search.calc_query_similarity()
        # rename documents back into the database column name, so it can be handled in the template properly
        sorted_programs = ai_search.compare_data.rename(columns={'documents': 'prog_name'})
        # we only need the id and prog_name columns
        sorted_programs = sorted_programs[['id', 'prog_name']]

        if not result.empty:
            # append the similar results from AISearch to the exact results and remove duplicates
            return pd.concat([result, sorted_programs]).drop_duplicates()
        else:
            return sorted_programs

    return None


def append_textanswer_to_user(user_id, users):
    try:
        return users.get(id=user_id).userquestionnaire_set.first().textquestionanswer_set.get().answer
    except AttributeError:
        return ''


def get_grammar_number(search_type, count):
    if search_type == 'users':
        return 'users' if count > 1 or count == 0 else 'user'
    if search_type == 'programs':
        return 'programs' if count > 1 or count == 0 else 'program'
    if search_type == 'institutions':
        return 'institutions' if count > 1 or count == 0 else 'institution'


def get_page_context(page_number, count):
    return {'page_number': page_number,
            'page_prev': page_number - 1,
            'page_next': page_number + 1,
            'page_last': count // per_page + (count % per_page > 0)}


def get_inst_progs(inst_id):
    inst = Institution.objects.get(id=inst_id)
    return ", ".join(prog.prog_name for prog in inst.programs.all())
