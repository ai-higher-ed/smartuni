from sbm.utils.data import create_random_data, AnswerProjector, assign_random_feedback
from spc.models import SmartUser
from sbm.models import MatchSample
import pytest
import numpy as np
from sbm.utils.matching import get_matches, store_matches, store_samples, DefaultModel, MatchNet, ClassicalRegression, GeneticModel


def test_db():
    users = SmartUser.objects.none()
    try:
        users = create_random_data(10, .2)
        ap = AnswerProjector()
        ap.get_user_vecs(users.values_list('username', flat=True).distinct())
    except Exception as e:
        pytest.fail(e)
    finally:
        users.delete()


def test_matching(n_users=2):
    """
    Tests the matching algorithm for a pool of 10 users.
    :param n_users: number of users to test for as protagonists.
    """
    users = SmartUser.objects.none()

    # store samples because they are not user-related and won't be deleted just by deleting the users
    sample_ids = []

    try:
        users = create_random_data(10)

        protagonists = np.random.choice(users, size=n_users, replace=False)

        for protagonist in protagonists:
            print("Calculating matches for user: ", protagonist.username)

            print("Calculating matches with standard technique.")
            standard_matches = get_matches(protagonist.username, 5, 0, 0, model=DefaultModel())
            print(standard_matches)

            print("Calculating matches with ANN technique.")
            print(get_matches(protagonist.username, 5, 0, 0, model=MatchNet()))
            print("Calculating matches with genetic technique.")
            print(get_matches(protagonist.username, 5, 0, 0, model=GeneticModel()))
            print("Calculating matches with regression technique.")
            print(get_matches(protagonist.username, 5, 0, 0, model=ClassicalRegression()))

            db_matches = store_matches(protagonist, standard_matches)
            print(f"Found {db_matches.count()} matches.")

            fbs = assign_random_feedback(protagonist)
            print(f"Found {fbs.count()} feedbacks.")
            samples = store_samples(protagonist)
            sample_ids += list(samples.values_list('id', flat=True).distinct())
            print(f"Found {samples.count()} complete matching samples.")
            assert db_matches.count() == fbs.count()
            assert db_matches.count() == samples.count()

        fbs = assign_random_feedback(protagonist)
        print(f"Found {fbs.count()} feedbacks.")
        samples = store_samples(protagonist)
        print(f"Found {samples.count()} complete matching samples.")
        assert db_matches.count() == fbs.count()
        assert db_matches.count() == samples.count()

        print("Success!")
    except Exception as e:
        pytest.fail(str(e))
    finally:
        users.delete()
        MatchSample.objects.filter(id__in=sample_ids).delete()

