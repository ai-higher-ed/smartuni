from itertools import permutations

import numpy as np

from sbm.utils.matching import inst_similarity, get_lookup


from itertools import permutations

import numpy as np
import pandas as pd
import pytest

from sbm.utils.matching import inst_similarity, get_lookup, MatchNet, GeneticModel


# @pytest.fixture
# def ds():
#     # TODO: Score is between in {0, 0.25, 0.5, 0.75, 1} or {0, 0.2, 0.4, 0.6, 0.8, 1}
#     data_array = np.random.rand(100, 4)
#     data_array = np.concatenate([data_array, np.random.randint(1, size=(100, 4))], axis=1)
#     data_array = np.concatenate([data_array, np.random.randint(5, size=(100, 1)) / 4], axis=1)
#     data = pd.DataFrame(data_array,
#                         columns=['wtc', 'ls', 'inst', 'pai', 'pp_wtc', 'pp_ls', 'pp_inst', 'pp_pai', 'score'])
#     return data
#
#
# def test_model_call(ds):
#     # TODO: Insert Lin. Regression
#     models = [MatchNet(), GeneticModel()]
#     for model in models:
#         assert np.isfinite(model(ds))


def test_inst_similarity():

    def get_lookup_idx(user_a, user_b):
        """

        :param user_a: user_a's institute vector
        :param user_b: user_b's institute vector
        :return: index for test lookup table for corresponding match vector
        """
        big_array = user_a
        small_array = user_b
        if user_b.size > user_a.size:
            big_array = user_b
            small_array = user_a
        idx = big_array[:, None] == small_array
        default = np.zeros(3, dtype=np.int64)
        z = np.argwhere(idx)
        default[z[:, 0]] += 1 + z[:, 1]
        return default

    def get_test_users(elems_user):
        """

        :param elems_user: Vector of elements from which we create all permutations of length 3
        :return: An nparray that represents all possible combinations of length 3 one can get from elems_user
        """
        iterator = permutations(elems_user, 3)
        arr = np.array(list(iterator))
        return arr

    # Lookup table for institute similarity scores
    # 3 for each of user 1's entries which can each match with 1 to 3 or with none
    test_lookup = np.empty((4, 4, 4))
    test_lookup[:] = np.nan
    # 0 is no match, 1 is match with first, 2 is match with second, 3 is match with third

    match_order = [[[0, 0, 0]],
                   [[0, 0, 3]],
                   [[0, 0, 2], [0, 3, 0]],
                   [[0, 3, 2]],
                   [[0, 0, 1], [3, 0, 0]],
                   [[0, 2, 0]],
                   [[0, 2, 3]],
                   [[0, 3, 1], [3, 0, 2]],
                   [[3, 0, 1]],
                   [[2, 0, 0], [0, 1, 0]],
                   [[2, 0, 3], [0, 1, 3], [3, 2, 0], [0, 2, 1]],
                   [[0, 1, 2], [2, 3, 0]],
                   [[2, 0, 1], [3, 1, 0]],
                   [[3, 1, 2], [2, 3, 1], [3, 2, 1]],
                   [[2, 1, 0]],
                   [[2, 1, 3]],
                   [[1, 0, 0]],
                   [[1, 0, 3]],
                   [[1, 0, 2], [1, 3, 0]],
                   [[1, 3, 2]],
                   [[1, 2, 0]],
                   [[1, 2, 3]]
                   ]

    for i, match_keys in enumerate(match_order):
        for match_key in match_keys:
            test_lookup[match_key[0]][match_key[1]][match_key[2]] = i

    # Normalize values
    test_lookup = test_lookup / np.nanmax(test_lookup)

    # Define the values that user a and b can take. 4, 5, 6 and 7, 8, 9 are numbers that only show up for a and b
    # respectively to simulate non-matches.
    elems_a = np.array([1, 2, 3, 4, 5, 6])
    elems_b = np.array([1, 2, 3, 7, 8, 9])

    users_a = get_test_users(elems_a)
    users_b = get_test_users(elems_b)

    # combs collects all possible matchings between the defined permutations
    combs = []
    for user_a in users_a:
        for user_b in users_b:
            user_a = np.array(user_a)
            user_b = np.array(user_b)
            combs.append(get_lookup_idx(user_a, user_b))

    # Only keep one representative of each matching. With this, we have exactly all possible matching cases covered.
    uniq_combs = np.unique(combs, axis=0)
    # Check that our test lookup has as many non-nan entries as the number of unique matches.
    assert len(uniq_combs) == np.count_nonzero(np.isfinite(test_lookup))
    # Check that lookup to test has as many non-nan entries as the number of unique matches.
    assert len(uniq_combs) == np.count_nonzero(np.isfinite(get_lookup()))

    # Test all the cases where both user arrays are full
    for user_a in users_a:
        for user_b in users_b:
            # Test for correctness of score values
            test_idx = get_lookup_idx(user_a, user_b)
            # FAQ: For the implementation from 16.06.2022, this assertion is non-sensical as both the
            # lookup table to be tested and the test table are implemented in equal ways anyway. However,
            # it is kept in case the lookup table to be tested gets changed in the future.
            assert inst_similarity(user_a, user_b) == test_lookup[test_idx[0], test_idx[1], test_idx[2]]
            # Test for symmetry
            assert inst_similarity(user_a, user_b) == inst_similarity(user_b, user_a)

    comp_users_a = []
    incomp_users_a = []
    comp_users_b = []
    incomp_users_b = []

    a_dummy = 4
    b_dummy = 7

    # Test cases where at least one of the user arrays is of len < 3
    for user_a in users_a:
        # Only pick arrays where the last few are left out
        if np.all(user_a[1:] >= user_a[:-1]) and a_dummy in user_a:
            comp_users_a.append(user_a)
            incomp_users_a.append(user_a[user_a < a_dummy])

    for user_b in users_b:
        if np.all(user_b[1:] >= user_b[:-1]) and b_dummy in user_b:
            comp_users_b.append(user_b)
            incomp_users_b.append(user_b[user_b < b_dummy])

    for incomp_user_a, comp_user_a in zip(incomp_users_a, comp_users_a):
        for user_b in users_b:
            assert inst_similarity(incomp_user_a, user_b) == inst_similarity(comp_user_a, user_b)

    for incomp_user_b, comp_user_b in zip(incomp_users_b, comp_users_b):
        for user_a in users_a:
            assert inst_similarity(incomp_user_b, user_a) == inst_similarity(comp_user_b, user_a)
