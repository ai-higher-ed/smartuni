import django

import pytest

from sbm.utils.data import AnswerProjector

import tensorflow_datasets as tfds
import django.db.utils
import numpy as np
from sbm.models import *
from tqdm import tqdm
import math


def load_pai_text(n):
    # Only load the amount of data needed
    percentage = math.ceil((n * 100) / 5452)
    # contains 500 data-points, if more data-points are needed load split = 'train' that 5452 data-points
    ds = tfds.load('trec', split=f'train[:{percentage}%]', shuffle_files=True)
    return ds


@pytest.fixture(params=[{"test_first_names": ["test", "test", "test", "test"],
                         "test_last_names": ["_one", "_two", "_three", "_four"],
                         "course_enrollments": [[], [0], [0, 1], [0, 1, 2]]}])
def create_random_data(request):
    """
    Create random data for the database.
    :param n: number of users to create
    :param percent_course_enrollments: share of courses a user will be enrolled to
    :return: queryset of created users
    """

    first_names_pool = request.param["test_first_names"]
    last_names_pool = request.param["test_last_names"]
    course_enrollments = request.param["course_enrollments"]

    n = len(first_names_pool)

    # If somehow, the test users have survived, remove them
    for test_first_name, test_last_name in zip(first_names_pool, last_names_pool):
        SmartUser.objects.all().exclude(username=test_first_name + test_last_name).delete()

    ds = load_pai_text(n)



    courses = Course.objects.all()

    users = []

    for i, text in tqdm(zip(range(n), ds)):

        first_name = first_names_pool[i]
        last_name = last_names_pool[i]
        username = first_name[0].lower() + last_name.lower()

        try:
            user = SmartUser.objects.create(
                username=username,
                email=username + '@uos.com',
                first_name=first_name,
                last_name=last_name,
            )
        except django.db.utils.IntegrityError:
            username = first_name[0:2].lower() + last_name.lower()
            user = SmartUser.objects.create(
                username=username,
                email=username + '@uos.com',
                first_name=first_name,
                last_name=last_name,
            )

        users.append(user)

        # # PAI
        #
        # pai_q = Questionnaire.objects.get(title='Personal Academic Interests')
        # pai_uq = UserQuestionnaire.objects.create(questionnaire=pai_q, user=user)
        # TextQuestionAnswer.objects.create(
        #     question=pai_q.get_questions()[0],
        #     user_questionnaire=pai_uq,
        #     answer=text['text'].numpy().decode("utf-8")
        #
        # )
        #
        # # LS
        # ls_q = Questionnaire.objects.get(title='Learning Styles')
        # ls_uq = UserQuestionnaire.objects.create(questionnaire=ls_q, user=user)
        # ls_qa = MultipleChoiceQuestionAnswer.objects.create(
        #     question=ls_q.get_questions()[0],
        #     user_questionnaire=ls_uq
        # )
        # if np.random.rand() > .5:
        #     AnswerItem.objects.create(
        #         question_answer=ls_qa,
        #         answer_key=0
        #     )
        # if np.random.rand() > .5:
        #     AnswerItem.objects.create(
        #         question_answer=ls_qa,
        #         answer_key=1
        #     )
        # if np.random.rand() > .5:
        #     AnswerItem.objects.create(
        #         question_answer=ls_qa,
        #         answer_key=3
        #     )
        # if np.random.rand() > .5:
        #     AnswerItem.objects.create(
        #         question_answer=ls_qa,
        #         answer_key=4
        #     )
        #
        # # WTC
        # wtc_q = Questionnaire.objects.get(title='Willingness to Communicate')
        # wtc_uq = UserQuestionnaire.objects.create(questionnaire=wtc_q, user=user)
        # for question in wtc_q.get_questions():
        #     SingleChoiceQuestionAnswer.objects.create(
        #         user_questionnaire=wtc_uq,
        #         question=question,
        #         answer=int(np.random.rand() * 100)
        #     )

        # Course enrollments
        for j in course_enrollments[i]:
            print(courses[j])
            grade = (np.random.rand() * 5) if np.random.rand() > .5 else None
            CourseEnrollment.objects.get_or_create(
                user=user,
                course=courses[j],
                grade=grade
            )

        # # Partner Preferences
        # pp_q = Questionnaire.objects.get(title='Partner Preferences')
        # pp_uq = UserQuestionnaire.objects.create(questionnaire=pp_q, user=user)
        #
        # PartnerPreferencesAnswers.objects.create(
        #     user_questionnaire=pp_uq,
        #     institute=institutes[np.random.randint(institutes.count())].institution_name,
        #     course=courses[np.random.randint(courses.count())].title,
        #     # choose up to 4 random answers without replacement
        #     media=np.sort(np.random.choice(range(4), size=np.random.randint(5), replace=False)),
        #     # choose up to 5 random answers without replacement
        #     partner=np.sort(np.random.choice(range(5), size=np.random.randint(6), replace=False)),
        # )

    usernames = [user.username for user in users]
    users = SmartUser.objects.filter(username__in=usernames)
    expected_inst_lengths = dict(zip(usernames, [len(course_enrollment) for course_enrollment in course_enrollments]))

    # Teardown the artificially created user after a test run
    def delete_test_users():
        for test_username in usernames:
            SmartUser.objects.all().exclude(username=test_username).delete()

    request.addfinalizer(delete_test_users)

    return users, expected_inst_lengths


def test_get_top_n_institutes(create_random_data):
    ap = AnswerProjector()
    test_users, expected_inst_lengths = create_random_data
    # assert shape of institute vectors are as expected
    for i, test_user in enumerate(test_users):
        top_n_institutes = ap.get_top_n_institutes(test_user.username)
        assert top_n_institutes.shape == (expected_inst_lengths[test_user.username], )
