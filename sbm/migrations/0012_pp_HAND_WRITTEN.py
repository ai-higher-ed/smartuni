from django.db import migrations, models


def create_pp(apps, schema_editor):
    """
    In this method a PartnerPreferences Questionaire is created.
    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")
    TextQuestion = apps.get_model("sbm", "TextQuestion")

    # Create your questionnaire object.
    pp_questionnaire = Questionnaire.objects.create(
        title="Partner Preferences",
        description="The following questions will help us to better understand your need and preferences."
    )

    communication_options = {
        "0": "University Email/Element Chat/BigBlueButton",
        "1": "Cell Phone/Whatsapp",
        "2": "Telegram",
        "3": "Discord",
        "4": "Other"
    }

    MultipleChoiceQuestion.objects.create(
        title="Partner Preferences Question 1",
        question_text="Which of the following communication media do you prefer?",
        answer_options=communication_options,
        questionnaire=pp_questionnaire
    )

    partner_options = {
        "0": "A buddy who has same academic interests as me",
        "1": "A buddy who tends to initiate the communication",
        "2": "A buddy whose learning style is the same as mine",
        "3": "I don't have any preferences"
    }

    MultipleChoiceQuestion.objects.create(
        title="Partner Preferences Question 2",
        question_text="Please specify your partner preferences (if any):",
        answer_options=partner_options,
        questionnaire=pp_questionnaire
    )

    TextQuestion.objects.create(
        title="Institute preference",
        question_text="In which institute should your buddy be enrolled?",
        questionnaire=pp_questionnaire
    )

    TextQuestion.objects.create(
        title="Course preference",
        question_text="In which course do you need help in?",
        questionnaire=pp_questionnaire
    )


def undo_create_pp(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.
    Questionnaire.objects.filter(title="Partner Preferences").delete()


class Migration(migrations.Migration):

    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0011_institute_partnerpreferencesanswers'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(create_pp, reverse_code=undo_create_pp),
    ]
