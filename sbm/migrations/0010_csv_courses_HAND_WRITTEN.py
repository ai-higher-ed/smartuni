from django.db import migrations
import csv

"""
in this file we want to read out the csv file and create a course object for every single course with the respective institute etc
"""


def read_csv(apps, schema_editor):
    Course = apps.get_model("sbm", "Course")
    # open csv file and create course for each line
    list_title = []

    with open("sbm/static/sbm/courses_21_22.csv", 'r', encoding='utf-8') as course_file_22_21:
        reader = csv.reader(course_file_22_21, delimiter=',')

        for row in reader:
            if row[0] != "semester":

                if row[3] not in list_title:
                    Course.objects.update_or_create(
                        title=row[3],
                        defaults={
                            'course_number': row[1],
                            'course_type': row[2],
                            'institute': row[4]
                        }
                    )
                    list_title.append(row[3])


class Migration(migrations.Migration):
    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0009_course_course_number_course_course_type_and_more'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(read_csv, reverse_code=migrations.RunPython.noop),
    ]
