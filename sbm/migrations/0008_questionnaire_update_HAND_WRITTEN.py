from django.db import migrations


def des_pai(apps, schema_editor):
    TextQuestion = apps.get_model("sbm", "TextQuestion")
    text_ques=TextQuestion.objects.filter(title='Personal Interest')
    text_ques.question_text = "Please enter your personal academic interest below"


def change_wtc_descriptions(apps, schema_editor):
    SingleChoiceQuestion = apps.get_model("sbm", "SingleChoiceQuestion")

    questions = SingleChoiceQuestion.objects.filter(
        title="Talk with a friend while standing in line",
        question_text="Talk with a stranger while standing in line"
    )
    for question in questions:
        question.question_text = "Talk with a friend while standing in line"
        question.save()

    questions = SingleChoiceQuestion.objects.filter(
        title="Talk with an acquaintance while standing in line",
        question_text="Talk with an aquaitance while standing in line"
    )
    for question in questions:
        question.question_text = "Talk with an acquaintance while standing in line"
        question.save()

    questions = SingleChoiceQuestion.objects.filter(
        title="Talk in a large meeting of friends",
        question_text="Talk in a large meeting with friends"
    )
    for question in questions:
        question.question_text = "Talk in a large meeting of friends"
        question.save()

    questions = SingleChoiceQuestion.objects.filter(
        title="Talk in a large meeting of friends",
        question_text="Talk in a large meeting with friends"
    )
    for question in questions:
        question.question_text = "Talk in a large meeting of friends"
        question.save()


class Migration(migrations.Migration):

    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0007_course_courseenrollment'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(des_pai, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(change_wtc_descriptions, reverse_code=migrations.RunPython.noop),
    ]
