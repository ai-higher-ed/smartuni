# Generated by Django 4.0.5 on 2022-07-07 09:40

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import picklefield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('spc', '0003_alter_institution_programs_and_more'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sbm', '0014_inst_foreignkey_HAND_WRITTEN'),
    ]

    operations = [
        migrations.CreateModel(
            name='MatchSample',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('wtc', models.DecimalField(decimal_places=4, max_digits=5, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('ls', models.DecimalField(decimal_places=4, max_digits=5, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('inst', models.DecimalField(decimal_places=4, max_digits=5, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('pai', models.DecimalField(decimal_places=4, max_digits=5, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('wtc_pp', models.BooleanField(default=False)),
                ('ls_pp', models.BooleanField(default=False)),
                ('inst_pp', models.BooleanField(default=False)),
                ('pai_pp', models.BooleanField(default=False)),
                ('user_score', models.DecimalField(decimal_places=4, max_digits=5, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)])),
                ('identifier', models.CharField(max_length=32, unique=True)),
                ('mkdate', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AddField(
            model_name='answeritem',
            name='chdate',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='answeritem',
            name='mkdate',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='multiplechoicequestionanswer',
            name='chdate',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='multiplechoicequestionanswer',
            name='mkdate',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='partnerpreferencesanswers',
            name='other_media',
            field=models.CharField(blank=True, default=None, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='questionnaireresult',
            name='chdate',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='questionnaireresult',
            name='mkdate',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='singlechoicequestionanswer',
            name='chdate',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='singlechoicequestionanswer',
            name='mkdate',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='textquestionanswer',
            name='chdate',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='textquestionanswer',
            name='mkdate',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='partnerpreferencesanswers',
            name='course',
            field=models.CharField(blank=True, default=None, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='partnerpreferencesanswers',
            name='institute',
            field=models.CharField(blank=True, default=None, max_length=200, null=True),
        ),
        migrations.CreateModel(
            name='UserMatchingStatus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('ac', 'active'), ('ps', 'passive'), ('ia', 'inactive'),('ur', ' unready')], default='ur', max_length=2)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='StudyBuddy',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.IntegerField(default=0)),
                ('pinned', models.BooleanField(default=False)),
                ('match', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='match', to=settings.AUTH_USER_MODEL)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('user', 'match')},
            },
        ),
        migrations.CreateModel(
            name='NLPObject',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nlp_obj', picklefield.fields.PickledObjectField(editable=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='FeedbackSBM',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rating', models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)])),
                ('studybuddy', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='sbm.studybuddy')),
            ],
        ),
        migrations.CreateModel(
            name='WTCSimilarity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.DecimalField(decimal_places=4, max_digits=5)),
                ('mkdate', models.DateTimeField(auto_now_add=True)),
                ('chdate', models.DateTimeField(auto_now=True)),
                ('user_a', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='wtc_similarity_a', to=settings.AUTH_USER_MODEL)),
                ('user_b', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='wtc_similarity_b', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'unique_together': {('user_a', 'user_b')},
            },
        ),
        migrations.CreateModel(
            name='PAISimilarity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.DecimalField(decimal_places=4, max_digits=5)),
                ('mkdate', models.DateTimeField(auto_now_add=True)),
                ('chdate', models.DateTimeField(auto_now=True)),
                ('user_a', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pai_similarity_a', to=settings.AUTH_USER_MODEL)),
                ('user_b', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='pai_similarity_b', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'unique_together': {('user_a', 'user_b')},
            },
        ),
        migrations.CreateModel(
            name='LSSimilarity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.DecimalField(decimal_places=4, max_digits=5)),
                ('mkdate', models.DateTimeField(auto_now_add=True)),
                ('chdate', models.DateTimeField(auto_now=True)),
                ('user_a', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ls_similarity_a', to=settings.AUTH_USER_MODEL)),
                ('user_b', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ls_similarity_b', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'unique_together': {('user_a', 'user_b')},
            },
        ),
        migrations.CreateModel(
            name='INSTSimilarity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('similarity', models.DecimalField(decimal_places=4, max_digits=5)),
                ('mkdate', models.DateTimeField(auto_now_add=True)),
                ('chdate', models.DateTimeField(auto_now=True)),
                ('user_a', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ints_similarity_a', to=settings.AUTH_USER_MODEL)),
                ('user_b', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ints_similarity_b', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
                'unique_together': {('user_a', 'user_b')},
            },
        ),
    ]
