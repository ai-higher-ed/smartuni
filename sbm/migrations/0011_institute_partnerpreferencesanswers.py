# Generated by Django 4.0.2 on 2022-04-14 14:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sbm', '0010_csv_courses_HAND_WRITTEN'),
    ]

    operations = [
        migrations.CreateModel(
            name='Institute',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inst', models.CharField(default=None, max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='PartnerPreferencesAnswers',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('media', models.CharField(default=None, max_length=200)),
                ('partner', models.CharField(default=None, max_length=200)),
                ('institute', models.CharField(default=None, max_length=200)),
                ('course', models.CharField(default=None, max_length=200)),
                ('user_questionnaire', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, to='sbm.userquestionnaire')),
            ],
        ),
    ]
