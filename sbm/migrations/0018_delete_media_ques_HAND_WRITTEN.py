from django.db import migrations, models


def delete_media(apps, schema_editor):
    """
    In this method a PartnerPreferences Questionaire is created.
    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")

    MultipleChoiceQuestion.objects.get(title="Partner Preferences Question 1").delete()


def undo_delete_media(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.
    pp_questionnaire = Questionnaire.objects.filter(title="Partner Preferences")

    communication_options = {
        "0": "University Email/Element Chat/BigBlueButton",
        "1": "Cell Phone/Whatsapp",
        "2": "Telegram",
        "3": "Discord",
        "4": "Other"
    }

    MultipleChoiceQuestion.objects.create(
        title="Partner Preferences Question 1",
        question_text="Which of the following communication media do you prefer?",
        answer_options=communication_options,
        questionnaire=pp_questionnaire
    )

    
class Migration(migrations.Migration):

    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0017_alter_feedbacksbm_studybuddy'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(delete_media, reverse_code=undo_delete_media),
    ]
