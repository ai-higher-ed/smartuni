from django.db import migrations


def create_ls(apps, schema_editor):
    """
    In this method you define what to do. It will be run by the migration class.
    If you just want to perform structural changes to the database, django.migrations offers various
    pre-defined methods.
    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")

    # Create your questionnaire object.
    # This might through certain errors if e.g. the title is already in use.
    ls_questionnaire = Questionnaire.objects.create(
        title="Learning Styles",
        description="<div class=\"container-ls\">"
                        "<div class=\"icon clearfix\"><img src=\"/static/sbm/images/eye.png\" alt=\"Visual Learner\" style=\"height:80px; width:80px\"></div>"
                        "<div class=\"learning-style\"><h3 class=\"learner-type\">Visual Learner</h3>"
                        "<h4 class=\"learner-description\">Learns by seeing and observing things, including pictures, diagrams, written directions and more.</h4></div>"
                    "</div>"
                    "<div class=\"container-ls\">"
                        "<div class=\"icon clearfix\"><img src=\"/static/sbm/images/ear.png\" alt=\"Auditory Learner\" style=\"height:80px; width:80px\"></div>"
                        "<div class=\"learning-style\"><h3 class=\"learner-type\">Auditory Learner</h3>"
                        "<h4 class=\"learner-description\">Learns by reinforcing the subject matter by sound; would rather listen to a lecture than read written notes, and use your own voices to reinforce new concepts and ideas.</h4></div>"
                    "</div>"
                    "<div class=\"container-ls\">"
                       "<div class=\"icon clearfix\"><img src=\"/static/sbm/images/reading-book.png\" alt=\"Reading/writing Learner\" style=\"height:80px; width:80px\"></div>"
                        "<div class=\"learning-style\"><h3 class=\"learner-type\">Reading/writing Learner</h3>"
                        "<h4 class=\"learner-description\">Learns through written words; while there is some overlap with visual learning, these types of learners are drawn to expression through writing, reading articles or books, writing in diaries, looking up words in the dictionary and searching the internet for just about everything.</h4></div>"
                    "</div>"
                    "<div class=\"container-ls\">"
                        "<div class=\"icon clearfix\"><img src=\"/static/sbm/images/run.png\" alt=\"Kinesthetic Learner\" style=\"height:80px; width:80px\"></div>"
                        "<div class=\"learning-style\"><h3 class=\"learner-type\">Kinesthetic Learner</h3>"
                        "<h4 class=\"learner-description\">Learns through experiencing or doing things,  like to get involved by acting out events or using hands to touch and handle in order to understand concepts. These types of learners might struggle to sit still and often excel at sports or like to dance. They may need to take more frequent breaks when studying.</h4></div>"
                    "</div>"
    )

    learning_styles_options = {
        "0": "Visual learner",
        "1": "Auditory learner",
        "3": "Kinesthetic learner",
        "4": "Reading/writing learner"
    }

    MultipleChoiceQuestion.objects.create(
        title="Learning Styles question 1",
        question_text="What's your learning style?",
        answer_options=learning_styles_options,
        questionnaire=ls_questionnaire
    )


def undo_create_ls(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.
    Questionnaire.objects.filter(title="Learning Styles").delete()


class Migration(migrations.Migration):
    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0005_auto_20220301_1044'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(create_ls, reverse_code=undo_create_ls),
    ]
