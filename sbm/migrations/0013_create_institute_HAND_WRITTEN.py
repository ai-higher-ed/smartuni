from django.db import migrations
import csv

"""
in this file we want to read out the csv file and create a course object for every single course with 
the respective institute etc
"""


def get_institute(apps, schema_editor):
    Institute = apps.get_model("sbm", "Institute")
    # open csv file and create course for each line
    list_title = []

    with open("sbm/static/sbm/courses_21_22.csv", 'r', encoding='utf-8') as course_file_22_21:
        reader = csv.reader(course_file_22_21, delimiter=',')

        for row in reader:
            if row[0] != "semester":

                if row[4] not in list_title:
                    Institute.objects.create(

                        inst=row[4],

                    )
                    list_title.append(row[4])


def undo_get_institute(apps, schema_editor):
    Institute = apps.get_model("sbm", "Institute")
    institutes = Institute.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0012_pp_HAND_WRITTEN'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(get_institute, reverse_code=undo_get_institute),
    ]
