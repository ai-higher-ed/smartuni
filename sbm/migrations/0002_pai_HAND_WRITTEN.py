from django.db import migrations


"""
This is an example of how to create a questionnaire.
After this file has been created, execute `python manage.py migrate` and the migration will be applied.
The file  has to be named like that: 
`<4 digit ID with leading zeros following the last ID incrementally>_whatever_you_want.py`.
"""


def create_pai(apps, schema_editor):
    """
    In this method you define what to do. It will be run by the migration class.
    If you just want to perform structural changes to the database, django.migrations offers various
    pre-defined methods.
    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    TextQuestion = apps.get_model("sbm", "TextQuestion")

    # Create your questionnaire object.
    # This might through certain errors if e.g. the title is already in use.
    example_questionnaire = Questionnaire.objects.create(
        title="Personal Academic Interests",
        description="Here you can enter your personal academic interests.",
    )

    TextQuestion.objects.create(
        title="Personal interest",
        question_text="Please enter your personal academic interest below",
        questionnaire=example_questionnaire
    )


def undo_create_pai(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.
    Questionnaire.objects.filter(title="Personal Academic Interests").delete()


class Migration(migrations.Migration):

    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0001_initial'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(create_pai, reverse_code=undo_create_pai),
    ]
