from django.db import migrations

from sbm.models import SingleChoiceQuestion

import json


def create_wtc(apps, schema_editor):
    """
    In this method a Willingness to Communicate Questionnaire will be created based on:
    https://www.midss.org/sites/default/files/willingness_to_communicate.pdf

    1) create Questionnaire with title and description
    2) create 20 Questions
    3) create the Answers


    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    SingleChoiceQuestion = apps.get_model("sbm", "SingleChoiceQuestion")

    # Create questionnaire
    wtc_questionnaire = Questionnaire.objects.create(
        title="Willingness to Communicate",
        #citate of the webpage! Do we have to quote here?
        description="Below are 20 situations in which a person might choose to communicate or not to communicate. Presume you have completely free choice. Indicate the percentage of times you would choose to communicate in each type of situation. Indicate in the space at the left of the item what percent of the time you would choose to communicate. (0 = Never to 100 = Always)"
    )

    answers = json.loads(json.dumps(dict(enumerate(range(100)))))

    # create the questions
    wtc_question1 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a service station attendant",
        question_text="Talk with a service station attendant",
        answer_options=answers)

    wtc_question2 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk to a physician",
        question_text="Talk to a physician",
        answer_options=answers)

    wtc_question3 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Present a talk to a group of strangers",
        question_text="Present a talk to a group of strangers",
        answer_options=answers)

    wtc_question4 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with an acquaintance while standing in line",
        question_text="Talk with an aquaitance while standing in line",
        answer_options=answers)

    wtc_question5 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a salesperson in a store",
        question_text="Talk with a salesperson in a store",
        answer_options=answers)

    wtc_question6 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a large meeting of friends",
        question_text="Talk in a large meeting with friends",
        answer_options=answers)

    wtc_question7 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a police officer",
        question_text="Talk with a police officer",
        answer_options=answers)

    wtc_question8 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a small group of strangers",
        question_text="Talk in a small group of strangers",
        answer_options=answers)

    wtc_question9 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a friend while standing in line",
        question_text="Talk with a stranger while standing in line",
        answer_options=answers)

    wtc_question10 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a waiter/waitress in a restaurant",
        question_text="Talk with a waiter/waitress in a restaurant",
        answer_options=answers)

    wtc_question11 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a large meeting of acquaintances",
        question_text="Talk in a large meeting of acquaintances",
        answer_options=answers)

    wtc_question12 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a stranger while standing in line",
        question_text="Talk with a stranger while standing in line",
        answer_options=answers)

    wtc_question13 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a secretary",
        question_text="Talk with a secretary",
        answer_options=answers)

    wtc_question14 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Present a talk to a group of friends",
        question_text="Present a talk to a group of friends",
        answer_options=answers)

    wtc_question15 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a small group of acquaintances",
        question_text="Talk in a small group of acquaintances",
        answer_options=answers)

    wtc_question16 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a garbage collector",
        question_text="Talk with a garbage collector",
        answer_options=answers)

    wtc_question17 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a large meeting of strangers",
        question_text="Talk in a large meeting of strangers",
        answer_options=answers)

    wtc_question18 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk with a spouse (or girl/boyfriend)",
        question_text="Talk with a spouse (or girl/boyfriend)",
        answer_options=answers)

    wtc_question19 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Talk in a small group of friends",
        question_text="Talk in a small group of friends",
        answer_options=answers)

    wtc_question20 = SingleChoiceQuestion.objects.create(
        questionnaire=wtc_questionnaire,
        title="Present a talk to a group of acquaintances",
        question_text="Present a talk to a group of acquaintances",
        answer_options=answers)


def undo_create_wtc(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.
    Questionnaire.objects.filter(title="Willingness to Communicate").delete()


class Migration(migrations.Migration):
    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0002_pai_HAND_WRITTEN'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(create_wtc, reverse_code=undo_create_wtc),
    ]
