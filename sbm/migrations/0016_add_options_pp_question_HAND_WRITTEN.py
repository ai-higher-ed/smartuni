from django.db import migrations, models


def modify_pp(apps, schema_editor):
    """
    In this method a PartnerPreferences Questionaire is created.
    """
    # Get your model classes on that way, because you can't simply import form your apps here.
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")

    # filter the pp questionnaire
    pp_questionnaire = Questionnaire.objects.get(
        title="Partner Preferences"
    )

    MultipleChoiceQuestion.objects.filter(
        title="Partner Preferences Question 2").delete()

    partner_options = {
        "0": "A buddy who has same academic interests as me",
        "1": "A buddy who tends to initiate the communication",
        "2": "A buddy whose learning style is the same as mine",
        "3": "A buddy whose course history is similar to mine",
        "4": "I don't have any preferences"
    }

    MultipleChoiceQuestion.objects.create(
        title="Partner Preferences Question 2",
        question_text="Please specify your partner preferences (if any):",
        answer_options=partner_options,
        questionnaire=pp_questionnaire,
    )


def undo_modify_pp(apps, schema_editor):
    """
    This function should always be implemented so that we can migrate back and forth as we want.
    """
    Questionnaire = apps.get_model("sbm", "Questionnaire")
    MultipleChoiceQuestion = apps.get_model("sbm", "MultipleChoiceQuestion")

    # Here we just delete the questionnaire we created.
    # Because of the argument `on_delete=models.CASCADE` in the ForeignKey attribute of Question,
    # all related questions will be deleted as well.

    pp_questionnaire = Questionnaire.objects.get(
        title="Partner Preferences"
    )

    MultipleChoiceQuestion.objects.filter(
        title="Partner Preferences Question 2").delete()

    partner_options = {
        "0": "A buddy who has same academic interests as me",
        "1": "A buddy who tends to initiate the communication",
        "2": "A buddy whose learning style is the same as mine",
        "3": "I don't have any preferences",

    }
    MultipleChoiceQuestion.objects.create(
        title="Partner Preferences Question 2",
        question_text="Please specify your partner preferences (if any):",
        answer_options=partner_options,
        questionnaire=pp_questionnaire
    )


class Migration(migrations.Migration):

    dependencies = [
        # Add the previous migration here.
        # Dependencies are a tuple: (app_name, migration_name).
        ('sbm', '0015_matchsample_answeritem_chdate_answeritem_mkdate_and_more'),
    ]

    operations = [
        # Here you define the schedule of your migrations.
        # When you have written custom methods like we did, we need migrations.RunPython.
        # There you have to add your forward method and the backward method.
        migrations.RunPython(modify_pp, reverse_code=undo_modify_pp),
    ]
