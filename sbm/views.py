import datetime
from threading import Thread

from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.urls import reverse
from functools import wraps
from django.http import JsonResponse

from sbm.forms import *
from sbm.responses import *
from sbm.utils.matching import *
from sbm.models import StudyBuddy
from spc.notifications import CoreNotificationHelper

GLOBAL_CONTEXT = {
    'name_course_submission': 'Courses and Grades',
    'make_ready' : False
}

SEPARATOR = ", "
# points to current best model
CURRENT_MODEL = DefaultModel()
# Dummy time
last_update = datetime.datetime(2022, 1, 1, tzinfo=datetime.timezone.utc)
# Says whether thread for perform_matching already exists or not
match_thread_exists = False


class MatchHTMLWrapper:
    """
    Wrapper class that allows StudyBuddy instances to be embedded into HTML templates.
    """

    def __init__(self, buddy: StudyBuddy):
        self.pinned = buddy.pinned
        self.pic = buddy.match.pic
        self.username = buddy.match.username
        self.first_name = buddy.match.first_name
        self.last_name = buddy.match.last_name
        buddy_institutions = buddy.match.institutions.all()
        buddy_programs = buddy.match.programs.all()

        progs_and_insts = "".join([SEPARATOR + str(prog) + " at " + str(inst)
                                   for (prog, inst) in zip(buddy_programs, buddy_institutions)])
        self.progs_and_insts = progs_and_insts[len(SEPARATOR):]
        if not progs_and_insts:
            self.progs_and_insts = "No program / institution specified"

        self.score_percent = "{0:.0%}".format(buddy.score)
        self.email = buddy.match.email
        self.pk = buddy.match.pk
        self.feedback = None
        if hasattr(buddy, 'feedback'):
            self.feedback = buddy.feedback.rating


def check_seen_questionnaires(func):
    @wraps(func)
    def wrap(request, *args, **kwargs):
        if 'pai_seen' not in GLOBAL_CONTEXT.keys() or not GLOBAL_CONTEXT['pai_seen']:
            GLOBAL_CONTEXT['pai_seen'] = UserQuestionnaire.objects.filter(
                user=request.user, questionnaire__title='Personal Academic Interests'
            ).exists()

        if 'ls_seen' not in GLOBAL_CONTEXT.keys() or not GLOBAL_CONTEXT['ls_seen']:
            GLOBAL_CONTEXT['ls_seen'] = UserQuestionnaire.objects.filter(
                user=request.user, questionnaire__title='Learning Styles'
            ).exists()

        if 'wtc_seen' not in GLOBAL_CONTEXT.keys() or not GLOBAL_CONTEXT['wtc_seen']:
            GLOBAL_CONTEXT['wtc_seen'] = UserQuestionnaire.objects.filter(
                user=request.user, questionnaire__title='Willingness to Communicate'
            ).exists()

        if 'pp_seen' not in GLOBAL_CONTEXT.keys() or not GLOBAL_CONTEXT['pp_seen']:
            GLOBAL_CONTEXT['pp_seen'] = UserQuestionnaire.objects.filter(
                user=request.user, questionnaire__title='Partner Preferences'
            ).exists()

        if 'matching_seen' not in GLOBAL_CONTEXT.keys() or not GLOBAL_CONTEXT['matching_seen']:
            GLOBAL_CONTEXT['matching_seen'] = StudyBuddy.objects.filter(user=request.user
                                                                        ).exists()

        return func(request, *args, **kwargs)

    return wrap


def check_ready_for_matching(func):
    @wraps(func)
    def wrap(request, *args, **kwargs):
        if UserMatchingStatus.objects.get(user=request.user).status == UserMatchingStatus.UNREADY:
            # redirect
            messages.error(request, 'You have to answer all questionnaires before entering this page.')
            return redirect('sbm_startpage')
        else:
            return func(request, *args, **kwargs)

    return wrap



@login_required
@check_seen_questionnaires
def sbm_startpage(request):
    page = 'sbm_startpage'  # for highlighting current page

    # create matching status object, by default if new one created 'not_ready'
    ums, ums_created = UserMatchingStatus.objects.get_or_create(user=request.user)
    context = {
        'matchingstatus': ums.status
    }
    context.update(GLOBAL_CONTEXT)

    if ums.status != 'unready':
        return redirect('sbm_matching')


    return render(request, 'sbm/sbm_startpage.html', context=GLOBAL_CONTEXT)


@login_required
@check_seen_questionnaires
def pai(request):
    # get general objects
    try:
        questionnaire = Questionnaire.objects.get(title='Personal Academic Interests')
        question = TextQuestion.objects.get(questionnaire=questionnaire)
    except (Questionnaire.DoesNotExist, TextQuestion.DoesNotExist):
        messages.error(request, 'Could not find Questionnaire or Question.')
        return render(request, 'sbm/questionnaire_not_defined.html', context={'name_personal': 'Personal Interests'})

    # get or create user-specific objects
    uq, uq_created = UserQuestionnaire.objects.get_or_create(
        user=request.user,
        questionnaire=questionnaire
    )
    try:
        qa = TextQuestionAnswer.objects.get(
            user_questionnaire=uq,
            question=question,
        )
        GLOBAL_CONTEXT['pai_completed'] = True
    except TextQuestionAnswer.DoesNotExist:
        qa = None
        GLOBAL_CONTEXT['pai_completed'] = False
    title = questionnaire.title

    result = qa is not None

    if request.method == 'POST':

        qa, qa_created = TextQuestionAnswer.objects.get_or_create(
            user_questionnaire=uq,
            question=question,
        )

        form = TextQuestionAnswerForm(request.POST, user=request.user)
        if form.is_valid():
            qa.answer = form.cleaned_data['answer']
            qa.save()

            # if the user is re-taking the questionnaire, delete all existing related similarity scores
            PAISimilarity.objects.filter(Q(user_a=request.user) | Q(user_b=request.user)).delete()

          #  messages.success(request, 'Your answer has been saved.')
            GLOBAL_CONTEXT['pai_completed'] = True
            if GLOBAL_CONTEXT['make_ready']:
                GLOBAL_CONTEXT['make_ready']=False
                CoreNotificationHelper.send_notifications(sender="SBM", recipient=request.user, setting_name="SBM_READYTOMATCH", message="Congratulations! As you filled out all questionnaires, you can now enter the matching process and finally find your study buddy.", url='/sbm/matching_page')
                return redirect('sbm_matching')
            else:
                return HttpResponseRedirect(reverse('sbm_wtc'))


    else:
        form = TextQuestionAnswerForm(initial={'answer': qa.answer if qa else ''})

    usm = UserMatchingStatus.objects.get(user=request.user).status

    context = {
        'form': form,
        'question': question,
        'name_personal': 'Personal Interests',
        'answer': result,
        'title': title,
        'usm' : usm,
    }

    context.update(GLOBAL_CONTEXT)

    return render(request, 'sbm/single_question_questionnaire.html', context=context)


@login_required
@check_seen_questionnaires
def ls(request):
    # get general objects
    try:
        questionnaire = Questionnaire.objects.get(title='Learning Styles')
        question = MultipleChoiceQuestion.objects.get(questionnaire=questionnaire)
    except (Questionnaire.DoesNotExist, MultipleChoiceQuestion.DoesNotExist):
        messages.error(request, 'Could not find Questionnaire or Question.')
        return render(request, 'sbm/questionnaire_not_defined.html', context={})

    uq, uq_created = UserQuestionnaire.objects.get_or_create(
        user=request.user,
        questionnaire=questionnaire
    )

    try:
        qa = MultipleChoiceQuestionAnswer.objects.get(
            user_questionnaire=uq,
            question=question
        )
    except MultipleChoiceQuestionAnswer.DoesNotExist:
        qa = None

    result = qa is not None
    if result:
        GLOBAL_CONTEXT['ls_completed'] = True

    title = questionnaire.title

    if request.method == 'POST':

        form = LearningStylesForm(request.POST)

        qa, qa_created = MultipleChoiceQuestionAnswer.objects.get_or_create(
            user_questionnaire=uq,
            question=question
        )

        if not qa_created:
            # if the user is re-taking the questionnaire, delete all existing related similarity scores
            LSSimilarity.objects.filter(Q(user_a=request.user) | Q(user_b=request.user)).delete()

        if form.is_valid():
            answers_keys = form.cleaned_data['answers']
            if not qa_created:
                # if it's a re-submission, delete answers which were not given again
                AnswerItem.objects.filter(question_answer=qa).exclude(answer_key__in=answers_keys).delete()
            # Create new answer item objects
            for answer_key in answers_keys:
                AnswerItem.objects.get_or_create(
                    question_answer=qa,
                    answer_key=answer_key
                )

          #  messages.success(request, 'Your answer has been saved.')
            GLOBAL_CONTEXT['ls_completed'] = True

            if GLOBAL_CONTEXT['make_ready']:
                GLOBAL_CONTEXT['make_ready']=False
                CoreNotificationHelper.send_notifications(sender="SBM", recipient=request.user, setting_name="SBM_READYTOMATCH", message="Congratulations! As you filled out all questionnaires, you can now enter the matching process and finally find your study buddy.", url='/sbm/matching_page')
                return redirect('sbm_matching')
            else:
                return HttpResponseRedirect(reverse('sbm_course_submission'))

    else:
        form = LearningStylesForm(initial={'answers': qa.get_answer_keys() if qa else []})

    usm = UserMatchingStatus.objects.get(user=request.user).status

    context = {
        'form': form,
        'question': question,
        'name_style': 'Learning Styles',
        'name_w': 'Willingness to Communicate',
        'name_personal': 'Personal Interests',
        'answer': result,
        'qa': qa,
        'title': title,
        'usm' : usm,
    }

    context.update(GLOBAL_CONTEXT)

    return render(request, 'sbm/single_question_questionnaire.html', context=context)


@login_required
def delete_answers_text(request, question_id):
    question = TextQuestion.objects.get(pk=question_id)
    uq = UserQuestionnaire.objects.get(questionnaire=question.questionnaire, user=request.user)
    answer = TextQuestionAnswer.objects.filter(user_questionnaire=uq, question=question).delete()
    messages.success(request, 'Your answer has been deleted')
    GLOBAL_CONTEXT['pai_completed'] = False
    usm = UserMatchingStatus.objects.get(user=request.user).delete()
    UserMatchingStatus.objects.create(user=request.user, status='unready')
    StudyBuddy.objects.filter(user=request.user, pinned=False).delete()
    return HttpResponseRedirect(reverse('sbm_pai'))


@login_required
def delete_answers_ls(request, question_id):
    question = MultipleChoiceQuestion.objects.get(pk=question_id)
    uq = UserQuestionnaire.objects.get(questionnaire=question.questionnaire, user=request.user)
    answer = MultipleChoiceQuestionAnswer.objects.filter(user_questionnaire=uq, question=question).delete()
    messages.success(request, 'Your answer has been deleted')
    GLOBAL_CONTEXT['ls_completed'] = False
    usm = UserMatchingStatus.objects.get(user=request.user).delete()
    UserMatchingStatus.objects.create(user=request.user, status='unready')
    StudyBuddy.objects.filter(user=request.user, pinned=False).delete()
    return HttpResponseRedirect(reverse('sbm_ls'))


@login_required
@check_seen_questionnaires
def wtc(request):
    try:
        questionnaire = Questionnaire.objects.get(title='Willingness to Communicate')
        questions = SingleChoiceQuestion.objects.filter(questionnaire=questionnaire)
    except (Questionnaire.DoesNotExist, TextQuestion.DoesNotExist):
        messages.error(request, 'Could not find Questionnaire or Question.')
        return render(request, 'sbm/questionnaire_not_defined.html', context={})

    # get or create user-specific objects
    uq, uq_created = UserQuestionnaire.objects.get_or_create(
        user=request.user,
        questionnaire=questionnaire
    )

    # get description of the questionnaire
    description = questionnaire.description
    result = QuestionnaireResult.objects.filter(user_questionnaire=uq)
    sin_an = []
    result = result.exists()

    zip_list = []
    sin_question = []
    if result:
        GLOBAL_CONTEXT['wtc_completed'] = True
        single_answers = SingleChoiceQuestionAnswer.objects.filter(user_questionnaire=uq)
        single_answers = list(single_answers)
        for sin in single_answers:
            sin_an.append(int(sin.answer))
        for sin1 in questions:
            sin_question.append(sin1.title)
        zip_list = zip(sin_question, sin_an)

    else:
        single_answers = None
        for sin1 in questions:
            sin_question.append(sin1.title)

    if request.method == 'POST':

        answer = request.POST

        if 'delete' in answer:
            result = QuestionnaireResult.objects.filter(user_questionnaire=uq).delete()
            messages.success(request, 'Your answer has been deleted')
            GLOBAL_CONTEXT['wtc_completed'] = False
            usm = UserMatchingStatus.objects.get(user=request.user).delete()
            UserMatchingStatus.objects.create(user=request.user, status='unready')
            StudyBuddy.objects.filter(user=request.user, pinned=False).delete()
            return HttpResponseRedirect(reverse('sbm_wtc'))

        elif 'submit' in answer:

          #  messages.success(request, 'Your answer has been saved.')
            GLOBAL_CONTEXT['wtc_completed'] = True
            if GLOBAL_CONTEXT['make_ready']:
                GLOBAL_CONTEXT['make_ready']=False
                CoreNotificationHelper.send_notifications(sender="SBM", recipient=request.user, setting_name="SBM_READYTOMATCH", message="Congratulations! As you filled out all questionnaires, you can now enter the matching process and finally find your study buddy.", url='/sbm/matching_page')
                #TODO redirect to matching page
                return WTCProcessingResponseRedirect(
                    reverse('sbm_matching'), request=request, questions=questions, uq=uq,
                )

            else:

                return WTCProcessingResponseRedirect(
                    reverse('sbm_ls'), request=request, questions=questions, uq=uq,
                )
    usm = UserMatchingStatus.objects.get(user=request.user).status

    context = {
        'description': description, 'questions_sin': sin_question, 'name_w': 'Willingness to Communicate',
        'name_personal': 'Personal Interests', 'result': result, 'single_answers': single_answers, 'zip_list': zip_list,
        'usm' : usm,
    }

    context.update(GLOBAL_CONTEXT)

    return render(
        request, 'sbm/wtc.html',
        context=context,
    )


@login_required
@check_seen_questionnaires
def course_submission(request):
    help_text_course = "Please enter courses you have applied to throughout your studies. " \
                       "You will be suggested courses which we already have in our database. " \
                       "Nevertheless, if you can't find the course you are looking for, please feel free to enter it " \
                       "with its correct name."
    help_text_grade = "If you like, please also enter the grade you received in this course."

    if request.method == 'POST':
        form = CourseSubmissionForm(request.POST)

        if form.is_valid():
            course_title = form.cleaned_data['course_title']
            # If user didn't leave the course title field blank and the course title is valid, create enrollment
            if course_title:
                course = Course.objects.get(title=course_title)
                course_grade = form.cleaned_data['course_grade']
                # Delete existing course enrollments with same course name
                CourseEnrollment.objects.filter(user=request.user, course=course).delete()
                ce, ce_created = CourseEnrollment.objects.get_or_create(
                    user=request.user, course=course, grade=course_grade
                )

                if ce_created:
                    # Whenever the user submits new course enrollments, delete all existing related similarity scores
                    INSTSimilarity.objects.filter(Q(user_a=request.user) | Q(user_b=request.user)).delete()

                messages.success(
                    request, f"Your enrollment to the course {course.title} has been saved"
                             f"{(' with the grade ' + str(ce.grade)) if ce.grade is not None else ''}."
                )

            return HttpResponseRedirect(reverse('sbm_course_submission'))

    else:
        form = CourseSubmissionForm()

    present_enrollments = CourseEnrollment.objects.filter(user=request.user)
    present_data = [(ce.course.title, ce.grade, ce.id) for ce in present_enrollments]
    usm = UserMatchingStatus.objects.get(user=request.user).status
    context = {
        'form': form,
        'present_data': present_data,
        'help_text_course': help_text_course,
        'help_text_grade': help_text_grade,
        'usm' : usm
    }

    context.update(GLOBAL_CONTEXT)

    return render(request, 'sbm/course_submission.html', context=context)


@login_required
@check_seen_questionnaires
def course_description(request, course_id):
    course = get_object_or_404(Course, id=course_id)

    if request.method == 'POST':
        form = CourseDescriptionForm(request.POST)
        if form.is_valid():
            description = form.cleaned_data['course_description']
            course.description = description
            course.save()
            messages.success(
                request,
                f"Your description for the course {course.title} has been saved. Thank you for your support!"
            )
            return HttpResponseRedirect(reverse('sbm_course_submission'))
    else:
        form = CourseDescriptionForm(initial={'course_description': course.description})

    context = {
        'form': form,
        'course': course,
    }

    context.update(GLOBAL_CONTEXT)

    return render(request, 'sbm/course_description.html', context=context)


@login_required
def delete_course_enrollment(request, ce_id):
    ce = get_object_or_404(CourseEnrollment, id=ce_id)
    ce.delete()
    return redirect('sbm_course_submission')


@login_required
@check_seen_questionnaires
def pp(request):
    try:
        questionnaire = Questionnaire.objects.get(title='Partner Preferences')
        question_2 = MultipleChoiceQuestion.objects.filter(questionnaire=questionnaire,
                                                           title='Partner Preferences Question 2').first()

    except (Questionnaire.DoesNotExist, MultipleChoiceQuestion.DoesNotExist):
        messages.error(request, 'Could not find Questionnaire or Question.')
        return render(request, 'sbm/questionnaire_not_defined.html', context={})

    uq, uq_created = UserQuestionnaire.objects.get_or_create(
        user=request.user,
        questionnaire=questionnaire
    )

    try:
        qa = PartnerPreferencesAnswers.objects.get(
            user_questionnaire=uq)
        GLOBAL_CONTEXT['pp_completed'] = True
    except PartnerPreferencesAnswers.DoesNotExist:
        qa = None
        GLOBAL_CONTEXT['pp_completed'] = False

    description = questionnaire.description
    title = questionnaire.title
    help_text_institute = "Type in the institute your study buddy should come from"
    help_text_course = "Type in a course your potential partner should already have taken"
    usm = UserMatchingStatus.objects.get(user=request.user).status

    if request.method == 'POST':

        form = PartnerPreferencesForm(request.POST)

        if 'delete' in request.POST:
            PartnerPreferencesAnswers.objects.filter(user_questionnaire=uq).delete()
            messages.success(request, 'Your answer has been deleted')
            UserMatchingStatus.objects.get(user=request.user).delete()
            UserMatchingStatus.objects.create(user=request.user, status='unready')
            StudyBuddy.objects.filter(user=request.user, pinned=False).delete()
            return HttpResponseRedirect(reverse('sbm_pp'))

        if form.is_valid():

            partner = form.cleaned_data['partner']
            institute = form.cleaned_data['institute']
            course = form.cleaned_data['course']

            if PartnerPreferencesAnswers.objects.filter(user_questionnaire=uq).exists():
                PartnerPreferencesAnswers.objects.filter(user_questionnaire=uq).delete()

            PartnerPreferencesAnswers.objects.get_or_create(user_questionnaire=uq, partner=partner,
                                                            institute=institute, course=course)

         #   messages.success(request, 'Your answer has been saved.')

            GLOBAL_CONTEXT['pp_completed'] = True
            if GLOBAL_CONTEXT['make_ready']:
                GLOBAL_CONTEXT['make_ready']=False
                CoreNotificationHelper.send_notifications(sender="SBM", recipient=request.user, setting_name="SBM_READYTOMATCH", message="Congratulations! As you filled out all questionnaires, you can now enter the matching process and finally find your study buddy.", url='/sbm/matching_page')
                return redirect('sbm_matching')
            elif UserMatchingStatus.objects.get(user=request.user).status != 'unready':
                return redirect('sbm_matching')
            else:
                return HttpResponseRedirect(reverse('sbm_pp'))


    else:
        form = PartnerPreferencesForm(initial={
                                               'partner': list(qa.partner) if qa else [],
                                               'institute': qa.institute if qa else '',
                                               'course': qa.course if qa else ''})

    context = {
        'qa_exist': qa,
        'description': description,
        'form': form,
        'title': title,
        'question_2': question_2,
        'help_text_institute': help_text_institute,
        'help_text_course': help_text_course,
        'usm' : usm
    }

    context.update(GLOBAL_CONTEXT)

    return render(request, 'sbm/sbm_pp.html', context=context)


# autocompletion
def search_course(request):
    title = request.GET.get('course')
    payload = []
    if title:
        fake_course_objs = Course.objects.filter(title__icontains=title)

        for fake_adress_obj in fake_course_objs:
            payload.append(fake_adress_obj.title)

    return JsonResponse({'status': 200, 'data': payload})


# autocompletion
def search_institute(request):
    inst = request.GET.get('institute')
    payload = []
    if inst:
        fake_institute_objs = Institute.objects.filter(name__icontains=inst)

        for fake_inst_obj in fake_institute_objs:
            payload.append(fake_inst_obj.name)

    return JsonResponse({'status': 200, 'data': payload})


def do_matching(request, inst_filter=.2, course_filter=True):
    """ Actually calculate and store matches for request.user in a non-parallel concurrent manner

    :param request:
    """

    protagonist = request.user
    try:

        # Check for newest model and potentially use it
        if os.path.exists(UPDATE_MODEL_PATH):
            # HACK: This could maybe lead to Dirty Reads if f is getting written to elsewhere, but
            # should be easily fixable by just e.g. writing a try-catch here where one just skips the
            # model updating in case an exception gets caught (I would say even this isn't necessary as Django
            # doesn't crash upon exceptions anyway).
            with open(UPDATE_MODEL_PATH, "r") as f:
                # Only update model if there was a new update
                update_model_args = json.load(f)
                new_update = datetime.datetime.fromisoformat(update_model_args["last_update"])
                global last_update
                if new_update > last_update:
                    last_update = new_update
                    global CURRENT_MODEL
                    CURRENT_MODEL = eval(update_model_args["last_model_class"] + "()")
                    CURRENT_MODEL.load_model(update_model_args["last_model_load_path"])

        match_df = get_matches(protagonist, model=CURRENT_MODEL, institute_threshold=inst_filter,
                               use_course_filter=course_filter)

        # Delete all past study buddies except for pinned ones
        StudyBuddy.objects.filter(user=protagonist, pinned=False).delete()

        store_matches(protagonist, match_df)

        # after calculating matches we can set the user status to active
        UserMatchingStatus.objects.update_or_create(
            user=protagonist,
            defaults={'status': 'active'}
        )

        CoreNotificationHelper.send_notifications(
            sender='SBM',
            recipient=protagonist,
            setting_name="SBM_NEWMATCH",
            message="Your match recommendations have been calculated! See your matches in the 'Matching Page' section.",
            data={'title': 'SmartUni: New Match Recommendations'},
            url='/sbm/matching_page'
        )

    except Exception as e:
        CoreNotificationHelper.send_notifications(
            sender='SBM',
            recipient=protagonist,
            setting_name="SBM_NEWMATCH",
            message="There was an error while calculating your match recommendations. Please try again later.",
            data={'title': 'SmartUni: Error in Matching calculation'},
            url='/sbm'
        )
        logger.error(f"Error in matching calculation:\n{str(e)}")

    finally:
        # Allow another match thread to be started - at this point, it's safe.
        global match_thread_exists
        match_thread_exists = False


# this should go on the button
@login_required
def perform_matching(request):
    inst_filter = .2
    matching_kwargs = {}
    if request.method == 'POST':
        if 'inst_filter' in request.POST:
            matching_kwargs['inst_filter'] = 0.
        matching_kwargs['course_filter'] = 'course_filter' not in request.POST

    global match_thread_exists
    # If not match thread exists, create a match thread and start.
    if not match_thread_exists:
        messages.success(request,
                         'Matching procedure successfully triggered! You will be notified once it\'s completed!')
        match_thread_exists = True
        Thread(target=do_matching, args=[request], kwargs=matching_kwargs).start()
    else:
        messages.success(request, 'Matching procedure already in progress. You will be notified once it\'s completed!')

    return redirect('sbm_matching')


@login_required
@check_ready_for_matching
def matching(request):
    context = {}

    context.update(GLOBAL_CONTEXT)

    protagonist = request.user

    buddies = StudyBuddy.objects.filter(user=protagonist).order_by('-score')

    usm, _ = UserMatchingStatus.objects.get_or_create(user=protagonist)
    context['usm'] = usm.status
    context['button_text']="Match me!"

    if buddies.exists():
        context["matches"] = [MatchHTMLWrapper(buddy) for buddy in buddies]
        context['button_text'] = "Match me again!"
    elif usm.status == 'active':
        # we've just tried to match but found nobody

        return render(request, 'sbm/no_matches_found.html', context=context)

    return render(request, 'sbm/sbm_matching_page.html', context=context)


def change_status(request, status):

    if 'make_ready' in request.GET:
        GLOBAL_CONTEXT['make_ready'] = request.GET['make_ready']

    UserMatchingStatus.objects.update_or_create(
        user=request.user,
        defaults={
            'status': status
        }
    )

    if status != UserMatchingStatus.ACTIVE:
        # If the user sets their status on passive or inactive they can't get recommendations anymore.
        StudyBuddy.objects.filter(user=request.user, pinned=False).delete()

    # messages.success(request, 'Your user status has been changed.')
    return redirect('sbm_matching')


def create_feedback(request):
    clicked = request.GET['element']
    feedback = clicked[-1]
    match = clicked[:-1]
    match = SmartUser.objects.get(username=match)
    studybuddy = StudyBuddy.objects.get(user=request.user, match=match)
    if FeedbackSBM.objects.filter(studybuddy=studybuddy).exists():
        FeedbackSBM.objects.filter(studybuddy=studybuddy).delete()
    fb = FeedbackSBM.objects.create(rating=int(feedback), studybuddy=studybuddy)
    pp_answer = get_prefs_vec(request.user)

    MatchSample.objects.update_or_create(
        identifier=MatchSample.calculate_identifier(request.user, studybuddy.match),
        defaults={
            'wtc': get_cached_similarity(studybuddy.user, studybuddy.match, 'wtc'),
            'ls': get_cached_similarity(studybuddy.user, studybuddy.match, 'ls'),
            'inst': get_cached_similarity(studybuddy.user, studybuddy.match, 'inst'),
            'pai': get_cached_similarity(studybuddy.user, studybuddy.match, 'pai'),
            'wtc_pp': pp_answer[0],
            'ls_pp': pp_answer[1],
            'inst_pp': pp_answer[2],
            'pai_pp': pp_answer[3],
            'user_score': MatchSample.normalize_user_score(fb.rating)
        }
    )

    return redirect('sbm_matching')


def pin_match(request):
    match_user = request.GET['username']
    matched = SmartUser.objects.get(username=match_user)
    if StudyBuddy.objects.filter(user=request.user, match=matched, pinned=True).exists():
        StudyBuddy.objects.filter(user=request.user, match=matched).update(pinned=False)

    else:
        StudyBuddy.objects.filter(user=request.user, match=matched).update(pinned=True)

    return redirect('sbm_matching')
