from django.core.exceptions import ValidationError
from django.db import models
from picklefield.fields import PickledObjectField
from spc.models import SmartUser, Feedback
from django.core.validators import MinValueValidator, MaxValueValidator
import hashlib
import logging
logger = logging.getLogger('sbm')


def is_valid_number(test_str: str):
    if not test_str.isdecimal():
        return False
    return str(int(test_str)) == test_str


class Questionnaire(models.Model):
    """
    General model class for all questionnaires.
    """
    title = models.CharField(max_length=100, unique=True)
    description = models.TextField(blank=True)

    def get_questions(self):
        """
        Helper function which returns all questions belonging to this questionnaire using a simple django query.
        :return list of questions:
        """
        result = list(SingleChoiceQuestion.objects.filter(questionnaire=self))
        result += list(MultipleChoiceQuestion.objects.filter(questionnaire=self))
        result += list(TextQuestion.objects.filter(questionnaire=self))

        return result

    def __str__(self):
        return self.title


class UserQuestionnaire(models.Model):
    """
    N:M relation between User and Questionnaire.
    """
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    user = models.ForeignKey(SmartUser, on_delete=models.CASCADE)

    class Meta:
        # Each user should only be able to take the questionnaire once.
        # Re-answering of course should be possible.
        unique_together = ('questionnaire', 'user')


class Question(models.Model):
    """
    Model class for questionnaire questions realized as a 1:N relation (One Questionnaire has many Questions).
    """
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    question_text = models.CharField(max_length=500)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class SingleChoiceQuestion(Question):
    """
    Model class for single choice questions (e.g. one single WTC score).
    """
    answer_options = models.JSONField(null=True)

    def _check_valid_keys(self):
        """
        Helper function which checks if the keys of the answer_options field are valid numbers.
        """

        for key in list(self.answer_options.keys()):
            if not is_valid_number(key):
                return False
        return True

    def clean(self):
        if not self._check_valid_keys():
            raise ValidationError("answer_options has invalid key(s).")


class MultipleChoiceQuestion(Question):
    """
    Model class for multiple choice questions (e.g. the Learning Styles question).
    """
    answer_options = models.JSONField(null=True)

    def _check_valid_keys(self):
        """
        Helper function which checks if the keys of the answer_options dictionary are valid numbers.
        """
        for key in list(self.answer_options.keys()):
            if not is_valid_number(key):
                return False
        return True

    def clean(self):
        if not self._check_valid_keys():
            raise ValidationError("answer_options has invalid key(s).")

    def get_answer_text(self, answer_key: int):
        return self.answer_options[str(answer_key)]


class TextQuestion(Question):
    """Model class questions with textual answers."""
    pass


class QuestionAnswer(models.Model):
    """
    Model class for answers to questionnaire-questions.
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user_questionnaire = models.ForeignKey(UserQuestionnaire, on_delete=models.CASCADE)
    answer = models.TextField(blank=False, null=True, default=None, max_length=500)
    mkdate = models.DateTimeField(auto_now_add=True)
    chdate = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        unique_together = ('question', 'user_questionnaire')

    def clean(self):
        # check if question and user_questionnaire relate to the same questionnaire
        if self.question.questionnaire != self.user_questionnaire.questionnaire:
            raise ValidationError('Question and UserQuestionnaire do not relate to the same questionnaire.')


class SingleChoiceQuestionAnswer(QuestionAnswer):
    """
    Model class for answers to single choice questions.
    """
    question = models.ForeignKey(SingleChoiceQuestion, on_delete=models.CASCADE)
    answer = models.IntegerField(blank=False, null=False)

    def clean(self):
        super().clean()

        if self.answer is not None and str(self.answer) not in list(self.question.answer_options.keys()):
            raise ValidationError('Answer is not in answer_options.')

    def get_answer_text(self):
        """
        Returns the given answer as text.
        :return str:
        """
        if self.answer is None:
            return "Unanswered"
        return self.question.answer_options[str(self.answer)]


class TextQuestionAnswer(QuestionAnswer):
    """
    Model class for answers to text questions.
    """
    question = models.ForeignKey(TextQuestion, on_delete=models.CASCADE)
    answer = models.TextField(blank=False, null=False, max_length=500)


class MultipleChoiceQuestionAnswer(QuestionAnswer):
    """
    Model class for answers to multiple choice questions.
    """
    question = models.ForeignKey(MultipleChoiceQuestion, on_delete=models.CASCADE)
    answer = models.TextField(blank=False, null=True, default=None, max_length=500)

    def get_answers(self):
        """
        Returns the given answer as a list of integers.
        :return QuerySet of AnswerItem:
        """
        return AnswerItem.objects.filter(question_answer=self)

    def get_answer_keys(self):
        """
        Returns the integer keys of the answers given.
        :return list of int:
        """
        answer_items = self.get_answers()
        return [answer_item.answer_key for answer_item in answer_items]

    def clean(self):
        super().clean()

        for answer in self.get_answers():
            if answer.answer_key is not None \
                    and str(answer.answer_key) not in list(self.question.answer_options.keys()):
                raise ValidationError('Answer is not in answer_options.')

    def get_answer_text(self):
        """
        Returns the given answer as text.
        :return str:
        """
        answers = self.get_answers()
        if not answers:
            return "Unanswered"

        answer_options = MultipleChoiceQuestion(self.question).answer_options
        answer_texts = []
        for answer in answers:
            answer_texts.append(answer_options[str(answer.answer_key)])
        return ",".join(answer_texts)


class AnswerItem(models.Model):
    """
    Model class for answers to multiple choice questions.
    """
    question_answer = models.ForeignKey(MultipleChoiceQuestionAnswer, on_delete=models.CASCADE)
    answer_key = models.IntegerField(blank=False, null=False)
    mkdate = models.DateTimeField(auto_now_add=True)
    chdate = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('question_answer', 'answer_key')


class QuestionnaireResult(models.Model):
    """
    Model class for the results of a questionnaire.
    """
    user_questionnaire = models.ForeignKey(UserQuestionnaire, on_delete=models.CASCADE)
    answer = models.IntegerField(blank=False, null=False)
    mkdate = models.DateTimeField(auto_now_add=True)
    chdate = models.DateTimeField(auto_now=True)


class PartnerPreferencesAnswers(models.Model):
    """
    Model class for the answers to the partner preferences questionnaire.
    """
    user_questionnaire = models.ForeignKey(UserQuestionnaire, on_delete=models.CASCADE, default="")
    partner = models.CharField(blank=False, default=None, max_length=200)
    institute = models.CharField(blank=True, default=None, max_length=200, null=True)
    course = models.CharField(blank=True, default=None, max_length=200, null=True)

    @staticmethod
    def _convert_answer(answer):
        """
        Converts the string answer to a boolean.
        """
        answer = list(set(answer))
        if '[' in answer:
            answer.remove('[')
        if ']' in answer:
            answer.remove(']')
        if ' ' in answer:
            answer.remove(' ')
        return answer

    def get_partner_list(self):
        return self._convert_answer(self.partner)


class Institute(models.Model):
    """
    Model class for institutes (as in Stud.IP).
    """
    name = models.CharField(default=None, max_length=200)


class Course(models.Model):
    """
    Model class for courses.
    """
    course_number = models.CharField(max_length=100, default="")
    course_type = models.CharField(max_length=100, default="")
    title = models.CharField(max_length=255, unique=True, default="")
    institute = models.ForeignKey(Institute, on_delete=models.SET_NULL, null=True)


class CourseEnrollment(models.Model):
    """
    Model class for course enrollment relations between SmartUser and Course objects.
    """
    user = models.ForeignKey(SmartUser, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    grade = models.DecimalField(
        blank=True, null=True, decimal_places=1, max_digits=3,
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )


# Models used to store similarity scores.
class BidirectionalSimilarity(models.Model):
    """
    Model class for storing similarity scores.
    """
    similarity = models.DecimalField(max_digits=5, decimal_places=4)
    user_a = models.ForeignKey(SmartUser, on_delete=models.CASCADE)
    user_b = models.ForeignKey(SmartUser, on_delete=models.CASCADE)
    mkdate = models.DateTimeField(auto_now_add=True)
    chdate = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        unique_together = ('user_a', 'user_b')

    def __str__(self):
        return f'similarity score= {self.similarity} Users: {self.user_a.username} and {self.user_b.username}'


class PAISimilarity(BidirectionalSimilarity):
    """
    Model class for storing PAI similarity scores.
    """
    user_a = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='pai_similarity_a')
    user_b = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='pai_similarity_b')


class WTCSimilarity(BidirectionalSimilarity):
    """
    Model class for storing WTC similarity scores.
    """
    user_a = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='wtc_similarity_a')
    user_b = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='wtc_similarity_b')


class LSSimilarity(BidirectionalSimilarity):
    """
    Model class for storing LS similarity scores.
    """
    user_a = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='ls_similarity_a')
    user_b = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='ls_similarity_b')


class INSTSimilarity(BidirectionalSimilarity):
    """
    Model class for storing INST similarity scores.
    """
    user_a = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='ints_similarity_a')
    user_b = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='ints_similarity_b')


class NLPObject(models.Model):
    """
    Model class for storing NLP objects.
    """
    user = models.ForeignKey(SmartUser, on_delete=models.CASCADE)
    nlp_obj = PickledObjectField()


class UserMatchingStatus(models.Model):
    """
    Model class for storing the matching status of users.
    """
    ACTIVE = 'active'
    PASSIVE = 'active'
    INACTIVE = 'inactive'
    UNREADY = 'unready'
    MATCHING_STATUSES = [
        (ACTIVE, 'Active'),
        (PASSIVE, 'Passive'),
        (INACTIVE, 'Inactive'),
        (UNREADY, 'Unready'),
    ]

    user = models.OneToOneField(SmartUser, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=MATCHING_STATUSES, default=UNREADY)


class StudyBuddy(models.Model):
    """
    Model class for storing study buddy recommendations.
    If pinned is True, the study buddy will be stored beyond new calculations.
    """
    user = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='user')
    match = models.ForeignKey(SmartUser, on_delete=models.CASCADE, related_name='match')
    score = models.DecimalField(max_digits=3, decimal_places=2, default=0)
    pinned = models.BooleanField(default=False)

    class Meta:
        unique_together = ['user', 'match']


class FeedbackSBM(models.Model):
    """
    Model class for storing feedback for match recommendations.
    """
    rating = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)], null=False)
    studybuddy = models.OneToOneField(StudyBuddy, on_delete=models.CASCADE, related_name='feedback')


class MatchSample(models.Model):
    """
    Model class for storing anonymized match recommendations plus the data that lead to the recommendation
    and the user's feedback.
    """
    wtc = models.DecimalField(decimal_places=4, max_digits=5, validators=[MinValueValidator(0), MaxValueValidator(1)])
    ls = models.DecimalField(decimal_places=4, max_digits=5, validators=[MinValueValidator(0), MaxValueValidator(1)])
    inst = models.DecimalField(decimal_places=4, max_digits=5, validators=[MinValueValidator(0), MaxValueValidator(1)])
    pai = models.DecimalField(decimal_places=4, max_digits=5, validators=[MinValueValidator(0), MaxValueValidator(1)])
    wtc_pp = models.BooleanField(default=False)
    ls_pp = models.BooleanField(default=False)
    inst_pp = models.BooleanField(default=False)
    pai_pp = models.BooleanField(default=False)
    user_score = models.DecimalField(
        decimal_places=4, max_digits=5, validators=[MinValueValidator(0), MaxValueValidator(1)], null=True
    )
    identifier = models.CharField(max_length=32, unique=True)
    mkdate = models.DateTimeField(auto_now_add=True)

    @staticmethod
    def calculate_identifier(protagonist: SmartUser, match: SmartUser) -> str:
        """
        Determine anonymised identifier for a pair of users.
        :param protagonist: first user
        :param match: second user
        :return: Anonymised identifier
        """
        id_str = str(protagonist.id) + str(match.id)
        return hashlib.sha256(id_str.encode('utf-8')).hexdigest()

    @staticmethod
    def normalize_user_score(user_score: int) -> float:
        """
        Normalize a user score in [1;5] to [0;1]
        :param user_score: Feedback score from the user in [1;5]
        :return: normalized score in [0;1]
        """
        return (user_score - 1.) / 4.
