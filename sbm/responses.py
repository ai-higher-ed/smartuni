from django.http import HttpResponseRedirect
from django.db.models import Q

from sbm.models import *
import numpy as np


class WTCProcessingResponseRedirect(HttpResponseRedirect):
    def __init__(self, redirect_to, *args, **kwargs):
        self.questions = kwargs.pop('questions')
        self.request = kwargs.pop('request')
        self.uq = kwargs.pop('uq')
        super().__init__(redirect_to, *args, **kwargs)

    def close(self):

        one_updated = False

        for question in self.questions:
            answer = int(self.request.POST.get(question.title))

            qa, qa_created = SingleChoiceQuestionAnswer.objects.update_or_create(
                question=question, user_questionnaire=self.uq,
                defaults={'answer': answer}
            )

            if not qa_created:
                one_updated = True

        if one_updated:
            # if the user is re-taking the questionnaire, delete all existing related similarity scores
            WTCSimilarity.objects.filter(Q(user_a=self.uq.user) | Q(user_b=self.uq.user)).delete()

        answer8 = int(self.request.POST.get("Talk in a small group of strangers"))
        answer15 = int(self.request.POST.get("Talk in a small group of acquaintances"))
        answer19 = int(self.request.POST.get("Talk in a small group of friends"))
        answer6 = int(self.request.POST.get("Talk in a large meeting of friends"))
        answer11 = int(self.request.POST.get("Talk in a large meeting of acquaintances"))
        answer17 = int(self.request.POST.get("Talk in a large meeting of strangers"))
        answer4 = int(self.request.POST.get("Talk with an acquaintance while standing in line"))
        answer9 = int(self.request.POST.get("Talk with a friend while standing in line"))
        answer12 = int(self.request.POST.get("Talk with a stranger while standing in line"))
        answer3 = int(self.request.POST.get("Present a talk to a group of strangers"))
        answer14 = int(self.request.POST.get("Present a talk to a group of friends"))
        answer20 = int(self.request.POST.get("Present a talk to a group of acquaintances"))

        group_discussion = np.mean([answer8, answer15, answer19])
        meetings = np.mean([answer6, answer11, answer17])
        interpersonal = np.mean([answer4, answer9, answer12])
        public_speaking = np.mean([answer3, answer14, answer20])

        stranger = np.mean([answer3, answer8, answer12, answer17])
        acquaintance = np.mean([answer4, answer11, answer15, answer20])
        friend = np.mean([answer6, answer9, answer14, answer19])

        total = np.mean([stranger, acquaintance, friend])

        QuestionnaireResult.objects.update_or_create(user_questionnaire=self.uq, defaults={'answer': total})
