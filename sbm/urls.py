from django.urls import include, path
from django.views.generic import TemplateView

from . import views


urlpatterns = [
    path('', views.sbm_startpage, name='sbm_startpage'),
    # personal academic interest submission
    path('pai/', views.pai, name='sbm_pai'),
    path('ls/', views.ls, name='sbm_ls'),
    path('wtc/', views.wtc, name='sbm_wtc'),
    path('pp/', views.pp, name='sbm_pp'),
    path('course_submission/', views.course_submission, name='sbm_course_submission'),
    path('course_submission/search_courses/', views.search_course),
    path('course_description/<int:course_id>', views.course_description, name='sbm_course_description'),
    path('delete_course_enrollment/<int:ce_id>', views.delete_course_enrollment, name='sbm_delete_course_enrollment'),
    path('delete_answers_text/<question_id>/', views.delete_answers_text, name='sbm_delete_answers_text'),
    path('delete_answers_ls/<question_id>/', views.delete_answers_ls, name='sbm_delete_answers_ls'),
    path('pp/search_courses/', views.search_course),
    path('pp/search_institutes/', views.search_institute),
    path('matching_page/', views.matching, name='sbm_matching'),
    path('perform_matching', views.perform_matching, name='perform_matching'),
    path('change_status/<str:status>/', views.change_status, name='change_status'),
    path('create_feedback',views.create_feedback, name='create_feedback'),
    path('pin_match', views.pin_match, name='pin_match'),
    
]
