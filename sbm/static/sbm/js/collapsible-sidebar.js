// Desktop view
const collapsedClass = "sidebar-right--collapsed";
const lskey = "navCollapsed";
const sidebarStateKey = "sidebarStateKey";
const sidebarStateCollapsed = "collapsed";
// class name of left and right arrow css icon
const leftArrowClass = "bi-chevron-double-left";
const rightArrowClass = "bi-chevron-double-right";
const arrowClass = "bi"


// returns the entire nav bar
const nav = document.querySelector(".sidebar-right");
//anchor tag with nav-link class
const navLink = document.querySelector(".nav-icon").parentElement;
// the current arrow being displayed e.g., double-left, double-right double-up
const arrowElem = document.querySelector("." + arrowClass);

//changes direction of the arrows
function collapseSidebar() {
    arrowElem.classList.remove(rightArrowClass);
    arrowElem.classList.add(leftArrowClass);
    // actually it would also be okay to leave it as "right", but "left" looks better when collapsed
    navLink.style.float = "left";
    // Store that sidebar is collapsed
    sessionStorage.setItem(sidebarStateKey, sidebarStateCollapsed);
}

function toggleSidebar() {
    nav.classList.toggle(collapsedClass);
    // if sidebar collapsed, expand it
    if (sessionStorage.getItem(sidebarStateKey)) {
        arrowElem.classList.remove(leftArrowClass);
        arrowElem.classList.add(rightArrowClass);
        // necessary since the arrow should stay to the right of the sidebar
        navLink.style.float = "right";
        // Store that sidebar is expanded
        sessionStorage.setItem(sidebarStateKey, "");
    }
    // if sidebar expanded, collapse it
    else {
        collapseSidebar()
    }
}

// collapse sidebar upon loading webpage if it was previously collapsed
if (sessionStorage.getItem(sidebarStateKey)) {
    nav.classList.toggle(collapsedClass);
    collapseSidebar();
}

navLink.addEventListener("click", () => {
    toggleSidebar();
});

// Mobile view
let firstTime = true
let currentScreenSize = 0
let showBar = false
const swipeUp = document.getElementsByClassName("display-bar")[0] // button
const swipeDown = document.getElementsByClassName("hide-bar")[0] //button
const sidebarRight = document.getElementsByClassName("sidebar-right")[0]
// swipe up
const asideTag = document.getElementsByClassName("touch-area")[0]
const rightBar = document.querySelector(".sidebar-right");
const MOVE_THRESHOLD = 100;
// variable to store initial y position
let initialY = 0;
let moveY = 0;
let isBarOpen = false;

// if we start in mobile view, remove sidebar's arrow icon since it's unnecessary in mobile view
const mobileWidth = 768;
if (window.innerWidth <= mobileWidth) {
    
    if (rightBar.querySelector("." + arrowClass)) {
        rightBar.removeChild(navLink.parentElement)

    }
}

// swipe up on phones
window.addEventListener('load', function () {


    asideTag.addEventListener("touchstart", e => {

        initialY = e.touches[0].pageY;

    });


    asideTag.addEventListener("touchmove", e => {
        let currentY = e.touches[0].pageY;
        moveY = currentY - initialY;
    });


    asideTag.addEventListener("touchend", e => {
        if (moveY < MOVE_THRESHOLD * Math.sign(moveY) && !isBarOpen) {
            rightBar.style.transform = "scaleY(1)";
            isBarOpen = true;
            swipeUp.style.visibility = 'hidden'
            swipeDown.style.visibility = 'visible'
        } else if (moveY > MOVE_THRESHOLD * Math.sign(moveY) && isBarOpen) {
            rightBar.style.transform = "scaleY(0)";
            isBarOpen = false;
            swipeUp.style.visibility = ' visible'
            swipeDown.style.visibility = 'hidden'
        }

        moveY = 0;
    });


}, false)


// click on swipe up arrow to make nav bar visible

swipeUp.addEventListener('click', e => {
    showBar = true
    // make bar visible at the bottom of screen
    sidebarRight.style.transform = 'scaleY(1)'
    // make swipe down button visible
    swipeDown.style.visibility = 'visible'
    // hide swipe up button
    swipeUp.style.visibility = 'hidden'

})


swipeDown.addEventListener('click', e => {
    showBar = false
    sidebarRight.style.transform = 'scaleY(0)'
    swipeDown.style.visibility = 'hidden'
    swipeUp.style.visibility = 'visible'
})

window.addEventListener('resize', function (event) {
    var newWidth = window.innerWidth;

    if (newWidth > mobileWidth) {
        sidebarRight.style.transform = 'scaleY(1)'
        swipeDown.style.visibility = 'hidden'
        swipeUp.style.visibility = 'hidden'
        // add back sidebar's arrow icon if it has been removed
        if (!rightBar.querySelector("." + arrowClass)) {
            rightBar.prepend(navLink.parentElement)
        }

    } else if (newWidth <= mobileWidth) {

        if (currentScreenSize != newWidth && firstTime != true && showBar) {
            swipeDown.style.visibility = 'visible'
            sidebarRight.style.transform = 'scaleY(1)'
        } else {
            firstTime = false
            swipeUp.style.visibility = 'visible'
            sidebarRight.style.transform = 'scaleY(0)'
        }
        currentScreenSize = newWidth
        // remove sidebar's arrow icon since it's unnecessary in mobile view
        if (rightBar.querySelector("." + arrowClass)) {
            rightBar.removeChild(navLink.parentElement)
        }
    }

});

// bring html elements to the front

let sub_button = document.getElementById("submit-id-submit");
let start_button = document.getElementById("start-id-submit");
let next_button_course_sub = document.getElementById("next-b-course-s");
let delete_pai = document.getElementById("delete-pai");

if (sub_button != null) {
    sub_button.style.cssText = "z-index: 2; position: sticky";
}
    if (start_button != null) {
        start_button.style.cssText = "z-index: 2; position: sticky";
    }
    if (next_button_course_sub != null){
    next_button_course_sub.style.cssText = "z-index: 2; position: sticky";
}
    if (delete_pai != null){
    delete_pai.style.cssText = "z-index: 2; position: sticky";
}




