/* globals feather:false */

(function () {
  'use strict'

  // load feather icons
  feather.replace({ 'aria-hidden': 'true' })

  // put all javascript init stuff here
  // ...

})()
