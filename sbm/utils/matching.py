import json
from math import ceil

from django.db.models import Q
from django.db.models import QuerySet
from django_q.models import Schedule, OrmQ
from sbm.models import *
from spc.models import SmartUser
from sbm.utils.data import AnswerProjector
import pandas as pd
import numpy as np
import logging
from abc import ABC, abstractmethod
from tensorflow import keras
import os
import datetime
from django.utils import timezone
from django.core.management import call_command
import scipy
from scipy import special
from sklearn.metrics import mean_squared_error
import random
import pickle
from sklearn.linear_model import LogisticRegression
from django_pandas.io import read_frame
from sbm.utils.NLP_pipeline import NLPPipeline
from sklearn.model_selection import KFold
import pathlib
from io import StringIO
from typing import Union

UPDATE_MODEL_PATH = os.path.join(pathlib.Path(__file__).parent.parent.resolve(), 'static', 'sbm', 'models',
                                 'update_model.json')

# 3 for each of user 1's entries which can each match with 1 to 3 or with none
lookup = np.empty((4, 4, 4), dtype=float)
lookup[:] = np.nan
# 0 is no match, 1 is match with first, 2 is match with second, 3 is match with third

match_order = [[[0, 0, 0]],
               [[0, 0, 3]],
               [[0, 0, 2], [0, 3, 0]],
               [[0, 3, 2]],
               [[0, 0, 1], [3, 0, 0]],
               [[0, 2, 0]],
               [[0, 2, 3]],
               [[0, 3, 1], [3, 0, 2]],
               [[3, 0, 1]],
               [[2, 0, 0], [0, 1, 0]],
               [[2, 0, 3], [0, 1, 3], [3, 2, 0], [0, 2, 1]],
               [[0, 1, 2], [2, 3, 0]],
               [[2, 0, 1], [3, 1, 0]],
               [[3, 1, 2], [2, 3, 1], [3, 2, 1]],
               [[2, 1, 0]],
               [[2, 1, 3]],
               [[1, 0, 0]],
               [[1, 0, 3]],
               [[1, 0, 2], [1, 3, 0]],
               [[1, 3, 2]],
               [[1, 2, 0]],
               [[1, 2, 3]]
               ]

for i, match_keys in enumerate(match_order):
    for match_key in match_keys:
        lookup[match_key[0]][match_key[1]][match_key[2]] = i

# Normalize values
lookup = lookup / np.nanmax(lookup)


def get_lookup():
    """
    :return: lookup table for institute value matchings
    """
    return lookup


def inst_similarity(user_a, user_b):
    """
    Similarity function between two INST user vectors as illustrated in the top right of
    https://app.mural.co/t/virtuos1242/m/virtuos1242/1642756697455/0a6906336e3df6e587587cf8ee6a3b6a57a508ae?sender=1d7202f1-767b-4744-9217-f304e5a88802
    :param user_a: INST vector of user a
    :param user_b: INST vector of user b
    :return: INST similarity score in [0;1]
    """
    big_array = user_a
    small_array = user_b
    if user_b.size > user_a.size:
        big_array = user_b
        small_array = user_a
    if type(big_array) == pd.Series:
        big_array = big_array.values
    if type(small_array) == pd.Series:
        small_array = small_array.values
    idx = big_array[:, None] == small_array
    default = np.zeros(3, dtype=np.int64)
    z = np.argwhere(idx)
    default[z[:, 0]] += 1 + z[:, 1]
    sim = lookup[default[0], default[1], default[2]]
    if np.isnan(sim):
        logger = logging.getLogger('sbm')
        logger.error(f"INST similarity: invalid matching. Institute vectors of user_a and user_b are: {user_a} and "
                     f"{user_b}")
    return sim


def wtc_similarity(user_a, user_b) -> float:
    """
    Similarity function between two WTC user vectors:
    sim = (1 + avg(|user_a - user_b|))^-1
    :param user_a: WTC vector of user a
    :param user_b: WTC vector of user b
    :return: WTC similarity score in [0;1]
    """
    return 1 / (1 + np.mean(np.abs(user_a - user_b)))


def ls_similarity(user_a, user_b) -> float:
    """
    Similarity function between two LS user vectors:
    sim = intersection(user_a, user_b) / len(user_a)
    :param user_a: LS vector of user a
    :param user_b: LS vector of user b
    :return: LS similarity score in [0;1]
    """
    np.reshape(user_a, -1)
    np.reshape(user_b, -1)
    if user_a.shape[0] != user_b.shape[0]:
        logger = logging.getLogger('sbm')
        logger.error(f"LS similarity: invalid dimensions {user_a.shape} and {user_b.shape}")
        return np.nan
    return np.sum(np.equal(user_a, user_b)) / user_a.shape[0]


def pai_similarity(user_a: str, user_b: str) -> float:
    """
    Load PAI similarity score between user_a and user_b.
    :param user_a: username of user_a
    :param user_b: username of user_b
    :return: float similarity score
    """
    # calculate matches if necessary
    score = get_cached_similarity(user_a, user_b, 'pai')
    if score is None:
        score = np.nan
    return score


def get_cached_similarity(user_a, user_b, similarity_type) -> Union[float, None]:
    """
    Try to load similarity score between user_a and user_b from cache.
    :param user_a: username of user a
    :param user_b: username of user b
    :param similarity_type: str options: (wtc, ls, inst, pai)
    :return: cached similarity score or None if not cached
    """

    # look for similarity score in the database
    cached = []
    if similarity_type == 'wtc':
        cached = WTCSimilarity.objects.filter(Q(user_a__username=user_a, user_b__username=user_b)
                                              | Q(user_a__username=user_b, user_b__username=user_a))
    elif similarity_type == 'ls':
        cached = LSSimilarity.objects.filter(Q(user_a__username=user_a, user_b__username=user_b)
                                             | Q(user_a__username=user_b, user_b__username=user_a))
    elif similarity_type == 'inst':
        cached = INSTSimilarity.objects.filter(Q(user_a__username=user_a, user_b__username=user_b)
                                               | Q(user_a__username=user_b, user_b__username=user_a))
    elif similarity_type == 'pai':
        cached = PAISimilarity.objects.filter(Q(user_a__username=user_a, user_b__username=user_b)
                                              | Q(user_a__username=user_b, user_b__username=user_a))

    # If only one is found, everything is correct and that score can be returned.
    if len(cached) == 1:
        return cached[0].similarity
    # If more are found, something went wrong in the caching. Delete all of them to be sure.
    if len(cached) > 1:
        logger = logging.getLogger("sbm")
        logger.error("There are more than one similarity scores stored for the same pair of users. "
                     "Deleting all of them.")
        cached.delete()
    return None


def store_similarity(user_a, user_b, similarity_type, similarity) -> BidirectionalSimilarity:
    """
    Store similarity score between user_a and user_b in cache.
    :param user_a: username of user a
    :param user_b: username of user b
    :param similarity_type: str options: (wtc, ls, inst, pai)
    :param similarity: float similarity score
    :return: stored similarity object
    """
    if similarity_type not in ['wtc', 'ls', 'inst', 'pai']:
        raise ValueError("Unknown similarity type")

    if similarity_type == 'wtc' and get_cached_similarity(user_a, user_b, 'wtc') is None:
        return WTCSimilarity.objects.create(
            user_a=SmartUser.objects.get(username=user_a),
            user_b=SmartUser.objects.get(username=user_b),
            similarity=similarity
        )
    elif similarity_type == 'ls' and get_cached_similarity(user_a, user_b, 'ls') is None:
        return LSSimilarity.objects.create(
            user_a=SmartUser.objects.get(username=user_a),
            user_b=SmartUser.objects.get(username=user_b),
            similarity=similarity
        )
    elif similarity_type == 'inst' and get_cached_similarity(user_a, user_b, 'inst') is None:
        return INSTSimilarity.objects.create(
            user_a=SmartUser.objects.get(username=user_a),
            user_b=SmartUser.objects.get(username=user_b),
            similarity=similarity
        )
    elif similarity_type == 'pai' and get_cached_similarity(user_a, user_b, 'pai') is None:
        return INSTSimilarity.objects.create(
            user_a=SmartUser.objects.get(username=user_a),
            user_b=SmartUser.objects.get(username=user_b),
            similarity=similarity
        )


class MLModel(ABC):
    """
    This model represents  a base interface for ML models which will be used to adapt the influence weights of the data
    sources in our matching procedure.
    """

    save_dir = os.path.join('sbm', 'static', 'sbm', 'models')

    @abstractmethod
    def __call__(self, x):
        """
        Calculate the combined similarity score out of the partial similarity scores respecting the partner preferences.
        :param x: numpy.ndarray containing the partial scores and the answes to the partner preferences
            question in the form (wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp).
        :return: combined score
        """
        pass

    @abstractmethod
    def train(self, ds):
        """
        Initially adapt the model to the given dataset.
        :param ds: pd.DataFrame with the columns (wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, score).
            score in this case is matching goodness perceived by the user, i.e. our label.
        """
        pass

    @abstractmethod
    def finetune(self, old_ds, new_ds):
        """
        Finetune model with a small amount of new data.
        :param ds: pd.DataFrame with the columns (wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, score).
            score in this case is matching goodness perceived by the user, i.e. our label..
        """
        pass

    @abstractmethod
    def save_model(self, save_path=None):
        """
        Store the model to given path.
        :param save_path: Path where to store the model. If None, the path will be generated automatically.
            See MLModel.build_save_path.
        """
        pass

    @abstractmethod
    def load_model(self, model_path=None):
        """
        Load a model from a given path.
        :param model_path: If None the latest file in MLModel.save_dir will be chosen.
            See MLModel.build_save_path.
        """
        pass

    def build_save_path(self):
        """
        Build save_path automatically. The default storage type is pkl. To change this override this method.
        :return: str save path
        """
        return os.path.join(
            MLModel.save_dir,
            f"{timezone.now().strftime('%d-%m-%y')}_{self.__class__.__name__}_model.pkl"
        )

    def validate_model_path(self, model_path):
        """
        Validate the model_path.
        """
        if not os.path.exists(model_path):
            logger = logging.getLogger("sbm")
            logger.error(f"{self.__class__.__name__}: Model path {model_path} does not exist.")
            return False
        return True


class DefaultModel(MLModel):
    """
    This model is the default model which is used if no other model is specified. The strategy of this model is to
    combine the partial similarity scores and give the partial scores which are preferred more influence.
    """

    def __call__(self, x):
        # high values mean more similarity
        # WTC   LS    INST  PAI
        # [0;1] [0;1] [0;1] [0;1]
        # preferences applied
        # idea for applying the preferences:
        # average over the data sources which are preferred and add the result
        #
        # PREF = avg([LS, WTC])  (example for the case when LS and WTC are preferred)
        # X = avg([WTC, LS, INST, PAI, PREF)

        x = x.reshape((-1, 8))
        scores = np.nan_to_num(x[:, :4], nan=.5)
        user_prefs = x[:, 4:]
        preferences = user_prefs > 0
        # Where user has no values, set it as if they have all preferences
        no_prefs = np.logical_not(np.any(preferences, axis=1))
        preferences[no_prefs] = np.logical_not(preferences[no_prefs])

        pref_score = np.mean(scores, axis=1, where=preferences)
        pref_score = np.reshape(pref_score, (-1, 1))
        # Add pref score to the other scores
        scores = np.concatenate((scores, pref_score), axis=1)
        # Calculate final score by averaging
        final_scores = np.mean(scores, axis=1)
        if len(x) == 1:
            final_scores = final_scores[0]
        return final_scores

    def train(self, ds):
        pass

    def finetune(self, old_ds, new_ds):
        pass

    def save_model(self, save_path=None):
        return True

    def load_model(self, model_path=None):
        return True


class MatchNet(MLModel):
    """
    Wrapper class for an artificial neural network modelling the combined similarity function that calculates a
    combined similarity out of partial similarities.
    """

    def __init__(self, name='match_net'):
        # build a keras model
        self.model = keras.Sequential([
            keras.layers.Input(8),
            keras.layers.Dense(4, activation="relu", name="hidden"),
            keras.layers.Dense(1, activation="sigmoid", name="output")
        ], name)

    def __call__(self, x):
        x = x.reshape((-1, 8))
        # remove nan values
        x = np.nan_to_num(x, nan=.5)
        # ensure correct shape
        if len(x.shape) != 2:
            x = np.reshape(x, (1, -1))
        out = self.model(x).numpy().reshape(-1)
        if len(x) == 1:
            out = out[0]
        # return single value
        return out

    def _fit_model(self, ds, learning_rate):
        # separate data and labels
        y = ds.user_score.values.reshape((-1, 1))
        ds = ds.drop(columns=['user_score'])
        x = ds.values

        # choose optimizer and loss function and compile model
        optim = keras.optimizers.Adam(learning_rate=learning_rate)
        self.model.compile(loss='mse', optimizer=optim)

        # determine fitting batch size
        batch_size = 32
        while len(ds) < batch_size * 20:
            batch_size /= 2
        batch_size = np.max([int(batch_size), 1])

        # train model
        history = self.model.fit(x, y, batch_size=batch_size, validation_split=.1, epochs=10)

        # return training history
        return history.history

    def train(self, ds):
        self._fit_model(ds, .001)

    def finetune(self, old_ds, new_ds):
        self._fit_model(new_ds, .0001)

    def build_save_path(self):
        path = super().build_save_path()
        return os.path.splitext(path)[0]

    def save_model(self, save_path=None):
        if save_path is None:
            save_path = self.build_save_path()
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        self.model.save(save_path)
        return os.path.isdir(save_path)

    def validate_model_path(self, model_path):
        if not os.path.isdir(model_path):
            logger = logging.getLogger("sbm")
            logger.error(f"{self.__class__.__name__}: Model path {model_path} is not a directory.")
            return False
        return True

    def load_model(self, model_path=None):
        if model_path is None:
            model_path = self.build_save_path()
        if self.validate_model_path(model_path):
            self.model = keras.models.load_model(model_path)
            return True
        return False


class GeneticModel(MLModel):

    def __init__(self):
        self.weights = [random.random() for i in range(5)]
        self.weights = scipy.special.softmax(self.weights)

    """
    this model should learn the weights for the similarity scores based on the labels
    """

    def create_pop(self, pop_size) -> list:
        """
        creates population of a given size
        use this function if no weights are already calculated beforehand
        :param pop_size: int; defines the population size
        :return pop: list; list of created individuals
        """
        pop = []
        for i in range(pop_size - 1):
            individual = []
            for x in range(5):
                individual.append(random.random())

            # normalize individual
            individual = scipy.special.softmax(individual)
            pop.append(individual)
        pop.append(self.weights)
        return pop

    def evaluate_fitness(self, score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, ind):
        """
        Evaluates the fitness of an individual.
        :param score: float; score of the individual
        :param : int (1,5); feedback from matching by human being
        :param wtc: float (0,1); wtc similarity score computed by our algorithm
        :param ls: float (0,1); ls similarity score computed by our algorithm
        :param inst: float (0,1); institute similarity score computed by our algorithm
        :param pai: float (0,1); pai similarity score computed by our algorithm
        :param wtc_pp: wtc preferred
        :param ls_pp: ls preferred
        :param inst_pp: inst preferred
        :param pai_pp: pai preferred
        :param ind: list; individual that should be evaluated
        :return: float; loss_function/fitness value for this individual
        """
        pref = []
        if wtc_pp > 0:
            pref.append(wtc)
        if ls_pp > 0:
            pref.append(ls)
        if inst_pp > 0:
            pref.append(inst)
        if pai_pp > 0:
            pref.append(pai)
        if wtc_pp == ls_pp == inst_pp == pai_pp:
            pref = [wtc, ls, inst, pai]

        combined_score = ind[0] * wtc + ind[1] * ls + ind[2] * inst + ind[3] * pai
        if len(pref) > 0:
            combined_score += ind[4] * np.mean(pref)
        return mean_squared_error([score], [combined_score])

    def selection_parents(self, pop, score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp):
        """
        param
        pop: list; list of possible candidate parents

        return
        pop_new: list; new population

        compute the two best individuals from the population
        compute the worst two individuals

        remove the worst two individuals
        reproduce the two best individuals

        """

        evaluated_individuals = [self.evaluate_fitness(score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, ind)
                                 for ind in pop]

        sorted_evaluated = np.argsort(evaluated_individuals)
        best_ones = sorted_evaluated[:2]
        best_ones = pop[best_ones[0]], pop[best_ones[1]]

        worst_ones = sorted_evaluated[-2:]
        worst_ones = pop[worst_ones[0]], pop[worst_ones[1]]

        pop = np.array(pop)
        np.delete(pop, np.argwhere(pop == worst_ones[0]))
        np.delete(pop, np.argwhere(pop == worst_ones[1]))

        pop = pop.tolist()
        reproduced1 = self.reproduce(best_ones[0], best_ones[1])
        reproduced2 = self.reproduce(best_ones[1], best_ones[0])

        pop.append(reproduced1)
        pop.append(reproduced2)

        return pop

    def reproduce(self, p1, p2):
        """
        performs one point crossover

        param
        p1: list; represents the first parent
        p2: list; represent

        return
        individual: list; new child

        """
        data = []
        child = np.zeros(len(p1))
        for i in range(5):
            data.append(random.random())
        for ran in range(len(data)):
            if data[ran] > 0.5:
                child[ran] = p1[ran]
            else:
                child[ran] = p2[ran]

        individual = scipy.special.softmax(child)
        return individual

    def mutation(self, individual):
        """
        mutates the given individual

        param:
        individual: list; list representing the individual that should mutate

        return:
        return mutated and normalized individual
        """
        rand_int = random.randint(5)
        individual[rand_int] = random.random()

        individual = scipy.special.softmax(individual)

        return individual

    def __call__(self, x):
        """
        compute the combined similarity out of the given similarity scores and the preference
        """
        x = x.reshape((-1, 8))
        x = np.nan_to_num(x, nan=.5)
        user_prefs = x[:, 4:]
        scores = x[:, :4]
        pref_args = user_prefs > 0
        # Where user has no values, set it as if they have all preferences
        no_prefs = np.logical_not(np.any(pref_args, axis=1))
        pref_args[no_prefs] = np.logical_not(pref_args[no_prefs])
        pref_score = np.mean(scores, axis=1, where=pref_args)
        pref_score = np.reshape(pref_score, (-1, 1))
        # Add pref score to the other scores
        scores = np.concatenate((scores, pref_score), axis=1)
        weighted_scores = scores * self.weights
        # Calculate final score by averaging
        final_scores = np.mean(scores, axis=1)
        if len(x) == 1:
            final_scores = final_scores[0]
        return final_scores

    def train(self, ds):
        """
        finetune the model with a smaller set of data, take also into account the previous weights
        """
        columns = ds.columns
        best_one_data_set = []
        for i, row in ds.iterrows():
            wtc = row[0]  # wtc=row[columns[0]]
            ls = row[1]
            inst = row[2]
            pai = row[3]
            wtc_pp = row[4]
            ls_pp = row[5]
            inst_pp = row[6]
            pai_pp = row[7]
            score = row[8]

            pop = self.create_pop(500)
            curr_best = 0  # stores best score
            old_best = 20

            best_one_overall = []
            for epoch in range(100):

                # parents tournament
                pop_new = self.selection_parents(pop, score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp)

                # mutate individuals
                for rand_ind in pop_new:
                    mutate_or_not = random.choices([0, 1], weights=[0.998, 0.002])
                    if mutate_or_not == 1:
                        pop_new.remove(rand_ind)
                        pop.append(self.mutation(rand_ind))

            # compute best individual in pop_new
            curr_best = [self.evaluate_fitness(score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, ind) for ind
                         in pop_new]

            best_ls = np.argsort(curr_best)
            best_one = pop_new[best_ls[0]]
            best_one_data_set.append(best_one)
            curr_best_score = max(curr_best)

            # add the last best and best one up and weight them
            if len(best_one_data_set) > 1:
                best_one_last, best_one = best_one_data_set[-2:]
                best_one_last = [i * 0.8 for i in best_one_last]
                best_one = [i * 0.2 for i in best_one]
                for i in range(0, len(best_one)):
                    best_one_overall.append(best_one_last[i] + best_one[i])
                best_one_data_set.append(best_one_overall)
            else:
                best_one_data_set.append(best_one)
                best_one_overall = best_one

        self.weights = best_one_overall

    def finetune(self, new_ds, old_ds):
        """
        finetune the model with a smaller set of data, take also into account the previous weights
        """
        columns = new_ds.columns
        best_one_data_set = []
        for i, row in new_ds.iterrows():
            wtc = row[0]  # wtc=row[columns[0]]
            ls = row[1]
            inst = row[2]
            pai = row[3]
            wtc_pp = row[4]
            ls_pp = row[5]
            inst_pp = row[6]
            pai_pp = row[7]
            score = row[8]

            pop = self.create_pop(500)
            curr_best = 0  # stores best score
            old_best = 20

            best_one_overall = []
            for epoch in range(100):

                # parents tournament
                pop_new = self.selection_parents(pop, score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp)

                # mutate individuals
                for rand_ind in pop_new:
                    mutate_or_not = random.choices([0, 1], weights=[0.998, 0.002])
                    if mutate_or_not == 1:
                        pop_new.remove(rand_ind)
                        pop.append(self.mutation(rand_ind))

            # compute best individual in pop_new
            curr_best = [self.evaluate_fitness(score, wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, ind) for ind
                         in pop_new]

            best_ls = np.argsort(curr_best)
            best_one = pop_new[best_ls[0]]
            best_one_data_set.append(best_one)
            curr_best_score = max(curr_best)

            # add the last best and best one up and weight them
            if len(best_one_data_set) > 1:
                best_one_last, best_one = best_one_data_set[-2:]
                best_one_last = [i * 0.9 for i in best_one_last]
                best_one = [i * 0.1 for i in best_one]
                for i in range(0, len(best_one)):
                    best_one_overall.append(best_one_last[i] + best_one[i])
                best_one_data_set.append(best_one_overall)
            else:
                best_one_data_set.append(best_one)
                best_one_overall = best_one

        self.weights = best_one_overall

    def validate_model_path(self, model_path):
        if not os.path.isdir(model_path):
            logger = logging.getLogger("sbm")
            logger.error(f"{self.__class__.__name__}: Model path {model_path} is not a directory.")
            return False
        return True

    def save_model(self, save_path=None):
        """
        stores model in a given path
        """
        if save_path is None:
            save_path = self.build_save_path()

        os.makedirs(os.path.dirname(save_path), exist_ok=True)
        with open(save_path, 'wb') as f:
            pickle.dump(self.weights, f)
        return os.path.isdir(save_path)

    def load_model(self, model_path=None):
        """
        load model from a given path
        """
        if model_path is None:
            model_path = self.build_save_path()
        if self.validate_model_path(model_path):
            with open(model_path + '/gen_model', 'rb') as f:
                self.weights = pickle.load(f)
                return True
        return False


class ClassicalRegression(MLModel):
    """
    Classical regression model. This model uses a classical regression algorithm to predict the similarity.
    """

    def __init__(self, num_labels=5, C=1.0):
        self.models = [None for _ in range(15)]
        self.C = C
        self.max_label = num_labels - 1

    def __call__(self, x):
        x = x.reshape((-1, 8))
        results = []
        x = pd.DataFrame(x,
                         columns=['wtc', 'ls', 'inst', 'pai', 'wtc_pp', 'ls_pp', 'inst_pp', 'pai_pp'])
        train_data, orig_indices = self._preprocess(x)
        for i in np.arange(15):
            train_datum = train_data[i]
            if train_datum.shape[0] > 0:
                model = self.models[i]
                # Check if model has been fit for this combination
                if model:
                    out = self.models[i].predict_proba(train_datum)
                    # Calculate weighted sum of class labels
                    out = np.sum(out * model.classes_, axis=1)
                    # Normalize again to output score between 0 and 1
                    out /= self.max_label
                    results.append(out)
                else:
                    # Return 0.5 if there exist no submodel for this input
                    dummy_out = np.empty(train_datum.shape[0])
                    dummy_out[:] = 0.5
                    results.append(dummy_out)
                    logging.getLogger("sbm")
                    logger.error(f"{self.__class__.__name__}: No trained model for i = {i}")
        results = np.concatenate(results, axis=None)
        orig_indices = np.concatenate(orig_indices)
        # Return results in original order
        results = results[np.argsort(orig_indices)]
        if len(results) == 1:
            results = results[0]
        return results

    def _split_data(self, ds):
        orig_indices = []
        train_data = []
        for wtc_pp in np.arange(2):
            for ls_pp in np.arange(2):
                for inst_pp in np.arange(2):
                    for pai_pp in np.arange(2):
                        # based on each of the 16 (actually 15) condition combinations for pp,
                        # we want to train a linear regression model for each.
                        data = ds[(ds["wtc_pp"] == wtc_pp) & (ds["ls_pp"] == ls_pp)
                                  & (ds["inst_pp"] == inst_pp) & (ds["pai_pp"] == pai_pp)]
                        # Add original indices to reconstruct later
                        orig_indices.append(data.index)
                        # Drop the pps and index as they don't get used during fitting or calling
                        data = data.drop(columns=["wtc_pp", "ls_pp", "inst_pp", "pai_pp"])
                        train_data.append(data)
        # As having no preferences and having all preferences is equal, concat them
        train_data[0] = pd.concat([train_data[0], train_data.pop(-1)])
        orig_indices[0] = orig_indices[0].append(orig_indices.pop(-1))
        return train_data, orig_indices

    def _preprocess(self, ds):
        # Map scores from float to ints
        if "user_score" in ds.columns:
            ds["user_score"] *= self.max_label
            ds["user_score"] = ds["user_score"].astype(np.int16)
        data, orig_indices = self._split_data(ds)
        return data, orig_indices

    def train(self, ds):
        train_data, _ = self._preprocess(ds)

        for i in np.arange(15):
            train_datum = train_data[i]
            if train_datum.shape[0] > 0:
                if train_datum["user_score"].nunique() < 5:
                    logger = logging.getLogger("sbm")
                    logger.debug(f"{self.__class__.__name__}: For a particular pp combination, not all scores were "
                                 f"covered, specifically for i = {i}")
                else:
                    model = LogisticRegression(C=self.C, multi_class="multinomial")
                    model.fit(train_datum.drop(columns=["user_score"]), train_datum["user_score"])
                    self.models[i] = model
            else:
                logger = logging.getLogger("sbm")
                logger.debug(f"{self.__class__.__name__}: For training, not all pp combinations were covered, "
                             f"specifically for i = {i}")

    def finetune(self, old_ds, new_ds):
        self.train(pd.concat([old_ds, new_ds]))

    def build_save_path(self):
        return super().build_save_path()[:-4]

    def save_model(self, save_dir=None):
        if save_dir is None:
            save_dir = self.build_save_path()
        for i, model in enumerate(self.models):
            os.makedirs(save_dir, exist_ok=True)
            with open(os.path.join(save_dir, "lin_reg_" + str(i)) + ".pkl", "wb") as save_file:
                pickle.dump(model, save_file)
        return os.path.isdir(save_dir)

    def validate_model_path(self, model_path):
        if not os.path.isdir(model_path):
            logger = logging.getLogger("sbm")
            logger.error(f"{self.__class__.__name__}: Model path {model_path} is not a directory.")
            return False
        return True

    def load_model(self, model_dir=None):
        if model_dir is None:
            model_dir = self.build_save_path()
        if self.validate_model_path(model_dir):
            for i in range(len(self.models)):
                with open(os.path.join(model_dir, "lin_reg_" + str(i)) + ".pkl", "rb") as model_file:
                    self.models[i] = pickle.load(model_file)
            return True
        return False


def get_prefs_vec(user: SmartUser) -> np.ndarray:
    """
    Retrieve user's preferences.
    user_prefs is a bool array representing the answers of the user in the partner preferences question.
    The representation is in that order: (wtc, ls, inst, pai)
    :param user: SmartUser object
    :return: np.ndarray of user's preferences
    """

    pp_answer = PartnerPreferencesAnswers.objects.get(user_questionnaire__user=user)

    user_prefs = np.zeros(4)
    partner = pp_answer.get_partner_list()
    if '4' in partner:
        # no preferences
        return user_prefs
    if '1' in partner:
        # wtc is important
        user_prefs[0] = 1
    if '2' in partner:
        # ls is important
        user_prefs[1] = 1
    if '3' in partner:
        # inst is important
        user_prefs[2] = 1
    if '0' in partner:
        # pai is important
        user_prefs[3] = 1
    return user_prefs


def get_matches(username,
                top_n=5,
                institute_threshold=.2,
                use_course_filter=True,
                model=None,
                full_df=False) -> pd.DataFrame:
    """
    Returns the unidirectional matching scores for the user with the given username.
    :param username: username of the user to calculate the matches for.
    :param top_n: Number of top matches to return.
    :param institute_threshold: Candidates with a lower course-share in the institute given by the user in
        Partner Preferences will be filtered.
    :param use_course_filter: If true, candidates who have not taken the course given by the user in
        Partner Preferences will be filtered.
    :param model: str Specify the technique to calculate the matching score.
        Options are: 'standard', 'ann', 'regression', 'genetic'
    :param full_df: If True, the complete dataframe with all similarities is returned ignoring top_n.
    :return: [pd.Series|pd.DataFrame]
    """
    user = SmartUser.objects.get(username=username)
    ap = AnswerProjector()
    pp_answer = PartnerPreferencesAnswers.objects.get(user_questionnaire__user=user)
    user_wtc_vec = ap.get_wtc_vector(user.username)
    user_ls_vec = ap.get_ls_vector(user.username)
    user_inst_vec = ap.get_top_n_institutes(user.username)

    # trigger PAI similarity calculation
    pipe = NLPPipeline()
    pipe()
    del pipe

    candidate_usernames = list(
        UserMatchingStatus.objects.filter(
            # exclude staff
            user__is_staff=False
        ).exclude(
            # only consider users that want to be recommended
            status=UserMatchingStatus.INACTIVE
        ).exclude(
            # only consider users that fullfil requirements to be matched
            status=UserMatchingStatus.UNREADY
        ).exclude(
            # remove the current user
            user__username=user.username
        ).values_list(
            # only return the usernames
            'user__username',
            flat=True
        ).distinct()
    )

    # apply hard filters

    ## filter by the course the candidates have to have taken
    if use_course_filter and pp_answer.course:
        candidate_enrollments = CourseEnrollment.objects.filter(course__title=pp_answer.course)
        enrollment_usernames = set(candidate_enrollments.values_list('user__username', flat=True).distinct())
        candidate_usernames = list(set(candidate_usernames).intersection(enrollment_usernames))

    ## filter by the institute the candidates have to be experienced in
    if pp_answer.institute:
        candidate_inst_share_df = ap.get_inst_share_df(candidate_usernames)
        preferred_institute = pp_answer.institute
        candidate_inst_share_df = candidate_inst_share_df[candidate_inst_share_df[preferred_institute]
                                                          >= institute_threshold]
        candidate_usernames = list(candidate_inst_share_df.index)

    # calculate partial similarity scores
    similarity_df = pd.DataFrame(columns=['username', 'wtc', 'ls', 'inst', 'pai', 'combined'])

    if len(candidate_usernames) == 0:
        return similarity_df

    similarity_df.username = candidate_usernames

    user_prefs = get_prefs_vec(user)

    # If no model was provided, use the DefaultModel
    if not model:
        model = DefaultModel()

    def add_similarities(x):

        # WTC
        x['wtc'] = get_cached_similarity(username, x['username'], 'wtc')
        if x['wtc'] is None:
            candidate_wtc_vec = ap.get_wtc_vector(x['username'])
            x['wtc'] = wtc_similarity(user_wtc_vec, candidate_wtc_vec)
            if not np.isnan(x['wtc']):
                store_similarity(username, x['username'], 'wtc', x['wtc'])

        # LS
        x['ls'] = get_cached_similarity(username, x['username'], 'ls')
        if x['ls'] is None:
            candidate_ls_vec = ap.get_ls_vector(x['username'])
            x['ls'] = ls_similarity(user_ls_vec, candidate_ls_vec)
            if not np.isnan(x['ls']):
                store_similarity(username, x['username'], 'ls', x['ls'])

        # Institute
        x['inst'] = get_cached_similarity(username, x['username'], 'inst')
        if x['inst'] is None:
            candidate_inst_vec = ap.get_top_n_institutes(x.username)
            x['inst'] = inst_similarity(user_inst_vec, candidate_inst_vec)
            if not np.isnan(x['inst']):
                store_similarity(username, x['username'], 'inst', x['inst'])

        # PAI
        x['pai'] = pai_similarity(username, x['username'])

        # prepare vector to be fed into the similarity model
        vec = x.drop(['username', 'combined']).values.astype(np.float)
        vec = np.concatenate([vec, user_prefs])

        x['combined'] = vec

        return x

    similarity_df = similarity_df.apply(add_similarities, axis=1)
    similarity_df['combined'] = model(np.stack(similarity_df['combined'].values))
    similarity_df.set_index('username', inplace=True)

    if not full_df:
        # only return top n candidates
        similarity_df = similarity_df.combined.nlargest(top_n)

    return similarity_df


def store_matches(protagonist: SmartUser, matches: pd.Series) -> QuerySet[StudyBuddy]:
    """
    Store matches calculated with the get_matches method.
    :param protagonist: The user for whom the matches where calculated.
    :param matches: The calculated matches.
    :return QuerySet: The matches that were stored.
    """
    db_matches = []
    if len(matches) == 0:
        return StudyBuddy.objects.none()
    for username, score in matches.items():
        match = SmartUser.objects.get(username=username)
        db_match, created = StudyBuddy.objects.update_or_create(
            user=protagonist,
            match=match,
            defaults={
                'score': score
            }
        )
        db_matches.append(db_match.pk)
    return StudyBuddy.objects.filter(pk__in=db_matches)


def store_samples(protagonist: SmartUser) -> QuerySet[MatchSample]:
    """
    Store anonymous samples of the matches the given user has rated.
    :param protagonist: The user for whom the samples are stored.
    :return: The stored samples.
    """
    feedbacks = FeedbackSBM.objects.filter(studybuddy__user=protagonist)
    user_prefs = get_prefs_vec(protagonist)
    samples = []

    for fb in feedbacks:
        wtc = get_cached_similarity(protagonist.username, fb.studybuddy.match.username, 'wtc')
        ls = get_cached_similarity(protagonist.username, fb.studybuddy.match.username, 'ls')
        inst = get_cached_similarity(protagonist.username, fb.studybuddy.match.username, 'inst')
        pai = get_cached_similarity(protagonist.username, fb.studybuddy.match.username, 'pai')

        if None in [wtc, ls, inst, pai]:
            continue

        sample = MatchSample.objects.update_or_create(
            identifier=MatchSample.calculate_identifier(protagonist, fb.studybuddy.match),
            defaults={
                'wtc': wtc,
                'ls': ls,
                'inst': inst,
                'pai': pai,
                'wtc_pp': user_prefs[0],
                'ls_pp': user_prefs[1],
                'inst_pp': user_prefs[2],
                'pai_pp': user_prefs[3],
                'user_score': fb.rating
            }
        )[0]
        samples.append(sample.pk)

    return MatchSample.objects.filter(pk__in=samples)


def store_all_samples() -> QuerySet[MatchSample]:
    """
    Store all anonymous samples of all matches.
    :return: The stored samples.
    """
    users = SmartUser.objects.all()
    samples = []

    for user in users:
        samples.extend(store_samples(user).values_list('pk', flat=True).distinct())

    return MatchSample.objects.filter(pk__in=samples)


def db_to_df_matching(min_time=None,
                      max_time=None,
                      labeled=True) -> pd.DataFrame:
    """
    Loading MatchSample objects from the database and converting them into a pd.DataFrame.
        Note that the labels are not normalized yet!
    :return: pd.DataFrame made from a MatchSample query with the columns
        [wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, user_score, mkdate] and the MatchSample.identifier
        as a row index.
    """
    query = Q()
    if min_time is not None:
        query = query & Q(mkdate__gte=min_time)
    if max_time is not None:
        query = query & Q(mkdate__lte=max_time)
    if labeled:
        query = query & Q(user_score__isnull=False)

    queryset = MatchSample.objects.filter(query)

    df = read_frame(queryset)
    df.drop(columns=['id'], inplace=True)
    df.set_index('identifier', inplace=True)

    return df


def compare_models(ds, n_splits, metric, higher_is_better):
    """
    Compare all existing models with each other on the whole dataset using cross-validation.

    :param ds: pd.DataFrame with the columns (wtc, ls, inst, pai, wtc_pp, ls_pp, inst_pp, pai_pp, user_score).
            score in this case is matching goodness perceived by the user, i.e. our label.
    :param n_splits: number of splits for KFold cross-validation
    :param metric: Metric function according to which we measure error between label and predicted values.
                   Make sure that lower metric values mean better.
    :param higher_is_better: True if higher metric value means a model performed better,
                    False if lower metric value is better
    :return: ordered list of couples of class name and the corresponding model's avg metric value, from best
             performing model to worst performing model.
    """
    ml_model_names = [ml_model_class.__name__ for ml_model_class in MLModel.__subclasses__()]
    kf = KFold(n_splits=n_splits, shuffle=True)
    model_to_metric = dict(zip(ml_model_names, [[] for _ in ml_model_names]))
    for train_index, test_index in kf.split(ds):
        # Collect for each split for each model the average metric value
        for ml_model_name in ml_model_names:
            train_set = ds.iloc[train_index]
            test_set = ds.iloc[test_index]
            ml_model = eval(ml_model_name + "()")
            ml_model.train(train_set)
            preds = ml_model(np.array(test_set.drop(columns="user_score")))
            labels = np.array(test_set["user_score"])
            avg_error = metric(preds, labels)
            model_to_metric[ml_model_name].append(np.mean(avg_error))
    # Store the total average metric values
    for ml_model_name in ml_model_names:
        model_to_metric[ml_model_name] = np.mean(model_to_metric[ml_model_name])
    # Replace nans with worst values
    for k, v in model_to_metric.items():
        if np.isnan(v):
            if higher_is_better:
                model_to_metric[k] = 0
            else:
                model_to_metric[k] = 1
    # Sort according to if higher is better or lower is better
    model_to_metric = sorted(model_to_metric.items(), key=lambda item: item[1])
    if higher_is_better:
        model_to_metric = reversed(model_to_metric)
    return model_to_metric


def update_model(ds, last_update, next_compare, next_finetune, calc_next_compare, calc_next_finetune,
                 last_model_class, last_model_load_path, compare_models_args):
    """
    Method to (potentially) update our matching model. We either compare all models to each other based on their
    performance and pick the best one as our new matching model or just finetune our latest model.
    :param ds: Dataset as described in the abstract class MLModel's train method's doc string.
    :param last_update: pandas.Timestamp object for when the last comparison or finetune took place. Used to
                        differentiate between old and new data
    :param next_compare: At what size of ds the next comparison of models should take place
    :param next_finetune: At what size of ds the next finetune of the last model should take place
    :param calc_next_compare: Function to calc when we should compare the models next time
    :param calc_next_finetune: Function to calc when we should compare the models next time
    :param last_model_class: Name of the last used model's class name
    :param last_model_load_path: Load path of the last used model's state
    :param compare_models_args: dict for arguments to pass to compare_models except for new_ds
    """
    COLUMNS = ["wtc", "ls", "inst", "pai", "wtc_pp", "ls_pp", "inst_pp", "pai_pp", "user_score"]
    # Convert some columns to float to avoid errors
    ds[COLUMNS] = ds[COLUMNS].applymap(lambda x: float(x))
    tz = ds["mkdate"].iloc[0].tz
    ds_len = len(ds)
    # Placeholders for data that we want to save
    data_to_save = {}
    data_to_save["next_compare"] = next_compare
    data_to_save["next_finetune"] = next_finetune
    data_to_save["last_model_class"] = last_model_class
    data_to_save["last_model_load_path"] = last_model_load_path
    new_model = None
    if ds_len >= next_compare:
        # Make sure order of columns is correct and drop unnecessary columns
        ds = ds[COLUMNS]
        compare_models_args["ds"] = ds
        model_to_metric = compare_models(**compare_models_args)
        # Assign the best model
        new_model_class = model_to_metric[0][0]
        # Train best model from scratch
        new_model = eval(new_model_class + "()")
        new_model.train(ds)
        # Save data we want to save
        data_to_save["last_model_class"] = new_model_class
        data_to_save["last_model_load_path"] = None
        data_to_save["next_compare"] = calc_next_compare(ds_len)
        data_to_save["next_finetune"] = calc_next_finetune(ds_len)
    elif ds_len >= next_finetune:
        # Save data we want to save
        data_to_save["next_finetune"] = calc_next_finetune(ds_len)
        data_to_save["last_model_load_path"] = None
        new_model = eval(last_model_class + "()")
        new_model.load_model(last_model_load_path)
        # Filter new and old data
        new_ds = ds[ds["mkdate"] >= last_update]
        new_ds = new_ds[COLUMNS]
        old_ds = ds[ds["mkdate"] < last_update]
        old_ds = old_ds[COLUMNS]
        # Finetune
        new_model.finetune(old_ds, new_ds)
    # Save data if we did training or fineutning
    if ds_len >= next_compare or ds_len >= next_finetune:
        # Save model
        new_model.save_model(data_to_save["last_model_load_path"])
        # Save other data in json
        data_to_save["last_update"] = datetime.datetime.now(tz).isoformat()
        os.makedirs(os.path.dirname(UPDATE_MODEL_PATH), exist_ok=True)
        with open(UPDATE_MODEL_PATH, "w") as f:
            json.dump(data_to_save, f)


def scheduled_update_matching_model():
    # Get data from database
    data = MatchSample.objects.all().values()
    data = pd.DataFrame.from_records(data)

    # dict containing args for update_model
    update_model_args = {}
    # If update_model has ever been called, load some data acquired from update_model's last call
    if os.path.exists(UPDATE_MODEL_PATH):
        with open(UPDATE_MODEL_PATH, "r") as f:
            update_model_args = json.load(f)
            update_model_args["last_update"] = datetime.datetime.fromisoformat(update_model_args["last_update"])
    else:
        update_model_args["last_update"] = datetime.datetime.now(data["mkdate"].iloc[0].tz)
        update_model_args["next_compare"] = 100
        update_model_args["next_finetune"] = 0
        update_model_args["last_model_class"] = "DefaultModel"
        update_model_args["last_model_load_path"] = None

    update_model_args["ds"] = data
    update_model_args["calc_next_compare"] = lambda x: 2 * x if x > 100 else 100
    update_model_args["calc_next_finetune"] = lambda x: ceil(x * 1.10) if x < 1000 else x + 100
    update_model_args["compare_models_args"] = {"n_splits": 10, "metric": mean_squared_error,
                                                "higher_is_better": False}

    # Call update_model
    update_model(**update_model_args)


# Only run this if no migrations are required
out = StringIO()
call_command("showmigrations", "--list", stdout=out)
if not "[ ]" in out.getvalue():
    # Schedule scheduled_update_matching_model if it hasn't been scheduled or queued.
    for schedule in Schedule.objects.all():
        if scheduled_update_matching_model.__name__ in schedule.func:
            break
    else:
        for queue in OrmQ.objects.all():
            if scheduled_update_matching_model.__name__ in queue.func():
                break
        # Only schedule scheduled_update_matching_model if it hasn't been scheduled or queued.
        else:
            # Set next_run as 00:00am UTC time of the next day
            next_run = datetime.datetime.now(tz=datetime.timezone.utc)
            next_run += datetime.timedelta(days=1)
            next_run = next_run.replace(hour=0, minute=0, second=0, microsecond=0)

            # Schedule updating models to run every day at 00:00am UTC
            Schedule.objects.create(name="Update Matching models", func='sbm.utils.matching.scheduled_update_matching_model', schedule_type="D",
                                    repeats=-1, next_run=next_run)
            # FAQ: For debugging, try the following to run the task every minute instead of every day:
            # Schedule.objects.create(func='sbm.utils.matching.scheduled_update_matching_model',
            # schedule_type="I", minutes=1, repeats=-1)
