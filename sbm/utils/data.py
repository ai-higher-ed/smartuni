from typing import Union
import django
import tensorflow_datasets as tfds
import django.db.utils
from django.db.models import QuerySet
import numpy as np
import random
import string
from sbm.models import *
from spc.models import Institution, Program, UserInstitutionProgram
from names_dataset import NameDataset
from tqdm import tqdm
from sbm.utils.wtc import UserWTC
from django_pandas.io import read_frame
import pandas as pd
import math


def load_pai_text(n):
    """
    Loads trec dataset from tensorflow datasets.
    :param n: number of records to load
    :return: pandas dataframe with n records
    """
    # Only load the amount of data needed
    percentage = math.ceil((n * 100) / 5452)
    # contains 500 data-points, if more data-points are needed load split = 'train' that 5452 data-points
    ds = tfds.load('trec', split=f'train[:{percentage}%]', shuffle_files=True, data_dir='./.tfds/trec')
    return ds


def get_random_string(length):
    """
    Generates a random string of length characters.
    :param length: length of the string
    :return: random string
    """
    return ''.join(np.random.choice(list(string.ascii_letters), size=length))


def create_random_data(n=100, percent_course_enrollments=0.05):
    """
    Create random data for the database.
    :param n: number of users to create
    :param percent_course_enrollments: share of courses a user will be enrolled to
    :return: queryset of created users
    """

    if n % 2 != 0:
        n += 1
        print(f"n will be made even. n = {n}")
    if n > 5452:
        print('You can only create 5452 data points at a time, n will default to 5452')
        n = 5452
    ds = load_pai_text(n)

    nd = NameDataset()
    first_names = nd.get_top_names(n=int(n / 2), country_alpha2='GB')
    first_names_pool = first_names['GB']['M'] + first_names['GB']['F']
    random.shuffle(first_names_pool)
    last_names_pool = nd.get_top_names(n=n, country_alpha2='GB', use_first_names=False)['GB']
    random.shuffle(last_names_pool)

    institutes = Institute.objects.all()
    courses = Course.objects.all()
    # synthetic users can only be enrolled to max 5% of the available courses
    max_enrollments = int(courses.count() * percent_course_enrollments)

    institutions = Institution.objects.all()
    programs = Program.objects.all()

    users = []

    for i, text in tqdm(zip(range(n), ds)):

        first_name = first_names_pool[i]
        last_name = last_names_pool[i]
        username = first_name[0].lower() + last_name.lower()

        try:
            user = SmartUser.objects.create(
                username=username,
                email=username + '@uos.com',
                first_name=first_name,
                last_name=last_name,
            )
        except django.db.utils.IntegrityError:
            username = first_name[0:2].lower() + last_name.lower()
            user = SmartUser.objects.create(
                username=username,
                email=username + '@uos.com',
                first_name=first_name,
                last_name=last_name,
            )

        for uip_idx in range(np.random.randint(1, 4)):
            institution = np.random.choice(institutions)
            program = np.random.choice(institution.programs.all())
            UserInstitutionProgram.objects.create(
                user=user,
                user_institution=institution,
                user_program=program,
            )

        users.append(user)

        # PAI

        pai_q = Questionnaire.objects.get(title='Personal Academic Interests')
        pai_uq = UserQuestionnaire.objects.create(questionnaire=pai_q, user=user)
        TextQuestionAnswer.objects.create(
            question=pai_q.get_questions()[0],
            user_questionnaire=pai_uq,
            answer=text['text'].numpy().decode("utf-8")
        )

        # LS
        ls_q = Questionnaire.objects.get(title='Learning Styles')
        ls_uq = UserQuestionnaire.objects.create(questionnaire=ls_q, user=user)
        ls_qa = MultipleChoiceQuestionAnswer.objects.create(
            question=ls_q.get_questions()[0],
            user_questionnaire=ls_uq
        )
        if np.random.rand() > .5:
            AnswerItem.objects.create(
                question_answer=ls_qa,
                answer_key=0
            )
        if np.random.rand() > .5:
            AnswerItem.objects.create(
                question_answer=ls_qa,
                answer_key=1
            )
        if np.random.rand() > .5:
            AnswerItem.objects.create(
                question_answer=ls_qa,
                answer_key=3
            )
        if np.random.rand() > .5:
            AnswerItem.objects.create(
                question_answer=ls_qa,
                answer_key=4
            )

        # WTC
        wtc_q = Questionnaire.objects.get(title='Willingness to Communicate')
        wtc_uq = UserQuestionnaire.objects.create(questionnaire=wtc_q, user=user)
        for question in wtc_q.get_questions():
            SingleChoiceQuestionAnswer.objects.create(
                user_questionnaire=wtc_uq,
                question=question,
                answer=int(np.random.rand() * 100)
            )

        # Course enrollments
        for j in range(np.random.randint(max_enrollments)):
            grade = (np.random.rand() * 5) if np.random.rand() > .5 else None
            CourseEnrollment.objects.get_or_create(
                user=user,
                course=courses[np.random.randint(courses.count())],
                grade=grade
            )

        # Partner Preferences
        pp_q = Questionnaire.objects.get(title='Partner Preferences')
        pp_uq = UserQuestionnaire.objects.create(questionnaire=pp_q, user=user)

        PartnerPreferencesAnswers.objects.create(
            user_questionnaire=pp_uq,
            institute=institutes[
                np.random.randint(institutes.count())].name if np.random.rand() > .3 else None,
            course=courses[np.random.randint(courses.count())].title if np.random.rand() > .3 else None,
            # choose up to 5 random answers without replacement
            partner=np.sort(np.random.choice(range(5), size=np.random.randint(6), replace=False)),
        )

        # create active matching status
        UserMatchingStatus.objects.create(user=user, status=UserMatchingStatus.ACTIVE)

    usernames = [user.username for user in users]
    users = SmartUser.objects.filter(username__in=usernames)

    return users


def assign_random_feedback(users: Union[QuerySet, SmartUser]) -> QuerySet:
    """
    Randomly rates match recommendations given to users.
    :param users: queryset of users in whose name feedback will be given.
    :return: queryset of FeedbackSBM objects.
    """
    if type(users) == SmartUser:
        users = [users]
    recs = StudyBuddy.objects.filter(user__in=users)
    fbs = []

    for rec in recs:
        fb = FeedbackSBM.objects.update_or_create(
            studybuddy=rec,
            defaults={
                'rating': np.random.randint(1, 6)
            }
        )[0]
        fbs.append(fb.pk)

    return FeedbackSBM.objects.filter(pk__in=fbs)


class AnswerProjector:
    """
    Manager class for projecting questionnaire answers into vectors, matrices and dataframes.
    """
    user_wtcs = {}
    ls_df = None
    ce_df = None
    course_df = None
    pai_df = None

    def __init__(self):
        # enumerate institutes by their alphabetical order
        inst_names = list(Institute.objects.values_list('name', flat=True).distinct())
        inst_names.sort()
        # save bidirectional mapping
        self.indexed_inst_names = dict(enumerate(inst_names))
        self.inst_indices = {y: x for x, y in self.indexed_inst_names.items()}

    def get_user_wtc(self, username) -> dict:
        """
        Returns an instance of the UserWTC helper class for calculating the WTC scores for a given user.
        :param username: The username of the user to get the WTC scores for.
        :return: An instance of the UserWTC helper class.
        """
        if username not in self.user_wtcs.keys():
            self.user_wtcs[username] = UserWTC(username)
        return self.user_wtcs[username]

    @staticmethod
    def _extend_ls_df(x) -> pd.Series:
        """
        Extends a Learning Styles DataFrame row with the answer text and the username.
        This function is only  meant for being used with pandas.DataFrame.apply on the LS DataFrame.
        :param x: A row of the LS DataFrame.
        :return: The row extended with the answer text and the username.
        """
        answer_item = AnswerItem.objects.get(id=x['id'])
        q = answer_item.question_answer.question

        x['answer_text'] = q.get_answer_text(answer_key=x['answer_key'])
        x['username'] = answer_item.question_answer.user_questionnaire.user.username
        return x

    def get_ls_df(self) -> pd.DataFrame:
        """
        :return: A pandas.DataFrame containing the answers to the LS questions for all users.
        """

        if self.ls_df is None:
            answer_items = AnswerItem.objects.filter(
                question_answer__user_questionnaire__questionnaire__title='Learning Styles'
            )
            self.ls_df = read_frame(answer_items).drop(columns=['question_answer'])
            self.ls_df = self.ls_df.apply(AnswerProjector._extend_ls_df, axis='columns')

        return self.ls_df

    def get_ce_df(self) -> pd.DataFrame:
        """
        :return: A pandas.DataFrame containing all course enrollments for all users.
        """
        if self.ce_df is None:
            ce_fieldnames = ['course__title', 'course__course_number', 'course__institute__name',
                             'user__username', 'grade']
            ce_fieldname_map = {
                'course__title': 'course',
                'course__course_number': 'course_number',
                'course__institute__name': 'institute',
                'user__username': 'username'
            }
            self.ce_df = read_frame(CourseEnrollment.objects.all(), fieldnames=ce_fieldnames)
            self.ce_df.rename(ce_fieldname_map, axis=1, inplace=True)
        return self.ce_df

    def get_course_df(self) -> pd.DataFrame:
        """
        :return: A pandas.DataFrame containing all courses.
        """
        if self.course_df is None:
            self.course_df = read_frame(
                Course.objects.all(),
                fieldnames=['course_number', 'institute__name']
            )
            self.course_df.rename({'institute__name': 'institute'}, axis=1, inplace=True)
        return self.course_df

    def get_pai_df(self) -> pd.DataFrame:
        """
        :return: A pandas.DataFrame containing all PAI answers for all users.
        """
        if self.pai_df is None:
            fieldnames = ['user_questionnaire__user__username', 'answer', 'id', 'question__title']
            fieldname_map = {
                'user_questionnaire__user__username': 'username',
                'question__title': 'question'
            }
            self.pai_df = read_frame(
                TextQuestionAnswer.objects.filter(
                    user_questionnaire__questionnaire__title="Personal Academic Interests"
                ),
                fieldnames=fieldnames
            )
            self.pai_df.rename(fieldname_map, axis=1, inplace=True)
        return self.pai_df

    def get_wtc_vector(self, username) -> pd.Series:
        """
        :param username: The username of the user.
        """
        try:
            user_wtc = self.get_user_wtc(username)
        except ValueError:
            # User has not taken that questionnaire yet
            ser = pd.Series(index=[
                'group_discussion',
                'meetings',
                'interpersonal',
                'public_speaking',
                'stranger',
                'acquaintance',
                'friend',
                'total'
            ], dtype='float32')
            ser[:] = np.nan
            return ser

        return pd.Series(
            {
                'group_discussion': user_wtc.get_group_discussion(),
                'meetings': user_wtc.get_meetings(),
                'interpersonal': user_wtc.get_interpersonal(),
                'public_speaking': user_wtc.get_public_speaking(),
                'stranger': user_wtc.get_stranger(),
                'acquaintance': user_wtc.get_acquaintance(),
                'friend': user_wtc.get_friend(),
                'total': user_wtc.get_total()
            }
        ) / 100

    def get_ls_vector(self, username) -> pd.Series:
        """
        Calculates the vector representation of the LS answers of a given user as a pandas.Series.
        :param username: The username of the user.
        :return: A pandas.Series containing the LS answers of the user.
        """
        ls_df = self.get_ls_df()
        ls_df = ls_df[ls_df['username'] == username]

        if len(ls_df) == 0:
            # User has not taken that questionnaire yet
            ser = pd.Series(index=['visual', 'kinesthetic', 'rw', 'auditory'], dtype='float32')
            ser[:] = np.nan
            return ser

        visual = len(ls_df[ls_df['answer_text'] == 'Visual learner'])
        kinesthetic = len(ls_df[ls_df['answer_text'] == 'Kinesthetic learner'])
        rw = len(ls_df[ls_df['answer_text'] == 'Reading/writing learner'])
        auditory = len(ls_df[ls_df['answer_text'] == 'Auditory learner'])

        return pd.Series({'visual': visual, 'kinesthetic': kinesthetic, 'rw': rw, 'auditory': auditory})

    def get_institute_vector(self, username) -> pd.Series:
        """
        Calculates the vector representation of the institute-wise number of course enrollments of a given user as a
        pandas.Series.
        :param username: The username of the user.
        :return: A pandas.Series containing the institute-wise number of course enrollments of the user.
        """
        ce_df = self.get_ce_df()

        inst_names = list(Institute.objects.all().values_list('name', flat=True).distinct())

        # create series with all indices
        user_institute_n = pd.Series(index=[inst_names], dtype='float32')
        user_institute_n.fillna(0, inplace=True, downcast=False)
        user_institute_n = user_institute_n.astype('float32')

        # count enrollments per institute
        institute_enrollments = ce_df[ce_df.username == username].groupby('institute').size()

        # fill actual series with counted values
        user_institute_n[institute_enrollments.index] = institute_enrollments.values

        # sort by institute name
        user_institute_n.sort_index(inplace=True)

        return user_institute_n

    def get_institute_share_vector(self, username) -> pd.Series:
        """
        Calculates the vector representation of the shares of courses a user is enrolled in an institute.
        :param username: The username of the user.
        :return: A pandas.Series containing the shares of courses a user is enrolled in an institute.
        """
        course_df = self.get_course_df()
        n_courses_total = course_df.groupby('institute').size().sort_index()
        inst_vec = self.get_institute_vector(username)
        # convert multiindex to single index
        inst_vec = inst_vec.reset_index().set_index('level_0')[0]
        return inst_vec / n_courses_total

    def get_top_n_institutes(self, username, n=3):
        """
        Returns the IDs of the institutes the given user has the highest share of enrollments in.
        :param username: Username of user of whose institutes we want to get.
        :param n: Number of top institutes to determine.
        :return: Return top n institutes' ids, from highest to lowest as an np.ndarray of length 0 to 3
        """
        inst_vec = self.get_institute_vector(username)

        inst_vec_greater_zero = inst_vec[inst_vec > 0]
        top_n = inst_vec_greater_zero.nlargest(n)
        top_indices = np.array([self.inst_indices[key[0]] for key, value in top_n.items()])

        return top_indices

    def get_user_vector(self, username, n_inst=3) -> np.ndarray:
        """
        Calculates the vector representation of a given user as a numpy.ndarray.
        :param username: Username of the given user.
        :param n_inst: Number of top institutes to be determined.
        :return: np.ndarray. First 8 dimensons hold the WTC scores, next 4 hold the binary LS answers, last n
            dimensions hold the top n institutes.
        """
        return np.concatenate([
            self.get_wtc_vector(username).values,
            self.get_ls_vector(username).values,
            self.get_top_n_institutes(username, n_inst)
        ], axis=0)

    def get_inst_df(self, usernames=None) -> pd.DataFrame:
        """
        Calculates the institute shares for all given users.
        :param usernames: List of usernames
        :return: pd.DataFrame containing all institute vectors
        """
        if usernames is None:
            # if no selected usernames are given, get all usernames
            usernames = list(SmartUser.objects.filter(is_staff=False).values_list('username', flat=True).distinct())

        inst_names = list(Institute.objects.values_list('name', flat=True).distinct())
        df = pd.DataFrame(columns=['username'] + inst_names)

        for username in tqdm(usernames):
            df.loc[len(df)] = [username] + list(self.get_institute_vector(username))
        df.set_index('username', inplace=True)

        return df

    def get_inst_share_df(self, usernames=None) -> pd.DataFrame:
        """
        Calculates the institute shares for all given users.
        :param usernames: List of usernames
        :return: pd.DataFrame containing all institute shares
        """
        if usernames is None:
            # if no selected usernames are given, get all usernames
            usernames = list(SmartUser.objects.filter(is_staff=False).values_list('username', flat=True).distinct())

        inst_names = list(Institute.objects.values_list('name', flat=True).distinct())
        df = pd.DataFrame(columns=['username'] + inst_names)

        for username in tqdm(usernames):
            df.loc[len(df)] = [username] + list(self.get_institute_share_vector(username))
        df.set_index('username', inplace=True)

        return df

    def get_user_vecs(self, usernames=None) -> pd.DataFrame:
        """
        Calculates the vector representation of a given set of users as a pandas.DataFrame.
        :param usernames: The set of usernames. If None, all users are used. Default: None.
        :return: pandas.DataFrame containing the vector representation of the users.
        """
        columns = [
            'username',
            'group_discussion',
            'meetings',
            'interpersonal',
            'public_speaking',
            'stranger',
            'acquaintance',
            'friend',
            'total',
            'visual',
            'kinesthetic',
            'rw',
            'auditory',
            'inst1',
            'inst2',
            'inst3'
        ]
        user_vecs = pd.DataFrame(columns=columns)

        if usernames is None:
            # if no selected usernames are given, get all usernames
            usernames = list(SmartUser.objects.filter(is_staff=False).values_list('username', flat=True).distinct())

        for username in tqdm(usernames):
            vec = self.get_user_vector(username)
            user_vecs.loc[len(user_vecs)] = [username] + list(vec)

        user_vecs.set_index('username', inplace=True)
        user_vecs.round(3)
        return user_vecs

