from sbm.models import UserQuestionnaire, SingleChoiceQuestionAnswer
import numpy as np


class UserWTC:
    """
    This is a helper class for calculating the validated WTC scores for a given user.
    """

    question_texts = {
        8: "Talk in a small group of strangers",
        15: "Talk in a small group of acquaintances",
        19: "Talk in a small group of friends",
        6: "Talk in a large meeting of friends",
        11: "Talk in a large meeting of acquaintances",
        17: "Talk in a large meeting of strangers",
        4: "Talk with an acquaintance while standing in line",
        9: "Talk with a friend while standing in line",
        12: "Talk with a stranger while standing in line",
        3: "Present a talk to a group of strangers",
        14: "Present a talk to a group of friends",
        20: "Present a talk to a group of acquaintances"
    }

    def __init__(self, username):

        self.user_questionnaire = None
        self.group_discussion = None
        self.meetings = None
        self.interpersonal = None
        self.public_speaking = None
        self.stranger = None
        self.acquaintance = None
        self.friend = None
        self.question_answers = {}

        try:
            self.user_questionnaire = UserQuestionnaire.objects.get(
                user__username=username,
                questionnaire__title="Willingness to Communicate"
            )
        except UserQuestionnaire.DoesNotExist:
            raise ValueError("User has not taken that questionnaire yet")

        for question_no in self.question_texts.keys():
            self.question_answers[question_no] = self.get_question_answer(question_no)

    def get_question_answer(self, question_no):
        """
        Returns the answer to a question indicated by the WTC question ID.
        :param question_no: WTC question ID
        :return: answer to the question
        """

        if self.question_answers.get(question_no) is not None and not np.isnan(self.question_answers.get(question_no)):
            return self.question_answers[question_no]

        try:
            return int(SingleChoiceQuestionAnswer.objects.get(
                user_questionnaire=self.user_questionnaire,
                question__question_text=self.question_texts[question_no]
            ).answer)
        except SingleChoiceQuestionAnswer.DoesNotExist:
            return np.nan

    def get_group_discussion(self):
        """
        Returns the group discussion score.
        :return: group discussion score
        """
        if self.group_discussion is None or np.isnan(self.group_discussion):
            answer8 = self.get_question_answer(8)
            answer15 = self.get_question_answer(15)
            answer19 = self.get_question_answer(19)

            self.group_discussion = np.mean([answer8, answer15, answer19])
        return self.group_discussion

    def get_meetings(self):
        """
        Returns the meetings score.
        :return: meetings score
        """
        if self.meetings is None or np.isnan(self.meetings):
            answer6 = self.get_question_answer(6)
            answer11 = self.get_question_answer(11)
            answer17 = self.get_question_answer(17)

            self.meetings = np.mean([answer6, answer11, answer17])

        return self.meetings

    def get_interpersonal(self):
        """
        Returns the interpersonal score.
        :return: interpersonal score
        """
        if self.interpersonal is None or np.isnan(self.interpersonal):
            answer4 = self.get_question_answer(4)
            answer9 = self.get_question_answer(9)
            answer12 = self.get_question_answer(12)

            self.interpersonal = np.mean([answer4, answer9, answer12])
        return self.interpersonal

    def get_public_speaking(self):
        """
        Returns the public speaking score.
        :return: public speaking score
        """
        if self.public_speaking is None or np.isnan(self.public_speaking):
            answer3 = self.get_question_answer(3)
            answer14 = self.get_question_answer(14)
            answer20 = self.get_question_answer(20)

            self.public_speaking = np.mean([answer3, answer14, answer20])
        return self.public_speaking

    def get_stranger(self):
        """
        Returns the stranger score.
        :return: stranger score
        """
        if self.stranger is None or np.isnan(self.stranger):
            answer3 = self.get_question_answer(3)
            answer8 = self.get_question_answer(8)
            answer12 = self.get_question_answer(12)
            answer17 = self.get_question_answer(17)

            self.stranger = np.mean([answer3, answer8, answer12, answer17])
        return self.stranger

    def get_acquaintance(self):
        """
        Returns the acquaintance score.
        :return: acquaintance score
        """
        if self.acquaintance is None or np.isnan(self.acquaintance):
            answer4 = self.get_question_answer(4)
            answer11 = self.get_question_answer(11)
            answer15 = self.get_question_answer(15)
            answer20 = self.get_question_answer(20)

            self.acquaintance = np.mean([answer4, answer11, answer15, answer20])
        return self.acquaintance

    def get_friend(self):
        """
        Returns the friend score.
        :return: friend score
        """
        if self.friend is None or np.isnan(self.friend):
            answer6 = self.get_question_answer(6)
            answer9 = self.get_question_answer(9)
            answer14 = self.get_question_answer(14)
            answer19 = self.get_question_answer(19)

            self.friend = np.mean([answer6, answer9, answer14, answer19])
        return self.friend

    def get_total(self):
        """
        Returns the mean of all answers.
        :return: mean of all answers
        """
        for key, answer in self.question_answers.items():
            if answer is None:
                self.question_answers[key] = self.get_question_answer(key)
        return np.mean(list(self.question_answers.values()))
