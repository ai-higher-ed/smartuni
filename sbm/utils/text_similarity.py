import pandas as pd
import numpy as np
import spacy
from nltk.corpus import stopwords
import re
from operator import itemgetter

import nltk
nltk.download('stopwords')


def most_similar(doc, all_docs):
    """
    a function that receives the spaCy document and compares it to all other documents,
    returning an array of tuples sorted from most to least similar
    """
    doc1 = nlp(doc)
    similarities = [(doc2, doc1.similarity(nlp(doc2))) for doc2 in all_docs if
                    all_docs.index(doc) != all_docs.index(doc2)]

    most_similar_texts = sorted(similarities, key=itemgetter(1), reverse=True)

    return most_similar_texts


# need to download EN web corpus (md or lg): python -m spacy download en_core_web_lg
nlp = spacy.load("en_core_web_lg")

# Sample corpus
documents = ['AI, linguistics, nlp, pysychology, neuroscience....',
             'philosophy, ethics, chimpanzees, neurology, basic programming stuff.',
             'I try to be interested in all disciplines of cognitive science but mostly anthropology',
             'VR and computer science',
             'computer science, VR',
             'Machine learning',
             'Deep Learning applications in language',
             'mechanical engineering, game developement',
             'web development, javascipt',
             'cognitive neuropsychology, informatics',
             'robots, human computer interaction',
             'Theory, utopian ideas, history',
             'To gain more knowledge of both lab techniques and computing skills',
             'To reveal a neuromulecular process in neurodegenerative diseases',
             'My academic interests are mainly revolved around humanities and literature',
             'I like anything but AI and NLP',
             'Mainly not anything that has to do with AI!!!'
             ]

# make it a pandas dataframe
documents_df = pd.DataFrame(documents, columns=['documents'])

# removing special characters and stop words from the text
stop_words_l = stopwords.words('english')
documents_df['documents_cleaned'] = documents_df.documents.apply(
    lambda x: " ".join(re.sub(r'[^a-zA-Z]', ' ', w).lower() for w in x.split() if re.sub(r'[^a-zA-Z]', ' ', w).lower()))

# run the function most_similar to receive similarities
documents_df['similarities'] = [most_similar(doc, list(documents_df['documents_cleaned'])) for doc in
                                documents_df["documents_cleaned"]]

documents_df

# •	Principal Components Analysis

# •	Singular Value Decomposition (Yesid)

# •	Non-Negative Matrix Factorization (Raia) (couldn't find any resources that use this for word embeddings(?) so I didn't implement it)

# •	t-SNE (Raia) (some sources say it might be as good as PCA; although it is usually used for visualization)

# +
# implementing t-SNE -- 
# ALTHOUGH I don't think it's a good idea to use it since I'm sure the meaning got lost when reducing
from sklearn.manifold import TSNE
documents_df['embedding'] = documents_df.documents_cleaned.apply(lambda x: nlp(x).vector)
documents_df['three_dim'] = documents_df.embedding.apply(lambda x: TSNE(n_components = 3, random_state=0).fit_transform(x.reshape(-1, 1))[:3,1])

# the result is a (3,) vector for each embedding
documents_df['three_dim'][0]
# -




