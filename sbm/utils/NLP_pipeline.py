from django.db.models import Q
import functools as ft
import pandas as pd
from sklearn.decomposition import TruncatedSVD
import numpy as np
import spacy
import re
import nltk
from sbm.models import *
from spc.models import SmartUser
from tqdm import tqdm
from spacy.tokens import Doc
import cProfile
logger = logging.getLogger('sbm')
nltk.download('stopwords')


class NLPPipeline():
    """
    Reads PAI data from the database, processes, embeds and computes similarity scores.
    """
    nlp = spacy.load("en_core_web_lg")

    def __init__(self):

        self.pai_df = None

    @classmethod
    def valid_answer(cls, row_answer):
        """
        Returns true if the response has no meaning, that is,
        if its embedding only contains zeros.
        Args:
            row_answer: User's PAI response
        """
        doc = NLPPipeline.nlp(NLPPipeline.clean_data(row_answer))
        return np.any(doc.vector)

    @classmethod
    def clean_data(cls, row_text):
        """
        Takes user's response and cleans it.
        Args:
            row_text: user's response
        """
        answer_cleaned = " ".join(
            re.sub(r'[^a-zA-Z]', ' ', w).lower() for w in row_text.split() if re.sub(r'[^a-zA-Z]', ' ', w).lower())
        return answer_cleaned

    @classmethod
    def create_nlp(cls, smart_u, answer_cleaned):
        """
        Creates user's serialized NLP objects.
        Args:
            smart_u: SmartUser instance.
            answer_cleaned: Clean user's response.
        """

        # save serialized nlp object to database
        NLPObject.objects.create(
            user=smart_u,
            nlp_obj=NLPPipeline.nlp(answer_cleaned).to_bytes(exclude=["sentiment", "user_data",
                                                                      "user_data_keys", "user_data_values",
                                                                      "tensor"])
        )

    def get_nlp_object(self, candidate_usernames):
        """
        Saves serialized spacy objects to database.
        Args:
            candidate_usernames: tuple that contains all users in the matching pool.
        """
        # Access the  PAI answer
        questionnaire = Questionnaire.objects.get(title='Personal Academic Interests')
        question = TextQuestion.objects.get(questionnaire=questionnaire)

        # solves issue 152

        for user in candidate_usernames:

            smart_u = SmartUser.objects.get(username=user)
            # if there is no NLP object for this user; compute one
            if len(NLPObject.objects.filter(user=smart_u.id)) == 0:
                # get user-specific object
                uq = UserQuestionnaire.objects.get(
                    user=smart_u,
                    questionnaire=questionnaire)

                qa = TextQuestionAnswer.objects.get(
                    user_questionnaire=uq,
                    question=question,
                )

                # compute nlp object for that user
                NLPPipeline.create_nlp(smart_u, NLPPipeline.clean_data(qa.answer))

    def dimensionality_reduction(self):
        """
        Implement SVD and returns dataframe 
        """
        self.get_nlp_object()
        list_embed = [nlp_obj['nlp_object'][3].vector for _, nlp_obj in self.pai_df.iterrows()]
        # create 2d array shape (number_of_users, word_embedding)
        matrix_embed = np.column_stack(list_embed).transpose()
        # Fitting the SVD class
        trun_svd = TruncatedSVD(n_components=4)
        embedding_transformed = trun_svd.fit_transform(matrix_embed)
        data_reduced = {"reduced": list(embedding_transformed)}
        self.pai_df = pd.concat([self.pai_df, pd.DataFrame(data_reduced, columns=['reduced'])], axis=1)
        return self.pai_df

    @ft.lru_cache(maxsize=None)
    def deserialize_nlp(self, serialized_nlp):
        """
        Deserializes nlp object
        Args:
            serialized_nlp: serialized nlp object
        """
        deserialized_nlp = Doc(NLPPipeline.nlp.vocab).from_bytes(serialized_nlp)

        return deserialized_nlp

    def pai_similarity(self, u1, user_similarity_compare):
        """
        Calculates the bidirectional similarity score between two texts.
        Args:
            u1: a user's username.
            user_similarity_compare: tuple that contains all users in the matching pool.
        """
        # fetch NlP object
        doc1 = SmartUser.objects.get(username=u1).nlpobject_set.all()[0]

        for u2 in user_similarity_compare:
            # fetch NlP object
            doc2 = SmartUser.objects.get(username=u2).nlpobject_set.all()[0]
            # check if users 1+2 are not the same, and that the pair is not already saved in the data
            # the set statement excludes cases such as 7,5 and 5,7 from being computed
            if doc1.user.id != doc2.user.id and len({doc2.user.id, doc1.user.id}) == 2:
                # if similarity score does exist in the database; creates one.
                if len(PAISimilarity.objects.filter(Q(user_a=doc1.user.id) & Q(user_b=doc2.user.id))) == 0 and len(
                        PAISimilarity.objects.filter(Q(user_a=doc2.user.id) & Q(user_b=doc1.user.id))) == 0:
                    # normalize similarity scores
                    similarity = (self.deserialize_nlp(doc1.nlp_obj).similarity(
                        self.deserialize_nlp(doc2.nlp_obj)) + 1) / 2
                    # save users and their similarity score to database
                    PAISimilarity.objects.create(

                        user_a=doc1.user,
                        user_b=doc2.user,
                        similarity=similarity
                    )

    def __call__(self):
        """
        Computes spacy.tokens.doc.Doc object for every PAI text, iterates over all users in the matching pool and
        generates a bidirectional similarity score.
        """
        # filter users
        candidate_usernames = tuple(
            UserMatchingStatus.objects.filter(
                # exclude staff
                user__is_staff=False
            ).exclude(
                # only consider users that want to be recommended
                status=UserMatchingStatus.INACTIVE
            ).exclude(
                # only consider users that fullfil requirements to be matched
                status=UserMatchingStatus.UNREADY
            ).values_list(
                # only return the usernames
                'user__username',
                flat=True
            ).distinct()
        )

        # self.dimensionality_reduction()
        self.get_nlp_object(candidate_usernames)
        # progress bar
        # Init progress bar

        user_similarity1 = candidate_usernames
        user_similarity_compare = candidate_usernames
        pbar = tqdm(total=len(user_similarity1))
        prof = cProfile.Profile()
        prof.enable()
        for u1 in user_similarity1:
            self.pai_similarity(u1, user_similarity_compare)
            pbar.update(n=1)

        prof.disable()
        # Uncomment to print statistics
        # prof.print_stats()

        # Before Clearing cache
        logger.debug(f'Before clearing deserialize_nlp cache: {self.deserialize_nlp.cache_info()}')
        # Clear cache
        self.deserialize_nlp.cache_clear()
        # After Clearing
        logger.debug(f'After clearing deserialize_nlp cache: {self.deserialize_nlp.cache_info()}')



