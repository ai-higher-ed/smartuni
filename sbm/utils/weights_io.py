import json
from django.conf import settings
import os
from datetime import datetime


default_path = os.path.join(settings.BASE_DIR, 'sbm', 'static', 'sbm', 'matching_weights.json')
default_weights = {
    'wtc_preference_factor': 2,
    'wtc_influence_factor': 1/8,
    'ls_preference_factor': 2,
    'ls_influence_factor': 1/4,
    'inst_preference_factor': 2,
    'inst_influence_factor': 1/3,
}


def load_weights(path=None):
    if path is None:
        path = default_path

    if not os.path.exists(path):
        return default_weights

    with open(path, 'r') as f:
        weights = json.load(f)

    for key in default_weights.keys():
        if key not in weights.keys():
            weights[key] = default_weights[key]

    return weights


def save_weights(weights, path=None):
    if path is None:
        path = default_path
    weights['date'] = datetime.now().isoformat()
    with open(path, 'w') as f:
        json.dump(weights, f)
