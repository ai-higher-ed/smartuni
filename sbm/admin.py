from django.contrib import admin
from .models import *


admin.site.register(Questionnaire)
admin.site.register(UserQuestionnaire)
admin.site.register(SingleChoiceQuestion)
admin.site.register(SingleChoiceQuestionAnswer)
admin.site.register(TextQuestion)
admin.site.register(TextQuestionAnswer)
admin.site.register(QuestionnaireResult)
admin.site.register(MultipleChoiceQuestion)
admin.site.register(MultipleChoiceQuestionAnswer)
admin.site.register(AnswerItem)
admin.site.register(PartnerPreferencesAnswers)
admin.site.register(Institute)
admin.site.register(PAISimilarity)
admin.site.register(WTCSimilarity)
admin.site.register(LSSimilarity)
admin.site.register(INSTSimilarity)
admin.site.register(NLPObject)
admin.site.register(Course)
admin.site.register(CourseEnrollment)
admin.site.register(UserMatchingStatus)
admin.site.register(MatchSample)
admin.site.register(StudyBuddy)

