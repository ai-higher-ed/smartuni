from django.apps import AppConfig
from django.db.models.signals import pre_save
from sbm.signals import similarity_model

class SbmConfig(AppConfig):
    name = 'sbm'

    def ready(self):
        pai_s = self.get_model('PAISimilarity')
        wtc_s = self.get_model('WTCSimilarity')
        ls_s = self.get_model('LSSimilarity')
        inst_s = self.get_model('INSTSimilarity')
        pre_save.connect(similarity_model, sender=pai_s)
        pre_save.connect(similarity_model, sender=wtc_s)
        pre_save.connect(similarity_model, sender=ls_s)
        pre_save.connect(similarity_model, sender=inst_s)
