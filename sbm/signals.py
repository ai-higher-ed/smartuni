from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
import logging
logger = logging.getLogger('sbm')

def similarity_model(sender, instance, *args, **kwargs):


            if instance.similarity < 0 or instance.similarity > 1:
                logger.error(f'Similarity score is not within the range [0,1]. Saving failed while attempting to save, '
                             f'similarity = {instance.similarity}, user_a: {instance.user_a.username}, user_b: {instance.user_b.username}')
                raise ValidationError(
                    _('%(value)s is not within a valid range [0,1]'),
                    params={'value': instance.similarity},
                )
