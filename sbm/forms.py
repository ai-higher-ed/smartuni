from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django.core.validators import MinValueValidator, MaxValueValidator
from sbm.utils.NLP_pipeline import NLPPipeline
from .models import SingleChoiceQuestion, MultipleChoiceQuestion, Question, Course, Institute, NLPObject


def validate_course(course_title):
    """
    Validate that course exists if user enters one.
    :param course_title: Course title entered by user.
    :raise: ValidationError if course does not exist.
    """

    if course_title and not Course.objects.filter(title=course_title).exists():
        raise forms.ValidationError("Such a course doesn't exist! Please pick one from the auto-completion.")


class TextQuestionAnswerForm(forms.Form):
    """
    Form class for text questions.
    """
    answer = forms.CharField(max_length=100,
                             widget=forms.TextInput(attrs={'spellcheck': 'true'}))

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(TextQuestionAnswerForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-text-question-answer-form'
        self.helper.form_class = 'form-control'
        self.helper.form_method = 'post'
        self.helper.form_action = ""

    def clean_answer(self):

        answer = self.cleaned_data['answer']
        if len(answer) < 3:
            raise forms.ValidationError("Answer must be at least 3 characters long.")
        elif not NLPPipeline.valid_answer(answer):
            raise forms.ValidationError("Please enter a valid answer!")
        else:
            # generate NLP object and save it to the database

            # if the user changed her answer; her NLPObject has to be generated again.

            NLPObject.objects.filter(user=self.user).delete()
            answer_cleaned = NLPPipeline.clean_data(answer)
            NLPPipeline.create_nlp(self.user, answer_cleaned)

        return answer


class SingleChoiceQuestionForm(forms.Form):
    """
    Form class for single choice questions.
    """
    class Meta:
        model = SingleChoiceQuestion
        fields = ('title',)


class LearningStylesForm(forms.Form):
    """
    Form class for the learning styles questionnaire.
    """
    answers = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        super(LearningStylesForm, self).__init__(*args, **kwargs)
        options_dict = MultipleChoiceQuestion.objects.get(
            questionnaire__title='Learning Styles'
        ).answer_options
        options_tuple_list = list(options_dict.items())
        self.fields['answers'].choices = options_tuple_list

        self.helper = FormHelper()
        self.helper.form_id = 'id-learning-styles-form'
        self.helper.form_class = 'form-control'
        self.helper.form_method = 'post'
        self.helper.form_action = ""

    def clean_answers(self):
        answers = self.cleaned_data['answers']
        for answer_key in answers:
            if answer_key not in MultipleChoiceQuestion.objects.get(
                    questionnaire__title='Learning Styles'
            ).answer_options.keys():
                raise forms.ValidationError("Invalid answer.")
        return answers


class PartnerPreferencesForm(forms.Form):
    """
    Form class for the partner preferences questionnaire.
    """

    partner = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)
    institute = forms.CharField(max_length=150, required=False)
    course = forms.CharField(max_length=300, required=False)

    def __init__(self, *args, **kwargs):
        super(PartnerPreferencesForm, self).__init__(*args, **kwargs)

        partner_options = list(MultipleChoiceQuestion.objects.get(title='Partner Preferences Question 2'
                                                                  ).answer_options.items())

        self.fields['partner'].choices = partner_options
        self.helper = FormHelper()
        self.helper.form_id = 'id-partner-preferences-form'
        self.helper.form_class = 'form-control'
        self.helper.form_method = 'post'
        self.helper.form_action = ""
        self.helper.form_show_labels = False

    def clean_course(self):
        course = self.cleaned_data['course']
        validate_course(course)
        return course

    def clean_institute(self):
        institute = self.cleaned_data['institute']
        if institute and not Institute.objects.filter(name=institute).exists():
            raise forms.ValidationError("Such an institute doesn't exist! Please pick one from the auto-complete.")
        return institute

    def clean_partner(self):
        partner = self.cleaned_data['partner']
        return partner


class CourseSubmissionForm(forms.Form):
    """
    Form class for the course submission questionnaire.
    """
    course_title = forms.CharField(
        max_length=300,
        help_text="Please enter courses you have applied to throughout your studies. "
                  "In order to make the courses comparable, please use the correct course title from Stud.IP.",
        required=False
    )
    course_grade = forms.DecimalField(
        max_digits=3, decimal_places=1,
        validators=[MinValueValidator(1), MaxValueValidator(5)],
        required=False,
        help_text="If you like, please enter your grade in the course."
    )

    def __init__(self, *args, **kwargs):
        super(CourseSubmissionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'id-course_submission-form'
        self.helper.form_class = 'form-control'
        self.helper.form_method = 'post'
        self.helper.form_action = ""

        self.helper.add_input(Submit('submit', 'Submit'))

    def clean_course_title(self):
        course_title = self.cleaned_data['course_title']
        validate_course(course_title)
        return course_title

    def clean_course_grade(self):
        course_grade = self.cleaned_data['course_grade']
        return course_grade
