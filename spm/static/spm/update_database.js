var dataUpdateTimepoint = localStorage.getItem("lastDataUpdateTimepoint");
if (dataUpdateTimepoint == null || isNaN(dataUpdateTimepoint)) {
    dataUpdateTimepoint = Date.now(); // Initialize with the current timepoint
}

// Calendar should be updated from time to time
function updateDecisionTreeDataset() {
    // Get those events since the last update until now
    let eventsSinceLastUpdate = [];
    let yesterday = Date.now() - 24 * 60 * 60 * 1000;
    let theDayBeforeLastUpdate =  dataUpdateTimepoint - 24 * 60 * 60 * 1000;
    calendar.getEvents().forEach(function (event) {
        //add event if it ended before yesterday and has started since a day before the last update
        if (event.start >= theDayBeforeLastUpdate && event.end <= yesterday) {
            eventsSinceLastUpdate.push(event);
        }
    });
    // Sort the events according to their start time
    eventsSinceLastUpdate.sort(function (first, second) {
        if (formatTime(first['start']) > formatTime(second['start'])) {
            return 1;
        } else {
            return -1;
        }
    });

    // Now iterate through the events to check whether they were an event or task
    eventsSinceLastUpdate.forEach(function (event, idx) {
        if (event.extendedProps.type === 'task') {
            let event2compare = null;
            if (idx >= 1) {
                event2compare = eventsSinceLastUpdate[idx - 1];
            }
            // Create the url that initiates the data update
            let new_ref = window.location.href + event.id + '/';

            // If there is a comparison event for the category, also check if it is on the same day
            if (event2compare && formatDate(event.start) === formatDate(event2compare.start)) {
                new_ref += event2compare.id;
            } else {
                new_ref += 'noID';
            }
            new_ref += '/updateDecisionTreeData/';
            addEventToDTT(new_ref);
        }
    });
    dataUpdateTimepoint = Date.now();
    localStorage.setItem("lastDataUpdateTimepoint", dataUpdateTimepoint);
}
// Update once a day
window.onbeforeunload = function () {
    if((Date.now() - dataUpdateTimepoint) >= 24 * 60 * 60 * 1000){
        updateDecisionTreeDataset();
    }
}

// a function that adds events to the decision tree task database via ajax request
function addEventToDTT(href) {
    var async_bool = true;
    if(navigator.userAgent.indexOf("Firefox") !== -1 ) {
        async_bool = false;
    }
    $.ajax({
        // points to the url where your data will be posted
        url: href,
        // post for security reason
        method: 'POST',
        headers: {'X-CSRFToken': csrftoken},
        mode: 'same-origin', // Do not send CSRF token to another domain.
        async: async_bool,
    });
}


