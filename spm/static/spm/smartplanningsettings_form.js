const smartStartdatePrev = document.getElementById('id_smart-startdate-prev');
const smartStartdateNext = document.getElementById('id_smart-startdate-next');
const smartStartdate = document.getElementById('id_smart_startdate');

const smartEnddatePrev = document.getElementById('id_smart-enddate-prev');
const smartEnddateNext = document.getElementById('id_smart-enddate-next');
const smartEnddate = document.getElementById('id_smart_enddate');

const smartStarttimePrev = document.getElementById('id_smart-starttime-prev');
const smartStarttimeNext = document.getElementById('id_smart-starttime-next');
const smartStarttime = document.getElementById('id_smart_starttime');

const smartEndtimePrev = document.getElementById('id_smart-endtime-prev');
const smartEndtimeNext = document.getElementById('id_smart-endtime-next');
const smartEndtime = document.getElementById('id_smart_endtime');

var tasks2useField = document.getElementById('hidden-field-task2use');

// Validates that starttime cannot be past endtime and startdate cannot be past enddate
function checkSmartDateTime(start, end, endprev) {
    if (start.value >= end.value) {
        end.value = start.value;
        disableEnable(endprev, true);
    } else {
        disableEnable(endprev, false);
    }
}

// Disable/ enable toggle buttons
function disableEnable(btn, value) {
    btn.disabled = value;
}

// Startdate functionalities
smartStartdatePrev.addEventListener('click', () => {
    if (!(new Date(smartStartdate.value) < new Date())) {
        smartStartdate.stepDown(1);
    } else {
        disableEnable(smartStartdatePrev, true);
    }
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

smartStartdateNext.addEventListener('click', () => {
    smartStartdate.stepUp(1);
    disableEnable(smartStartdatePrev, false);
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

smartStartdate.addEventListener('focusout', () => {
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

// Enddate has a datepicker popup upon click
$("#id_smart_startdate").datepicker(
    {
        onSelect: function () {
            checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
        },
        beforeShow: function () {
            $("#id_smart_startdate").datepicker("option", "minDate", formatDate(new Date()));
        },
        showOtherMonths: true,
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: 'yy-mm-dd',
        firstDay: '1'
    });

// Enddate functionalities
smartEnddatePrev.addEventListener('click', () => {
    // Enddate cannot be past the current day
    if (!(new Date(smartEnddate.value) < new Date())) {
        smartEnddate.stepDown(1);
    } else {
        disableEnable(smartEnddatePrev, true);
    }
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

smartEnddateNext.addEventListener('click', () => {
    smartEnddate.stepUp(1);
    //toggleSmartEnddatePrev(false);
    disableEnable(smartEnddatePrev, false);
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

smartEnddate.addEventListener('focusout', () => {
    checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
});

// Enddate has a datepicker popup upon click
$("#id_smart_enddate").datepicker(
    {
        onSelect: function () {
            checkSmartDateTime(smartStartdate, smartEnddate, smartEnddatePrev);
        },
        beforeShow: function () {
            $("#id_smart_enddate").datepicker("option", "minDate", formatDate(new Date(smartStartdate.value)));
        },
        showOtherMonths: true,
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: 'yy-mm-dd',
        firstDay: '1'
    });

// Starttime functionalities
smartStarttimePrev.addEventListener('click', () => {
    if (smartStarttime.value >= '01:00') {
        if (smartStarttime.value === '23:59') {
            smartStarttime.stepDown(59);
        } else {
            smartStarttime.stepDown(60);
        }
        disableEnable(smartEndtimeNext, false);
    } else {
        disableEnable(smartStarttimePrev, true);
    }
    checkSmartDateTime(smartStarttime, smartEndtime, smartEndtimePrev);
});

smartStarttimeNext.addEventListener('click', () => {
    if (smartStarttime.value <= '23:00') {
        smartStarttime.stepUp(60);
    } else {
        smartStarttime.value = '23:59'
        disableEnable(smartEndtimeNext, true);
    }
    checkSmartDateTime(smartStarttime, smartEndtime, smartEndtimePrev);
});

smartStarttime.addEventListener('focusout', () => {
    checkSmartDateTime(smartStarttime, smartEndtime, smartEndtimePrev);
});

// Endtime functionalities
smartEndtimePrev.addEventListener('click', () => {
    if (smartEndtime.value === '23:59') {
        smartEndtime.stepDown(59);
    } else {
        smartEndtime.stepDown(60);
    }
    checkSmartDateTime(smartStarttime, smartEndtime, smartEndtimePrev);
    disableEnable(smartEndtimeNext, false);
});

smartEndtimeNext.addEventListener('click', () => {
    if (smartEndtime.value >= '23:59') {
        disableEnable(smartEndtimeNext, true);
    } else {
        smartEndtime.stepUp(60);
        disableEnable(smartEndtimePrev, false);
    }
});

smartEndtime.addEventListener('focusout', () => {
    checkSmartDateTime(smartStarttime, smartEndtime, smartEndtimePrev);
});

// The selected tasks for smartplanning should be displayed in green (selected). If no task is selected, the planning
// process should not be able to be started (submit button disabled)
// Also, at least one weekday has to be selected
var tasks2use = [];
var smartTaskBtns = document.querySelectorAll('.smart-task-button');
var submitButton = document.getElementById('id_submit-button-smart-settings');
var weekdaySelection = document.querySelectorAll('.widget-weekday');
var daysSelected = [];

// Clicking on a task leads to selection/ unselection of that task in the planning process. A selected task is added to
// a task list (via its id) which is used after the submit button is pressed
smartTaskBtns.forEach(smartTaskBtn => {
    smartTaskBtn.addEventListener('click', () => {
        var taskID = (smartTaskBtn.id).split('-button')[0];

        if (tasks2use.includes(taskID)) {
            smartTaskBtn.style.backgroundColor = 'white';
            tasks2use = tasks2use.filter(function (value) {
                return value !== taskID;
            })
        } else {
            smartTaskBtn.style.backgroundColor = '#27AE6080';
            tasks2use.push(taskID);
        }

        tasks2useField.setAttribute('value', JSON.stringify(tasks2use));
        checkSubmitButton();
    });
});

// A selected weekday appears in another color as an unselected weekday. Selected weekdays are passed on after the
// submit button is pressed
weekdaySelection.forEach(weekdayBtn => {
    checkWeekdaySelect(weekdayBtn);
    weekdayBtn.addEventListener('click', () => {
        checkWeekdaySelect(weekdayBtn);
        checkSubmitButton();
    });
});

function checkWeekdaySelect(weekdayBtn) {
    if (weekdayBtn.checked) {
        daysSelected.push(weekdayBtn.name)
    } else {
        daysSelected = daysSelected.filter(function (value) {
            return value !== weekdayBtn.name;
        })
    }
}

checkSubmitButton();

function checkSubmitButton() {
    if (tasks2use.length === 0 || daysSelected === 0) {
        submitButton.disabled = true;
    } else {
        submitButton.disabled = false;
    }
}

// The smartplanning popup has a collapsible explanation, the following implements the collapse functionality
const smartExplanation = document.getElementById('id_smart-explanation-text');
const smartExplanationBtn = document.getElementById('id_smart-explanation-button');
var explanationContent1 = 'Welcome to our AI component! Here, you can allow an AI to plan your open tasks into the ' +
    'calendar. In the background, the AI is trying to find the most suitable timeslot for your task. That is the ' +
    'timeslot in which you will most likely fulfil the task. The calculations for that are based on your very own ' +
    'behaviour. We collect whether you completed tasks in the timeslots they appeared in your calendar. Over time, ' +
    'this allows the AI to make informed decisions about where to place a certain task in your calendar such that ' +
    'you will most likely complete it. The AI just needs some information to plan your tasks. Please work yourself ' +
    'through steps one to four. ' +
    'In your selections, you do not have to worry about events and tasks that already are in your calendar. The AI ' +
    'can only access free timeslots. It might happen that we do not find a suitable slot. In that case, you will be' +
    'notified. You also do not have to worry about deadlines, this is done in the background for you.';
var explanationContent2 = 'Welcome to our AI component! Click here to receive more information.';
var collapsed = true;
smartExplanation.textContent = explanationContent2; // initial setting
smartExplanation.addEventListener('click', () => {
    smartExplanationBtn.classList.toggle('down-icon');
    smartExplanationBtn.classList.toggle('up-icon');
    if (collapsed) {
        smartExplanation.textContent = explanationContent1;
    } else {
        smartExplanation.textContent = explanationContent2;
    }
    collapsed = !collapsed;
})