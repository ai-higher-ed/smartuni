const openPopupButtons = document.querySelectorAll('[data-popup]')
const addButton = document.getElementById('add-button')
let addButtonIsOpen = false;

addButton.addEventListener('click', () => {

    const addButtons = document.querySelectorAll('[data-add-button]')
    if (addButtonIsOpen){
        addButtons.forEach(button =>{
            closeAddButton(button)
        })
    }
    else {
        addButtons.forEach(button =>{
            openAddButton(button)
        })
    }
})

openPopupButtons.forEach(button => {
    button.addEventListener('click', () => {
        closeAddButtonWhenOpenPopup()
    })
})

function closeAddButtonWhenOpenPopup() {

    const addButtons = document.querySelectorAll('[data-add-button]')
    addButtons.forEach(button =>{
        closeAddButton(button)
    })
}

function closeAddButton(addButton) {
    if (addButton == null) return
    addButton.classList.remove('active')
    addButtonIsOpen = false
}

function openAddButton(addButton) {
    if (addButton == null) return
    addButton.classList.add('active')
    addButtonIsOpen = true
}
