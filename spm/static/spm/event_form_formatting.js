// Event fields
const newTitleInputField = document.getElementById('id_title')
const newTitleBackground = document.getElementById('id_new-event-title')
const locationInputField = document.getElementById('id_location')
const locationIcon = document.getElementById('id_location-icon')
const startDate = document.getElementById('id_startdate')
const endDate = document.getElementById('id_enddate')
const startTime = document.getElementById('id_starttime')
const endTime = document.getElementById('id_endtime')
const repeatEndDate = document.getElementById('id_repeat_end')
const recurrenceTrue = document.getElementById('id_recurrence_true')

// Buttons
const closeButton = document.getElementById('id_close-button-new-event')
const submitButton = document.getElementById('id_submit-button-new-event')
const startDatePrev = document.getElementById('id_startdate-prev')
const startDateNext = document.getElementById('id_startdate-next')
const endDatePrev = document.getElementById('id_enddate-prev')
const endDateNext = document.getElementById('id_enddate-next')
const startTimePrev = document.getElementById('id_starttime-prev')
const startTimeNext = document.getElementById('id_starttime-next')
const endTimePrev = document.getElementById('id_endtime-prev')
const endTimeNext = document.getElementById('id_endtime-next')
const repeatEndPrev = document.getElementById('id_repeat-end-prev')
const repeatEndNext = document.getElementById('id_repeat-end-next')

// Warning Popup
const warningPopup = document.getElementById('id_warning-popup-new-event')
const cancelButton = document.getElementById('id_cancel-button-warning-popup')
const overlay = document.getElementById('id_overlay-new-event')

// Flags
let endDateEnabled = false;

// Recurrence Pattern
const recurrenceTrueLabel = document.getElementById('id_recurring-checkbox-label')
const recurrencePattern = document.getElementById('id_recurrence-pattern')
const recurrenceType = document.getElementById('id_repeat_type')


// Onload of the template check if create or update view
$(document).ready(() => {
    enableSubmitIfUpdate();

    // enable or disable the color field depending on if a category is given or not
    let colorInputField = document.getElementById('id_color')
    let categoryInputField = document.getElementById('id_name')

    if (categoryInputField.value === null || categoryInputField.value === '') {
        colorInputField.value = '#334b7b';
        colorInputField.classList.remove('active');
    } else {
        colorInputField.classList.add('active');
    }

    // Enable and disable recurrence
    openRecurrencePattern();

    // enable and disable weekdays
    enableWeekdays();

    // select startdate weekday as default
    checkStartdateCheckboxEnabled();

})


// ----------------------- EVENT LISTENERS --------------------------
// using event listeners there is no need to use an onclick() function in the html file
// the function is called directly in the addEventListener() function

// Enable and disable recurrence
recurrenceTrue.addEventListener('change', () => {
    openRecurrencePattern();
    enableWeekdays();
    checkStartdateCheckboxEnabled();
})

// enable and disable weekdays
recurrenceType.addEventListener('change', () => {
    enableWeekdays();
    checkStartdateCheckboxEnabled();
})

// display warning message before closing new-event-form popup
closeButton.addEventListener('click', () => {
    warningPopup.classList.add('active');
    overlay.style.zIndex = 15000;
})

cancelButton.addEventListener('click', () => {
    warningPopup.classList.remove('active');
    overlay.style.zIndex = 9999;
})


// enable title field and submit button on valid input
newTitleInputField.addEventListener('keyup', () => {
    enableSubmit();
})

// enable location icon on valid input
locationInputField.addEventListener('keyup', () => {
    enableLocationIcon();
})

// enable enddate and prevent default firefox datepicker from showing up
endDate.addEventListener('click', function (event) {
    enableEndDate(false);
    event.preventDefault();
}, false)

// prevent default firefox datepicker from showing up
startDate.addEventListener('click', function (event) {
    event.preventDefault();
}, false)

// enable repeat enddate
repeatEndDate.addEventListener('click', function (event) {
  //  enableRepeatEndDate(false);
    event.preventDefault();
}, false)

// toggle button actions
startDatePrev.addEventListener('click', () => {
    startDate.stepDown(1); // easy way to toggle without having to implement it
    // as long as the enddate is not enabled, it will not be a multi day event
    if (!endDateEnabled) {
        endDate.stepDown(1);
    } else {
        enablePrevToggle();
    }
    checkStartdateCheckboxEnabled();
})

startDateNext.addEventListener('click', () => {
    startDate.stepUp(1);
    checkEndDate();
    checkEndTime();
    checkStartdateCheckboxEnabled();
})

endDatePrev.addEventListener('click', () => {
    endDate.stepDown(1);
    checkEndDate();
    checkEndTime();
})

endDateNext.addEventListener('click', () => {
    endDate.stepUp(1);
    enablePrevToggle();
})

repeatEndPrev.addEventListener('click', () => {
    repeatEndDate.stepDown(1);
    checkRepeatEndDate();
})


repeatEndNext.addEventListener('click', () => {
    repeatEndDate.stepUp(1);
    enablePrevToggleRecEvents();
})


startTimePrev.addEventListener('click', () => {
    if (startTime.value >= '01:00') {
        startTime.stepDown(60);
    }
    // manually implementing switching to previous date
    else {
        startTime.stepUp(1380);
        startDate.stepDown(1);
        // triggers multi date event
        // enables prev enddate toggle
        enableEndDate(true);
    }
    endTimePrev.disabled = false;
    checkStartdateCheckboxEnabled();
})

startTimeNext.addEventListener('click', () => {
    if (startTime.value < '23:00') {
        startTime.stepUp(60);
    }
    // manually implementing switching to next date
    else {
        startTime.stepDown(1380);
        endTime.stepDown(1380);
        startDate.stepUp(1);
        checkEndDate()
    }
    checkEndTime();
    checkStartdateCheckboxEnabled();
})

endTimePrev.addEventListener('click', () => {
    if (endTime.value >= '01:00') {
        endTime.stepDown(60);
    }
    // manually implementing switching to previous date
    else {
        endTime.stepUp(1380);
        endDate.stepDown(1);
        checkEndDate();
    }
    checkEndTime();
})

endTimeNext.addEventListener('click', () => {
    if (endTime.value < '23:00') {
        endTime.stepUp(60);
    }
    // manually implementing switching to next date
    else {
        endTime.stepDown(1380);
        endDate.stepUp(1);
        // triggers multi date event
        // enables prev enddate toggle
        enableEndDate(true);
    }
    endTimePrev.disabled = false;
})

// check for valid form after manual input
// use 'focusout' because with 'input' or 'change' not all dates can be entered, although they might be valid
endDate.addEventListener('focusout', () => {
    checkEndDate();
    checkEndTime();
})

startDate.addEventListener('focusout', () => {
    checkEndDate();
    // no automatic multi day event for manual input
    if (startDate.value < endDate.value) {
        endDate.value = startDate.value
    }
    checkEndTime();
    checkStartdateCheckboxEnabled();
})

endTime.addEventListener('focusout', () => {
    checkEndTime();
})

startTime.addEventListener('focusout', () => {
    checkEndTime();
})


repeatEndDate.addEventListener('focusout', () => {
    checkRepeatEndDate();
})


// ----------------- FUNCTIONS -------------------------
// Checks for valid enddate
// disables toggle button
function checkEndDate() {
    if (endDate.value <= startDate.value) {
        endDate.value = startDate.value;
        endDatePrev.disabled = true;
    }
}

// checks for valid endtime
// disables toggle button
function checkEndTime() {
    if (endDate.value <= startDate.value && endTime.value <= startTime.value) {
        endTime.value = startTime.value;
        endTimePrev.disabled = true;
    }
}

function checkRepeatEndDate() {
    if (repeatEndDate.value <= startDate.value) {
        repeatEndDate.value = startDate.value;
        repeatEndPrev.disabled = true;
    }
}

// enables date field and toggles
// timeToggle=true if prev toggle should be enabled as well
function enableEndDate(timeToggle) {
    if (!endDateEnabled) {
        endDate.classList.add('active');
        endDateNext.disabled = false;
        endDateEnabled = true;
    }

    // enables also the prev toggle when multi date event is caused by time toggle
    if (timeToggle) {
        endDatePrev.disabled = false;
    }
}

// checks for multi date event and enables prev toggles
function enablePrevToggle() {
    if (startDate.value < endDate.value) {
        endDatePrev.disabled = false;
        endTimePrev.disabled = false;
    }
}

function enablePrevToggleRecEvents() {
    if (startDate.value < repeatEndDate.value) {
        repeatEndNext.disabled = false;
        repeatEndPrev.disabled = false;
    }
}

// checks if input is valid and enables submit button and title
function enableSubmit() {

    // Spaces are not a valid input for the title
    if (newTitleInputField.value.trim() !== '') {
        newTitleBackground.classList.add('active');
        submitButton.disabled = false;
    } else {
        newTitleBackground.classList.remove('active');
        submitButton.disabled = true;
    }
}

function enableLocationIcon() {
    // Spaces are not a valid input for the location
    if (locationInputField.value.trim() !== '') {
        locationIcon.classList.add('active');
    } else {
        locationIcon.classList.remove('active');
    }
}

// called on template load
// enables submit for the update view since there is a valid title input already
function enableSubmitIfUpdate() {
    if (startDate.value < endDate.value) {
        enableEndDate(true);
    }
    enableLocationIcon();
    enableSubmit();
}

// open or close recurrence pattern selection
function openRecurrencePattern() {
    if (recurrenceTrue.checked) {
        recurrenceTrueLabel.innerText = 'Remove recurrence pattern';
        recurrenceTrueLabel.style.color = '#000000';
        recurrencePattern.style.display = 'block';
    } else {
        recurrenceTrueLabel.innerText = 'Add recurrence pattern';
        recurrenceTrueLabel.style.color = '#aba8a5';
        recurrencePattern.style.display = 'none';
    }
}

// disable weekdays if monthly or yearly recurrence is selected
function enableWeekdays() {
    if (recurrenceTrue.checked) {
        let weekdayButtons = document.querySelectorAll('[data-weekday-label]');

        weekdayButtons.forEach(button => {
            if (recurrenceType.value === '1') {
                button.style.pointerEvents = null;
                button.style.backgroundColor = null;
            } else {
                button.style.pointerEvents = 'none';
                button.style.backgroundColor = '#d3d1d0';
            }
        })
    }
}

// default selection of startdate weekday checkbox and disable it for unchecking
function checkStartdateCheckboxEnabled () {

    if (recurrenceType.value === '1' && recurrenceTrue.checked) {

        let weekdayCheckboxes = document.querySelectorAll('.widget-weekday');

        weekdayCheckboxes.forEach(checkbox => {

            let weekdayCheckboxID = getIdOfWeekdayCheckboxForStartdate(startDate.value);
            let label = checkbox.nextSibling.nextSibling;

            if (weekdayCheckboxID === checkbox.id) {
                label.style.pointerEvents = 'none';
                label.style.backgroundColor = '#f57f00';
            } else {
                label.style.pointerEvents = null;
                label.style.backgroundColor = null;
            }

        })
    }
}

// get the weekday checkbox ID based on the weekday of the startdate
function getIdOfWeekdayCheckboxForStartdate(date) {
  const dayOfWeek = new Date(date).getDay();
  return isNaN(dayOfWeek) ? null :
    ['id_sunday', 'id_monday', 'id_tuesday', 'id_wednesday', 'id_thursday', 'id_friday', 'id_saturday'][dayOfWeek];
}

// datepicker functioning and formatting
$("#id_startdate").datepicker(
    {
        onSelect: function () {
            checkEndDate();
            //no automatic multi day event for datepicker input
            if (startDate.value < endDate.value) {
                endDate.value = startDate.value;
            }
            checkStartdateCheckboxEnabled();
        },
        showOtherMonths: true,
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: 'yy-mm-dd',
        firstDay: '1',
    });

$("#id_enddate").datepicker(
    {

        onSelect: function () {
            checkEndTime();
            checkEndDate();
            enablePrevToggle();
        },
        beforeShow: function () {
            $("#id_enddate").datepicker("option", "minDate", startDate.value); // only allow enddates later than startdates
        },
        showOtherMonths: true,
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: 'yy-mm-dd',
        firstDay: '1'
    });


$("#id_repeat_end").datepicker(
    {
        onSelect: function () {
                 checkRepeatEndDate();
                 enablePrevToggleRecEvents();
        },
        beforeShow: function () {
            $("#id_repeat_end").datepicker("option", "minDate", startDate.value);
        },
        showOtherMonths: true,
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: 'yy-mm-dd',
        firstDay: '1'
    }
);
