let DAY_NAMES_SHORT = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']
let DAY_NAMES = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']
let DAY_CLASS_NAMES = ['fc-weekend-day', 'fc-week-day', 'fc-week-day', 'fc-week-day', 'fc-week-day', 'fc-week-day', 'fc-weekend-day']

let filterOpen = false;

// https://stackoverflow.com/questions/3514784/what-is-the-best-way-to-detect-a-mobile-device
var isMobile = false;
if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    isMobile = true;
}

/* Checks whether we want the daily or weekly view as the current one, depending on the current url */
let initialViewURL;
let currentOverview;
if ((window.location.href).includes('weekly')) {
    initialViewURL = 'timeGridWeek';
    currentOverview = 'weekly';
} else {
    initialViewURL = 'timeGridDay';
    currentOverview = 'daily';
}

/* The filter inputs are stored within the session such that when the page is reloaded or switched to daily/ weekly
view, the filters remain active */
var activeFilters = JSON.parse(localStorage.getItem("activeFiltersKey"));
if (activeFilters === null) {
    activeFilters = [];
}

/* Pads a single-digit string with a 0 */
function padTo2Digits(num) {
    return String(num).padStart(2, '0')
}

/* Format a date such that it fits into the YYYY-mm-dd format for the url */
function formatDate(date) {
    return [
        date.getFullYear(),
        padTo2Digits(date.getMonth() + 1),
        padTo2Digits(date.getDate()),
    ].join('-');
}

/* Format a time such that it fits into the hh:mm format */
function formatTime(time) {
    return [
        padTo2Digits(time.getHours()),
        padTo2Digits(time.getMinutes())
    ].join(':');
}

/* Functionality for the buttons that allow to toggle to the previous/ next week or day */
function togglePreviousAndNextView(previous=true) {

    /* Toggle to the previous or next week or day based on which button was clicked */
    if (previous) {
        calendar.prev();
    } else {
        calendar.next();
    }

    /* Update the url that is displayed */
    let newDate = formatDate(calendar.view.activeStart)
    window.history.replaceState({}, '', (window.location.href).replace(date2Initialize, newDate)) /* Update the url */
    date2Initialize = newDate
    if (currentOverview === 'daily' && !isMobile) {
        updateDayList();
    }
}

/* Initialize calendar */
var calendarEl = document.getElementById('calendar-overview');
var calendar = new FullCalendar.Calendar(calendarEl, {
    /* Calendar height, dynamically resize the calendar height if window size is changed */
    contentHeight: $(window).height() - 220,
    windowResize: function() {
        calendar.setOption('contentHeight', $(window).height() - 220);
    },

    locale: 'de', /* for date format */
    initialView: initialViewURL, /* the type of overview that is shown */
    initialDate: date2Initialize, /* initialize with given date */
    firstDay: '1', /* calendar starts with Monday, not Sunday */
    nowIndicator: true, /* indicates the current time */
    scrollTime : '08:00:00', /* determines how far forward the scroll pane is initially scrolled */
    allDaySlot: false, /* do not show slot with a simple list of all events */
    slotEventOverlap: false, /* show events side by side instead of overlapping */

    /* Define buttons that are shown in the calendar header */
    headerToolbar: {
        left: 'toggle2Previous,go2Date,toggle2Next filterCategory smartButton',
        right: 'toggle2Week,toggle2Day'
    },

    /* Change the texts of the week and day buttons */
    buttonText: {
        week: 'Week',
        day: 'Day'
    },

    /* Set the date format (dd.mm.yyyy) */
    titleFormat: {
        day: '2-digit',
        month: '2-digit',
        year: 'numeric'
    },

    /* Set labels for displayed times (on the left) */
    slotLabelFormat: {
        hour: 'numeric',
        minute: '2-digit',
        hour12: false
    },

    /* Custom buttons for the calendar */
    customButtons: {
        /* Button to toggle to the previous week/ day */
        toggle2Previous: {
            text: '',
            click: function () {

                // closing filter when toggeling between days/weeks
                removeFilterInputWrapper();
                togglePreviousAndNextView(previous=true);
            }
        },
        /* Button to toggle to the next week/ day */
        toggle2Next: {
            text: '',
            click: function () {
                // closing filter when toggeling between days/weeks
                removeFilterInputWrapper();
                togglePreviousAndNextView(previous=false);
            }
        },
        /* The go2Date button sits between the 'previous' and 'next' toggle buttons, shows the date range of the
        currently selected week and a calendar opens up if clicked such that users can select a date to be displayed */
        go2Date: {
            text: '', /* no initial text in button since it is set dynamically depending on the current displayed week */
            click: function() {
                /* Create a datepicker attached to the button */
                var go2DateButton = $('.fc-go2Date-button');
                go2DateButton.after('<input type="hidden" id="hiddenDate" class="datepicker"/>');

                /* Datepicker settings */
                $("#hiddenDate").datepicker({
                    showOn: "button",
                    onSelect: function (dateText) {
                            // Go to selected date in calendar
                            calendar.gotoDate(dateText)

                            // Update the text presented in selector button
                            $('.fc-go2Date-button').text(calendar.view.title)

                            // Update the url
                            window.history.replaceState({}, '',
                                (window.location.href).replace(date2Initialize, dateText)) /* Update the url*/
                            date2Initialize = dateText
                            if (currentOverview === 'daily' && !isMobile) {
                                updateDayList();
                            }
                        },
                    showOtherMonths: true,
                    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
                    dateFormat: 'yy-mm-dd',
                    firstDay: '1'
                });

                /* Behavious after a new date is selected */
                var buttonDatepicker = $(".ui-datepicker-trigger");
                buttonDatepicker.trigger("click");
                buttonDatepicker.remove();
            }
        },
        /* Button to filter the categories displayed */
        filterCategory: {
            text: '',
            click: function() {

                // opens the filter
                if (!filterOpen) {

                    // puts the filter button into a div
                    if (!document.getElementById('id_filter-wrapper')) {
                        let filterButton = $('.fc-filterCategory-button');

                        let tempWrapper = document.createElement('div');
                        tempWrapper.setAttribute('class', 'filter-wrapper');
                        tempWrapper.id = 'id_filter-wrapper';

                        filterButton.wrap(tempWrapper);
                    }

                    filterOpen = true;

                    // creates an input field and adds it to the filter button
                    let inputField = document.createElement('input');
                    inputField.id = 'id_input-filter';
                    inputField.autofocus = true;

                    let inputFieldDiv = document.createElement('div');
                    inputFieldDiv.setAttribute('class', 'autocomplete');
                    inputFieldDiv.id = 'id_filter-field';
                    inputFieldDiv.appendChild(inputField);

                    let filterDiv = document.getElementById('id_filter-wrapper');
                    filterDiv.appendChild(inputFieldDiv);

                    // combines the autocompletion and filtering of the input field
                    filterAutocomplete(inputField, categories);

                } else {
                    // closes the filter
                    filterOpen = false;

                    // removes the input field
                    let filterDiv = document.getElementById('id_filter-wrapper');
                    filterDiv.removeChild(document.getElementById('id_filter-field'));
                }
            },
        },
        /* test button for inserting smart tasks*/
        smartButton: {
            text: '', /* title set separately to avoid appearing twice upon clicking*/
            click: function () {
                window.location.href = window.location.href + 'smartPlanningSettings/'
            }
        },

        /* Button to switch to the weekly overview */
        toggle2Week: {
            text: 'Week',
            click: function() {
                toggleViewButtonsActive(true);
                calendar.changeView('timeGridWeek');
                /* Update the url*/
                window.location.href = (window.location.href).replace(currentOverview, 'weekly');
                currentOverview = 'weekly';
            }
        },
        /* Button to switch to the daily view */
        toggle2Day: {
            text: 'Day',
            click: function() {
                toggleViewButtonsActive(false);
                calendar.changeView('timeGridDay');
                /* Update the url*/
                window.location.href = (window.location.href).replace(currentOverview, 'daily');
                currentOverview = 'daily';
            }
        }
    },

    /* Set the header information (date and weekday) for calendar days*/
    dayHeaderContent: function(arg) {
        var innerText = '';

        // Depending on current view (week or day), the header should be different
        if (calendar.view.type === 'timeGridWeek') {
            // Weekly overview shows the date in the format dd.mm. and a shortened weekday
            innerText += (padTo2Digits(arg.date.getDate()) + '.' +
                padTo2Digits(arg.date.getMonth() + 1) + '.').bold(); // Date (without year)
            innerText += '<br>'; // line break to separate date and weekday
            innerText += DAY_NAMES_SHORT[arg.date.getDay()] ;// Short weekday
        } else {
            innerText += DAY_NAMES[arg.date.getDay()];
        }

        return { html: innerText }
    },

    /* Set class names for days to access in css file such that weekends can be styled differently to weekdays */
    dayCellClassNames: function(arg) {
        return DAY_CLASS_NAMES[arg.date.getDay()]
    },

    eventSources: [{events: tasklist}, {events: eventlist}],

    eventDidMount: function(info) {
        info.el.style.borderWidth = '6px';
    },

    eventTimeFormat: {
        hour: "2-digit",
        minute: "2-digit",
        hour12: false
    },
    /* Redirection to the detail view if event is clicked */
    eventClick: function (info) {
        window.location.href = info.event.extendedProps.edit_href;
    },
    /* Set what is displayed inside the calendar per event */
    eventContent: function (arg) {

        var innerText = '';
        if (arg.event.extendedProps.type === 'task' && arg.event.extendedProps.complete === 'True') {
            innerText += '<s>' + (arg.event.title).bold() + '</s>';
        } else {
            innerText = (arg.event.title).bold();
        }

        // Display further information only if event is longer than one hour
        var startEvent = arg.event.start;
        var endEvent = arg.event.end;

        if (!((endEvent - startEvent) < 3600000)) { //if the event is longer than an hour
            // Display start and end times
            var startTime = formatTime(startEvent);
            var endTime = formatTime(endEvent);
            innerText += "<br>" + startTime + " - " + endTime;

            // Add location description only if the info is provided in event creation
            var locationInfo = arg.event.extendedProps.location;
            if (locationInfo.length !== 0) {
                innerText += "<br>" + locationInfo;
            }
        }
        return { html: innerText };
    },
    /* Give element class references based on their length */
    eventClassNames: function (arg) {
        var startEvent = arg.event.start;
        var endEvent = arg.event.end;
        let classNameArray = [];
        /* If the event is 15 Minutes or shorter it is a small event */
        if (((endEvent - startEvent) <= 1200000)) {
            classNameArray.push('smallEvent');
        }
        if (arg.event.extendedProps['type'] === 'task') {
            classNameArray.push('task');
        }
        return classNameArray;
    },
    /* enabling drag and drop of tasks onto calendar */
    editable: true,
    droppable: true, // this allows things to be dropped onto the calendar
    /* Check if the task is already in the calendar before allowing it to be dropped*/
    dropAccept: function(element) {
        var drop = true;
        if (tasklist) {
            var newID = element.getAttribute('data-id');
            tasklist.forEach(function(taskEl) {
                if (newID === taskEl.id) {
                    drop = false;
                }
            })
        }
        if (element.getAttribute('data-done')) {
            drop = false;
        }
        return drop;
    },
    snapDuration: '00:05:00',
    /* save the date and time position of the task upon the task being received by / dropped into the calendar*/
    eventReceive: function (info) {
        //get the bits of data we want to send into a simple object
        var start = info.event.start;
        var end = info.event.end;
        $.ajax({
            // points to the url where your data will be posted
            url: window.location.href + 'updateTaskCalendar/',
            // post for security reason
            method: 'POST',
            headers: {'X-CSRFToken': csrftoken},
            mode: 'same-origin', // Do not send CSRF token to another domain.
            // data that you will like to return
            data: {
                id: info.event.id, title: info.event.title,
                startTime: formatTime(start), startDate: formatDate(start),
                endTime: formatTime(end), endDate: formatDate(end)
            },
            // what to do when the call is success
            success: function () {
            },
            // what to do when the call is complete (you can right your clean from code here)
            complete: function () {
                location.reload();
            },
            // what to do when there is an error
            error: function (xhr, textStatus, thrownError) {
                alert('TASK CANNOT BE SAVED');
            }
        });
    },
    /* save the new date and time position of the task upon the task being moved in the calendar*/
    eventDrop: function (info) {
        //get the bits of data we want to send into a simple object
        var start = info.event.start;
        var end = info.event.end;
        $.ajax({
            // points to the url where your data will be posted
            url: window.location.href + 'updateTaskCalendar/',
            // post for security reason
            method: 'POST',
            headers: {'X-CSRFToken': csrftoken},
            mode: 'same-origin', // Do not send CSRF token to another domain.
            // data that you will like to return
            data: {id : info.event.id, title: info.event.title,
                startTime: formatTime(start), startDate: formatDate(start),
                endTime: formatTime(end), endDate: formatDate(end)},
            // what to do when the call is success
            success: function () {
                saveToggleSetting();
            },
            // what to do when the call is complete (you can right your clean from code here)
            complete: function () {
                location.reload();
            },
            // what to do when there is an error
            error: function (xhr, textStatus, thrownError) {
                alert('TASK CANNOT BE UPDATED');
            }
        });
    },
    /* remove tasks dragged out of bounds from the calendar*/
    eventDragStop: function (info) {
        if (isEventOverSidebar(info.jsEvent.clientX, info.jsEvent.clientY)) {
            calendar.getEventById(info.event.id).remove();
            $.ajax({
                // points to the url where your data will be posted
                url: window.location.href + 'removeTaskCalendar/',
                // post for security reason
                method: 'POST',
                headers: {'X-CSRFToken': csrftoken},
                mode: 'same-origin', // Do not send CSRF token to another domain.
                data: {id : info.event.id},
                // what to do when the call is success
                success: function () {
                    saveToggleSetting();
                },
                // what to do when the call is complete (you can right your clean from code here)
                complete: function () {
                    location.reload();
                },
                // what to do when there is an error
                error: function (xhr, textStatus, thrownError) {
                    alert('Removal Failed');
                }
            });
        }
    },

});

/* Checks if the dragged event is over the tasks sidebar*/
function isEventOverSidebar (x, y) {
    var external_events = document.getElementById('id_calendar-overview-sidenav');
    // Compare
    return x >= external_events.offsetLeft
        && y >= external_events.offsetTop
        && x <= (external_events.offsetLeft + external_events.clientWidth)
        && y <= (external_events.offsetTop + external_events.clientHeight);
}

calendar.render();

/* The button that has the datepicker should always contain as a text the information on the current view */
$('.fc-go2Date-button').text(calendar.view.title);

/* Irrespective of which calendar header button is pressed, update the displayed date(s) based on current view */
const calendarButtons = document.querySelectorAll(".fc-button");
calendarButtons.forEach(button => {
    button.addEventListener('click', () => {
        $('.fc-go2Date-button').text(calendar.view.title);
    })
})

/* The smart button should always contain the text SMART */
$('.fc-smartButton-button').text('SMART');

/* Depending on the overview type, the buttons for the weekly or daily view are disabled and enabled to change the
appearance of the button (indicate the current view) */
const weeklyViewButtons = document.querySelectorAll(".fc-toggle2Week-button");
const dailyViewButtons = document.querySelectorAll(".fc-toggle2Day-button");
function toggleViewButtonsActive(weekly) {
    weeklyViewButtons.forEach(button => {
        button.disabled = weekly;
    })
    dailyViewButtons.forEach(button => {
        button.disabled = !weekly;
    })
}
toggleViewButtonsActive(currentOverview.includes('weekly'));

/* initialize the external events -----------------------------------------------------------------*/
function stringToTime(stringEl) {
    var splits = stringEl.split(':');
    return [padTo2Digits(splits[0]), padTo2Digits(splits[1])].join(':');
}
let containerEl = document.getElementById('id_task-accordion');

if (containerEl) {
    new FullCalendar.Draggable(containerEl, {
        itemSelector: '.list-group-item-task',
        eventData: function(eventEl) {
            var taskDuration = eventEl.getAttribute('data-duration');
            var categoryColor = eventEl.getAttribute('data-color')
            return {
                title: eventEl.innerText.trim(),
                id: eventEl.getAttribute('data-id'),
                duration: stringToTime(taskDuration),
                backgroundColor: "#ffffff",
                textColor: "#000000",
                borderColor: categoryColor,
                editable: true,
                durationEditable: false,
                extendedProps: {
                    location: ' ',
                    type: 'task',
                }
            }
        }
    });
}

/* Add button position in calendar relative to the inside of the calendar */
const cal = document.querySelector('.fc-scrollgrid');
const addDiv = document.getElementById('id_add');
cal.append(addDiv);

/* Set the functionalities for the slider in the daily view to have a split view of the day calendar and the list view
of the events */
/* A list view is only required in the mobile version */
if (!isMobile) {

    if (currentOverview === 'daily') {
        /* Set the functionalities for the slider in the daily view to have a split view of the day calendar and the list view
        of the events */
        var handler = document.querySelector('.slider');
        var wrapper = handler.closest('.fc-daily-calendar');
        var boxA = wrapper.querySelector('.fc-view-harness-active');
        var boxB = wrapper.querySelector('.events-daily-wrapper');
        var isHandlerDragging = false;

        document.addEventListener('mousedown', function(event) {
            // If mousedown event is fired from .handler, toggle flag to true
            if (event.target === handler) {
                isHandlerDragging = true;
            }
        });

        document.addEventListener('mousemove', function(event) {
            // Don't do anything if dragging flag is false
            if (!isHandlerDragging) {
                return false;
            }

            // Get offset
            let containerOffsetLeft = wrapper.offsetLeft;
            let availableWidth = wrapper.offsetWidth;
            let pointerPosition = event.clientX;

            // get relative distances
            let relativeMinBoxA = Math.round(200 / availableWidth * 100);
            let relativeMinBoxB = 1 / availableWidth * 100;
            let relativeSizeSlider = Math.ceil(10 / availableWidth * 100);
            let relativeWidthBoxA = Math.round((pointerPosition - containerOffsetLeft) / availableWidth * 100);

            // get box widths
            let boxAWidth = Math.min(Math.max(relativeMinBoxA, relativeWidthBoxA), 100 - relativeMinBoxB - relativeSizeSlider);
            let boxBWidth = 100 - boxAWidth - relativeSizeSlider;

            boxA.style.width = boxAWidth + '%';
            boxB.style.width = boxBWidth + '%';

            /* Update the inner width of the calendar */
            calendar.updateSize();
            updateListViewHeight();
        });

        document.addEventListener('mouseup', function() {
            // Turn off dragging flag when user mouse is up
            isHandlerDragging = false;
        });

        /* Set all functionalities for the events in the list view, like determining the actual content that is shown */
        const dailyListView = document.getElementById('id_events-daily');
        var clicked = []; /* Keeps track about which buttons on the current daily list view have been clicked */

        function openExtended(eventInformation) {

            let currentListEvent = document.querySelectorAll('.events-daily-single-appointments');
            let index = eventInformation.slice(-1);
            let currentHTML = currentListEvent[index].innerHTML;
            let info = eventInformation.split(',');

            /* Display more info if button is clicked once, go back to normal view if clicked again */
            if (!clicked[index]) {

                if (info[8] === 'event') {
                    /* Determine the information from the given eventInformation */
                    let time = '<button class="detail-list-icons clock-icon" disabled></button>'
                        + info[2] + ' - ' + info[3];
                    let location = '';
                    if (info[4].length > 0) {
                        location = '<br><button class="detail-list-icons location-icon" disabled></button>' + info[4];
                    }
                    let comment = '';
                    if (info[5].length > 0) {
                        comment = '<br><button class="detail-list-icons comment-icon" disabled></button>' + info[5];
                    }

                    /* Set the new inner html and set the specific button as clicked */
                    currentListEvent[index].innerHTML = currentHTML + time + location + comment;
                } else {
                    let priority = '';

                    if (info[4] === '1') {
                        priority = '<div class="priority-button-row-daily-list spaced-alignment">';
                        priority += '<input class="priority-button priority-button-daily-list active" type="button" value="!">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!!">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!!!"></div>';
                    } else if (info[4] === '2') {
                        priority = '<div class="priority-button-row-daily-list spaced-alignment">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!">';
                        priority += '<input class="priority-button priority-button-daily-list active" type="button" value="!!">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!!!"></div>';
                    } else {
                        priority = '<div class="priority-button-row-daily-list spaced-alignment"">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!">';
                        priority += '<input class="priority-button priority-button-daily-list" type="button" value="!!">';
                        priority += '<input class="priority-button priority-button-daily-list active" type="button" value="!!!"></div>';
                    }
                    let time = '<button class="detail-list-icons clock-icon" disabled></button>'
                        + info[2] + ' - ' + info[3]  + '\n';
                    let deadline = '<br><button class= "detail-list-icons deadline-icon" disabled></button>'
                        + info[9].split(' ')[0] + ', ' + info[9].split(' ')[2];
                    let description = '';
                    if (info[5].length > 0) {
                        description = '<button class="detail-list-icons comment-icon" disabled></button>' + info[5];
                    }
                    /* Set the new inner html and set the specific button as clicked */
                    currentListEvent[index].innerHTML = currentHTML + '<div class="spaced-alignment"><div>'
                        + time + deadline + '</div>' + priority + '</div>' + description;
                }

                clicked[index] = true;
            } else {
                /* Reset the event list view item to the normal view */
                currentListEvent[index].innerHTML = currentHTML.split('<button class="detail-list-icons')[0];
                clicked[index] = false;
            }

            /* Toggle class between down and up to determine displayed button image */
            let currentListEventButton = document.querySelectorAll('.up-down-toggle')[index];
            currentListEventButton.classList.toggle('down-icon');
            currentListEventButton.classList.toggle('up-icon');
        }

        function openEdit(ref) {
            window.location.href = ref + 'update/';
        }

        function openDelete(ref) {
            window.location.href = ref + 'delete/';
        }

        function updateDayList() {
            let events2Display = [];
            let events2DisplayExtended = [];
            let new_html = '<p class="list-view-col-header"> APPOINTMENTS OF THE DAY </p>';
            new_html += '<div class="all-single-events">';
            var clicked = [];

            /* Get all events of the current day */
            var calendarEvents = [];
            calendar.getEvents().forEach(function sth(event) {
                if (formatDate(event.start) === date2Initialize && event.display !== 'none') {
                    calendarEvents.push(event);
                }
            });

            /* getEvents() returns an unsorted array, so sort the displayed events according to their start time */
            calendarEvents.sort(function (first, second) {
                if (formatTime(first['start']) > formatTime(second['start'])) {
                    return 1;
                } else {
                    return -1;
                }
            });

            /* Get all the necessary information about the events for the current day */
            calendarEvents.forEach(function sth(event) {
                if (event.extendedProps.type === 'event') {
                    events2Display.push(event.title)
                    events2DisplayExtended.push([
                        formatDate(event.start),
                        formatDate(event.end),
                        formatTime(event.start),
                        formatTime(event.end),
                        event.extendedProps.location,
                        event.extendedProps.comment,
                        event.extendedProps.edit_href,
                        event.extendedProps.categoryColor,
                        event.extendedProps.type,
                    ]);
                } else {
                    events2Display.push(event.title)
                    events2DisplayExtended.push([
                        formatDate(event.start),
                        formatDate(event.end),
                        formatTime(event.start),
                        formatTime(event.end),
                        event.extendedProps.priority,
                        event.extendedProps.description,
                        event.extendedProps.edit_href,
                        event.extendedProps.categoryColor,
                        event.extendedProps.type,
                        event.extendedProps.deadline,
                    ]);
                }

                /* Initially, a button is set as not clicked (determined when more information is shown)*/
                clicked.push(false);
            });

            /* Set the new html */
            for (let i = 0; i < events2Display.length; i += 1) {
                let eventInfo = events2DisplayExtended[i].concat([i]);
                new_html += '<div class="events-daily-single-appointments">';
                new_html += '<div class="daily-appointments-inner-block">';
                new_html += '<b>' + events2Display[i] + '</b>';
                new_html += '<div class="daily-appointments-icons">' +
                    '<button class="daily-appointments-buttons edit-button detail-view-button" style="" ' +
                    'onclick="openEdit(\'' + eventInfo[6] + '\')"></button>' +
                    '<button class="daily-appointments-buttons delete-button detail-view-button" style="" ' +
                    'onclick="openDelete(\'' + eventInfo[6] + '\')"></button>' +
                    '<button class="daily-appointments-buttons down-icon detail-view-button up-down-toggle" style="" ' +
                    'onclick="openExtended(\'' + eventInfo + '\')"></button></div>';
                new_html += '</div>';
                let horLine = document.createElement('hr');
                horLine.style.backgroundColor = eventInfo[7];
                new_html += horLine.outerHTML;
                new_html += '</div>';

            }

            new_html += '</div>';

            dailyListView.innerHTML = new_html;
        }

        /* Default call to get the daily list view */
        updateDayList();
    }
} else {
    /* If the mobile view is shown, all elements that belong to the list view have to be hidden */
    var slider = document.getElementById('id_slider');
    var listWrapper = document.getElementById('id_events-daily-wrapper');

    if (slider && listWrapper) {
        slider.style.display = "none";
        listWrapper.style.display = "none";

        /* Update the inner size of the calendar */
        calendar.updateSize();
    }
}

// -------------------- autocomplete --------------------------
function filterAutocomplete(filterInputField, catList) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    filterInputField.addEventListener("input", function () {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);

        /*for each item in the array...*/
        for (i = 0; i < catList.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (catList[i].name.substr(0, val.length).toUpperCase() === val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong style='white-space: break-spaces'>" + catList[i].name.substr(0, val.length) + "</strong>";
                b.innerHTML += "<span style='white-space: break-spaces'>" + catList[i].name.substr(val.length) + "</span>";
                /*insert an input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + catList[i].name + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function () {
                    /*insert the value for the autocomplete text field:*/
                    filterInputField.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                    /* Update the list of active filters */
                    if (!activeFilters.includes(filterInputField.value.toUpperCase())) {
                        activeFilters.push(filterInputField.value.toUpperCase());
                        localStorage.setItem("activeFiltersKey", JSON.stringify(activeFilters));
                        updateTagList();
                    }
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    filterInputField.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode === 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode === 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode === 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt !== x[i] && elmnt !== filterInputField) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}

/* Filter the events based on the activeFilters list that is contained in the local storage and initialized as an emtpy
list in the beginning of a session */
function filter() {
    calendar.batchRendering(function () {
        let events = calendar.getEvents();
        for (let i = 0; i < events.length; i++) {
            let event = events[i];

            if (activeFilters.length === 0) {
                event.setProp('display', 'auto');
            } else {
                if (!activeFilters.includes(event.extendedProps.category.toUpperCase())) {
                    event.setProp('display', 'none');
                } else {
                    event.setProp('display', 'auto');
                }
            }
        }
        if (currentOverview === 'daily' && !isMobile) {
            updateDayList();
        }
    })
}

// Remove the filter input wrapper when toggling between days/weeks
function removeFilterInputWrapper() {

    let filterDiv = document.getElementById('id_filter-wrapper');
    if (filterDiv) {
        // closes the filter
        filterOpen = false;
        // removes the wrapper
        filterDiv.remove();
    }
}

/* Remove a filter of the tag that is clicked */
function deleteTag(tag) {
    /* Remove the tag element */
    var elem = document.getElementById(tag);
    elem.remove();

    /* Update also the tag list */
    var tag_name = elem.id.split('_')[1].toUpperCase();
    activeFilters = activeFilters.filter(e => e !== tag_name);
    localStorage.setItem("activeFiltersKey", JSON.stringify(activeFilters));
    filter();
}


/* Updates the displayed tags - there is a local storage that keeps the filter tags upon reloading, this function
serves to display the tags */
function updateTagList() {
    filter();

    activeFilters.forEach(filterInput => {

        /* Only if the filter tag does not exist yet, create a new one */
        if (!document.getElementById('id_' + filterInput)) {

            /* Get the corresponding category information (correct name, background and text color of the current
            category) and have default values available */
            var currentCategory = filterInput;
            var currentColor = '#334b7b';
            var currentTextColor = '#ffffff';
            categories.forEach(category => {
                if (category.name.toUpperCase() === filterInput) {
                    currentCategory = category.name;
                    currentColor = category.color;
                    currentTextColor = category.textColor;
                }
            })

            /* Create a new tag: a tag consists of a html label element and a button element */
            /* the label element has the category name and the background styling*/
            var filterTag = document.createElement("label");
            filterTag.innerHTML = currentCategory;
            var currentTag = "id_" + filterInput;
            filterTag.setAttribute("id", currentTag);
            filterTag.setAttribute("class", "filter-tag");
            filterTag.style.backgroundColor = currentColor;
            filterTag.style.color = currentTextColor;
            document.querySelectorAll('.fc-toolbar-chunk')[0].appendChild(filterTag);

            /* the button element has a cross and serves to delete the tag*/
            var filterTagBtn = document.createElement("button");
            filterTagBtn.innerHTML = 'x';
            var currentTagBtn = "id_" + filterInput + "_Btn";
            filterTagBtn.setAttribute("id", currentTagBtn);
            filterTagBtn.setAttribute("class", "filter-tag filter-tag-btn");
            filterTagBtn.style.backgroundColor = currentColor;
            filterTagBtn.style.color = currentTextColor;
            filterTagBtn.onclick = function () { deleteTag(currentTag) };
            document.getElementById(currentTag).appendChild(filterTagBtn);
        }
    })
}

updateTagList();

/* Update the height of the list view in the daily view - to make sure that the scrollbar appears at the correct point */
function updateListViewHeight() {
    /* Update the height of the event list in the daily view */
    var listEventsContainer = document.querySelector('.all-single-events');
    if (listEventsContainer) {
        var one = document.querySelector('.fc-view-harness').clientHeight;
        var two = document.querySelector('.list-view-col-header').clientHeight;
        listEventsContainer.style.height = (one - two - 10).toString() + 'px'; // minus 10 to make up for padding
    }
}
updateListViewHeight();
window.onresize = function () {
    updateListViewHeight();
    if(currentOverview === 'daily') {
        boxA.style.width = null;
        boxA.style.min_width = 200 + 'px';
    }
}

/* Make those task in the calendar that are marked as done half transparent */
const allEvents = document.querySelectorAll(".fc-timegrid-event");
allEvents.forEach(event => {
    if (event.innerHTML.includes('<s>')) {
        event.style.opacity = 0.5;
    }
})

const smartBtn = document.querySelector('.fc-smartButton-button');
smartBtn.classList.add('tooltip-initiator');
smartBtn.innerHTML += '<span class="tooltiptext">Click here to let the AI component place your open tasks in your ' +
    'calendar for you.</span>'