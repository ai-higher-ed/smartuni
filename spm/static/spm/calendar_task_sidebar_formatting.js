var accordionCollapse = document.querySelectorAll(".accordion-collapse");
accordionCollapse.forEach(collapse => {
    collapse.classList.add('collapse');
    collapse.classList.add('show');
})

var accordionButtons = document.querySelectorAll(".accordion-button");
accordionButtons.forEach(button => {
     /* Start in the collapsed view when the page is loaded //button.classList.add('collapsed');*/
    /* save toggle setting after opening/ closing accordeons */
    button.addEventListener('click', function () {
        saveToggleSetting();
    })
})

/* make sure done tasks do not show */
var doneCollapse = document.getElementById("id_collapseDone");
if (doneCollapse) {
    doneCollapse.classList.remove('show');
}
var doneAccordionButton = document.getElementById("id_doneTasksButton");
if (doneAccordionButton) {
    doneAccordionButton.classList.add('collapsed');
}

/* keep the opening of the sidebar accordions upon page reload for a session*/
setToggleSetting();
saveToggleSetting();

/* Shows the detail view of a task when it is clicked */
const allTasks = document.querySelectorAll(".list-group-item-task");
allTasks.forEach(task => {
    task.addEventListener('click', () => {
        url_suffix = task.getAttribute('data-id');
        window.location.href += 'task/' + url_suffix;
    })
    if (task.getAttribute('data-trigger')) {
        var val = task.getAttribute('data-trigger');
        if (val === "1") {
            document.getElementById("id_overdueLow").style.display = "block";
        } else if (val === "2") {
            document.getElementById("id_overdueMedium").style.display = "block";
        } else {
            document.getElementById("id_overdueHigh").style.display = "block";
        }
    }
})

/* If the checkbox of a task is clicked, the detail view is not to be shown */
const allCheckboxes = document.querySelectorAll('.form-check-input');
allCheckboxes.forEach(box => {
    box.addEventListener('click', (e) => {
        e.stopPropagation();
    })
})

/* save and get sidebar toogle information using an element id in the session storage*/
function saveToggleSetting() {
    var toggleSettings = [];
    var button_ids = [];
    accordionButtons.forEach(button => {
        toggleSettings.push(button.classList.contains('collapsed'));
        button_ids.push(button.getAttribute('id'));
    })
    if (sessionStorage.getItem('toggleSettings')) {
        sessionStorage.removeItem('toggleSettings');
        sessionStorage.setItem('toggleSettings', toggleSettings);
    } else {
        sessionStorage.setItem('toggleSettings', toggleSettings);
    }
    if (sessionStorage.getItem('toggleButtonIDs')) {
        sessionStorage.removeItem('toggleButtonIDs');
        sessionStorage.setItem('toggleButtonIDs', button_ids);
    } else {
        sessionStorage.setItem('toggleButtonIDs', button_ids);
    }
    var navSetting = document.getElementById("id_calendar-overview-sidenav").style.width;
    if (sessionStorage.getItem('navSetting')) {
        sessionStorage.removeItem('navSetting');
        sessionStorage.setItem('navSetting', navSetting);
    } else {
        sessionStorage.setItem('navSetting', navSetting);
    }
}

function setToggleSetting() {
    /* keep the opening of the sidebar upon page reload for a session*/
    if (sessionStorage.getItem('toggleSettings') && sessionStorage.getItem('toggleButtonIDs')) {
        var toggleSettings = sessionStorage.getItem('toggleSettings').split(',');
        var button_ids = sessionStorage.getItem('toggleButtonIDs').split(',');
        var counter = 0;
        accordionButtons.forEach(button => {
            var hasToggleSetting = false;
            var index = 0;
            button_ids.forEach(id => {
                if (id === button.getAttribute('id')) {
                    index = counter;
                    hasToggleSetting = true;
                } else {
                    counter +=1;
                }
            })
            counter = 0;

            if (hasToggleSetting) {
                let collapse = document.getElementById(button.getAttribute('data-bs-target')
                    .replace('#',''));

                if (toggleSettings[index] === 'true') {
                    if (!button.classList.contains('collapsed')) {
                        button.classList.add('collapsed');
                    }
                    if (collapse.classList.contains('show')) {
                        collapse.classList.remove('show');
                    }
                } else {
                    if (button.classList.contains('collapsed')) {
                        button.classList.remove('collapsed');
                        if (!collapse.classList.contains('show')) {
                            collapse.classList.add('show');
                        }
                    }
                }
            }
        })
    }
    if (sessionStorage.getItem('navSetting') !== '20vw') {
        toggleNav();
    }
}

/* open and close sidebar and adapt calendar size and margin to it */
function toggleNav() {
    var navSize = document.getElementById("id_calendar-overview-sidenav").style.width;
    if (navSize === '20vw') {
        document.getElementById("id_calendar-overview-sidenav").style.width = "35px";
        document.getElementById("id_main-smartplanner").style.marginRight = "35px";
        document.getElementById("id_sidenav-content").style.display = "none";
        document.getElementById("id_toggle-sidebar-button")
            .className = 'toggle-sidebar-button toggle-sidebar-button-open';

    } else {
        document.getElementById("id_calendar-overview-sidenav").style.width = "20vw";
        var width = (document.getElementById("id_calendar-overview-sidenav").offsetWidth).toString() + "px";
        document.getElementById("id_main-smartplanner").style.marginRight = width;
        document.getElementById("id_sidenav-content").style.display = "block";
        document.getElementById("id_toggle-sidebar-button")
            .className = 'toggle-sidebar-button toggle-sidebar-button-close';

    }
    saveToggleSetting();
    calendar.updateSize();
}

document.getElementById("id_toggle-sidebar-button").addEventListener("click", toggleNav);
toggleNav();

/* adapt top margin of sidebar and calendar margin to width of the screen */
$(window).resize(function () {
    var marginWidth = (document.getElementById("id_calendar-overview-sidenav").offsetWidth).toString() + "px";
    document.getElementById("id_main-smartplanner").style.marginRight = marginWidth;
    winWidth = $(window).width();
    if ($(window).width() < 768) {
        document.getElementById("id_calendar-overview-sidenav").style.top = "120px";
        document.getElementById("id_toggle-sidebar-button").style.top = "150px";
    } else {
        document.getElementById("id_calendar-overview-sidenav").style.top = "40px";
        document.getElementById("id_toggle-sidebar-button").style.top = "65px";
    }
});

