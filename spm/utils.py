import datetime
import pandas as pd
import numpy as np


# converts the duration string into a datetime.timedelta object
def parse_duration(value):

    # since our widget inherits from time input, the value for creating a task would be a datetime object,
    # while after saving it is a string
    # to treat both versions the same, the value is converted first
    duration_str = str(value)
    minutes = int(duration_str[3] + duration_str[4])
    hours = int(duration_str[0] + duration_str[1])
    seconds = minutes * 60 + hours * 3600  # needs to be converted because timedelta is saved in seconds

    return datetime.timedelta(days=0, seconds=seconds, microseconds=0)


def get_recurrence_dates(selection, event_form, recurrence):
    dropdown = {
        1: str(recurrence.repeat_freq * 7) + 'D',
        2: str(recurrence.repeat_freq) + 'MS',
        3: str(recurrence.repeat_freq) + 'YS'
    }

    if selection == 1:
        wkds = [recurrence.weekdays.monday, recurrence.weekdays.tuesday, recurrence.weekdays.wednesday,
                recurrence.weekdays.thursday, recurrence.weekdays.friday, recurrence.weekdays.saturday,
                recurrence.weekdays.sunday]
        include_days = [i for i, wkd in enumerate(wkds) if wkd]
        day_deltas = np.unique(np.array([(day - event_form.instance.startdate.weekday()) % 7 for day in include_days]))

        for i, day_delta in enumerate(day_deltas):

            new_startdate = event_form.instance.startdate + datetime.timedelta(days=int(day_delta))
            datetime_series = pd.bdate_range(new_startdate, recurrence.repeat_end,
                                             freq=dropdown[selection]).to_series()

            if i == 0:
                timeslots = pd.DataFrame({'datetime': datetime_series, 'weekday': datetime_series.dt.weekday})
            else:
                timeslots = timeslots.append(
                    (pd.DataFrame({'datetime': datetime_series, 'weekday': datetime_series.dt.weekday})))
    else:
        if selection == 2:
            datetime_series = (pd.bdate_range(datetime.date(event_form.instance.startdate.year,
                                                            event_form.instance.startdate.month,
                                                            1),
                                              recurrence.repeat_end,
                                              freq=dropdown[selection]) + pd.DateOffset(
                days=event_form.instance.startdate.day - 1)).to_series()
        elif selection == 3:
            datetime_series = (pd.bdate_range(datetime.date(event_form.instance.startdate.year, 1, 1),
                                              recurrence.repeat_end,
                                              freq=dropdown[selection]) + pd.DateOffset(
                days=event_form.instance.startdate.day - 1, months=event_form.instance.startdate.month - 1)).to_series()

        # Drop those days that do not fit the scheme (e.g. there is no 30th of February, hence the dataframe uses
        # the 2nd of March -> has to be deleted)
        drop_days = [date for date in datetime_series if date.day != event_form.instance.startdate.day]
        datetime_series = datetime_series.drop(drop_days)

        timeslots = pd.DataFrame({'datetime': datetime_series, 'weekday': datetime_series.dt.weekday})

    # Bring the timeslots to the correct event_format
    datetime_timeslots = timeslots['datetime'].index.strftime('%Y-%m-%d')

    return datetime_timeslots
