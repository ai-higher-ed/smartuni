from django.urls import path
from . import views

urlpatterns = [
    path('weekly/<str:date>/', views.WeeklyListView.as_view(), name='weekly'),
    path('daily/<str:date>/', views.DailyListView.as_view(), name='daily'),
    path('', views.planner_startpage, name='planner-startpage'),
    path('weekly/', views.weekly_to_date, name='weekly-to-date'),
    path('daily/', views.daily_to_date, name='daily-to-date'),

    path('<str:overview>/<str:date>/insertSmartTasks/', views.insertSmartTasksIntoCalendar,
         name='calendar-insert-smart-tasks'),
]

# <type:name> can be used as url parameters
urlpatterns += [
    path('<str:overview>/<str:date>/event/new/', views.EventCreate.as_view(), name='event-new'),
    path('<str:overview>/<str:date>/task/new/', views.TaskCreate.as_view(), name='task-new'),
    path('<str:overview>/<str:date>/<uuid:pk1>/<str:pk2>/updateDecisionTreeData/', views.createDecisionTreeEntry,
         name='decision-tree-task-new')
]

urlpatterns += [
    path('<str:overview>/<str:date>/event/<uuid:pk>/update/', views.EventUpdate.as_view(), name='event-update'),
    path('<str:overview>/<str:date>/task/<uuid:pk>/update/', views.TaskUpdate.as_view(), name='task-update'),
]

urlpatterns += [
    path('<str:overview>/<str:date>/event/<uuid:pk>/delete/', views.EventDelete.as_view(), name='event-delete'),
    path('<str:overview>/<str:date>/task/<uuid:pk>/delete/', views.TaskDelete.as_view(), name='task-delete'),
]

urlpatterns += [
    path('<str:overview>/<str:date>/event/<uuid:pk>/', views.EventDetailView.as_view(), name='event-detail'),
    path('<str:overview>/<str:date>/task/<uuid:pk>/', views.TaskDetailView.as_view(), name='task-detail'),
]

urlpatterns += [
    path('<str:overview>/<str:date>/updateTaskCalendar/', views.updateTaskForCalendar, name='calendar-update'),
    path('<str:overview>/<str:date>/task/<uuid:pk>/update/updateTaskLength/',
         views.updateTaskLengthAfterDurationChange, name='task-update-length'),
    path('<str:overview>/<str:date>/removeTaskCalendar/', views.removeTaskFromCalendar, name='calendar-update-removal'),
    path('<str:overview>/<str:date>/task/<uuid:pk>/toggleComplete/', views.completeTask, name='complete'),
]

urlpatterns += [
    path('<str:overview>/<str:date>/smartPlanningSettings/', views.SmartPlanning.as_view(),
         name='smart-planning-settings'),
    path('<str:overview>/<str:date>/insertSmartTasks/openTasksLeft/', views.OpenTasks.as_view(),
         name='open-task-warning'),
]