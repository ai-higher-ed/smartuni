from django import forms
from django.forms import ModelForm
from .models import Event, Task, Category, DecisionTreeTask, Weekdays, SmartPlanningSettings, TemporarySmartTasks, Recurrence
from .widgets import CustomDurationInput, ColorInput
from django.core.exceptions import ValidationError


class CategoryForm(ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'color')

        widgets = {
            'name': forms.TextInput(
                attrs={
                    'placeholder': 'Category',
                    'class': 'widget-inputs widget-text-fields'
                }
            ),
            'color': ColorInput(
                attrs={
                    'placeholder': '#334B7B',
                    'class': 'widget-inputs widget-color clickable active'
                }
            )
        }


class EventForm(ModelForm):

    # clean_<field-name> functions can be used to raise validation errors if an input is not valid
    def clean_endtime(self):
        # normally use the cleaned data but not here because this will contradict the clean_enddate function
        enddate = self.data.get('enddate')
        startdate = self.data.get('startdate')
        # here use the cleaned data again
        starttime = self.cleaned_data['starttime']
        endtime = self.cleaned_data['endtime']

        if (startdate == enddate) & (endtime < starttime):
            raise ValidationError('Invalid time', code='invalid')

        return endtime

    def clean_enddate(self):
        enddate = self.cleaned_data['enddate']
        startdate = self.cleaned_data['startdate']

        if startdate > enddate:
            raise ValidationError('Invalid date', code='invalid')

        return enddate

    class Meta:
        model = Event
        fields = ('title', 'startdate', 'enddate', 'starttime', 'endtime',
                  'location', 'comment', 'recurrence_true')
        # this is needed for the test_forms.py
        # since the user is a required field, it wouldn't work otherwise
        exclude = ('user',)

        widgets = {
            'title': forms.TextInput(
                attrs={
                    'placeholder': 'New event',
                    'class': 'widget-inputs widget-title',  # add css styling classes here
                    # multiple classes are also possible
                    # don't make one class for every field
                    # add a specific call only if there needs to be a special design for that field
                }
            ),
            'startdate': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields active'
                }
            ),
            'enddate': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields'
                }
            ),
            'starttime': forms.TimeInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-time-fields active'
                },
                format='%H:%M'
                # format is needed because the .stepUp()/.stepDown() functions in the js file use this format
                # default would be '%H:%M:%S' so comparison between changed and unchanged inputs
                # are not possible without it
            ),
            'endtime': forms.TimeInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-time-fields active'
                },
                format='%H:%M'
            ),
            'location': forms.TextInput(
                attrs={
                    'placeholder': 'Location',
                    'class': 'widget-inputs widget-text-fields'
                }
            ),
            'comment': forms.Textarea(
                attrs={
                    'placeholder': 'Comment',
                    'cols': '0',
                    'rows': '0',
                    'class': 'widget-inputs widget-text-fields'
                }
            ),
            'recurrence_true': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-recurrence-checkbox'
                }
            ),
        }


class TaskForm(ModelForm):

    # insert clean functions here if needed (see also in views.py forms.is_invalid() for TaskCreate)

    # clean_<field-name> functions can be used to raise validation errors if an input is not valid
    def clean_duration(self):
        duration = self.cleaned_data['duration']
        if duration.total_seconds() < 300:  # smaller than 5 minutes
            raise ValidationError('Duration is too small', code='invalid')

        return duration

    class Meta:
        model = Task
        fields = ('title', 'deadline_date', 'deadline_time',
                  'duration', 'priority', 'description', 'complete', 'ai_scheduler')
        exclude = ('user',)

        widgets = {
            'title': forms.TextInput(
                attrs={
                    'placeholder': 'New task',
                    'class': 'widget-inputs widget-title',
                }
            ),
            'description': forms.Textarea(
                attrs={
                    'placeholder': 'Description',
                    'class': 'widget-inputs widget-description'
                }
            ),
            'deadline_date': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields active'
                }
            ),
            'deadline_time': forms.TimeInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-time-fields active'
                },
                format='%H:%M'
            ),
            'duration': CustomDurationInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-duration display-none'
                }
            ),
            'priority': forms.Select(
                attrs={
                    'class': 'widget-inputs widget-category'
                }
            ),
            'ai_scheduler': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-ai_scheduler'
                }
            ),
        }


class DecisionTreeTaskForm(ModelForm):

    class Meta:
        model = DecisionTreeTask
        fields = ('user', 'weekday', 'time', 'priority', 'deadline', 'duration', 'category', 'as_planned')


class WeekdaysForm(ModelForm):

    class Meta:
        model = Weekdays
        fields = ('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')
        # since the user is a required field, it wouldn't work otherwise
        exclude = ('user',)

        widgets = {
            'monday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'tuesday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'wednesday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'thursday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'friday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'saturday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
            'sunday': forms.CheckboxInput(
                attrs={
                    'class': 'widget-inputs widget-weekday'
                }
            ),
        }


class SmartPlanningSettingsForm(ModelForm):

    class Meta:
        model = SmartPlanningSettings
        fields = ('weekdays', 'smart_startdate', 'smart_enddate', 'smart_starttime', 'smart_endtime')
        # since the user is a required field, it wouldn't work otherwise
        exclude = ('user',)

        widgets = {
            'smart_startdate': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields active widget-smart-settings'
                }
            ),
            'smart_enddate': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields active widget-smart-settings'
                }
            ),
            'smart_starttime': forms.TimeInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-time-fields active widget-smart-settings'
                },
                format='%H:%M'
                # format is needed because the .stepUp()/.stepDown() functions in the js file use this format
                # default would be '%H:%M:%S' so comparison between changed and unchanged inputs
                # are not possible without it
            ),
            'smart_endtime': forms.TimeInput(
                attrs={
                    'type': 'time',
                    'class': 'widget-inputs widget-time-fields active widget-smart-settings'
                },
                format='%H:%M'
            ),
        }


class TemporarySmartTasksForm(ModelForm):

    class Meta:
        model = TemporarySmartTasks
        fields = ('task_id',)

        # since the user is a required field, it wouldn't work otherwise
        exclude = ('user',)


class RecurrenceForm(ModelForm):
    class Meta:
        model = Recurrence
        fields = ('weekdays', 'repeat_freq', 'repeat_type', 'repeat_end')

        widgets = {
            'repeat_end': forms.DateInput(
                attrs={
                    'type': 'date',
                    'class': 'widget-inputs widget-time-fields active'
                }
            ),
            'repeat_freq': forms.NumberInput(
                attrs={
                    'type': 'number',
                    'class': 'widget-inputs widget-recurrence-freq',
                    'min': 1
                }
            ),
            'repeat_type': forms.Select(
                attrs={
                    'class': 'widget-inputs'
                }
            )
        }