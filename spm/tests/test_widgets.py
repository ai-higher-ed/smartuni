import datetime
from django.test import TestCase
from spm.widgets import CustomDurationInput, ColorInput


class DurationInputWidgetTest(TestCase):

    # test if different inputs produce the same output format
    def test_duration_format_with_input_is_none(self):
        widget = CustomDurationInput()
        value = None
        expected_format = widget.format_value(value)
        self.assertEqual('00:30', expected_format)

    def test_duration_format_with_input_is_string(self):
        widget = CustomDurationInput()
        value = '00:30:00'
        expected_format = widget.format_value(value)
        self.assertEqual('00:30', expected_format)

    def test_duration_format_with_input_is_time_object(self):
        widget = CustomDurationInput()
        value = datetime.time(0, 30)
        expected_format = widget.format_value(value)
        self.assertEqual('00:30', expected_format)


class ColorInputWidgetTest(TestCase):

    # test if the right template is used
    def test_template(self):
        widget = ColorInput()
        excpected_template = widget.template_name
        self.assertEqual('django/forms/widgets/input.html', excpected_template)

    # test input type
    def test_input_type(self):
        widget = ColorInput()
        expected_type = widget.input_type
        self.assertEqual('color', expected_type)

