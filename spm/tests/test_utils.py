import datetime
from django.test import TestCase
from spm.utils import parse_duration, get_recurrence_dates
from spc.models import SmartUser
from spm.models import Recurrence, Weekdays
from spm.forms import EventForm


class ParseDurationTest(TestCase):

    # test the output format for different input formats
    def test_output_is_timedelta_with_input_is_string(self):
        value = '00:30:00'
        expected_parse_value = parse_duration(value)
        self.assertTrue(isinstance(expected_parse_value, datetime.timedelta))
        self.assertEqual(1800, expected_parse_value.seconds)

    def test_output_is_timedelta_with_input_is_datetime_time(self):
        value = datetime.time(0, 30)
        expected_parse_value = parse_duration(value)
        self.assertTrue(isinstance(expected_parse_value, datetime.timedelta))
        self.assertEqual(1800, expected_parse_value.seconds)


class GetRecurrenceDatesTest(TestCase):

    def setUp(self):
        # email, first_name and last_name are the required fields, username is needed for the login
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        # password cannot be set in create function because it will be hashed and cannot be accessed afterwards
        smart_user.set_password('user')
        smart_user.save()

        weekdays = Weekdays.objects.create(user=smart_user, monday=True, wednesday=True, friday=True)

        Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da3', weekdays=weekdays, repeat_freq=2,
                                  repeat_type=1, repeat_end='2022-08-28')

        Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da4', repeat_freq=3,
                                  repeat_type=2, repeat_end='2023-02-28')

        Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da5', repeat_freq=1,
                                  repeat_type=3, repeat_end='2023-08-28')

    def test_weekly_recurrence_time_slots(self):

        recurrence = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')

        form = EventForm(data={
            'title': 'TestEventWeekly',
            'startdate': datetime.date(2022, 8, 1),
            'enddate': datetime.date(2022, 8, 1),
            'starttime': datetime.time(12, 0),
            'endtime': datetime.time(13, 0),
            'location': 'Somewhere',
            'comment': 'Something',
            'category': None,
            'recurrence_true': True,
            'recurrence': recurrence,
            'groupID': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'
        })

        form.full_clean()

        expected_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)
        timeslots = ['2022-08-01', '2022-08-15', '2022-08-03', '2022-08-17', '2022-08-05', '2022-08-19']

        for i in range(len(expected_timeslots)):
            self.assertEqual(timeslots[i], expected_timeslots[i])

    def test_monthly_recurrence_time_slots(self):

        recurrence = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da4')

        form = EventForm(data={
            'title': 'TestEventWeekly',
            'startdate': datetime.date(2022, 8, 1),
            'enddate': datetime.date(2022, 8, 1),
            'starttime': datetime.time(12, 0),
            'endtime': datetime.time(13, 0),
            'location': 'Somewhere',
            'comment': 'Something',
            'category': None,
            'recurrence_true': True,
            'recurrence': recurrence,
            'groupID': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'
        })

        form.full_clean()

        expected_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)
        timeslots = ['2022-08-01', '2022-11-01', '2023-02-01']

        for i in range(len(expected_timeslots)):
            self.assertEqual(timeslots[i], expected_timeslots[i])

    def test_yearly_recurrence_time_slots(self):

        recurrence = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73da5')

        form = EventForm(data={
            'title': 'TestEventWeekly',
            'startdate': datetime.date(2022, 8, 1),
            'enddate': datetime.date(2022, 8, 1),
            'starttime': datetime.time(12, 0),
            'endtime': datetime.time(13, 0),
            'location': 'Somewhere',
            'comment': 'Something',
            'category': None,
            'recurrence_true': True,
            'recurrence': recurrence,
            'groupID': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da5'
        })

        form.full_clean()

        expected_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)
        timeslots = ['2022-08-01', '2023-08-01']

        for i in range(len(expected_timeslots)):
            self.assertEqual(timeslots[i], expected_timeslots[i])


