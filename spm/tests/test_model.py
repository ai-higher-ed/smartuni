import datetime
from django.test import TestCase
from spm.models import Category, Event, Task, Weekdays, TemporarySmartTasks, Recurrence
from spc.models import SmartUser
from datetime import timedelta

# the model tests contain all functions and all parameters that have an impact on the design
# only what is part of the models.py file and could potentially break due to changes is checked here
# everything else is handled by django


class CategoryModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()
        # Set up non-modified objects used by all test methods
        Category.objects.create(name='uni', user=smart_user)
        Category.objects.create(name=None, user=smart_user)

    # all modifications to the data that have to be done are in the functions below
    # mostly not used in model classes
    def test_category_label(self):
        category = Category.objects.get(id=1)
        field_label = category._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'category')

    def test_category_name_max_length(self):
        category = Category.objects.get(id=1)
        max_length = category._meta.get_field('name').max_length
        self.assertEqual(max_length, 16)

    def test_object_name_is_category_name(self):
        category = Category.objects.get(id=1)
        expected_object_name = f'{category.name}'
        self.assertEqual(str(category), expected_object_name)

    def test_object_name_is_default_category_name(self):
        category = Category.objects.get(id=2)
        expected_object_name = f'default {category.user} category'
        self.assertEqual(str(category), expected_object_name)

    def test_default_color_when_no_color(self):
        category = Category.objects.get(id=1)
        expected_color_value = f'{category.color}'
        self.assertEqual('#334B7B', expected_color_value)

    def test_category_color_max_length(self):
        category = Category.objects.get(id=1)
        max_length = category._meta.get_field('color').max_length
        self.assertEqual(max_length, 7)

    def test_recurrence_true_default(self):
        recurrence_true_default = Event._meta.get_field('recurrence_true').default
        self.assertFalse(recurrence_true_default)


class EventModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()
        # test event
        # the ID of an event is a uuid and needs to be specified to get access in the later functions
        # also the values for the __str__ method need to be defined here
        Event.objects.create(title='TestEvent', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

    # writing for every aspect its own function makes it easier to recognize which test failed
    # accordingly the functions should have clear names
    def test_title_max_length(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        max_length = event._meta.get_field('title').max_length
        self.assertEqual(max_length, 100)

    def test_location_max_length(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        max_length = event._meta.get_field('location').max_length
        self.assertEqual(max_length, 100)

    def test_comment_max_length(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        max_length = event._meta.get_field('comment').max_length
        self.assertEqual(max_length, 2000)

    def test_get_absolute_url_weekly(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        # This will also fail if the urlconf is not defined.
        self.assertEqual(event.get_absolute_url_weekly(),
                         '/spm/weekly/2022-05-17/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/')

    def test_get_absolute_url_daily(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        # This will also fail if the urlconf is not defined.
        self.assertEqual(event.get_absolute_url_daily(),
                         '/spm/daily/2022-05-17/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/')

    def test_object_name_is_title_date_and_time(self):
        event = Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        expected_object_name = f'{event.title}: {event.startdate}, {event.starttime} to {event.enddate}, {event.endtime}'
        self.assertEqual(str(event), expected_object_name)


class TaskModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()
        # test task
        # the ID of a task is a uuid and needs to be specified to get access in the later functions
        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1',
                            deadline_date='2022-05-17', deadline_time='16:00', duration=timedelta(minutes=30),
                            user=smart_user)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                            deadline_date='2022-05-17', deadline_time='16:00', duration=timedelta(minutes=30),
                            user=smart_user, startdate='2022-06-19')

    # writing for every aspect its own function makes it easier to recognize which test failed
    # accordingly the functions should have clear names
    def test_title_max_length(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        max_length = task._meta.get_field('title').max_length
        self.assertEqual(max_length, 100)

    def test_description_max_length(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        max_length = task._meta.get_field('description').max_length
        self.assertEqual(max_length, 2000)

    def test_priority_default(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        default_priority = task._meta.get_field('priority').default
        self.assertEqual(default_priority, 1)

    def test_complete_default(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        default_complete = task._meta.get_field('complete').default
        self.assertFalse(default_complete)

    def test_ai_scheduler_default(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        default_complete = task._meta.get_field('ai_scheduler').default
        self.assertTrue(default_complete)

    def test_get_absolute_url_weekly(self):
        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        # This will also fail if the urlconf is not defined.
        self.assertEqual(task1.get_absolute_url_weekly(),
                         '/spm/weekly/2022-05-17/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')
        self.assertEqual(task2.get_absolute_url_weekly(),
                         '/spm/weekly/2022-06-19/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/')

    def test_get_absolute_url_daily(self):
        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        # This will also fail if the urlconf is not defined.
        self.assertEqual(task1.get_absolute_url_daily(),
                         '/spm/daily/2022-05-17/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')
        self.assertEqual(task2.get_absolute_url_daily(),
                         '/spm/daily/2022-06-19/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/')

    def test_object_name_is_title_and_deadline(self):
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        expected_object_name = f'{task.title} until {task.deadline_date}, {task.deadline_time}'
        self.assertEqual(str(task), expected_object_name)


class WeekdaysModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()
        # Set up non-modified objects used by all test methods
        Weekdays.objects.create(user=smart_user)

    def test_monday_default(self):
        day_default = Weekdays._meta.get_field('monday').default
        self.assertFalse(day_default)

    def test_tuesday_default(self):
        day_default = Weekdays._meta.get_field('tuesday').default
        self.assertFalse(day_default)

    def test_wednesday_default(self):
        day_default = Weekdays._meta.get_field('wednesday').default
        self.assertFalse(day_default)

    def test_thursday_default(self):
        day_default = Weekdays._meta.get_field('thursday').default
        self.assertFalse(day_default)

    def test_friday_default(self):
        day_default = Weekdays._meta.get_field('friday').default
        self.assertFalse(day_default)

    def test_saturday_default(self):
        day_default = Weekdays._meta.get_field('saturday').default
        self.assertFalse(day_default)

    def test_sunday_default(self):
        day_default = Weekdays._meta.get_field('sunday').default
        self.assertFalse(day_default)

    def test_smart_settings_default(self):
        smart_settings_default = Weekdays._meta.get_field('smart_settings').default
        self.assertFalse(smart_settings_default)


class TemporarySmartTasksTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()
        # Set up non-modified objects used by all test methods
        TemporarySmartTasks.objects.create(user=smart_user, task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        TemporarySmartTasks.objects.create(user=smart_user, task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')

    def test_task_id_max_length(self):
        temp_task = TemporarySmartTasks.objects.get(task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        max_length = temp_task._meta.get_field('task_id').max_length
        self.assertEqual(max_length, 40)


class RecurrenceModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # user cannot be null so this has to be created
        smart_user = SmartUser.objects.create()

        weekdays_obj = Weekdays.objects.create(user=smart_user)

        Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73db7', repeat_end='2023-01-06',
                                  weekdays=weekdays_obj)

    def test_repeat_freq_default(self):
        recurrence_obj = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73db7')
        default_repeat_freq = recurrence_obj._meta.get_field('repeat_freq').default
        self.assertEqual(default_repeat_freq, 1)

    def test_repeat_type_default(self):
        recurrence_obj = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73db7')
        default_repeat_type = recurrence_obj._meta.get_field('repeat_type').default
        self.assertEqual(default_repeat_type, 1)
