from django.test import TestCase
from django.urls import reverse
from spc.models import SmartUser
from spm.models import Event, Task, Category, Weekdays, DecisionTreeTask, SmartPlanningSettings, \
    TemporarySmartTasks, Recurrence
import datetime


class SmartplannerStartpageRedirectionTest(TestCase):

    def setUp(self):
        # email, first_name and last_name are the required fields, username is needed for the login
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        # password cannot be set in create function because it will be hashed and cannot be accessed afterwards
        smart_user.set_password('user')
        smart_user.save()

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('planner-startpage'))
        # check if we go the response "found"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/')

    def test_url_redirection_to_weekly_overview_if_smartplanner_startpage(self):
        date = datetime.date.today()
        expected_response = '/spm/weekly/' + str(date) + '/'
        # login is required before redirection is possible
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('planner-startpage'))
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)


class WeeklyOverviewRedirectionTest(TestCase):

    def setUp(self):
        # email, first_name and last_name are the required fields, username is needed for the login
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        # password cannot be set in create function because it will be hashed and cannot be accessed afterwards
        smart_user.set_password('user')
        smart_user.save()

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('weekly-to-date'))
        # check if we go the response "found"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/')

    def test_url_redirection_to_weekly_overview_if_smartplanner_startpage(self):
        date = datetime.date.today()
        expected_response = '/spm/weekly/' + str(date) + '/'
        # login is required before redirection is possible
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly-to-date'))
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)


class DailyOverviewRedirectionTest(TestCase):

    def setUp(self):
        # email, first_name and last_name are the required fields, username is needed for the login
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        # password cannot be set in create function because it will be hashed and cannot be accessed afterwards
        smart_user.set_password('user')
        smart_user.save()

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in(self):
        response = self.client.get(reverse('daily-to-date'))
        # check if we go the response "found"
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/')

    def test_url_redirection_to_weekly_overview_if_smartplanner_startpage(self):
        date = datetime.date.today()
        expected_response = '/spm/daily/' + str(date) + '/'
        # login is required before redirection is possible
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily-to-date'))
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)


class WeeklyListViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged(self):
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/2022-04-28/')

    # check if the right url is chosen when calling the get method
    def test_get_weekly_view(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/weekly.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('weekly', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class DailyListViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged(self):
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/2022-04-28/')

    # check if the right url is chosen when calling the get method
    def test_get_weekly_view(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/daily.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('daily', kwargs={'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class EventCreateViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

        Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1', repeat_freq=1,
                                  repeat_type=3, repeat_end='2023-08-28')

        Weekdays.objects.create(id=1, user=smart_user, monday=True, wednesday=True, friday=True)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/2022-04-28/event/new/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/2022-04-28/event/new/')

    # check if the right url is chosen when calling the get method
    def test_get_create_event_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_create_event_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/event_form.html')

    # check if the right initial values are used
    def test_initial_startdate(self):
        self.client.login(username='user', password='user')
        date = '2022-04-28'
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': date}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].initial['startdate'], date)

    def test_initial_enddate(self):
        self.client.login(username='user', password='user')
        date = '2022-04-28'
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': date}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].initial['enddate'], date)

    def test_initial_starttime(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(12, 0)
        self.assertEqual(response.context['form'].initial['starttime'], expected_time)

    def test_initial_endtime(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(13, 0)
        self.assertEqual(response.context['form'].initial['endtime'], expected_time)

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_form' in response.context)
        self.assertTrue('weekdays_form' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_form' in response.context)
        self.assertTrue('weekdays_form' in response.context)

    # check success url
    def test_success_url_weekly(self):
        # set all required fields except user
        post = {
            'title': 'TestEvent',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        # the user is set using the form_valid function if logged in
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

    def test_success_url_daily(self):
        post = {
            'title': 'TestEvent',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/')

    # check if form is valid
    def test_valid_form_saved(self):
        post = {
            'title': 'TestEvent3',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        # gets the event and checks if the user was set correctly
        event = Event.objects.get(title='TestEvent3')
        self.assertIsNotNone(event)
        self.assertEqual(str(event.user), 'user')
        # checks the success message
        self.assertRaisesMessage(response, 'Event was successfully created!')

    # check response when invalid input
    def test_form_invalid_enddate_before_startdate(self):
        post = {
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today() - datetime.timedelta(days=1),
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'enddate', 'Invalid date')

    def test_form_invalid_endtime_before_starttime_if_enddate_equals_startdate(self):
        post = {
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(11),
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'endtime', 'Invalid time')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)

    def test_recurrence_form_initial_enddate(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('event-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        expected_enddate = response.context['recurrence_form'].initial['repeat_end']
        self.assertEqual(datetime.datetime.strptime('2022-04-28', '%Y-%m-%d')
                         + datetime.timedelta(days=90), expected_enddate)

    def test_normal_event(self):
        post = {
            'title': 'TestEvent3',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
            'recurrence_true': False
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        # gets the event and checks if the user was set correctly
        event = Event.objects.get(title='TestEvent3')
        self.assertIsNotNone(event)
        self.assertEqual(str(event.user), 'user')
        self.assertIsNone(event.groupID)
        # checks the success message
        self.assertRaisesMessage(response, 'Event was successfully created!')

    def test_weekly_recurrence_with_freq_2(self):
        post = {
            'title': 'TestEvent3',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=25),
            'repeat_type': 1,
            'repeat_freq': 2,
            'recurrence_true': True
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        # gets the event and checks if the user was set correctly
        events = Event.objects.filter(title='TestEvent3')
        self.assertEqual(2, len(events))
        recurrence = Recurrence.objects.get(groupID=events.get(startdate=datetime.date.today()).groupID)
        self.assertIsNotNone(recurrence)
        weekdays = Weekdays.objects.get(id=recurrence.weekdays.id)
        self.assertIsNotNone(weekdays)
        # checks the success message
        self.assertRaisesMessage(response, 'Event was successfully created!')

    def test_monthly_recurrence_with_freq_3(self):
        post = {
            'title': 'TestEvent3',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=100),
            'repeat_type': 2,
            'repeat_freq': 3,
            'recurrence_true': True
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        # gets the event and checks if the user was set correctly
        events = Event.objects.filter(title='TestEvent3')
        self.assertEqual(2, len(events))
        recurrence = Recurrence.objects.get(groupID=events.get(startdate=datetime.date.today()).groupID)
        self.assertIsNotNone(recurrence)
        self.assertIsNone(recurrence.weekdays)
        # checks the success message
        self.assertRaisesMessage(response, 'Event was successfully created!')

    def test_yearly_recurrence_with_freq_1(self):
        def test_monthly_recurrence_with_freq_3(self):
            post = {
                'title': 'TestEvent3',
                'startdate': datetime.date.today(),
                'enddate': datetime.date.today(),
                'starttime': datetime.time(12),
                'endtime': datetime.time(13),
                'name': 'TestCategry',
                'color': '#ffffff',
                'repeat_end': datetime.date.today() + datetime.timedelta(days=370),
                'repeat_type': 3,
                'repeat_freq': 1,
                'recurrence_true': True
            }
            self.client.login(username='user', password='user')
            response = self.client.post(reverse('event-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

            # gets the event and checks if the user was set correctly
            events = Event.objects.filter(title='TestEvent3')
            self.assertEqual(2, len(events))
            recurrence = Recurrence.objects.get(groupID=events.get(startdate=datetime.date.today()).groupID)
            self.assertIsNotNone(recurrence)
            self.assertIsNone(recurrence.weekdays)
            # checks the success message
            self.assertRaisesMessage(response, 'Event was successfully created!')


class TaskCreateViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/2022-04-28/task/new/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(reverse('task-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/2022-04-28/task/new/')

    # check if the right url is chosen when calling the get method
    def test_get_create_task_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_create_task_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/task_form.html')

    # check if the right initial values are used
    def test_initial_deadline_date(self):
        self.client.login(username='user', password='user')
        date = '2022-04-28'
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': date}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].initial['deadline_date'], date)

    def test_initial_deadline_time(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        time = datetime.datetime.now() + datetime.timedelta(hours=1)
        minutes = time.minute
        expected_time = (time - datetime.timedelta(minutes=minutes)).time()

        self.assertEqual(response.context['form'].initial['deadline_time'].hour, expected_time.hour)
        self.assertEqual(response.context['form'].initial['deadline_time'].minute, expected_time.minute)

    def test_initial_duration(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(0, 30)
        self.assertEqual(response.context['form'].initial['duration'], expected_time)

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check success url
    def test_success_url_weekly(self):
        # set all required fields except user
        post = {
            'title': 'TestTask',
            'deadline_date': datetime.date.today(),
            'deadline_time': (datetime.datetime.now() + datetime.timedelta(minutes=1)).time(),  # otherwise seconds are not equal
            'duration': datetime.time(0, 30),
            'priority': 1,
            'name': 'TestCategory',
            'color': '#ffffff'
        }

        # the user is set using the form_valid function if logged in
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

    def test_success_url_daily(self):
        post = {
            'title': 'TestTask',
            'deadline_date': datetime.date.today(),
            'deadline_time': (datetime.datetime.now() + datetime.timedelta(minutes=1)).time(),
            'duration': datetime.time(0, 30),
            'priority': 1,
            'name': 'TestCategory',
            'color': '#ffffff'
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('task-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/')

    # check if form is valid
    def test_valid_form_saved(self):
        post = {
            'title': 'TestTask',
            'deadline_date': datetime.date.today(),
            'deadline_time': (datetime.datetime.now() + datetime.timedelta(minutes=1)).time(),
            'duration': datetime.time(0, 30),
            'priority': 1,
            'name': 'TestCategory',
            'color': '#ffffff'
        }
        self.client.login(username='user', password='user')
        response = self.client.post(reverse('task-new', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        # check if a task was created
        self.assertEqual(Task.objects.count(), 7)
        # gets the task and checks if the user was set correctly
        task = Task.objects.get(title='TestTask')
        self.assertEqual(str(task.user), 'user')
        # checks the success message
        self.assertRaisesMessage(response, 'Task was successfully created!')

    # check response when invalid input
    def test_form_invalid_duration_smaller_five_minutes(self):
        post = {
            'duration': datetime.time(0, 0)
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'duration', 'Duration is too small')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class EventDetailViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        cat = Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Event.objects.create(title='TestEvent', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user, category=cat)

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                                last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        weekdays = Weekdays.objects.create(user=smart_user, friday=True)

        recurrence_1 = Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1', weekdays=weekdays,
                                                 repeat_end='2023-08-15')

        recurrence_2 = Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab2',
                                                 repeat_type=2, repeat_end='2023-08-15')

        Event.objects.create(title='TestEvent3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user, category=cat,
                             recurrence_true=True, recurrence=recurrence_1,
                             groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1')

        Event.objects.create(title='TestEvent4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da5',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user, category=cat,
                             recurrence_true=True, recurrence=recurrence_2,
                             groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab2')

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             '/accounts/login/?next=/spm/weekly/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             '/accounts/login/?next=/spm/daily/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')

    # check if the right url is chosen when calling the get method
    def test_get_detail_event_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_detail_event_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/event_detail.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        # pk is of type uuid and needs to be converted into string
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('color' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_obj' in response.context)
        self.assertFalse('recurrence_obj_weekdays' in response.context)
        self.assertIsNone(response.context['recurrence_obj'])

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('color' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_obj' in response.context)
        self.assertFalse('recurrence_obj_weekdays' in response.context)
        self.assertIsNone(response.context['recurrence_obj'])

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)

    def test_only_recurrence_in_context_data(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da5', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('recurrence_obj' in response.context)
        self.assertTrue('recurrence_obj_weekdays' in response.context)
        self.assertIsNone(response.context['recurrence_obj_weekdays'])
        recurrence = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab2')
        expected_recurrence = response.context['recurrence_obj']
        self.assertEqual(recurrence, expected_recurrence)

    def test_recurrence_and_weekdays_in_context_data(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('recurrence_obj' in response.context)
        self.assertTrue('recurrence_obj_weekdays' in response.context)
        recurrence = Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1')
        expected_recurrence = response.context['recurrence_obj']
        self.assertEqual(recurrence, expected_recurrence)
        weekdays = ['Friday']
        expected_weekdays = response.context['recurrence_obj_weekdays']
        self.assertEqual(weekdays, expected_weekdays)


class TaskDetailViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        cat = Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user,
                            category=cat)

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             '/accounts/login/?next=/spm/weekly/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             '/accounts/login/?next=/spm/daily/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da1/')

    # check if the right url is chosen when calling the get method
    def test_get_detail_task_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_detail_task_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/task_detail.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        # pk is of type uuid and needs to be converted into string
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('color' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('color' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-detail',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da1', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class EventUpdateViewTest(TestCase):

    # Tests for context data and form_valid are not complete!

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        cat = Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Event.objects.create(title='TestEvent', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user, category=cat)

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/weekly/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/daily/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/')

    # check if the right url is chosen when calling the get method
    def test_get_update_event_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_update_event_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/event_form.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_form' in response.context)
        self.assertTrue('weekdays_form' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_form' in response.context)
        self.assertTrue('weekdays_form' in response.context)

    # check success url
    def test_success_url_weekly(self):
        # set all required fields except user
        post = {
            'title': 'TestEventUpdate',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        # the user is set using the form_valid function if logged in
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

    def test_success_url_daily(self):
        post = {
            'title': 'TestEventUpdate',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('event-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/')

    # check if form is valid
    def test_valid_form_saved(self):
        post = {
            'title': 'TestEventUpdate',
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(13),
            'name': 'TestCategry',
            'color': '#ffffff',
            'repeat_end': datetime.date.today() + datetime.timedelta(days=1),
            'repeat_type': 1,
            'repeat_freq': 1,
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)

        # gets the event by its updated name
        self.assertTrue(Event.objects.get(title='TestEventUpdate'))
        # checks the success message
        self.assertRaisesMessage(response, 'Event was successfully updated!')

    # check response when invalid input
    def test_form_invalid_enddate_before_startdate(self):
        post = {
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today() - datetime.timedelta(days=1),
            'starttime': datetime.time(12),
            'endtime': datetime.time(12),
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'enddate', 'Invalid date')

    def test_form_invalid_endtime_before_starttime_if_enddate_equals_startdate(self):
        post = {
            'startdate': datetime.date.today(),
            'enddate': datetime.date.today(),
            'starttime': datetime.time(12),
            'endtime': datetime.time(11),
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'endtime', 'Invalid time')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class TaskUpdateViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        cat = Category.objects.create(name='TestCategory1', color='#ffffff', user=smart_user)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user,
                            category=cat)

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Category.objects.create(name='TestCategory2', color='#ffffff', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/weekly/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/daily/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/')

    # check if the right url is chosen when calling the get method
    def test_get_update_task_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_update_task_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/task_form.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('cat_form' in response.context)
        self.assertTrue('category_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check success url
    def test_success_url_weekly(self):
        # set all required fields except user
        post = {
            'title': 'TestTaskUpdate',
            'deadline_date': datetime.date.today(),
            'deadline_time': (datetime.datetime.now() + datetime.timedelta(minutes=1)).time(),
            'duration': datetime.time(0, 40),
            'priority': 1,
            'name': 'TestCategry',
            'color': '#ffffff'
        }
        # the user is set using the form_valid function if logged in
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/spm/weekly/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/updateTaskLength/',
            fetch_redirect_response=False)

    def test_success_url_daily(self):
        post = {
            'title': 'TestTaskUpdate',
            'deadline_date': datetime.date.today(),
            'deadline_time': (datetime.datetime.now() + datetime.timedelta(minutes=1)).time(),
            'duration': datetime.time(0, 40),
            'priority': 1,
            'name': 'TestCategry',
            'color': '#ffffff'
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/spm/daily/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/update/updateTaskLength/',
            fetch_redirect_response=False)

    # check if form is valid
    def test_valid_form_saved(self):
        post = {
            'title': 'TestTaskUpdate',
            'deadline_date': datetime.date.today() + datetime.timedelta(days=1),
            'deadline_time': (datetime.datetime.now() - datetime.timedelta(hours=1)).time(),
            'duration': datetime.time(0, 40),
            'priority': 2,
            'name': 'TestCategry',
            'color': '#ffffff'
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('task-update',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)

        # check if a task was created
        self.assertEqual(Task.objects.count(), 6)
        # gets the task by its updated name
        self.assertTrue(Task.objects.get(title='TestTaskUpdate'))
        # checks the success message
        self.assertRaisesMessage(response, 'Task was successfully updated!')

    # check response when invalid input
    def test_form_invalid_duration_smaller_five_minutes(self):
        post = {
            'duration': datetime.time(0, 0)
        }
        self.client.login(username='user', password='user')
        # call post method here to set values
        response = self.client.post(reverse('task-new', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 200)
        self.assertFormError(response, 'form', 'duration', 'Duration is too small')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_categories_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('category_list' in response.context)
        expected_cat_list = response.context['category_list']
        self.assertIn('TestCategory1', str(expected_cat_list))
        self.assertNotIn('TestCategory2', str(expected_cat_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class EventDeleteViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        Event.objects.create(title='TestEvent', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        weekdays = Weekdays.objects.create(user=smart_user, friday=True)

        recurrence = Recurrence.objects.create(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1', weekdays=weekdays,
                                               repeat_end='2023-08-15')

        Event.objects.create(title='TestEvent3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user,
                             recurrence_true=True, recurrence=recurrence,
                             groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1')

        Event.objects.create(title='TestEvent5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da6',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user,
                             recurrence_true=True, recurrence=recurrence,
                             groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1')

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                                last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/weekly/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/delete/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/daily/2022-04-28/event/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/delete/')

    # check if the right url is chosen when calling the get method
    def test_get_delete_event_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_delete_event_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/event_confirm_delete.html')

    # check if context data contains overview parameter
    def test_context_data_is_deleted_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_true' in response.context)
        self.assertFalse(response.context['recurrence_true'])

    def test_context_data_is_deleted_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        self.assertTrue('recurrence_true' in response.context)
        self.assertFalse(response.context['recurrence_true'])

    # check success url
    def test_success_url_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.delete(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

    def test_success_url_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.delete(
            reverse('event-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)

    def test_only_one_event_deleted(self):
        post = {
            'delete_this': ''
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

        try:
            Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
            flag = True
        except:
            flag = False

        self.assertFalse(flag)
        self.assertIsNotNone(Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1'))
        self.assertIsNotNone(Weekdays.objects.get(
            id=Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1').weekdays.id))
        self.assertIsNotNone(Event.objects.filter(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1'))

    def test_all_events_deleted(self):
        post = {
            'delete_all': ''
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('event-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2', 'date': '2022-04-28'}),
            post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

        try:
            Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2')
            Event.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da6')
            events_flag = True
        except:
            events_flag = False

        try:
            Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1')
            recurrence_flag = True
        except:
            recurrence_flag = False

        try:
            Weekdays.objects.get(
                id=Recurrence.objects.get(groupID='e4b34b82-fdf9-43c1-8811-cdd50bd73ab1').weekdays.id)
            weekdays_flag = True
        except:
            weekdays_flag = False

        self.assertFalse(events_flag)
        self.assertFalse(recurrence_flag)
        self.assertFalse(weekdays_flag)


class TaskDeleteViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user)

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da1',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/weekly/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/delete/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response, '/accounts/login/?next=/spm/daily/2022-04-28/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/delete/')

    # check if the right url is chosen when calling the get method
    def test_get_delete_task_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_delete_task_view_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/task_confirm_delete.html')

    # check if context data contains overview parameter
    def test_context_data_is_deleted_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    def test_context_data_is_deleted_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('pk' in response.context)
        self.assertTrue(str(response.context['pk']) == 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check success url
    def test_success_url_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.delete(
            reverse('task-delete',
                    kwargs={'overview': 'weekly', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/')

    def test_success_url_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.delete(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/')

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-delete',
                    kwargs={'overview': 'daily', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 1)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)


class CompleteTaskViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/toggleComplete/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/toggleComplete/')

    # check if the right url is chosen when calling the get method
    def test_get_complete_task_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_complete_task_view_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_changes_complete_state(self):
        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertFalse(task1.complete)
        self.assertIsNone(task1.complete_time)

        response_1 = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response_1.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertTrue(task2.complete)
        self.assertIsNotNone(task2.complete_time)

        response_2 = self.client.get(
            reverse('complete',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response_2.status_code, 302)

        task3 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertFalse(task3.complete)
        self.assertIsNone(task3.complete_time)


class UpdateTaskForCalendarViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('calendar-update',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/updateTaskCalendar/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('calendar-update',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/updateTaskCalendar/')

    # check if the right url is chosen when calling the get method
    def test_get_update_task_view_weekly(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
            'startDate': '2022-06-23',
            'startTime': '12:00',
            'endDate': '2022-06-23',
            'endTime': '13:00'
        }

        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('calendar-update',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}), post)

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_update_task_view_daily(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
            'startDate': '2022-06-23',
            'startTime': '12:00',
            'endDate': '2022-06-23',
            'endTime': '13:00'
        }

        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('calendar-update',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}), post)

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_dates_get_updated(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
            'startDate': '2022-06-23',
            'startTime': '12:00',
            'endDate': '2022-06-23',
            'endTime': '13:00'
        }

        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertIsNone(task1.startdate)
        self.assertIsNone(task1.starttime)
        self.assertIsNone(task1.enddate)
        self.assertIsNone(task1.endtime)

        response = self.client.post(
            reverse('calendar-update',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}), post)
        self.assertEqual(response.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertEqual(task2.startdate, datetime.date(2022, 6, 23))
        self.assertEqual(task2.starttime, datetime.time(12, 0))
        self.assertEqual(task2.enddate, datetime.date(2022, 6, 23))
        self.assertEqual(task2.endtime, datetime.time(13, 0))


class RemoveTaskFromCalendarViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user,
                            startdate='2022-06-23', starttime='12:00', enddate='2022-06-23', endtime='13:00')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('calendar-update-removal',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/removeTaskCalendar/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('calendar-update-removal',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/removeTaskCalendar/')

    # check if the right url is chosen when calling the get method
    def test_get_remove_task_view_weekly(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'
        }

        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('calendar-update-removal',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}), post)

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_remove_task_view_daily(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'
        }

        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('calendar-update-removal',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}), post)

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_dates_get_removed(self):
        post = {
            'id': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'
        }

        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertEqual(task1.startdate, datetime.date(2022, 6, 23))
        self.assertEqual(task1.starttime, datetime.time(12, 0))
        self.assertEqual(task1.enddate, datetime.date(2022, 6, 23))
        self.assertEqual(task1.endtime, datetime.time(13, 0))

        response = self.client.post(
            reverse('calendar-update-removal',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}), post)
        self.assertEqual(response.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertIsNone(task2.startdate)
        self.assertIsNone(task2.starttime)
        self.assertIsNone(task2.enddate)
        self.assertIsNone(task2.endtime)


class UpdateTaskLengthViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user,
                            startdate='2022-06-23', starttime='12:00')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/update/updateTaskLength/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/task/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/update/updateTaskLength/')

    # check if the right url is chosen when calling the get method
    def test_get_task_update_length_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_task_update_length_view_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_not_in_calendar(self):
        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertIsNone(task1.startdate)
        self.assertIsNone(task1.starttime)
        self.assertIsNone(task1.enddate)
        self.assertIsNone(task1.endtime)

        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3'}))
        self.assertEqual(response.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertIsNone(task2.startdate)
        self.assertIsNone(task2.starttime)
        self.assertIsNone(task2.enddate)
        self.assertIsNone(task2.endtime)

    def test_task_changes_complete_state(self):
        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4')
        self.assertIsNone(task1.enddate)
        self.assertIsNone(task1.endtime)

        response = self.client.get(
            reverse('task-update-length',
                    kwargs={'overview': 'daily', 'date': '1918-11-10',
                            'pk': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'}))
        self.assertEqual(response.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4')
        self.assertEqual(task2.startdate, datetime.date(2022, 6, 23))
        self.assertEqual(task2.starttime, datetime.time(12, 0))
        self.assertEqual(task2.enddate, datetime.date(2022, 6, 23))
        self.assertEqual(task2.endtime, datetime.time(13, 0))


class CreateDecisionTreeEntryViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        duration = datetime.timedelta(seconds=3600)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                            deadline_date='2023-05-17', deadline_time='18:00', duration=duration, user=smart_user,
                            startdate='2022-06-23', starttime='12:00')

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        DecisionTreeTask.objects.create(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3', user=smart_user, weekday=0,
                                        time=0, priority=0, deadline=0, duration=0, category=0, as_planned=False)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_task_weekly(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/e4b34b82-fdf9-43c1-8811-cdd50bd73da4/updateDecisionTreeData/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_task_daily(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/e4b34b82-fdf9-43c1-8811-cdd50bd73da4/updateDecisionTreeData/')

    # check if the right url is chosen when calling the get method
    def test_get_create_decision_tree_entry_view_task_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_create_decision_tree_entry_view_task_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_event_weekly(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/updateDecisionTreeData/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_event_daily(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/e4b34b82-fdf9-43c1-8811-cdd50bd73da2/updateDecisionTreeData/')

    # check if the right url is chosen when calling the get method
    def test_get_create_decision_tree_entry_view_event_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_create_decision_tree_entry_view_event_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da2'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_none_weekly(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'noID'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/noID/updateDecisionTreeData/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_none_daily(self):
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'noID'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/e4b34b82-fdf9-43c1-8811-cdd50bd73da3/noID/updateDecisionTreeData/')

    # check if the right url is chosen when calling the get method
    def test_get_create_decision_tree_entry_view_none_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'noID'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_create_decision_tree_entry_view_none_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            'pk2': 'noID'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_not_in_database(self):
        # login to get right url
        self.client.login(username='user', password='user')

        self.assertFalse(DecisionTreeTask.objects.filter(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4').exists())

        response = self.client.get(
            reverse('decision-tree-task-new',
                    kwargs={'overview': 'daily', 'date': '1918-11-10', 'pk1': 'e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                            'pk2': 'noID'}))

        self.assertEqual(response.status_code, 302)
        self.assertTrue(DecisionTreeTask.objects.filter(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4').exists())


class InsertSmartTasksIntoCalendarViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        weekdays = Weekdays.objects.create(user=smart_user, monday=True, smart_settings=True)

        SmartPlanningSettings.objects.create(user=smart_user, weekdays=weekdays, smart_startdate='2023-08-28',
                                             smart_enddate='2023-08-28', smart_starttime='11:00', smart_endtime='13:00')

        duration1 = datetime.timedelta(seconds=3600)
        duration2 = datetime.timedelta(seconds=3800)

        Task.objects.create(title='TestTask', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                            deadline_date='2024-05-17', deadline_time='18:00', duration=duration1, user=smart_user)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4',
                            deadline_date='2024-05-17', deadline_time='18:00', duration=duration2, user=smart_user)

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2023-08-28', starttime='11:00',
                             enddate='2023-08-28', endtime='12:00', user=smart_user)

        DecisionTreeTask.objects.create(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3', user=smart_user, weekday=0,
                                        time=0, priority=0, deadline=0, duration=0, category=0, as_planned=False)

        DecisionTreeTask.objects.create(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4', user=smart_user, weekday=0,
                                        time=0, priority=0, deadline=0, duration=0, category=0, as_planned=False)

        TemporarySmartTasks.objects.create(id=1, user=smart_user, task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/weekly/1918-11-10/insertSmartTasks/')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(
            response,
            '/accounts/login/?next=/spm/daily/1918-11-10/insertSmartTasks/')

    # check if the right url is chosen when calling the get method
    def test_get_insert_smart_tasks_view_fit_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}))

        expected_response = '/spm/weekly/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_insert_smart_tasks_view_fit_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))

        expected_response = '/spm/daily/1918-11-10/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    # check if the right url is chosen when calling the get method
    def test_get_insert_smart_tasks_view_no_fit_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        TemporarySmartTasks.objects.create(user=SmartUser.objects.get(username='user'), task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4')
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'weekly', 'date': '1918-11-10'}))

        expected_response = '/spm/weekly/1918-11-10/insertSmartTasks/openTasksLeft/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_get_insert_smart_tasks_view_no_fit_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        TemporarySmartTasks.objects.create(user=SmartUser.objects.get(username='user'),
                                           task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73da4')
        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))

        expected_response = '/spm/daily/1918-11-10/insertSmartTasks/openTasksLeft/'
        # check if it redirects correctly
        self.assertRedirects(response, expected_response)
        self.assertEqual(response.status_code, 302)

    def test_task_in_calendar(self):
        # login to get right url
        self.client.login(username='user', password='user')

        task1 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertIsNone(task1.startdate)
        self.assertIsNone(task1.starttime)
        self.assertIsNone(task1.enddate)
        self.assertIsNone(task1.endtime)

        response = self.client.get(
            reverse('calendar-insert-smart-tasks',
                    kwargs={'overview': 'daily', 'date': '1918-11-10'}))
        self.assertEqual(response.status_code, 302)

        task2 = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3')
        self.assertEqual(task2.startdate, datetime.date(2023, 8, 28))
        self.assertEqual(task2.starttime, datetime.time(12, 0))
        self.assertEqual(task2.enddate, datetime.date(2023, 8, 28))
        self.assertEqual(task2.endtime, datetime.time(13, 0))


class SmartPlanningViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                              last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        Task.objects.create(title='TestTask5', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db5',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=1)

        Task.objects.create(title='TestTask6', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db6',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3)

        Task.objects.create(title='TestTask7', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db7',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3, ai_scheduler=False)

        Task.objects.create(title='TestTask8', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db8',
                            deadline_date='2023-05-17', deadline_time='18:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=3,
                            startdate='2022-08-08')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_in_weekly(self):
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/2022-04-28/smartPlanningSettings/')

    def test_redirect_if_not_logged_in_daily(self):
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/2022-04-28/smartPlanningSettings/')

    # check if the right url is chosen when calling the get method
    def test_get_smart_planning_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_smart_planning_view_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/smartplanningsettings_form.html')

    # check if the right initial values are used
    def test_initial_smart_startdate(self):
        self.client.login(username='user', password='user')
        date = datetime.date.today()
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].initial['smart_startdate'], date)

    def test_initial_smart_enddate(self):
        self.client.login(username='user', password='user')
        date = datetime.date.today() + datetime.timedelta(days=7)
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['form'].initial['smart_enddate'], date)

    def test_initial_smart_starttime(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(8, 0)
        self.assertEqual(response.context['form'].initial['smart_starttime'], expected_time)

    def test_initial_smart_endtime(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(18, 0)
        self.assertEqual(response.context['form'].initial['smart_endtime'], expected_time)

    def test_initial_smart_starttime_with_settings(self):
        self.client.login(username='user', password='user')

        user = SmartUser.objects.get(username='user')

        weekdays = Weekdays.objects.create(user=user, monday=True, smart_settings=True)
        SmartPlanningSettings.objects.create(user=user, weekdays=weekdays, smart_startdate='2022-08-08',
                                             smart_enddate='2022-08-08', smart_starttime='11:00', smart_endtime='13:00')

        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(11, 0)
        self.assertEqual(response.context['form'].initial['smart_starttime'], expected_time)

    def test_initial_smart_endtime_with_settings(self):
        self.client.login(username='user', password='user')

        user = SmartUser.objects.get(username='user')

        weekdays = Weekdays.objects.create(user=user, monday=True, smart_settings=True)
        SmartPlanningSettings.objects.create(user=user, weekdays=weekdays, smart_startdate='2022-08-08',
                                             smart_enddate='2022-08-08', smart_starttime='11:00', smart_endtime='13:00')

        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)

        expected_time = datetime.time(13, 0)
        self.assertEqual(response.context['form'].initial['smart_endtime'], expected_time)

    # check if context data contains overview parameter
    def test_context_data_is_updated_weekly(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('weekdays_form' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('smart_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    def test_context_data_is_updated_daily(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'daily')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('weekdays_form' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('smart_tasks' in response.context)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)

    # check success url
    def test_success_url_weekly(self):
        # set all required fields except user
        post = {
            'smart_startdate': datetime.date.today(),
            'smart_enddate': datetime.date.today(),
            'smart_starttime': datetime.time(11),
            'smart_endtime': datetime.time(13)
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('smart-planning-settings', kwargs={'overview': 'weekly', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/weekly/2022-04-28/insertSmartTasks/', fetch_redirect_response=False)

    def test_success_url_daily(self):
        post = {
            'smart_startdate': datetime.date.today(),
            'smart_enddate': datetime.date.today(),
            'smart_starttime': datetime.time(11),
            'smart_endtime': datetime.time(13)
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/spm/daily/2022-04-28/insertSmartTasks/', fetch_redirect_response=False)

    # check if form is valid
    def test_valid_form_saved(self):
        post = {
            'smart_startdate': datetime.date.today(),
            'smart_enddate': datetime.date.today(),
            'smart_starttime': datetime.time(11),
            'smart_endtime': datetime.time(13)
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        self.assertEqual(response.status_code, 302)

        user = SmartUser.objects.get(username='user')

        weekdays = Weekdays.objects.filter(user=user).get(smart_settings=True)
        self.assertIsNotNone(weekdays)

        smart_planning_settings = SmartPlanningSettings.objects.get(user=user)
        self.assertIsNotNone(smart_planning_settings)

    def test_valid_form_overwrite(self):

        user = SmartUser.objects.get(username='user')

        weekday = Weekdays.objects.create(user=user, smart_settings=True)
        SmartPlanningSettings.objects.create(user=user, weekdays=weekday, smart_startdate=datetime.date.today(),
                                             smart_enddate=datetime.date.today(), smart_starttime='09:00',
                                             smart_endtime='13:00')

        post = {
            'monday': True,
            'smart_startdate': datetime.date.today(),
            'smart_enddate': datetime.date.today(),
            'smart_starttime': datetime.time(11),
            'smart_endtime': datetime.time(13)
        }
        self.client.login(username='user', password='user')
        response = self.client.post(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}), post)

        self.assertEqual(response.status_code, 302)

        user = SmartUser.objects.get(username='user')

        weekdays = Weekdays.objects.filter(user=user).get(smart_settings=True)
        self.assertIsNotNone(weekdays)
        self.assertTrue(weekdays.monday)

        smart_planning_settings = SmartPlanningSettings.objects.get(user=user)
        self.assertIsNotNone(smart_planning_settings)
        self.assertEqual(smart_planning_settings.smart_starttime, datetime.time(11))

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_count_by_priority(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('high_prio_task_count' in response.context)
        self.assertTrue('medium_prio_task_count' in response.context)
        self.assertTrue('low_prio_task_count' in response.context)
        expected_high_prio_task_count = response.context['high_prio_task_count']
        expected_medium_prio_task_count = response.context['medium_prio_task_count']
        expected_low_prio_task_count = response.context['low_prio_task_count']
        self.assertEqual(expected_high_prio_task_count, 3)
        self.assertEqual(expected_medium_prio_task_count, 1)
        self.assertEqual(expected_low_prio_task_count, 1)

    def test_smart_tasks_not_in_calendar_and_smart_planning_enabled(self):
        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('smart_tasks' in response.context)
        expected_smart_task_list = response.context['smart_tasks']
        self.assertIn('TestTask6', str(expected_smart_task_list))
        self.assertNotIn('TestTask7', str(expected_smart_task_list))
        self.assertNotIn('TestTask8', str(expected_smart_task_list))

    def test_weekdays_initial_settings(self):

        user = SmartUser.objects.get(username='user')

        weekdays = Weekdays.objects.create(user=user, monday=True, smart_settings=True)
        SmartPlanningSettings.objects.create(user=user, weekdays=weekdays, smart_starttime='11:00',
                                             smart_startdate='2022-08-08', smart_endtime='13:00',
                                             smart_enddate='2022-08-08')

        self.client.login(username='user', password='user')
        response = self.client.get(
            reverse('smart-planning-settings', kwargs={'overview': 'daily', 'date': '2022-04-28'}))

        self.assertEqual(response.status_code, 200)
        self.assertTrue('weekdays_form' in response.context)
        expected_value = response.context['weekdays_form'].initial['monday']
        self.assertTrue(expected_value)


class OpenTasksViewTest(TestCase):

    def setUp(self):
        smart_user = SmartUser.objects.create(email='user@uos.de', first_name='Random',
                                              last_name='User', username='user')
        smart_user.set_password('user')
        smart_user.save()

        smart_user_2 = SmartUser.objects.create(email='user2@uos.de', first_name='Random2',
                                                last_name='User2', username='user2')
        smart_user_2.set_password('user2')
        smart_user_2.save()

        Event.objects.create(title='TestEvent1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da2',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user)

        Event.objects.create(title='TestEvent2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73da3',
                             startdate='2022-05-17', starttime='16:00',
                             enddate='2022-05-18', endtime='11:00', user=smart_user_2)

        Task.objects.create(title='TestTask1', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db1',
                            deadline_date='2022-05-16', deadline_time='16:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 7, 0), priority=1)

        Task.objects.create(title='TestTask2', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db2',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user_2, priority=2)

        Task.objects.create(title='TestTask3', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3',
                            deadline_date='2022-05-17', deadline_time='17:00', complete=True,
                            duration=datetime.timedelta(minutes=30), user=smart_user,
                            complete_time=datetime.datetime(2022, 6, 22, 12, 8, 0), priority=3)

        Task.objects.create(title='TestTask4', id='e4b34b82-fdf9-43c1-8811-cdd50bd73db4',
                            deadline_date='2022-05-17', deadline_time='16:00', complete=False,
                            duration=datetime.timedelta(minutes=30), user=smart_user, priority=2)

        TemporarySmartTasks.objects.create(user=smart_user, task_id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3')

    # check for redirection if not logged in
    def test_redirect_if_not_logged_weekly(self):
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/weekly/2022-04-28/insertSmartTasks/openTasksLeft/')

    def test_redirect_if_not_logged_daily(self):
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/accounts/login/?next=/spm/daily/2022-04-28/insertSmartTasks/openTasksLeft/')

    # check if the right url is chosen when calling the get method
    def test_get_open_tasks_view_weekly(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    def test_get_open_tasks_view_daily(self):
        # login to get right url
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'daily', 'date': '2022-04-28'}))
        # Check that we got a response "success"
        self.assertEqual(response.status_code, 200)

    # check if the right template is used
    def test_uses_correct_template(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        # check again if the view call was successful in case the method is called alone
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'spm/open_task_warning.html')

    # check if context data contains overview parameter
    def test_context_data_is_updated(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('overview' in response.context)
        self.assertTrue(response.context['overview'] == 'weekly')
        self.assertTrue('date' in response.context)
        self.assertTrue(response.context['date'] == '2022-04-28')
        self.assertTrue('event_list' in response.context)
        self.assertTrue('task_list' in response.context)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        self.assertTrue('leftover_tasks' in response.context)

    # check if a user can have only own instances
    def test_only_events_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('event_list' in response.context)
        expected_event_list = response.context['event_list']
        self.assertIn('TestEvent1', str(expected_event_list))
        self.assertNotIn('TestEvent2', str(expected_event_list))

    def test_only_tasks_for_request_user(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list))
        self.assertNotIn('TestTask2', str(expected_task_list))

    # check filtering and ordering of context data
    def test_urgent_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('task_list' in response.context)
        expected_task_list = response.context['task_list']
        self.assertIn('TestTask1', str(expected_task_list[0]))
        self.assertIn('TestTask4', str(expected_task_list[1]))
        self.assertIn('TestTask3', str(expected_task_list[2]))

    def test_sort_task_list_in_completed_and_not_completed(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        self.assertTrue('open_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        expected_open_tasks = response.context['open_tasks']
        self.assertIn('TestTask1', str(expected_done_tasks))
        self.assertNotIn('TestTask1', str(expected_open_tasks))
        self.assertIn('TestTask4', str(expected_open_tasks))
        self.assertNotIn('TestTask4', str(expected_done_tasks))

    def test_last_done_task_comes_first(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('done_tasks' in response.context)
        expected_done_tasks = response.context['done_tasks']
        self.assertIn('TestTask3', str(expected_done_tasks[0]))

    def test_task_is_in_leftover_tasks_list(self):
        self.client.login(username='user', password='user')
        response = self.client.get(reverse('open-task-warning', kwargs={'overview': 'weekly', 'date': '2022-04-28'}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('leftover_tasks' in response.context)
        task = Task.objects.get(id='e4b34b82-fdf9-43c1-8811-cdd50bd73db3')
        self.assertIn(task, response.context['leftover_tasks'])
