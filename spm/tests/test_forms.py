from django.test import TestCase
from spm.forms import EventForm, TaskForm, CategoryForm, WeekdaysForm, SmartPlanningSettingsForm, RecurrenceForm
from spm.widgets import ColorInput
import datetime
import uuid


class CategoryFormTest(TestCase):

    # check validation errors
    def test_no_category_name_given(self):
        form = CategoryForm(data={'name': ''})
        self.assertFalse(form.is_valid())

    def test_string_color_input_over_7_chars(self):
        form = CategoryForm(data={'name': 'Test Category', 'color': 'lightsalmon'})
        self.assertFalse(form.is_valid())

    # check for valid inputs
    def test_string_color_input_under_7_chars(self):
        form = CategoryForm(data={'name': 'Test Category', 'color': 'salmon'})
        self.assertTrue(form.is_valid())

    def test_hex_color_input(self):
        form = CategoryForm(data={'name': 'Test Category', 'color': '#000000'})
        self.assertTrue(form.is_valid())

    # checking widget properties
    def test_name_placeholder(self):
        form = CategoryForm()
        # use this to access the widget properties
        expected_placeholder = form._meta.widgets['name'].attrs['placeholder']
        self.assertEqual('Category', expected_placeholder)

    def test_name_classes(self):
        form = CategoryForm()
        expected_classes = form._meta.widgets['name'].attrs['class']
        self.assertEqual('widget-inputs widget-text-fields', expected_classes)

    def test_color_placeholder(self):
        form = CategoryForm()
        expected_placeholder = form._meta.widgets['color'].attrs['placeholder']
        self.assertEqual('#334B7B', expected_placeholder)

    def test_color_classes(self):
        form = CategoryForm()
        expected_classes = form._meta.widgets['color'].attrs['class']
        self.assertEqual('widget-inputs widget-color clickable active', expected_classes)

    def test_color_widget_is_ColorInput(self):
        form = CategoryForm()
        expected_widget = form._meta.widgets['color']
        self.assertIsInstance(expected_widget, ColorInput)


class EventFormTest(TestCase):

    # check validation errors
    # title, date and time fields are all required fields, so they have to be set in all validation error tests
    # otherwise the validation could fail because of the missing field and not because of the test we created
    def test_no_title_given(self):
        date = datetime.date.today()
        time = datetime.time(12)
        form = EventForm(data={'title': '', 'startdate': date, 'enddate': date, 'starttime': time, 'endtime': time})
        self.assertFalse(form.is_valid())

    def test_enddate_before_startdate(self):
        startdate = datetime.date.today()
        enddate = datetime.date.today() - datetime.timedelta(days=1)
        time = datetime.time(12)
        form = EventForm(data={'title': 'TestEvent', 'startdate': startdate, 'enddate': enddate,
                               'starttime': time, 'endtime': time})
        self.assertFalse(form.is_valid())

    def test_endtime_before_starttime_when_startdate_equals_enddate(self):
        date = datetime.date.today()
        starttime = datetime.time(12)
        endtime = datetime.time(11)
        form = EventForm(data={'title': 'TestEvent', 'startdate': date, 'enddate': date,
                               'starttime': starttime, 'endtime': endtime})
        self.assertFalse(form.is_valid())

    # check for valid inputs
    def test_title_given(self):
        date = datetime.date.today()
        time = datetime.time(12)
        form = EventForm(data={'title': 'TestEvent', 'startdate': date, 'enddate': date,
                               'starttime': time, 'endtime': time})
        self.assertTrue(form.is_valid())

    def test_enddate_equals_startdate(self):
        date = datetime.date.today()
        time = datetime.time(12)
        form = EventForm(data={'title': 'TestEvent', 'startdate': date, 'enddate': date,
                               'starttime': time, 'endtime': time})
        self.assertTrue(form.is_valid())

    def test_enddate_after_startdate(self):
        startdate = datetime.date.today()
        enddate = datetime.date.today() + datetime.timedelta(days=1)
        time = datetime.time(12)
        form = EventForm(data={'title': 'TestEvent', 'startdate': startdate, 'enddate': enddate,
                               'starttime': time, 'endtime': time})
        self.assertTrue(form.is_valid())

    def test_endtime_equals_starttime_when_startdate_equals_enddate(self):
        date = datetime.date.today()
        time = datetime.time(12)
        form = EventForm(data={'title': 'TestEvent', 'startdate': date, 'enddate': date,
                               'starttime': time, 'endtime': time})
        self.assertTrue(form.is_valid())

    def test_endtime_after_starttime_when_startdate_equals_enddate(self):
        date = datetime.date.today()
        starttime = datetime.time(12)
        endtime = datetime.time(13)
        form = EventForm(data={'title': 'TestEvent', 'startdate': date, 'enddate': date,
                               'starttime': starttime, 'endtime': endtime})
        self.assertTrue(form.is_valid())

    def test_endtime_before_starttime_when_enddate_after_startdate(self):
        startdate = datetime.date.today()
        enddate = datetime.date.today() + datetime.timedelta(days=1)
        starttime = datetime.time(12)
        endtime = datetime.time(11)
        form = EventForm(data={'title': 'TestEvent', 'startdate': startdate, 'enddate': enddate,
                               'starttime': starttime, 'endtime': endtime})
        self.assertTrue(form.is_valid())

    # checking widget properties
    def test_title_placeholder(self):
        form = EventForm()
        # use this to access the widget properties
        # type cannot be accessed like this
        expected_placeholder = form._meta.widgets['title'].attrs['placeholder']
        self.assertEqual('New event', expected_placeholder)

    def test_title_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['title'].attrs['class']
        self.assertEqual('widget-inputs widget-title', expected_classes)

    def test_startdate_type(self):
        form = EventForm()
        # use this to access widget attributes (like type)
        # str(form['<fieldname>'] returns the html string for the field
        # use self.assertIn() to check if the html string contains the value
        expected_type = str(form['startdate'])
        self.assertIn('type="date"', expected_type)

    def test_startdate_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['startdate'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_enddate_type(self):
        form = EventForm()
        expected_type = str(form['enddate'])
        self.assertIn('type="date"', expected_type)

    def test_enddate_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['enddate'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields', expected_classes)

    def test_starttime_type(self):
        form = EventForm()
        expected_type = str(form['starttime'])
        self.assertIn('type="time"', expected_type)

    def test_starttime_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['starttime'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_starttime_format(self):
        form = EventForm()
        expected_format = form._meta.widgets['starttime'].format
        self.assertEqual('%H:%M', expected_format)

    def test_endtime_type(self):
        form = EventForm()
        expected_type = str(form['endtime'])
        self.assertIn('type="time"', expected_type)

    def test_endtime_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['endtime'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_endtime_format(self):
        form = EventForm()
        expected_format = form._meta.widgets['endtime'].format
        self.assertEqual('%H:%M', expected_format)

    def test_location_placeholder(self):
        form = EventForm()
        expected_placeholder = form._meta.widgets['location'].attrs['placeholder']
        self.assertEqual('Location', expected_placeholder)

    def test_location_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['location'].attrs['class']
        self.assertEqual('widget-inputs widget-text-fields', expected_classes)

    def test_comment_placeholder(self):
        form = EventForm()
        expected_placeholder = form._meta.widgets['comment'].attrs['placeholder']
        self.assertEqual('Comment', expected_placeholder)

    def test_comment_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['comment'].attrs['class']
        self.assertEqual('widget-inputs widget-text-fields', expected_classes)

    def test_comment_table_position(self):
        form = EventForm()
        expected_cols = form._meta.widgets['comment'].attrs['cols']
        self.assertEqual('0', expected_cols)
        expected_rows = form._meta.widgets['comment'].attrs['rows']
        self.assertEqual('0', expected_rows)

    def test_recurrence_true_classes(self):
        form = EventForm()
        expected_classes = form._meta.widgets['recurrence_true'].attrs['class']
        self.assertEqual('widget-inputs widget-recurrence-checkbox', expected_classes)


class TaskFormTest(TestCase):

    # check validation errors
    # title, date, time and duration fields are all required fields, so they have to be set in all validation error tests
    # otherwise the validation could fail because of the missing field and not because of the test we created
    def test_no_title_given(self):
        date = datetime.date.today()
        time = (datetime.datetime.now() + datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': '', 'deadline_date': date, 'deadline_time': time,
                              'duration': duration, 'priority': 1})
        self.assertFalse(form.is_valid())

    def test_deadline_date_before_today(self):
        date = datetime.date.today() - datetime.timedelta(days=1)
        time = (datetime.datetime.now() + datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_duration_is_smaller_than_five_minutes(self):
        date = datetime.date.today()
        time = (datetime.datetime.now() + datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 0)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertFalse(form.is_valid())

    def test_deadline_time_before_now_when_deadline_date_is_today(self):
        date = datetime.date.today()
        time = (datetime.datetime.now() - datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    # check for valid inputs
    def test_title_given_and_deadline_now(self):
        date = datetime.date.today()
        time = datetime.datetime.now().time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_deadline_date_after_today(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        time = datetime.datetime.now().time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_deadline_time_before_now_when_deadline_date_after_today(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        time = (datetime.datetime.now() - datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_deadline_time_after_now_when_deadline_date_is_today(self):
        # fails after 23:59 o'clock
        date = datetime.date.today()
        time = (datetime.datetime.now() + datetime.timedelta(minutes=1)).time()
        duration = datetime.time(0, 30)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_duration_is_five_minutes(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        time = (datetime.datetime.now() + datetime.timedelta(hours=1)).time()
        duration = datetime.time(0, 5)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    def test_duration_is_over_one_hour(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        time = (datetime.datetime.now() + datetime.timedelta(hours=1)).time()
        duration = datetime.time(1, 5)
        form = TaskForm(data={'title': 'TestTask', 'deadline_date': date,
                              'deadline_time': time, 'duration': duration, 'priority': 1})
        self.assertTrue(form.is_valid())

    # checking widget properties
    def test_title_placeholder(self):
        form = TaskForm()
        expected_placeholder = form._meta.widgets['title'].attrs['placeholder']
        self.assertEqual('New task', expected_placeholder)

    def test_title_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['title'].attrs['class']
        self.assertEqual('widget-inputs widget-title', expected_classes)

    def test_description_placeholder(self):
        form = TaskForm()
        expected_placeholder = form._meta.widgets['description'].attrs['placeholder']
        self.assertEqual('Description', expected_placeholder)

    def test_description_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['description'].attrs['class']
        self.assertEqual('widget-inputs widget-description', expected_classes)

    def test_deadline_date_type(self):
        form = TaskForm()
        expected_type = str(form['deadline_date'])
        self.assertIn('type="date"', expected_type)

    def test_deadline_date_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['deadline_date'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_deadline_time_type(self):
        form = TaskForm()
        expected_type = str(form['deadline_time'])
        self.assertIn('type="time"', expected_type)

    def test_deadline_time_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['deadline_time'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_deadline_time_format(self):
        form = TaskForm()
        expected_format = form._meta.widgets['deadline_time'].format
        self.assertEqual('%H:%M', expected_format)

    def test_duration_type(self):
        form = TaskForm()
        expected_type = str(form['duration'])
        self.assertIn('type="time"', expected_type)

    def test_duration_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['duration'].attrs['class']
        self.assertEqual('widget-inputs widget-duration display-none', expected_classes)

    def test_priority_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['priority'].attrs['class']
        self.assertEqual('widget-inputs widget-category', expected_classes)

    def test_ai_scheduler_classes(self):
        form = TaskForm()
        expected_classes = form._meta.widgets['ai_scheduler'].attrs['class']
        self.assertEqual('widget-inputs widget-ai_scheduler', expected_classes)


class WeekdaysFormTest(TestCase):
    def test_monday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['monday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_tuesday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['tuesday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_wednesday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['wednesday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_thursday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['thursday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_friday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['friday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_saturday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['saturday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)

    def test_sunday_classes(self):
        form = WeekdaysForm()
        expected_classes = form._meta.widgets['sunday'].attrs['class']
        self.assertEqual('widget-inputs widget-weekday', expected_classes)


class SmartPlanningSettingsFormTest(TestCase):
    # checking widget properties
    def test_smart_startdate_type(self):
        form = SmartPlanningSettingsForm()
        expected_type = str(form['smart_startdate'])
        self.assertIn('type="date"', expected_type)

    def test_smart_startdate_classes(self):
        form = SmartPlanningSettingsForm()
        expected_classes = form._meta.widgets['smart_startdate'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active widget-smart-settings', expected_classes)

    def test_smart_enddate_type(self):
        form = SmartPlanningSettingsForm()
        expected_type = str(form['smart_enddate'])
        self.assertIn('type="date"', expected_type)

    def test_smart_enddate_classes(self):
        form = SmartPlanningSettingsForm()
        expected_classes = form._meta.widgets['smart_enddate'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active widget-smart-settings', expected_classes)

    def test_smart_starttime_type(self):
        form = SmartPlanningSettingsForm()
        expected_type = str(form['smart_starttime'])
        self.assertIn('type="time"', expected_type)

    def test_smart_starttime_classes(self):
        form = SmartPlanningSettingsForm()
        expected_classes = form._meta.widgets['smart_starttime'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active widget-smart-settings', expected_classes)

    def test_starttime_format(self):
        form = SmartPlanningSettingsForm()
        expected_format = form._meta.widgets['smart_starttime'].format
        self.assertEqual('%H:%M', expected_format)

    def test_smart_endtime_type(self):
        form = SmartPlanningSettingsForm()
        expected_type = str(form['smart_endtime'])
        self.assertIn('type="time"', expected_type)

    def test_smart_endtime_classes(self):
        form = SmartPlanningSettingsForm()
        expected_classes = form._meta.widgets['smart_endtime'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active widget-smart-settings', expected_classes)

    def test_endtime_format(self):
        form = SmartPlanningSettingsForm()
        expected_format = form._meta.widgets['smart_endtime'].format
        self.assertEqual('%H:%M', expected_format)


class RecurrenceFormTest(TestCase):

    def test_repeat_end_classes(self):
        form = RecurrenceForm()
        expected_classes = form._meta.widgets['repeat_end'].attrs['class']
        self.assertEqual('widget-inputs widget-time-fields active', expected_classes)

    def test_repeat_end_type(self):
        form = RecurrenceForm()
        expected_type = str(form['repeat_end'])
        self.assertIn('type="date"', expected_type)

    def test_repeat_freq_classes(self):
        form = RecurrenceForm()
        expected_classes = form._meta.widgets['repeat_freq'].attrs['class']
        self.assertEqual('widget-inputs widget-recurrence-freq', expected_classes)

    def test_repeat_freq_minimum(self):
        form = RecurrenceForm()
        expected_min = form._meta.widgets['repeat_freq'].attrs['min']
        self.assertEqual(1, expected_min)

    def test_repeat_freq_type(self):
        form = RecurrenceForm()
        expected_type = str(form['repeat_freq'])
        self.assertIn('type="number"', expected_type)

    def test_repeat_type_classes(self):
        form = RecurrenceForm()
        expected_classes = form._meta.widgets['repeat_type'].attrs['class']
        self.assertEqual('widget-inputs', expected_classes)
