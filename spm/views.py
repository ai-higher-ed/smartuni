from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.db.models import Q
import datetime
from .forms import EventForm, TaskForm, CategoryForm, WeekdaysForm, SmartPlanningSettingsForm, RecurrenceForm
from .models import Event, Task, Category, DecisionTreeTask, Weekdays, SmartPlanningSettings, TemporarySmartTasks, \
    Recurrence
from django.urls import reverse_lazy
import numpy as np
from .decisionTree import createTree, predictTaskPlacement, getTimeslots, _clean_weekday, _clean_time, _clean_deadline,\
    _clean_duration, _clean_category, _conv_weekdays, _overlapExists
import pandas as pd
import json
from .utils import get_recurrence_dates


# redirects automatically to the weekly overview
@login_required
def planner_startpage(request):
    response = redirect('weekly', date=datetime.date.today().strftime('%Y-%m-%d'))
    return response


# needed for the redirection if the url is manually typed in
@login_required
def weekly_to_date(request):
    response = redirect('weekly', date=datetime.date.today().strftime('%Y-%m-%d'))
    return response


@login_required
def daily_to_date(request):
    response = redirect('daily', date=datetime.date.today().strftime('%Y-%m-%d'))
    return response


# --------------- Create Views ------------------------
# LoginRequiredMixin corresponds to @login_required
class EventCreate(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm

    # sets the inital values all in one function which provides a better overview
    def get_initial(self):
        initial = {
            'startdate': self.kwargs['date'],  # suggest date for the displayed week instead of the current
            'enddate': self.kwargs['date'],
            'starttime': datetime.time(12, 0),
            'endtime': datetime.time(13, 0),
        }
        return initial

    # adds the information of the url parameter to the context data
    # makes them available in the templates
    def get_context_data(self, **kwargs):
        context = super(EventCreate, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['cat_form'] = CategoryForm()
        context['category_list'] = Category.objects.filter(user=self.request.user)
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        context['recurrence_form'] = RecurrenceForm(
            initial={'repeat_end': datetime.datetime.strptime(self.kwargs['date'], '%Y-%m-%d') + datetime.timedelta(days=90)})
        context['weekdays_form'] = WeekdaysForm()
        return context

    # redirects to daily/weekly overview when form was submitted
    def get_success_url(self):
        return reverse_lazy(self.kwargs['overview'], args=[self.kwargs['date']])

    # combines the post data for category and event form
    def post(self, request, *args, **kwargs):

        self.object = None
        form = self.form_class(request.POST)
        cat_form = CategoryForm(request.POST)
        recurrence_form = RecurrenceForm(request.POST)
        weekdays_form = WeekdaysForm(request.POST)

        if form.is_valid() and cat_form.is_valid() and recurrence_form.is_valid() and weekdays_form.is_valid():
            return self.form_valid(form, cat_form, recurrence_form, weekdays_form)
        else:
            return self.form_invalid(form)

    # adds the user as author of a model instance
    # form is automatically saved afterwards by a predefined method
    def form_valid(self, form, cat_form, recurrence_form, weekdays_form):

        try:
            # try if the category already exists
            cat_name = cat_form.instance.name
            cat = Category.objects.filter(user=self.request.user).get(name=cat_name)
            # check if the color needs to be updated
            if (cat.color != cat_form.instance.color):
                cat.color = cat_form.instance.color
                cat.save()
        except:
            # create a category
            cat_form.instance.user = self.request.user
            cat = cat_form.save()

        # -------------------------------------------------------------------------------------------------------

        if form.instance.recurrence_true:
            if recurrence_form.instance.repeat_type == 1:
                wkds = [weekdays_form.instance.monday,
                        weekdays_form.instance.tuesday,
                        weekdays_form.instance.wednesday,
                        weekdays_form.instance.thursday,
                        weekdays_form.instance.friday,
                        weekdays_form.instance.saturday,
                        weekdays_form.instance.sunday]

                for i in range(len(wkds)):
                    if i == form.instance.startdate.weekday():
                        wkds[i] = True

                weekdays_form.instance.user = self.request.user
                weekdays = weekdays_form.save()

                Weekdays.objects.filter(id=weekdays.id).update(
                    monday=wkds[0],
                    tuesday=wkds[1],
                    wednesday=wkds[2],
                    thursday=wkds[3],
                    friday=wkds[4],
                    saturday=wkds[5],
                    sunday=wkds[6])

                weekdays_obj = Weekdays.objects.get(id=weekdays.id)

                recurrence_form.instance.weekdays = weekdays_obj

            recurrence = recurrence_form.save()

            datetime_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)

            for recurring_date in datetime_timeslots.to_numpy():
                Event.objects.create(user=self.request.user,
                                     title=form.instance.title,
                                     startdate=recurring_date,
                                     enddate=datetime.datetime.strptime(recurring_date, '%Y-%m-%d') + (
                                             form.instance.enddate - form.instance.startdate),
                                     starttime=form.instance.starttime,
                                     endtime=form.instance.endtime,
                                     location=form.instance.location,
                                     comment=form.instance.comment,
                                     category=cat,
                                     recurrence_true=form.instance.recurrence_true,
                                     recurrence=recurrence,
                                     groupID=recurrence.groupID)

        # ------------------------------------------------------------------------------------
        else:
            # create an event with this category
            form.instance.user = self.request.user
            form.instance.category = cat
            form.save()

        messages.success(self.request, 'Event was successfully created!')

        return HttpResponseRedirect(self.get_success_url())

    # changes the inputs if they are not valid to a valid input
    # potentially not needed if it is controlled properly in the javascript file
    def form_invalid(self, form, **kwargs):

        context = self.get_context_data(**kwargs)
        # form copy is needed because once a form is bound (has data) it cannot be changed anymore
        form.data = form.data.copy()

        # access the validation error from the forms.py class
        if form.has_error('enddate', 'invalid'):
            form.data['enddate'] = form.data['startdate']

        elif form.has_error('endtime', 'invalid'):
            form.data['endtime'] = form.data['starttime']

        context['form'] = form

        return self.render_to_response(context)


class TaskCreate(LoginRequiredMixin, CreateView):
    model = Task
    form_class = TaskForm

    def get_initial(self):
        # deadline init time is the next full hour
        time = datetime.datetime.now() + datetime.timedelta(hours=1)
        minutes = time.minute
        deadline_time = (time - datetime.timedelta(minutes=minutes)).time()
        initial = {
            'deadline_date': self.kwargs['date'],
            'deadline_time': deadline_time,
            'duration': datetime.time(0, 30),
        }
        return initial

    def get_context_data(self, **kwargs):
        context = super(TaskCreate, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['cat_form'] = CategoryForm()
        context['category_list'] = Category.objects.filter(user=self.request.user)
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context

    def get_success_url(self):
        return reverse_lazy(self.kwargs['overview'], args=[self.kwargs['date']])

    # combines the post data for category and task form
    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST)
        cat_form = CategoryForm(request.POST)

        if form.is_valid() and cat_form.is_valid():
            return self.form_valid(form, cat_form)
        else:
            return self.form_invalid(form)

    # adds the user as author of a model instance
    # form is automatically saved afterwards by a predefined method
    def form_valid(self, form, cat_form):

        try:
            # try if the category already exists
            cat_name = cat_form.instance.name
            cat = Category.objects.filter(user=self.request.user).get(name=cat_name)
            # check if the color needs to be updated
            if (cat.color != cat_form.instance.color):
                cat.color = cat_form.instance.color
                cat.save()
        except:
            # create a category
            cat_form.instance.user = self.request.user
            cat = cat_form.save()

        # create a task with this category
        form.instance.user = self.request.user
        form.instance.category = cat
        form.save()

        messages.success(self.request, 'Task was successfully created!')

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, **kwargs):

        context = self.get_context_data(**kwargs)
        form.data = form.data.copy()

        # access the validation error from the forms.py class
        if form.has_error('duration', 'invalid'):
            form.data['duration'] = datetime.time(0, 5)

        context['form'] = form

        return self.render_to_response(context)


# --------------- Overviews -----------------------
# the overviews are only a temporary solution for a better usability of the other functions

# in class-based views normally only one model can be used
# since we have the events and the tasks we want to have two
class WeeklyListView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'spm/weekly.html'

    # here are all the models added to the context data to access multiple models in one view function
    def get_context_data(self, **kwargs):
        context = super(WeeklyListView, self).get_context_data(**kwargs)
        context['overview'] = 'weekly'
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['date'] = self.kwargs['date']
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['category_list'] = Category.objects.filter(user=self.request.user)
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context


class DailyListView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'spm/daily.html'

    def get_context_data(self, **kwargs):
        context = super(DailyListView, self).get_context_data(**kwargs)
        context['overview'] = 'daily'
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['date'] = self.kwargs['date']
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['category_list'] = Category.objects.filter(user=self.request.user)
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context


# ----------------- Detail Views ---------------------
class EventDetailView(LoginRequiredMixin, generic.DetailView):
    model = Event

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['date'] = self.kwargs['date']
        try:
            context['recurrence_obj'] = Recurrence.objects.get(groupID=Event.objects.get(pk=self.kwargs['pk']).groupID)
            try:
                context['recurrence_obj_weekdays'] = list([weekday for weekday_bool, weekday in zip(
                    list(Weekdays.objects.filter(id=context['recurrence_obj'].weekdays.id).values_list())[0][2:],
                    ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']) if weekday_bool])
            except:
                context['recurrence_obj_weekdays'] = None
        except:
            context['recurrence_obj'] = None
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['color'] = Event.objects.get(id=self.kwargs['pk']).category.color
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        context['recurrence_form'] = RecurrenceForm()
        context['weekdays_form'] = WeekdaysForm()
        return context


class TaskDetailView(LoginRequiredMixin, generic.DetailView):
    model = Task

    def get_context_data(self, **kwargs):
        context = super(TaskDetailView, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['color'] = Task.objects.get(id=self.kwargs['pk']).category.color
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context


# ------------- Update Views --------------------
class EventUpdate(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventForm

    def get_context_data(self, **kwargs):
        context = super(EventUpdate, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        category = Event.objects.get(id=self.kwargs['pk']).category
        context['cat_form'] = CategoryForm(data={'name': category.name, 'color': category.color})
        context['category_list'] = Category.objects.filter(user=self.request.user)
        try:
            recurrence = Recurrence.objects.get(groupID=Event.objects.get(id=self.kwargs['pk']).groupID)
            context['recurrence_form'] = RecurrenceForm(data={'repeat_type': recurrence.repeat_type,
                                                              'repeat_freq': recurrence.repeat_freq,
                                                              'repeat_end': recurrence.repeat_end})
            try:
                weekdays = Weekdays.objects.get(id=recurrence.weekdays.id)
                context['weekdays_form'] = WeekdaysForm(data={'monday': weekdays.monday,
                                                              'tuesday': weekdays.tuesday,
                                                              'wednesday': weekdays.wednesday,
                                                              'thursday': weekdays.thursday,
                                                              'friday': weekdays.friday,
                                                              'saturday': weekdays.saturday,
                                                              'sunday': weekdays.sunday})
            except:
                context['weekdays_form'] = WeekdaysForm()
        except:
            context['recurrence_form'] = RecurrenceForm(
                initial={'repeat_end': datetime.datetime.strptime(
                    self.kwargs['date'], '%Y-%m-%d') + datetime.timedelta(days=90)})
            context['weekdays_form'] = WeekdaysForm()
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()

        return context

    def get_success_url(self):
        return reverse_lazy(self.kwargs['overview'], args=[self.kwargs['date']])

    # combines the post data for category and event form
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST)
        cat_form = CategoryForm(request.POST)
        recurrence_form = RecurrenceForm(request.POST)
        weekdays_form = WeekdaysForm(request.POST)

        if form.is_valid() and cat_form.is_valid() and recurrence_form.is_valid() and weekdays_form.is_valid():
            return self.form_valid(form, cat_form, recurrence_form, weekdays_form)
        else:
            return self.form_invalid(form)

    # adds the user as author of a model instance
    # form is automatically saved afterwards by a predefined method
    def form_valid(self, form, cat_form, recurrence_form, weekdays_form, **kwargs):  # recurrence_form, weekdays_form,

        try:
            # try if the category already exists
            cat_name = cat_form.instance.name
            cat = Category.objects.filter(user=self.request.user).get(name=cat_name)
            # check if the color needs to be updated
            if (cat.color != cat_form.instance.color):
                cat.color = cat_form.instance.color
                cat.save()
        except:
            # create a category
            cat_form.instance.user = self.request.user
            cat = cat_form.save()

        event = Event.objects.get(id=self.kwargs['pk'])

        if form.instance.recurrence_true is False:

            # the single event gets updated
            if event.recurrence_true is False:
                event.title = form.instance.title
                event.startdate = form.instance.startdate
                event.enddate = form.instance.enddate
                event.starttime = form.instance.starttime
                event.endtime = form.instance.endtime
                event.location = form.instance.location
                event.comment = form.instance.comment
                event.category = cat
                event.save()

            # recurring events get deleted and a single event is made
            else:
                groupID = event.groupID
                recurrence = Recurrence.objects.get(groupID=groupID)

                if recurrence.weekdays is not None:
                    weekdays = Weekdays.objects.get(id=recurrence.weekdays.id)
                    weekdays.delete()  # deletes also recurrence which deletes all recurring events

                else:
                    recurrence.delete()  # deletes also all recurring events

                form.instance.user = self.request.user
                form.instance.category = cat
                form.save()

        else:

            # making a recurring event out of a single event
            if event.recurrence_true is False:

                # only for weekly recurring
                if recurrence_form.instance.repeat_type == 1:
                    wkds = [weekdays_form.instance.monday,
                            weekdays_form.instance.tuesday,
                            weekdays_form.instance.wednesday,
                            weekdays_form.instance.thursday,
                            weekdays_form.instance.friday,
                            weekdays_form.instance.saturday,
                            weekdays_form.instance.sunday]

                    for i in range(len(wkds)):
                        if i == form.instance.startdate.weekday():
                            wkds[i] = True

                    weekdays_form.instance.user = self.request.user
                    weekdays = weekdays_form.save()

                    Weekdays.objects.filter(id=weekdays.id).update(
                        monday=wkds[0],
                        tuesday=wkds[1],
                        wednesday=wkds[2],
                        thursday=wkds[3],
                        friday=wkds[4],
                        saturday=wkds[5],
                        sunday=wkds[6])

                    weekdays_obj = Weekdays.objects.get(id=weekdays.id)

                    recurrence_form.instance.weekdays = weekdays_obj

                recurrence_form.instance.user = self.request.user
                recurrence = recurrence_form.save()

                # update the selected event
                event.title = form.instance.title
                event.startdate = form.instance.startdate
                event.enddate = form.instance.enddate
                event.starttime = form.instance.starttime
                event.endtime = form.instance.endtime
                event.location = form.instance.location
                event.comment = form.instance.comment
                event.category = cat
                event.recurrence_true = form.instance.recurrence_true
                event.recurrence = recurrence
                event.groupID = recurrence.groupID
                event.save()

                # get the dates for the recurring events
                datetime_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)

                # create new events
                for recurring_date in datetime_timeslots.to_numpy()[1:]:
                    Event.objects.create(user=self.request.user,
                                         title=form.instance.title,
                                         startdate=recurring_date,
                                         enddate=datetime.datetime.strptime(recurring_date, '%Y-%m-%d') + (
                                                 form.instance.enddate - form.instance.startdate),
                                         starttime=form.instance.starttime,
                                         endtime=form.instance.endtime,
                                         location=form.instance.location,
                                         comment=form.instance.comment,
                                         category=cat,
                                         recurrence_true=form.instance.recurrence_true,
                                         recurrence=recurrence,
                                         groupID=recurrence.groupID)

            else:
                recurrence = Recurrence.objects.get(groupID=event.groupID)

                if recurrence.repeat_type == 1:

                    # update if weekly repetition stays weekly
                    if recurrence_form.instance.repeat_type == 1:

                        wkds = [weekdays_form.instance.monday,
                                weekdays_form.instance.tuesday,
                                weekdays_form.instance.wednesday,
                                weekdays_form.instance.thursday,
                                weekdays_form.instance.friday,
                                weekdays_form.instance.saturday,
                                weekdays_form.instance.sunday]

                        for i in range(len(wkds)):
                            if i == form.instance.startdate.weekday():
                                wkds[i] = True

                        Weekdays.objects.filter(id=recurrence.weekdays.id).update(
                            monday=wkds[0],
                            tuesday=wkds[1],
                            wednesday=wkds[2],
                            thursday=wkds[3],
                            friday=wkds[4],
                            saturday=wkds[5],
                            sunday=wkds[6])

                        weekdays = Weekdays.objects.get(id=recurrence.weekdays.id)

                        recurrence.weekdays = weekdays

                    # delete if weekly repetition changes to monthly or yearly
                    else:
                        weekdays = Weekdays.objects.get(id=recurrence.weekdays.id)
                        recurrence.weekdays = None
                        recurrence.save()
                        weekdays.delete()

                # add weekdays object if it changes from monthly or yearly to weekly
                elif recurrence_form.instance.repeat_type == 1:
                    wkds = [weekdays_form.instance.monday,
                            weekdays_form.instance.tuesday,
                            weekdays_form.instance.wednesday,
                            weekdays_form.instance.thursday,
                            weekdays_form.instance.friday,
                            weekdays_form.instance.saturday,
                            weekdays_form.instance.sunday]

                    for i in range(len(wkds)):
                        if i == form.instance.startdate.weekday():
                            wkds[i] = True

                    weekdays_form.instance.user = self.request.user
                    weekdays = weekdays_form.save()

                    Weekdays.objects.filter(id=weekdays.id).update(
                        monday=wkds[0],
                        tuesday=wkds[1],
                        wednesday=wkds[2],
                        thursday=wkds[3],
                        friday=wkds[4],
                        saturday=wkds[5],
                        sunday=wkds[6])

                    weekdays_obj = Weekdays.objects.get(id=weekdays.id)

                    recurrence.weekdays = weekdays_obj

                # update recurrence object
                recurrence.repeat_type = recurrence_form.instance.repeat_type
                recurrence.repeat_freq = recurrence_form.instance.repeat_freq
                recurrence.repeat_end = recurrence_form.instance.repeat_end
                recurrence.save()

                # Create new events with the respective settings
                datetime_timeslots = get_recurrence_dates(recurrence.repeat_type, form, recurrence)

                groupID = event.groupID

                # Delete all events of this group greater than or equal the selected event
                Event.objects.filter(groupID=groupID).filter(
                    startdate__gte=event.startdate if event.startdate < form.instance.startdate
                    else form.instance.startdate).delete()

                for recurring_date in datetime_timeslots.to_numpy():
                    Event.objects.create(user=self.request.user,
                                         title=form.instance.title,
                                         startdate=recurring_date,
                                         enddate=datetime.datetime.strptime(recurring_date, '%Y-%m-%d') + (
                                                 form.instance.enddate - form.instance.startdate),
                                         starttime=form.instance.starttime,
                                         endtime=form.instance.endtime,
                                         location=form.instance.location,
                                         comment=form.instance.comment,
                                         category=cat,
                                         recurrence_true=form.instance.recurrence_true,
                                         recurrence=recurrence,
                                         groupID=groupID)

                # update the rest of the events with all non-recurring properties
                Event.objects.filter(groupID=groupID).update(
                    title=form.instance.title,
                    starttime=form.instance.starttime,
                    endtime=form.instance.endtime,
                    location=form.instance.location,
                    comment=form.instance.comment,
                    category=cat
                )

        messages.success(self.request, 'Event was successfully updated!')

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, **kwargs):

        context = self.get_context_data(**kwargs)
        form.data = form.data.copy()

        enddate = form.data['enddate']
        startdate = form.data['startdate']
        endtime = form.data['endtime']
        starttime = form.data['starttime']

        if startdate > enddate:
            form.data['enddate'] = startdate

        elif (enddate == startdate) & (endtime < starttime):
            form.data['endtime'] = starttime

        context['form'] = form

        return self.render_to_response(context)


class TaskUpdate(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = TaskForm

    def get_context_data(self, **kwargs):
        context = super(TaskUpdate, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        category = Task.objects.get(id=self.kwargs['pk']).category
        context['cat_form'] = CategoryForm(data={'name': category.name, 'color': category.color})
        context['category_list'] = Category.objects.filter(user=self.request.user)
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context

    def get_success_url(self):
        return reverse_lazy('task-update-length',
                            args=[self.kwargs['overview'], self.kwargs['date'], self.kwargs['pk']])

    # combines the post data for category and task form
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST)
        cat_form = CategoryForm(request.POST)

        if form.is_valid() and cat_form.is_valid():
            return self.form_valid(form, cat_form)
        else:
            return self.form_invalid(form)

    # adds the user as author of a model instance
    # form is automatically saved afterwards by a predefined method
    def form_valid(self, form, cat_form):

        try:
            # try if the category already exists
            cat_name = cat_form.instance.name
            cat = Category.objects.filter(user=self.request.user).get(name=cat_name)
            # check if the color needs to be updated
            if (cat.color != cat_form.instance.color):
                cat.color = cat_form.instance.color
                cat.save()
        except:
            # create a category
            cat_form.instance.user = self.request.user
            cat = cat_form.save()

        # this is needed for the task to not be created new
        try:
            task = Task.objects.get(id=self.kwargs['pk'])
            task.title = form.instance.title
            task.deadline_date = form.instance.deadline_date
            task.deadline_time = form.instance.deadline_time
            task.duration = form.instance.duration
            task.priority = form.instance.priority
            task.description = form.instance.description
            task.category = cat
            task.save()
        except:
            # create a task with this category
            form.instance.user = self.request.user
            form.instance.category = cat
            form.save()

        messages.success(self.request, 'Task was successfully updated!')

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, **kwargs):

        context = self.get_context_data(**kwargs)
        form.data = form.data.copy()

        # access the validation error from the forms.py class
        if form.has_error('duration', 'invalid'):
            form.data['duration'] = datetime.time(0, 5)

        context['form'] = form

        return self.render_to_response(context)


@login_required
def completeTask(request, **kwargs):
    task = Task.objects.get(pk=kwargs['pk'])
    if not task.complete:
        task.complete = True
        task.complete_time = datetime.datetime.now()
    else:
        task.complete = False
        task.complete_time = None
    task.save()

    response = redirect(kwargs['overview'], date=kwargs['date'])
    return response


@login_required
def updateTaskForCalendar(request, **kwargs):
    data = request.POST
    taskID = data.get('id', '')
    startdate = data.get('startDate', '')
    starttime = data.get('startTime', '')
    enddate = data.get('endDate', '')
    endtime = data.get('endTime', '')

    task = Task.objects.get(id=taskID)
    setattr(task, 'startdate', startdate)
    setattr(task, 'starttime', starttime)
    setattr(task, 'enddate', enddate)
    setattr(task, 'endtime', endtime)
    task.save()

    response = redirect(kwargs['overview'], date=kwargs['date'])
    return response


@login_required
def updateTaskLengthAfterDurationChange(request, **kwargs):
    task = Task.objects.get(id=kwargs['pk'])
    if task.startdate is not None:
        start = datetime.datetime.combine(task.startdate, task.starttime)
        end = start + task.duration

        setattr(task, 'enddate', str(end.date()))
        setattr(task, 'endtime', str(end.time()))
        task.save()

    response = redirect(kwargs['overview'], date=kwargs['date'])
    return response


@login_required
def removeTaskFromCalendar(request, **kwargs):
    data = request.POST
    taskID = data.get('id', '')

    task = Task.objects.get(id=taskID)
    setattr(task, 'startdate', None)
    setattr(task, 'starttime', None)
    setattr(task, 'enddate', None)
    setattr(task, 'endtime', None)
    task.save()

    response = redirect(kwargs['overview'], date=kwargs['date'])
    return response


# ------------------- Delete Views -------------------------
class EventDelete(LoginRequiredMixin, DeleteView):
    model = Event

    def get_success_url(self):
        return reverse_lazy(self.kwargs['overview'], args=[self.kwargs['date']])

    def get_context_data(self, **kwargs):
        context = super(EventDelete, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['recurrence_true'] = Event.objects.get(pk=self.kwargs['pk']).recurrence_true
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context

    def post(self, request, *args, **kwargs):
        self.object = None
        event = Event.objects.get(pk=self.kwargs['pk'])

        # Depending on the selected button, either all elements from one recurrence pattern are deleted, or just one
        if 'delete_all' in request.POST:
            try:
                recurrence = Recurrence.objects.get(groupID=event.groupID)

                if recurrence.weekdays:
                    Weekdays.objects.get(id=recurrence.weekdays.id).delete()
                else:
                    recurrence.delete()
            except:
                pass
        else:
            event.delete()

        return HttpResponseRedirect(self.get_success_url())


class TaskDelete(LoginRequiredMixin, DeleteView):
    model = Task

    def get_success_url(self):
        return reverse_lazy(self.kwargs['overview'], args=[self.kwargs['date']])

    def get_context_data(self, **kwargs):
        context = super(TaskDelete, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['pk'] = self.kwargs['pk']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context


# ----------------- smart Task insertion into the calendar ------------------
@login_required
def createDecisionTreeEntry(request, **kwargs):
    # Get the task by its id
    base_task = Task.objects.get(pk=kwargs['pk1'])
    # check if an item with the same id already exists in the db
    if not DecisionTreeTask.objects.filter(id=base_task.id).exists():
        if kwargs['pk2'] != 'noID':
            # The comparison task (for the category) could be a task or an event!
            try:
                category_inst_before = Task.objects.get(pk=kwargs['pk2']).category
            except:
                category_inst_before = Event.objects.get(pk=kwargs['pk2']).category
        else:
            category_inst_before = None  # If neither a task nor an event has been found

        if base_task.starttime:  # check if task has been an instance of the calendar
            # Create a basic entry in the task dataset
            dt_task_form = DecisionTreeTask.objects.create(
                id=base_task.id,
                user=base_task.user,
                weekday=_clean_weekday(base_task.startdate),
                time=_clean_time(base_task.starttime),
                priority=base_task.priority,
                deadline=_clean_deadline(base_task.deadline_date, base_task.deadline_time),
                duration=_clean_duration(base_task.duration),
                category=_clean_category(category_inst_before, base_task.category),
                as_planned=base_task.complete,
            )
            dt_task_form.save()

    response = redirect(kwargs['overview'], date=kwargs['date'])
    return response


@login_required
def insertSmartTasksIntoCalendar(request, **kwargs):
    """
    Function to plan specific tasks into the SmartPlanner using the Decision Tree process.
    """

    # Access a SmartPlanningSettings model instance to receive the relevant information about user preferences on when
    # tasks should be planned
    insertion_info = SmartPlanningSettings.objects.filter(user=request.user).last()
    date_earliest = insertion_info.smart_startdate if insertion_info.smart_startdate else datetime.datetime.date(
        datetime.datetime.now())
    date_latest = insertion_info.smart_enddate \
                  + datetime.timedelta(days=1)  # plus one day since internally the time defaults to 00:00
    time_earliest = insertion_info.smart_starttime
    time_latest = insertion_info.smart_endtime
    include_weekdays = _conv_weekdays(insertion_info.weekdays)
    num_random_events_per_iteration = 100

    # Build the Decision tree based on tasks already in the database from the respective user, and a base database
    taskDataBase = pd.DataFrame(list(DecisionTreeTask.objects.filter(Q(user=request.user) | Q(user=None))
                                     .values('weekday', 'time', 'priority', 'deadline', 'duration', 'category',
                                             'as_planned')))
    treeModel = createTree(taskDataBase)

    # Get the tasks from the user that are supposed to be scheduled by the AI component (not done)
    taskIDs = TemporarySmartTasks.objects.filter(user=request.user).values_list('task_id', flat=True)
    openTasks = Task.objects.filter(id__in=list(taskIDs))
    openTasks = np.array(openTasks.order_by('deadline_date', 'deadline_time', 'priority', 'duration'))

    # Get the available timeslots in the current week
    testTimeslots = getTimeslots(date_earliest.strftime('%Y-%m-%d'),
                                 date_latest.strftime('%Y-%m-%d'),
                                 time_earliest,
                                 time_latest,
                                 '15T',
                                 include_weekdays)
    np.random.shuffle(testTimeslots)

    taskSlots = np.zeros_like(openTasks).astype(np.bool)  # Keeps track which tasks have been assigned to a slot yet
    backup_predictions = np.array([{} for _ in range(len(openTasks))])
    iteration = 0
    while True:
        # Select a random number of timeslots (using indices for delete function later)
        selectTimelotsIndices = np.random.choice(np.arange(len(testTimeslots)),
                                                 min(len(testTimeslots), num_random_events_per_iteration),
                                                 replace=False)

        # For each task that has not yet been assigned to a slot
        for i, task in enumerate(openTasks):
            if not taskSlots[i]:
                # Get the decision tree predictions for the current task
                slots = np.array([testTimeslots[slot] for slot in selectTimelotsIndices])
                predictions = np.array(predictTaskPlacement(treeModel, task, slots))

                # Select the most suiting prediction
                while len(predictions) > 0:
                    # Get the slot with the max probability
                    max_prob = np.argmax(predictions)

                    # Check if the prediction spot actually fits an event of the respective length
                    predictionAsDatetime = datetime.datetime.strptime(slots[max_prob], '%Y-%m-%dT%H:%M:%SZ')
                    predictionDate = predictionAsDatetime.date()
                    time2start = predictionAsDatetime.time()
                    endTime = (predictionAsDatetime + task.duration)
                    time2end = endTime.time()

                    # There cannot be an event overlap, also the task cannot exceed the latest open time during the day
                    if not _overlapExists(date=predictionDate, startDate=time2start, endDate=time2end, user=request.user) \
                            and endTime <= predictionAsDatetime.replace(hour=time_latest.hour, minute=0):

                        # If the free timeslot is past the deadline, we continue the search but memorize the timeslot
                        # and block it from the other tasks. Is only replaced if a new fitting timesplot that is earlier
                        # is found.
                        deadline = datetime.datetime.combine(task.deadline_date, task.deadline_time)
                        if deadline < endTime:
                            # Check if another task has a backup timeslot within the same range, in that case, do not
                            # use this backup timeslot again
                            no_overlap = True
                            for backup_prediction in backup_predictions:
                                if 'end-time' in backup_prediction.keys():
                                    if predictionAsDatetime <= backup_prediction['end-time'] <= endTime \
                                            or predictionAsDatetime <= backup_prediction['start-range'] <= endTime \
                                            or backup_prediction['start-range'] <= predictionAsDatetime \
                                            <= backup_prediction['end-time'] or backup_prediction['start-range'] \
                                            <= endTime <= backup_prediction['end-time']:
                                        no_overlap = False
                                        break

                            if no_overlap:
                                if 'end-time' in backup_predictions[i].keys():
                                    if backup_predictions[i]['end-time'] > endTime:
                                        backup_predictions[i] = {
                                            'task-startdate': predictionDate,
                                            'task-enddate': predictionDate,
                                            'task-starttime': time2start,
                                            'task-endtime': time2end,
                                            'task': task,
                                            'end-time': endTime,
                                            'start-range': predictionAsDatetime
                                        }
                                else:
                                    backup_predictions[i] = {
                                        'task-startdate': predictionDate,
                                        'task-enddate': predictionDate,
                                        'task-starttime': time2start,
                                        'task-endtime': time2end,
                                        'task': task,
                                        'end-time': endTime,
                                        'start-range': predictionAsDatetime
                                    }
                        else:
                            # Place task in that slot
                            task.startdate = predictionDate
                            task.enddate = predictionDate
                            task.starttime = time2start
                            task.endtime = time2end
                            task.save()

                            # Delete the tasks from the temporary task list
                            TemporarySmartTasks.objects.filter(task_id=task.id).delete()

                            # check if other task has this timeslot as backup_ prediction
                            # if the timeslot is in a backup_prediction delete the backup_prediction
                            for indx, backup_prediction in enumerate(backup_predictions):
                                if 'end-time' in backup_prediction.keys():
                                    if predictionAsDatetime < backup_prediction['end-time'] < endTime \
                                            or predictionAsDatetime < backup_prediction['start-range'] < endTime \
                                            or backup_prediction['start-range'] < predictionAsDatetime \
                                            < backup_prediction['end-time'] or backup_prediction['start-range'] \
                                            < endTime < backup_prediction['end-time']:
                                        backup_predictions[indx] = {}


                            # break while loop since a prediction spot has been found
                            taskSlots[i] = True
                            break

                    # If no prediction spot has been found, continue for the left over predictions
                    predictions = np.delete(predictions, max_prob)
                    slots = np.delete(slots, max_prob)

        # When an iteration is over, we delete the tested timeslots (all did not suit the left over open tasks (if
        # there are left over open tasks))
        testTimeslots = np.delete(testTimeslots, selectTimelotsIndices)

        # Stop the assigning process if all tasks have been assigned to a slot, the max number of iteration has been
        # reached or there are no slots left for prediction
        if np.all(taskSlots) or iteration > 10 or len(testTimeslots) <= 0:

            # For those elements that might not have been grouped bc of deadline contraints it is now checked whether
            # they had a backup prediction spot
            for idx, tS in enumerate(taskSlots):
                if not tS:
                    if 'task-startdate' in backup_predictions[idx].keys():
                        task = backup_predictions[idx]['task']
                        task.startdate = backup_predictions[idx]['task-startdate']
                        task.enddate = backup_predictions[idx]['task-enddate']
                        task.starttime = backup_predictions[idx]['task-starttime']
                        task.endtime = backup_predictions[idx]['task-endtime']
                        task.save()
                        TemporarySmartTasks.objects.filter(task_id=task.id).delete()
                        taskSlots[idx] = True
            break
        iteration += 1

    # check if all tasks could be added to choose the appropriate redirection
    if np.all(taskSlots):
        response = redirect(kwargs['overview'], date=kwargs['date'])
    else:
        # Let the user know when an event could not be placed in the calendar
        # insert open tasks in the temporary smart task database for easy access
        response = redirect('./openTasksLeft/', overview=kwargs['overview'], date=kwargs['date'])

    return response


class SmartPlanning(LoginRequiredMixin, CreateView):
    model = SmartPlanningSettings
    form_class = SmartPlanningSettingsForm

    # sets the inital values all in one function which provides a better overview
    def get_initial(self):

        try:
            saved_settings = SmartPlanningSettings.objects.get(user=self.request.user)
            initial = {
                'smart_startdate': datetime.date.today(),
                'smart_enddate': datetime.date.today() + datetime.timedelta(days=7),
                'smart_starttime': saved_settings.smart_starttime,
                'smart_endtime': saved_settings.smart_endtime
            }
        except:
            initial = {
                'smart_startdate': datetime.date.today(),
                'smart_enddate': datetime.date.today() + datetime.timedelta(days=7),
                'smart_starttime': datetime.time(8, 0),
                'smart_endtime': datetime.time(18, 0)
            }

        return initial

    def get_context_data(self, **kwargs):
        context = super(SmartPlanning, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['date'] = self.kwargs['date']
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['weekdays_form'] = WeekdaysForm()
        try:
            saved_settings = SmartPlanningSettings.objects.get(user=self.request.user)
            context['weekdays_form'].initial = saved_settings.weekdays.__dict__
        except:
            pass
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        context['smart_tasks'] = context['open_tasks'].filter(startdate=None).filter(ai_scheduler=True)
        context['high_prio_task_count'] = context['open_tasks'].filter(priority=3).count()
        context['medium_prio_task_count'] = context['open_tasks'].filter(priority=2).count()
        context['low_prio_task_count'] = context['open_tasks'].filter(priority=1).count()
        return context

    # redirects to daily/weekly overview when form was submitted
    def get_success_url(self):
        return reverse_lazy('calendar-insert-smart-tasks', args=(self.kwargs['overview'], self.kwargs['date']))

    # combines the post data for category and event form
    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.form_class(request.POST)
        weekdays_form = WeekdaysForm(request.POST)

        # Save the tasks
        TemporarySmartTasks.objects.all().delete()
        data = request.POST
        try:
            for elem in json.loads(data.get('tasks2use', '')):
                task = TemporarySmartTasks.objects.create(user=request.user, task_id=elem)
                task.save()
        except:
            pass

        if form.is_valid() and weekdays_form.is_valid():
            return self.form_valid(form, weekdays_form)
        else:
            return self.form_invalid(form)

    # adds the user as author of a model instance
    # form is automatically saved afterwards by a predefined method
    def form_valid(self, form, weekdays_form):

        try:
            # try if the weekdays setting already exists
            weekdays = Weekdays.objects.filter(user=self.request.user).get(smart_settings=True)

            weekdays.monday = weekdays_form.instance.monday
            weekdays.tuesday = weekdays_form.instance.tuesday
            weekdays.wednesday = weekdays_form.instance.wednesday
            weekdays.thursday = weekdays_form.instance.thursday
            weekdays.friday = weekdays_form.instance.friday
            weekdays.saturday = weekdays_form.instance.saturday
            weekdays.sunday = weekdays_form.instance.sunday

            weekdays.save()
        except:
            # create a weekdays setting
            weekdays_form.instance.user = self.request.user
            weekdays_form.instance.smart_settings = True
            weekdays = weekdays_form.save()

        try:
            settings = SmartPlanningSettings.objects.get(user=self.request.user)

            settings.weekdays = weekdays
            settings.smart_startdate = form.instance.smart_startdate
            settings.smart_enddate = form.instance.smart_enddate
            settings.smart_starttime = form.instance.smart_starttime
            settings.smart_endtime = form.instance.smart_endtime

            settings.save()

        except:

            form.instance.user = self.request.user
            form.instance.weekdays = weekdays
            form.save()

        return HttpResponseRedirect(self.get_success_url())

    # changes the inputs if they are not valid to a valid input
    # potentially not needed if it is controlled properly in the javascript file
    def form_invalid(self, form, **kwargs):

        context = self.get_context_data(**kwargs)
        # form copy is needed because once a form is bound (has data) it cannot be changed anymore
        form.data = form.data.copy()

        context['form'] = form

        return self.render_to_response(context)


class OpenTasks(LoginRequiredMixin, generic.TemplateView):
    template_name = 'spm/open_task_warning.html'

    def get_context_data(self, **kwargs):
        context = super(OpenTasks, self).get_context_data(**kwargs)
        context['overview'] = self.kwargs['overview']
        context['date'] = self.kwargs['date']
        ids = [t.task_id for t in TemporarySmartTasks.objects.filter(user=self.request.user)]
        context['leftover_tasks'] = list(Task.objects.filter(user=self.request.user, id__in=ids))
        context['event_list'] = Event.objects.filter(user=self.request.user)
        context['task_list'] = Task.objects.filter(user=self.request.user).order_by('deadline_date', 'deadline_time')
        context['done_tasks'] = context['task_list'].filter(complete=True).order_by('-complete_time')[:10]
        context['open_tasks'] = context['task_list'].filter(complete=False)
        return context
