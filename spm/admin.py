from django.contrib import admin
from .models import Category, Event, Task, DecisionTreeTask, TemporarySmartTasks, SmartPlanningSettings, Weekdays, Recurrence

admin.site.register(Category)
admin.site.register(Event)
admin.site.register(Task)
admin.site.register(DecisionTreeTask)
admin.site.register(TemporarySmartTasks)
admin.site.register(SmartPlanningSettings)
admin.site.register(Weekdays)
admin.site.register(Recurrence)
