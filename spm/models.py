import datetime
import uuid
from django.db import models
from django.conf import settings
from django.urls import reverse
from .fields import CustomDurationModelField


# own model because new choice options should be added
# new options can be added in the /admin/ url
class Category(models.Model):
    name = models.CharField('category', max_length=16,
                            help_text='Enter a category (e.g. "work")', null=True, blank=True)
    color = models.CharField(max_length=7, default='#334B7B')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        if self.name is None:
            user = str(self.user)
            return 'default ' + user + ' category'
        return self.name


class Task(models.Model):

    # add choice options hard coded only if no new options should be added
    class Priority(models.IntegerChoices):
        HIGH = (3, 'high')
        MEDIUM = (2, 'medium')
        LOW = (1, 'low')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True)
    title = models.CharField(max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    deadline_date = models.DateField()
    deadline_time = models.TimeField()
    duration = CustomDurationModelField()
    # integer choice field makes interpolation later on possible
    priority = models.IntegerField(choices=Priority.choices, default=Priority.LOW)
    description = models.TextField(max_length=2000, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    startdate = models.DateField(null=True, default=None)  # tasks don't need a date but should be able to get one
    enddate = models.DateField(null=True, default=None)
    starttime = models.TimeField(null=True, default=None)
    endtime = models.TimeField(null=True, default=None)
    # bool to enable easy check of completion
    complete = models.BooleanField(default=False)
    complete_time = models.DateTimeField(null=True, default=None)
    ai_scheduler = models.BooleanField(default=True)

    def get_absolute_url_weekly(self):
        if self.startdate:
            return reverse('task-detail', kwargs={'overview': 'weekly', 'pk': self.id, 'date': self.startdate})
        else:
            return reverse('task-detail', kwargs={'overview': 'weekly', 'pk': self.id, 'date': self.deadline_date})

    def get_absolute_url_daily(self):
        if self.startdate:
            return reverse('task-detail', kwargs={'overview': 'daily', 'pk': self.id, 'date': self.startdate})
        else:
            return reverse('task-detail', kwargs={'overview': 'daily', 'pk': self.id, 'date': self.deadline_date})

    def __str__(self):
        return f'{self.title} until {self.deadline_date}, {self.deadline_time}'


class DecisionTreeTask(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    weekday = models.IntegerField()
    time = models.IntegerField()
    priority = models.IntegerField()
    deadline = models.IntegerField()
    duration = models.IntegerField()
    category = models.IntegerField()
    as_planned = models.BooleanField()


class Weekdays(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    monday = models.BooleanField(default=False)
    tuesday = models.BooleanField(default=False)
    wednesday = models.BooleanField(default=False)
    thursday = models.BooleanField(default=False)
    friday = models.BooleanField(default=False)
    saturday = models.BooleanField(default=False)
    sunday = models.BooleanField(default=False)
    smart_settings = models.BooleanField(default=False)


class Recurrence(models.Model):

    # add choice options hard coded only if no new options should be added
    class RepeatType(models.IntegerChoices):
        WEEK = (1, 'week(s)')
        MONTH = (2, 'month(s)')
        YEAR = (3, 'year(s)')

    groupID = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True)
    weekdays = models.ForeignKey(Weekdays, on_delete=models.CASCADE, null=True, blank=True)
    repeat_freq = models.IntegerField(default=1)
    repeat_type = models.IntegerField(choices=RepeatType.choices, default=RepeatType.WEEK)
    repeat_end = models.DateField()


class Event(models.Model):
    # uuids are standardized values and easier to handle as id than simply counting up
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, unique=True)
    title = models.CharField(max_length=100)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    startdate = models.DateField()
    enddate = models.DateField()
    starttime = models.TimeField()
    endtime = models.TimeField()
    location = models.CharField(max_length=100, blank=True)
    comment = models.TextField(max_length=2000, blank=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True, blank=True)
    recurrence_true = models.BooleanField(default=False)
    recurrence = models.ForeignKey(Recurrence, on_delete=models.CASCADE, null=True, blank=True)
    groupID = models.UUIDField(null=True, blank=True)

    # we need two absolute urls one for each overview
    # because for a method in the html file the parameters can't be called
    def get_absolute_url_weekly(self):
        return reverse('event-detail', kwargs={'overview': 'weekly', 'pk': self.id, 'date': self.startdate})

    def get_absolute_url_daily(self):
        return reverse('event-detail', kwargs={'overview': 'daily', 'pk': self.id, 'date': self.startdate})

    # this will be returned if the object is called as a string e.g. in print methods
    def __str__(self):
        return f'{self.title}: {self.startdate}, {self.starttime} to {self.enddate}, {self.endtime}'


class SmartPlanningSettings(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    weekdays = models.ForeignKey(Weekdays, on_delete=models.CASCADE, null=True, blank=True)
    smart_startdate = models.DateField(null=True)
    smart_enddate = models.DateField()
    smart_starttime = models.TimeField()
    smart_endtime = models.TimeField()


class TemporarySmartTasks(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    task_id = models.CharField(max_length=40)
