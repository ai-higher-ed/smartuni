from django.forms import widgets, Widget
from .utils import parse_duration


class CustomDurationInput(widgets.TimeInput):

    @staticmethod
    def format_value(value, **kwargs):
        # this method can be used for every different input widget to customize the displayed format

        # is needed for the test function because it doesn't use the initial value and is therefore None
        if value is None:
            return '00:30'

        duration = parse_duration(value)  # returns a datetime.timedelta object

        seconds = duration.seconds  # datetime.timedelta objects only have days, seconds and microseconds
        minutes = seconds // 60
        hours = minutes // 60
        minutes = minutes % 60

        return '{:02d}:{:02d}'.format(hours, minutes)


class ColorInput(Widget):
    input_type = 'color'
    template_name = "django/forms/widgets/input.html"

    def __init__(self, attrs=None):
        if attrs is not None:
            attrs = attrs.copy()
            self.input_type = attrs.pop("type", self.input_type)
        super().__init__(attrs)

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context["widget"]["type"] = self.input_type
        return context
