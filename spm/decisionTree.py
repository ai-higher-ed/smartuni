from sklearn import tree
import numpy as np
import pandas as pd
import datetime
from spm.models import Event, Task


def _clean_weekday(date_input):
    """
    Weekday should be an int from 0 to 6 representing the weekdays Monday to Sunday
    :param date_input:
    :return:
    """
    return date_input.isoweekday()


def _clean_time(time_input):
    """
    Time should be a mapping to one of seven ranges of possible daytimes (3 hours make one range, starting at 8am)
    :param time_input:
    :return:
    """
    time = time_input.hour
    time_mapped = (time - 8) % 24
    return np.int(time_mapped // 3)


def _clean_deadline(deadline_input_date, deadline_input_time):
    """
    Deadline should be a mapping to the range of 0 to 5 depending on when, from today, the deadline would be
    :param deadline_input_date:
    :param deadline_input_time:
    :return:
    """
    # Get the number of days until the deadline ( is negative if overdue)
    today = datetime.datetime.today().date()
    deadline = deadline_input_date
    days_until_deadline = (deadline - today).days

    # Return mapping to values from 0 to 5
    if days_until_deadline < 0:
        return -1
    elif days_until_deadline <= 1:
        return 0
    elif days_until_deadline <= 3:
        return 1
    elif days_until_deadline <= 7:
        return 2
    elif days_until_deadline <= 14:
        return 3
    elif days_until_deadline <= 30:
        return 4
    else:
        return 5


def _clean_duration(duration_input):
    """
    Duration should be a mapping from 0 to 6 depending on the duration needed for the task (XXS to XXL)
    :param duration_input:
    :return:
    """
    # Get the duration of the task in minutes
    duration = duration_input.total_seconds() / 60

    # Map duration to 15 minute ranges limited by 0 minutes to 480 minutes
    duration_mapping = 1 if duration <= 0 else 33 if duration > 480 else np.ceil(duration / 15)

    # Return mapping to range 0 to 6
    return np.int(np.ceil(np.log2(duration_mapping)))


def _clean_category(category_input_old, category_input_new=None):
    """
    Task category w.r.t. the category of the event before in the calendar
    :param category_input_old:
    :param category_input_new:
    :return:
    """
    if not category_input_old:
        return 2

    if category_input_old == category_input_new:
        return 0
    else:
        return 1


# create a decision tree model from the data of our database
def createTree(database):
    # Get the base data that we collected and the user data
    base_data_df = pd.read_csv(r'/var/www/smartuni/static/data/decision-tree-base-data.csv')
    base_data_df.columns = base_data_df.columns.str.lower()

    try:
        src = base_data_df.copy().drop(columns=base_data_df.columns[-1], axis=1)\
            .append(database.copy().drop(columns=database.columns[-1], axis=1), ignore_index=True)
        target = base_data_df['target'].append(database['as_planned'], ignore_index=True)
    except:
        src = base_data_df.copy().drop(columns=base_data_df.columns[-1], axis=1)
        target = base_data_df['target']

    # Build and fit the decision tree to the data
    treeModel = tree.DecisionTreeRegressor(max_depth=5, min_samples_leaf=0.05)
    treeModel = treeModel.fit(src.to_numpy(), target.to_numpy())
    return treeModel


def getTimeslots(startDay, endDay, startTime, endTime, interval, include_days=[]):
    """
    Get all timeslots between a given start and end day within the start and end times with a specific time interval
    between the timeslots. Neglect the weekdays that should not be included.
    :param startDay: Date from which the time slots should start
    :param endDay: Date from which the time slots should end
    :param startTime: Daily time from which the time slots should start
    :param endTime: Daily time from which the time slots should end
    :param interval: Interval between the timeslots
    :param include_days: Weekday array (with ints between 0-6 representing Mon-Sun) which weekdays to include
    :return: Array with all timeslots that match the given parameters
    """
    startDayAsDate = datetime.datetime.strptime(startDay, '%Y-%m-%d')
    firstSlot = datetime.datetime.combine(startDayAsDate, startTime)

    # if the first slot would be on the current day, but passed already, the affected timeslots should not be in the
    # possible timeslots
    if firstSlot < datetime.datetime.now():

        startTimeCurrentDay = datetime.datetime.now() + (datetime.datetime.min - datetime.datetime.now()) % datetime.timedelta(minutes=15)
        startTimeCurrentDayStr = datetime.datetime.strftime(startTimeCurrentDay, '%H:%M:%S')

        if startTimeCurrentDay.time() < endTime:

            # Get the timeslots for the current day
            datetime_series = pd.bdate_range(startDay, startDayAsDate + datetime.timedelta(days=1),
                                             freq=interval).to_series()

            timeslots1 = (pd.DataFrame({'datetime': datetime_series,
                                        'weekday': datetime_series.dt.weekday}).between_time(startTimeCurrentDayStr,
                                                                                             endTime))

            # Get the timeslots for all other days
            datetime_series = pd.bdate_range(startDayAsDate + datetime.timedelta(days=1), endDay, freq=interval).to_series()
            timeslots2 = (pd.DataFrame({'datetime': datetime_series,
                                        'weekday': datetime_series.dt.weekday}).between_time(startTime, endTime))

            timeslots = timeslots1.append(timeslots2)

        else:
            # Get the timeslots for all other days
            datetime_series = pd.bdate_range(startDayAsDate + datetime.timedelta(days=1), endDay,
                                             freq=interval).to_series()
            timeslots = (pd.DataFrame({'datetime': datetime_series,
                                        'weekday': datetime_series.dt.weekday}).between_time(startTime, endTime))

    else:
        # Get all slots starting with start date and time with the respective interval frequency
        datetime_series = pd.bdate_range(startDay, endDay, freq=interval).to_series()
        timeslots = (pd.DataFrame({'datetime': datetime_series,
                                   'weekday': datetime_series.dt.weekday}).between_time(startTime, endTime))

    # Keep only those slots that are on weekdays that should be included
    filtered_timeslots = timeslots[timeslots['weekday'].isin(include_days)]

    # Bring the timeslots to the correct format
    datetime_timeslots = filtered_timeslots['datetime'].index.strftime('%Y-%m-%dT%H:%M:%SZ')

    return datetime_timeslots.to_numpy()


def predictTaskPlacement(treeModel, task, freeTimeSlots):
    """
    Predict the placement of a task based on the decision tree model and the free time slots in the calendar
    :param treeModel: Decision tree
    :param task: The task that should be put in the calendar
    :param freeTimeSlots: An array with all free time slots the task could be grouped in
    :return:
    """

    # Create test tasks in the form of the DT dataset
    def _task2dttask(task2transform, timeslot):
        timeslot_info = datetime.datetime.strptime(timeslot, '%Y-%m-%dT%H:%M:%SZ')
        timeslot_date = timeslot_info.date()
        timeslot_time = timeslot_info.time()
        # dict for overview
        dttask = {'weekday': _clean_weekday(timeslot_date),
                  'time': _clean_time(timeslot_time),
                  'priority': task2transform.priority,
                  'deadline': _clean_deadline(task2transform.deadline_date, task2transform.deadline_time),
                  'duration': _clean_duration(task2transform.duration),
                  'category': _clean_category(task2transform.category)}
        return np.array(list(dttask.values()))

    # Predict based on the respective timeslot
    predictions = []
    for slot in freeTimeSlots:
        dttask_as_df = _task2dttask(task, slot)
        prediction = treeModel.predict([dttask_as_df])
        predictions.append(prediction)

    return predictions


def _conv_weekdays(weekday_obj):
    """
    Function to get those weekdays in which a task should be planned on.
    :param weekday_obj: Model instance of the Weekdays model
    :return: Array of weekday indices (0-6 representing Mon-Sun) for which the respecitive value in the model
    instance is set to true
    """
    weekday_list = list(weekday_obj.__class__.objects.all().values_list())[0][2:]

    return np.array([weekday_idx for weekday_idx, weekday in enumerate(weekday_list) if weekday])

def _overlapExists(date, startDate, endDate, user):
    """
    Determines whether an event or tasks exists already in a given timeslot; returns true if this is the case.
    :param date:
    :param startDate:
    :param endDate:
    :return:
    """
    # first filter for the day, then the time
    event_exists = Event.objects.filter(user=user).filter(startdate__gte=date, enddate__lte=date). \
        filter(endtime__gt=startDate, starttime__lt=endDate).exists()
    task_exists = Task.objects.filter(user=user).filter(startdate__gte=date, enddate__lte=date). \
        filter(endtime__gt=startDate, starttime__lt=endDate).exists()

    # Multi-day event overlap cases
    # Case 1: New date is on a day that is completely occupied by a multi-day event
    mult_day_event_exists = Event.objects.filter(user=user).filter(startdate__lt=date, enddate__gt=date).exists()
    # Case 2: Date is on the same day as the startdate for a multi-day event
    mult_day_event_overlaps_starttime = Event.objects.filter(startdate=date,
                                                             enddate__gt=date,
                                                             starttime__lt=endDate).exists()
    # Case 3: Date is on the same day as the enddate of a multi-day event
    mult_day_event_overlaps_endtime = Event.objects.filter(startdate__lt=date,
                                                           enddate=date,
                                                           endtime__gt=startDate).exists()

    return event_exists or task_exists or mult_day_event_exists or mult_day_event_overlaps_starttime or mult_day_event_overlaps_endtime